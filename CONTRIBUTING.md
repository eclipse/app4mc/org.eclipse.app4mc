# Contributing to Eclipse APP4MC

Thanks for your interest in this project.

## Project description

Application Platform Project for Multi-Core

Eclipse APP4MC™ is a platform for engineering embedded multi- and many-core
software systems. The platform enables the creation and management of complex
tool chains including simulation and validation. As an open platform, proven
in the automotive sector by Bosch and their partners, it supports
interoperability and extensibility and unifies data exchange in
cross-organizational projects.

* https://projects.eclipse.org/projects/automotive.app4mc

## Terms of Use

This repository is subject to the Terms of Use of the Eclipse Foundation

* https://www.eclipse.org/legal/termsofuse.php

## Developer resources

Information regarding source code management, builds, coding standards, and
more.

* https://projects.eclipse.org/projects/automotive.app4mc/developer

The project maintains the following source code repositories

* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.addon.migration.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.addon.transformation.git

* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.cloud.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.simulation.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.rtc.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.rtclib.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.examples.git
* https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.releng.git

## Eclipse Development Process

This Eclipse Foundation open project is governed by the Eclipse Foundation
Development Process and operates under the terms of the Eclipse IP Policy.

* https://eclipse.org/projects/dev_process
* https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf

## Eclipse Contributor Agreement

In order to be able to contribute to Eclipse Foundation projects you must
electronically sign the Eclipse Contributor Agreement (ECA).

* https://www.eclipse.org/legal/ECA.php

The ECA provides the Eclipse Foundation with a permanent record that you agree
that each of your contributions will comply with the commitments documented in
the Developer Certificate of Origin (DCO). Having an ECA on file associated with
the email address matching the "Author" field of your contribution's Git commits
fulfils the DCO's requirement that you sign-off on your contributions.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project's "dev" list.

* https://dev.eclipse.org/mailman/listinfo/app4mc-dev
