# Security Policy

This project implements the Eclipse Foundation Security Policy

* https://www.eclipse.org/security


## Reporting a Vulnerability

Please report vulnerabilities to the Eclipse Foundation Security Team at
security@eclipse.org


# The Eclipse Security Team

The Eclipse Security Team provides help and advice to Eclipse projects
on vulnerability issues and is the first point of contact
for handling security vulnerabilities.
Members of the Security Team are committers on Eclipse Projects
and members of the Eclipse Architecture Council.

Contact the [Eclipse Security Team](mailto:security@eclipse.org).


## Disclosure

Disclosure is initially limited to the reporter and all Eclipse Committers,
but is expanded to include other individuals, and the general public.
The timing and manner of disclosure is governed by the
[Eclipse Security Policy](https://www.eclipse.org/security/policy.php).

Publicly disclosed issues are listed on the
[Disclosed Vulnerabilities Page](https://www.eclipse.org/security/known.php).
