/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.editors.sirius.design.services;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.IExecutable;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;

public class RunnableService {

	public List<Label> getLabelWriteAccessesForRunnable(final Runnable runnable) {
		final List<Label> result = new ArrayList<>();

		if (runnable != null) {
			result.addAll(SoftwareUtil.getWriteLabelSet(runnable, null));
		}

		return result;
	}

	public List<Label> getLabelReadAccessesForRunnable(final Runnable runnable) {
		final List<Label> result = new ArrayList<>();

		if (runnable != null) {
			result.addAll(SoftwareUtil.getReadLabelSet(runnable, null));
		}

		return result;
	}

	public List<Runnable> getCommunicationForRunnable(final Runnable runnable) {
		final List<Runnable> result = new ArrayList<>();
		if (runnable == null) return result;

		for (Label label : SoftwareUtil.getWriteLabelSet(runnable, null)) {
			for (final LabelAccess labelAccess : label.getLabelAccesses()) {
				if (labelAccess.getAccess().equals(LabelAccessEnum.READ)) {
					final IExecutable exec = labelAccess.getContainingExecutable();
					if (exec instanceof Runnable && exec != runnable) {
						result.add((Runnable) exec);
					}
				}
			}
		}

		return result;
	}

}
