/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.logic.btf.model;

public class BtfEvent {

	private long lineNum;
	private long time;
	private String source;
	private int sourceInstance;
	private BtfEntityType targetType;
	private String target;
	private int targetInstance;
	private String event;
	private String note;

	public long getTime() {
		return this.time;
	}

	public void setTime(final long time) {
		this.time = time;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	public int getSourceInstance() {
		return this.sourceInstance;
	}

	public void setSourceInstance(final int sourceInstance) {
		this.sourceInstance = sourceInstance;
	}

	public BtfEntityType getTargetType() {
		return this.targetType;
	}

	public void setTargetType(final BtfEntityType targetType) {
		this.targetType = targetType;
	}

	public int getTargetInstance() {
		return this.targetInstance;
	}

	public void setTargetInstance(final int targetInstance) {
		this.targetInstance = targetInstance;
	}

	public String getEvent() {
		return this.event;
	}

	public void setEvent(final String event) {
		this.event = event;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	public long getLineNum() {
		return this.lineNum;
	}

	public void setLineNum(final long lineNum) {
		this.lineNum = lineNum;
	}


	public String getTarget() {
		return this.target;
	}


	public void setTarget(final String target) {
		this.target = target;
	}


}
