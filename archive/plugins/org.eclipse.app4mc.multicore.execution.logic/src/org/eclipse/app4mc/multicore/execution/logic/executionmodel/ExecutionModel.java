/**
 ********************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.executionmodel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.misc.EMTimeType;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMCore;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask.DeadlineEntry;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState;

public class ExecutionModel {

	private final Map<String, EMCore> cores = new HashMap<>();

	private final List<Consumer<EMCore>> coreAddedListener = new LinkedList<>();

	private final Map<String, EMTask> tasks = new HashMap<>();

	private final List<Consumer<EMTask>> taskAddedListener = new LinkedList<>();

	private final List<BiConsumer<EMTask, EMTaskHistState>> histEntryListener = new LinkedList<>();

	private final List<BiConsumer<EMTask, DeadlineEntry>> deadlineListener = new LinkedList<>();

	private EMTimeType timeScale = EMTimeType.NONE;

	private String path = "";

	public Map<String, EMCore> getCores() {
		return this.cores;
	}

	/**
	 * Tasks in of after execution.
	 *
	 * @see EMTask
	 * @return
	 */
	public Map<String, EMTask> getTasks() {
		return this.tasks;
	}

	public void setTimeScale(final EMTimeType t) {
		this.timeScale = t;
	}

	public EMTimeType getTimeScale() {
		return this.timeScale;
	}

	public void addTaskDeadlineMissedEntry(final String taskName, final String core, final Long time) {
		final EMTask t = this.tasks.get(taskName);
		final DeadlineEntry d = new DeadlineEntry(time, this.cores.get(core));
		t.getMissedDeadlines().add(d);
		notifyTaskDeadlineMissed(t, d);
	}

	/**
	 * Add a history entry to a task (e.g state running from time point 1000 to
	 * 1223). By adding the entry with this method observer will be notified
	 * about completed task hist entries.
	 *
	 * @param taskName
	 * @param e
	 */
	public void addTaskHistEntry(final String taskName, final EMTaskHistState e) {
		final EMTask t = this.tasks.get(taskName);
		t.getStateHistory().add(e);
		notifyTaskHistEntryListener(t, e);
	}

	/**
	 * Add a task to the model. Only this method will notify observers about
	 * added tasks.
	 *
	 * @param t
	 */
	public synchronized void addTask(final EMTask t) {
		this.tasks.put(t.getName(), t);
		notifyTaskAddedListener(t);
	}

	/**
	 * Add a core to the model. Only this method will notify observers about
	 * added cores.
	 *
	 * @param c
	 */
	public void addCore(final EMCore c) {
		this.cores.put(c.getName(), c);
		notifyAllCoreAddedObserver(c);
	}

	private void notifyAllCoreAddedObserver(final EMCore c) {
		this.coreAddedListener.forEach(x -> x.accept(c));
	}

	private void notifyTaskAddedListener(final EMTask t) {
		this.taskAddedListener.forEach(x -> x.accept(t));
	}

	private void notifyTaskHistEntryListener(final EMTask t, final EMTaskHistState e) {
		this.histEntryListener.forEach(x -> x.accept(t, e));
	}

	public void addTaskHistEntryAddedListener(final BiConsumer<EMTask, EMTaskHistState> e) {
		this.histEntryListener.add(e);
	}

	public void addTaskAddedListener(final Consumer<EMTask> t) {
		this.taskAddedListener.add(t);
	}

	public void addCoreAddedListener(final Consumer<EMCore> c) {
		this.coreAddedListener.add(c);
	}

	public void addTaskDeadlineListener(final BiConsumer<EMTask, DeadlineEntry> e) {
		this.deadlineListener.add(e);
	}

	private void notifyTaskDeadlineMissed(final EMTask t, final DeadlineEntry e) {
		this.deadlineListener.forEach(x -> x.accept(t, e));
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(final String path) {
		this.path = path;
	}
}
