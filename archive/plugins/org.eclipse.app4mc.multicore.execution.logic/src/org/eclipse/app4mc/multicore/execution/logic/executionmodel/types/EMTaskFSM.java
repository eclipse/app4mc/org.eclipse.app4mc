/**
 ********************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.executionmodel.types;


public class EMTaskFSM {


	private static final EMTaskState[][] multi = new EMTaskState[EMTaskState.COUNT][EMTaskEvent.COUNT];

	static {
		// every non transition is null
		/* outgoing transitions for state RUNNING */
		makeTransition(EMTaskState.RUNNING, EMTaskEvent.TERMINATE, EMTaskState.SUSPENDED);
		makeTransition(EMTaskState.RUNNING, EMTaskEvent.PREEMPT, EMTaskState.READY);
		makeTransition(EMTaskState.RUNNING, EMTaskEvent.WAIT, EMTaskState.WAITING);

		/* outgoing transitions for state READY */
		makeTransition(EMTaskState.READY, EMTaskEvent.START, EMTaskState.RUNNING);

		/* outgoing transitions for state WAITING */
		makeTransition(EMTaskState.WAITING, EMTaskEvent.RELEASE, EMTaskState.READY);

		/* outgoing transitions for state SUSPENDED */
		makeTransition(EMTaskState.SUSPENDED, EMTaskEvent.ACTIVATE, EMTaskState.READY);
	}

	private static void makeTransition(final EMTaskState oldState, final EMTaskEvent e, final EMTaskState newState) {
		multi[oldState.val()][e.val()] = newState;
	}

	private EMTaskState currentState;

	public EMTaskFSM(final EMTaskState start) {
		this.currentState = start;
	}

	public void setState(final EMTaskState state) {
		this.currentState = state;
	}

	public EMTaskState getState() {
		return this.currentState;
	}

	public boolean dispatchEvent(final EMTaskEvent event) {
		final EMTaskState newState = multi[this.currentState.val()][event.val()];
		if (newState != null) {
			this.currentState = newState;
			return true;
		}
		// no state change
		return false;
	}

}
