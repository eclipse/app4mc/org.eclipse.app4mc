/**
 ********************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.executionmodel.types;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.misc.ExecutionModelException;

public class EMTaskHistState {
	/* Constants */
	public static final long UNKNOWN_PAST = Long.MIN_VALUE;
	public static final long UNKNOWN_FUTURE = Long.MAX_VALUE;

	private static final String UNKNOWN_STRING = "unknown";

	private EMCore core;
	/** Time at event occurrence */
	private long start = UNKNOWN_PAST;
	/** Time the */
	private long end = UNKNOWN_FUTURE;
	/** The state the task remains between start and stop */
	private EMTaskState state;
	/** Additional information */
	private String note = UNKNOWN_STRING;
	/** The entity the event fired */
	private String eventSource = UNKNOWN_STRING;
	/** The event type */
	private EMTaskEvent event;
	/** Cause of the event. E.g. wait event caused by mutex "mux_xy" */
	private String eventCause = UNKNOWN_STRING;

	public static class EMTaskHistStateFactory {
		public static EMTaskHistState create(final EMTaskState state, final EMCore core)
				throws ExecutionModelException {
			if ((state == EMTaskState.RUNNING) && core == null) {
				throw new ExecutionModelException("");
			}
			final EMTaskHistState entry = new EMTaskHistState();
			entry.setCore(core);
			entry.setState(state);
			return entry;
		}

		public static EMTaskHistState create(final EMTaskState state) throws ExecutionModelException {
			return create(state, null);
		}
	}

	private EMTaskHistState() {
	}

	public EMCore getCore() {
		return this.core;
	}

	private void setCore(final EMCore core) {
		this.core = core;
	}

	public long getStart() {
		return this.start;
	}

	public void setStart(final long start) {
		this.start = start;
	}

	public long getEnd() {
		return this.end;
	}

	public void setEnd(final long end) {
		this.end = end;
	}

	public EMTaskState getState() {
		return this.state;
	}

	private void setState(final EMTaskState state) {
		this.state = state;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	public String getEventSource() {
		return this.eventSource;
	}

	public void setEventSource(final String eventSource) {
		this.eventSource = eventSource;
	}

	public void setEventCause(final String eventCause) {
		this.eventCause = eventCause;
	}

	public String getEventCause() {
		return this.eventCause;
	}

	public EMTaskEvent getEvent() {
		return this.event;
	}

	public void setEvent(final EMTaskEvent event) {
		this.event = event;
	}

	public boolean isCompleted() {
		return this.start != UNKNOWN_PAST && this.end != UNKNOWN_FUTURE;
	}


}
