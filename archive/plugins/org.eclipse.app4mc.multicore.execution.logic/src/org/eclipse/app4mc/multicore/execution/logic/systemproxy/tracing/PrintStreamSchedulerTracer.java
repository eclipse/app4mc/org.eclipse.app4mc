/**
 ********************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.systemproxy.tracing;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.ISchedulerEventListener;

public class PrintStreamSchedulerTracer implements ISchedulerEventListener {

	private PrintStream out;

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("mm:ss:SSS");

	private final String label;

	public PrintStreamSchedulerTracer(final String label, final PrintStream s) {
		this.out = s;
		this.label = label;
	}

	public PrintStreamSchedulerTracer(final String label) {
		this.label = label;
	}

	public void setPtrintStream(final PrintStream s) {
		this.out = s;
	}

	public PrintStream getPrintStream() {
		return this.out;
	}

	@Override
	public void onTaskAdded(final String taskName) {
		write("Added task to StepScheduler: " + taskName);
	}

	@Override
	public void onActivateTask(final String task, final long time) {
		write(time + " | onActivateTask " + task);
	}

	@Override
	public void onStartTask(final String task, final long time) {
		write(time + " | onStartTask " + task);
	}


	@Override
	public void onTerminateTask(final String task, final long time) {
		write(time + " | onTerminateTask " + task);
	}

	@Override
	public void onPreemptTask(final String task, final long time) {
		write(time + " | onPreemptTask " + task);
	}

	@Override
	public void onWaitTask(final String task, final long time, final String muxName, final String holder) {
		write(time + " | onWaitTask " + task);
	}

	@Override
	public void onReleaseTask(final String task, final long time) {
		write(time + " | onReleaseTask " + task);
	}

	@Override
	public void onTaskMissedDeadline(final String task, final long time, final long remainingExectime) {
		write(time + " | onTaskMissedDeadline " + task);
	}

	@Override
	public void onStartIdleCore(final long time) {
		write(time + " | onStartIdleCore ");
	}

	@Override
	public void onStopIdleCore(final long time) {
		write(time + " | onStopIdleCore ");
	}

	private void write(final String w) {
		final String x = LocalDateTime.now().format(this.formatter);
		this.out.println(x + " " + getCoreName() + " -> " + w);
	}

	@Override
	public String getCoreName() {
		return this.label;
	}


}
