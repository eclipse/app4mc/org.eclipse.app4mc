/*******************************************************************************
 * Copyright (c) 2017, 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.ui.simmenu.wizards;

import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.multicore.execution.logic.openmapping.MalformedModelException;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMCore;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMMapping;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMTask;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMUtil;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;

public class TaskVisuOverviewPage extends WizardPage {
	private Composite container;
	private final OMMapping model;

	public TaskVisuOverviewPage(final OMMapping model) {
		super("Task Visualisation: Overview");
		setTitle("Task Visualisation: Overview");
		setDescription("This page gives an overview of the visualisation input.");
		this.model = model;
	}

	@Override
	public void createControl(final Composite parent) {
		this.container = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		this.container.setLayout(layout);
		layout.numColumns = 1;

		setupAllocsOverview();
		setupCoreOverview();

		// required to avoid an error in the system
		setControl(this.container);
		setPageComplete(true);
	}

	private void setupCoreOverview() {
		final Label label1 = new Label(this.container, SWT.NONE);
		label1.setText("Cores");

		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.horizontalSpan = 1;

		final TableViewer tvCoreoverview = new TableViewer(this.container,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		final Table table = tvCoreoverview.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setLayoutData(gridData);

		final Map<OMCore, List<OMTask>> tmpmodel = OMUtil.getCoreTaskMap(this.model.getAllocationList());
		tvCoreoverview.setContentProvider(new ArrayContentProvider());
		tvCoreoverview.setInput(tmpmodel.keySet());

		final TableViewerColumn colCore = new TableViewerColumn(tvCoreoverview, SWT.NONE);
		colCore.getColumn().setWidth(100);
		colCore.getColumn().setText("Core");
		colCore.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(final Object element) {
				final OMCore core = (OMCore) element;
				return core.getCoreRef().getName();
			}
		});

		final TableViewerColumn colIPs = new TableViewerColumn(tvCoreoverview, SWT.NONE);
		colIPs.getColumn().setWidth(100);
		colIPs.getColumn().setText("Freq.");
		colIPs.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(final Object element) {
				final OMCore core = (OMCore) element;
				try {
					return "" + core.getFrequencyHz();
				}
				catch (final MalformedModelException e) {
					e.printStackTrace();
					return "Malformed Model";
				}
			}
		});

		final TableViewerColumn colUtilization = new TableViewerColumn(tvCoreoverview, SWT.NONE);
		colUtilization.getColumn().setWidth(100);
		colUtilization.getColumn().setText("Utilization");
		colUtilization.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(final Object element) {
				final OMCore core = (OMCore) element;
				double util;
				try {
					util = OMUtil.getUtilization(core, tmpmodel.get(core));
					return String.format("%1$,.4f", util);
				}
				catch (final MalformedModelException e) {
					e.printStackTrace();
					return "Malformed model";
				}

			}
		});
		tvCoreoverview.refresh(); // update to notify new columns
	}

	private void setupAllocsOverview() {
		final Label label1 = new Label(this.container, SWT.NONE);
		label1.setText("Allocations");
		new SWTAllocationTable(this.container, this.model.getAllocationList());
	}

}
