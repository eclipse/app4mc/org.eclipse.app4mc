/*******************************************************************************
 * Copyright (c) 2017, 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.ui.traceview.view;


import java.util.concurrent.ExecutionException;

import org.eclipse.app4mc.multicore.execution.ui.widget.misc.FXUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import javafx.application.Platform;
import javafx.embed.swt.FXCanvas;
import javafx.scene.Scene;


public abstract class FXViewPart extends ViewPart {

	FXCanvas view;


	@Override
	public void createPartControl(final Composite parent) {
		Platform.setImplicitExit(false); // bug-solution: if the view is opened
											// the second time it will not be
											// shown
		this.view = new FXCanvas(parent, SWT.NONE);
		try {
			FXUtil.runLaterWait(new Runnable() {
				@Override
				public void run() {
					FXViewPart.this.view.setScene(createFxScene());
					FXViewPart.this.view.update();
				}
			});
		}
		catch (final InterruptedException e1) {
			e1.printStackTrace();
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		}
		catch (final ExecutionException e1) {
			e1.printStackTrace();
		}
	}

	protected abstract Scene createFxScene();

	@Override
	public void setFocus() {
		this.view.setFocus();
		setFxFocus();
	}

	protected abstract void setFxFocus();

}
