/*******************************************************************************
 * Copyright (c) 2017, 2018 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.ui.traceview.view;

import org.eclipse.app4mc.multicore.execution.ui.widget.emtracewidget.EMTraceWidget;
import org.eclipse.app4mc.multicore.execution.ui.widget.emtracewidget.IEMTraceWidget;
import org.eclipse.app4mc.multicore.execution.ui.widget.tracewidget.ITraceWidget;

import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;

public class FXViewPartTraceView extends FXViewPart implements ITraceWidgetProvider, IEMTraceWidgetProvider {

	private EMTraceWidget tw;

	@Override
	protected Scene createFxScene() {
		this.tw = new EMTraceWidget();
		// tw.getStylesheets().add(getClass().getResource("/icons/ganttchart.css").toExternalForm());
		// tw = TraceViewTest.createTraceViewWithSampleData();

		final ScrollPane pane = new ScrollPane();
		pane.setContent(this.tw);
		pane.setFitToWidth(true);
		return new Scene(pane);
	}

	// private void disableMouseScroll(ScrollPane pane) {
	// Rectangle clipRectangle = new Rectangle();
	// clipRectangle.widthProperty().bind(pane.widthProperty());
	// clipRectangle.heightProperty().bind(pane.heightProperty());
	// pane.setClip(clipRectangle);
	// }

	@Override
	protected void setFxFocus() {
		// TODO Needs to be documented
	}

	@Override
	public ITraceWidget getTraceWidget() {
		return this.tw;
	}

	@Override
	public IEMTraceWidget getEMTraceWidget() {
		return this.tw;
	}


}
