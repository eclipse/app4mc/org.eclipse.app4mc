/*******************************************************************************
 * Copyright (c) 2017, 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.multicore.execution.ui.widget.model.elements;

public class XAccess {
	private final int state;
	private final XLabel label;

	XAccess(final int state, final XLabel label) {
		this.state = state;
		this.label = label;
	}

	public int getState() {
		return this.state;
	}

	public XLabel getLabel() {
		return this.label;
	}
}
