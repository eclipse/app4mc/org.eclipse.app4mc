/*******************************************************************************
 * Copyright (c) 2017, 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.multicore.execution.ui.widget.model.elements;

import java.util.HashMap;
import java.util.LinkedList;

public class XEventChain extends LinkedList<XEventElement> {
	private static final long serialVersionUID = -4498163087472641353L;
	private final XScheduler scheduler;
	private final LinkedList<String> orderlist;
	private final HashMap<String, XEventElement> lastFinish = new HashMap<>();
	private final HashMap<String, XEventElement> lastPromFinish = new HashMap<>();
	private HashMap<String, LinkedList<XEventElement>> fullrun = new HashMap<>();

	public XEventChain(final XScheduler scheduler) {
		super();
		this.orderlist = new LinkedList<>();
		this.fullrun = new HashMap<>();
		this.scheduler = scheduler;
	}

	public XScheduler getScheduler() {
		return this.scheduler;
	}

	public void betterAdd(final XEventElement element) {

		switch (element.getStatus()) {
			case 0:
			case 1:
			case 2:
				this.add(element);
				break;
			default:
				break;
		}

		if (this.fullrun.get(element.getTasks().getName()) == null) {
			this.fullrun.put(element.getTasks().getName(), new LinkedList<>());
		}
		this.fullrun.get(element.getTasks().getName()).add(element);
	}

	public LinkedList<XEventElement> getFullList(final String task) {
		return this.fullrun.get(task);
	}

	public int getIndexOf(final String s) {
		if (this.orderlist.size() > 0) {
			for (int i = 0; i < this.orderlist.size(); i++) {
				if (this.orderlist.get(i).compareTo(s) == 0) {
					return i;
				}
			}
		}
		return -1;
	}

	public int getIndexxOf(final XEventElement ob) {
		;
		final LinkedList<XEventElement> list = this.fullrun.get(ob.getTasks().getName());

		return list.indexOf(ob);
	}

	public void addSort(final String s) {
		this.orderlist.add(s);
		// System.out.println("Add" +s+" "+scheduler.getName());
	}

	public void addReplaceFinish(final XEventElement el) {

		// XUtil.writeToLog("Update "+el.getName());

		if (this.lastFinish.get(el.getName()) == null) {
			this.lastFinish.put(el.getName(), el);
			// System.out.println("Update "+el.getName());
		}
		else {
			if (this.lastPromFinish.get(el.getName()) == null) {
				this.lastPromFinish.put(el.getName(), this.lastFinish.get(el.getName()));
			}
			else {
				this.lastPromFinish.replace(el.getName(), this.lastFinish.get(el.getName()));
			}
			this.lastFinish.replace(el.getName(), el);

			// System.out.println("Replace "+el.getName());
		}

	}

	public XEventElement getLastFinish(final String key) {
		return this.lastFinish.get(key);
	}

	public XEventElement getLastPrmFinish(final String key) {
		return this.lastPromFinish.get(key);
	}

	public void printList() {
		// System.out.println("#######################");
		for (final XEventElement et : this) {
			//// System.out.println("#");
			//// System.out.println("# "+et.getName()+" "+et.getStatus());
			// System.out.println("#");

		}
		// System.out.println("# size: "+this.size());
		// System.out.println("#######################");

	}

}
