/*******************************************************************************
 * Copyright (c) 2017, 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.multicore.execution.ui.widget.model.elements;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Runnable;

public class XRunnable {
	private final LinkedList<XAccess> list;
	private XTasks task;
	private String name;

	public XRunnable(final Runnable next) {
		this.list = new LinkedList<>();
		this.name = next.getName();
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public XAccess addAccess(final int state, final XLabel label) {
		final XAccess out = new XAccess(state, label);
		// System.out.println(state +" "+label.getName());
		this.list.add(out);
		return out;
	}

	public int size() {
		return this.list.size();
	}

	public List<XAccess> getList() {
		return this.list;
	}

	public XTasks getTask() {
		return this.task;
	}

	public void setTask(final XTasks task) {
		this.task = task;
	}
}
