/*******************************************************************************
 * Copyright (c) 2017, 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.multicore.execution.ui.widget.model.elements;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Task;

public class XTasks {
	private final String name;
	private final HashMap<String, XTasks> producer = new HashMap<>();
	private final HashMap<String, XTasks> consumer = new HashMap<>();

	private final HashMap<String, XRunnable> map = new HashMap<>();

	public HashMap<String, XTasks> getProducer() {
		return this.producer;
	}

	public HashMap<String, XTasks> getConsumer() {
		return this.consumer;
	}

	public void addProducer(final XTasks linked) {
		this.producer.put(linked.getName(), linked);
	}

	public void addConsumer(final XTasks linked) {
		this.consumer.put(linked.getName(), linked);
	}

	public XTasks(final Task t) {

		// System.out.println("Task Creaded: "+t.getName());
		this.name = t.getName();
		// prepare(t);
	}

	public void addRunnable(final String name, final XRunnable run) {
		this.map.put(name, run);
	}

	public int size() {
		return this.map.size();
	}

	public String getName() {
		return this.name;
	}

	public HashMap<String, XRunnable> getMap() {
		return this.map;
	}

	public List<XAccess> getBySel(final int sel) {
		final Iterator<String> keys = this.map.keySet().iterator();
		XRunnable tmp;
		LinkedList<XAccess> read;
		LinkedList<XAccess> write;
		Iterator<XAccess> acc;
		XAccess aTmp;
		read = new LinkedList<>();
		write = new LinkedList<>();

		while (keys.hasNext()) {
			tmp = this.map.get(keys.next());
			acc = tmp.getList().iterator();
			while (acc.hasNext()) {
				aTmp = acc.next();
				switch (aTmp.getState()) {
					case 0:
						read.add(aTmp);
						break;
					case 1:
						write.add(aTmp);
						break;
					default:
						break;
				}
			}

		}
		if (sel == 0) {
			return read;
		}
		return write;
	}

}
