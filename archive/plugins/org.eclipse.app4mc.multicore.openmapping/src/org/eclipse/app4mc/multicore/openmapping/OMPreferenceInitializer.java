/*******************************************************************************
 * Copyright (c) 2015, 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *  
 * SPDX-License-Identifier: EPL-2.0
 *  
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.openmapping;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class OMPreferenceInitializer extends AbstractPreferenceInitializer {
	
	@Override
	public void initializeDefaultPreferences() {
		Bundle bundle = FrameworkUtil.getBundle(getClass());
		if (bundle != null) {
			IEclipsePreferences node = DefaultScope.INSTANCE.getNode(bundle.getSymbolicName());
			if (node == null) {
				return;
			}
			
			node.putBoolean(OMConstants.PRE_CHECK_LOGCON, true);
			node.putInt(OMConstants.PRE_MAPPING_ALG, 1);
			node.putInt(OMConstants.PRE_RADIO_OUTDIR, 0);
			node.put(OMConstants.PRE_STRING_OUTDIR, "output");

			node.putDouble(OMConstants.PRE_MAX_GAP, 0.1);
			node.putInt(OMConstants.PRE_MAX_IT_ABORT, Integer.MAX_VALUE);
			node.putInt(OMConstants.PRE_MAX_TIME_ABORT, Integer.MAX_VALUE);
			node.putInt(OMConstants.PRE_MAX_IT_SUFFICE, Integer.MAX_VALUE);
			node.putInt(OMConstants.PRE_MAX_TIME_SUFFICE, Integer.MAX_VALUE);
		}
	}
}
