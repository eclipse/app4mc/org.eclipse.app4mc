/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.algorithms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AccessPrecedenceSpec;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.RunnableSequencingConstraint;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.app4mc.multicore.partitioning.algorithms.CriticalPath.TimeFrame;
import org.eclipse.app4mc.multicore.partitioning.utils.Helper;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.jgrapht.DirectedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The critical path partitioning approach assigns runnables to ProcessPrototypes with respect to their execution cycles and dependencies. The first
 * ProcessPrototype will contain the critical path and other ProcessPrototypes will contain graph branches next to the critical path. The mechanism is thereby
 * able to provide maximal parallelism and minimal runtime. The boolean globalCPForAll defines whether a critical path is created (for a separate
 * ProcessPrototype) for each existing ProcessPrototype (e.g. created by the previous independent graph partitioning)
 */
public class CPP {
	private static final Logger LOGGER = LoggerFactory.getLogger(CPP.class);
	private final SWModel swm;
	private ConstraintsModel cm;
	int ctcp = 0;
	boolean bglobalCP = true;
	EList<ProcessPrototype> result = new BasicEList<>();
	private final Map<Runnable, CriticalPath.TimeFrame> cache = new HashMap<>();
	private CriticalPath globalCP = null;
	private final EList<Runnable> assignedNodes = new BasicEList<>();
	private DirectedGraph<Runnable, RunnableSequencingConstraint> graph;

	public CPP(final SWModel swm, final ConstraintsModel cm) {
		this.swm = swm;
		setCm(cm);
		for (final Runnable r : swm.getRunnables()) {
			this.runInstrCache.put(r, new Helper().getInstructions(r));
		}
	}


	/**
	 * INPUT: SWModel and ConstraintsModel featuring runnables and dependencies (graph structure, constructor)
	 *
	 * @return OUTPUT: Set of ProcessPrototypes with RunnableCalls
	 */
	public void build(final IProgressMonitor monitor) {
		/*
		 * In case activation analysis has been performed, globalCP must be set true
		 */
		if (getSwm() == null) {
			LOGGER.error("No SWmodel for CP partitioning. Stopping.");
			return;
		}
		final Set<AccessPrecedenceSpec> aps = new HashSet<>();
		if (!getSwm().getProcessPrototypes().isEmpty()) {
			aps.addAll(getSwm().getProcessPrototypes().get(0).getAccessPrecedenceSpec());
		}
		/* calculate global critical path */
		final CriticalPath gcp = new CriticalPath(getSwm(), getCm());
		monitor.beginTask("Find ciritcal path", 1);
		final EList<Runnable> gcpr = gcp.getCP();
		monitor.worked(1);
		/* graph is needed for easier assigneable runnable search */
		this.graph = gcp.getGraph();

		checkLitEitVals(gcp, gcpr);

		if (getSwm().getProcessPrototypes().size() > 1) {
			LOGGER.debug("GGP/AA performed. Partitioning starts for each ProcessPrototype!");
		}
		final AmaltheaFactory cf = AmaltheaFactory.eINSTANCE;
		final AmaltheaFactory swf = AmaltheaFactory.eINSTANCE;

		if (this.bglobalCP) {
			final ProcessPrototype pp0 = swf.createProcessPrototype();
			pp0.setName("GCP00");
			for (final Runnable r : gcpr) {
				this.assignedNodes.add(r);
				final RunnableCall trc = swf.createRunnableCall();
				trc.setRunnable(r);
				pp0.getRunnableCalls().add(trc);
			}
			this.result.add(pp0);
		}

		this.globalCP = gcp;
		long cpl = this.globalCP.getPathLength(gcpr);
		LOGGER.debug("GCP00: {}", this.globalCP.getPathString(gcpr));


		final SWModel swm2 = swf.createSWModel();
		final ConstraintsModel cm2 = cf.createConstraintsModel();
		for (final ProcessPrototype pp : getSwm().getProcessPrototypes()) {
			/* create subset models for each previous PP (independent graph / activation graph) */
			createSubModelsForIndepGraphs(swm2, cm2, pp);

			/* calculate local critical path for ProcesspPrototype */
			int ct = 0;
			if (!this.bglobalCP && !swm2.getRunnables().isEmpty()) {
				cpl = createLCP(swf, swm2, cm2, ct);
			}
			if (this.result.size() == 1) {
				ct++;
			}
			final int temp = swm2.getRunnables().size() - this.assignedNodes.size();
			monitor.beginTask("Assigning Runnables next to the ciritcal path to further ProcessProrotypes", temp);
			cpl = assignRemainingRuns(monitor, swf, cpl, swm2, ct);
		}

		/* Write result into console */
		LOGGER.debug("Critical Path Partitioning finished. Created ProcessPrototypes: {}", this.result.size());

		/* The following outout can be used to also illustrate each runnable's eit / lit values */
		printEitLitVals();

		/* Retain AccessPrecedences */
		new Helper().assignAPs(aps);

		/* write result into swm */
		resultToSwm();

		/* retain RSCs */
		retainRSCs(cm2);
	}


	private void checkLitEitVals(final CriticalPath gcp, final EList<Runnable> gcpr) {
		int gcpl = 0;
		for (final Runnable r : gcpr) {
			gcpl += this.runInstrCache.get(r);
		}
		LOGGER.debug("CP contains {} Runnables and consumes {} instructions.", gcpr.size(), gcpl);
		for (final Runnable r : getSwm().getRunnables()) {
			this.cache.put(r, gcp.getTF(r));
			if (gcp.getTF(r).lit < 0 || gcp.getTF(r).lit < gcp.getTF(r).eit) {
				LOGGER.error("LIT probelm: {}", gcp.getTF(r).lit < 0 ? "Negative LIT value" : "LIT < EIT");
				break;
			}
		}
	}


	private long assignRemainingRuns(final IProgressMonitor monitor, final AmaltheaFactory swf, long cpl, final SWModel swm2, int ct) {
		while (!this.assignedNodes.containsAll(swm2.getRunnables())) {
			LOGGER.debug("Creating PP {}", ct);
			monitor.worked(1);
			final ProcessPrototype ppt = swf.createProcessPrototype();
			ppt.setName("PP" + this.ctcp + "," + ct++);
			final long ttime = 0;

			/* check cpl: there may be a runnable with greater lit */
			cpl = assignNewTrcs(swf, cpl, swm2, ppt, ttime);
			checkPPIsNotEmpty(swm2, ppt);
			this.result.add(ppt);
		}
		if (this.bglobalCP) {
			this.ctcp++;
		}
		return cpl;
	}


	private void createSubModelsForIndepGraphs(final SWModel swm2, final ConstraintsModel cm2, final ProcessPrototype pp) {
		if (!cm2.getRunnableSequencingConstraints().isEmpty()) {
			final EList<RunnableSequencingConstraint> rscl = new BasicEList<>();
			for (final RunnableSequencingConstraint rsc : cm2.getRunnableSequencingConstraints()) {
				rscl.add(rsc);
			}
			for (final RunnableSequencingConstraint rsc : rscl) {
				getCm().getRunnableSequencingConstraints().add(rsc);
			}
		}
		cm2.getRunnableSequencingConstraints().clear();
		swm2.getRunnables().clear();
		swm2.getProcessPrototypes().clear();
		final EList<Runnable> llr = new Helper().getRunnables(pp);
		for (final Runnable ru : llr) {
			if (!this.assignedNodes.contains(ru)) {
				swm2.getRunnables().add(ru);
			}
		}


		final EList<RunnableSequencingConstraint> rscl = new BasicEList<>();
		for (final RunnableSequencingConstraint rsc : getCm().getRunnableSequencingConstraints()) {
			rscl.add(rsc);
		}
		for (final RunnableSequencingConstraint rsc : rscl) {
			if (llr.contains(rsc.getRunnableGroups().get(0).getRunnables().get(0))
					&& llr.contains(rsc.getRunnableGroups().get(1).getRunnables().get(0))) {
				cm2.getRunnableSequencingConstraints().add(rsc);
			}
		}

		swm2.getActivations().addAll(getSwm().getActivations());
	}


	private long createLCP(final AmaltheaFactory swf, final SWModel swm2, final ConstraintsModel cm2, final int ct) {
		long cpl;
		final CriticalPath lcp = new CriticalPath(swm2, cm2);
		final EList<Runnable> lcpr = lcp.getCP();
		final ProcessPrototype pp1 = swf.createProcessPrototype();
		pp1.setName("LCP" + ++this.ctcp + ct);
		for (final Runnable r : lcpr) {
			final RunnableCall trc = swf.createRunnableCall();
			trc.setRunnable(r);
			pp1.getRunnableCalls().add(trc);
		}
		this.result.add(pp1);
		cpl = lcp.getPathLength(lcpr);
		for (final Runnable r : lcpr) {
			this.assignedNodes.add(r);
		}
		return cpl;
	}


	private void checkPPIsNotEmpty(final SWModel swm2, final ProcessPrototype ppt) {
		if (ppt.getRunnableCalls().isEmpty()) {
			LOGGER.error("No Runnable found! Unassigned Nodes: ");
			final StringBuilder sb = new StringBuilder();
			for (final Runnable r : swm2.getRunnables()) {
				if (!this.assignedNodes.contains(r)) {
					sb.append(r.getName() + "(" + this.cache.get(r).eit + " " + this.cache.get(r).lit + "), ");
				}
			}
			LOGGER.trace("{}", sb);
		}
	}


	private void printEitLitVals() {
		for (final ProcessPrototype pp : this.result) {
			final StringBuilder sb = new StringBuilder();
			sb.append("ProcessPrototype " + pp.getName() + ": ");
			for (final RunnableCall trc : pp.getRunnableCalls()) {
				sb.append(trc.getRunnable().getName() + " (" + this.cache.get(trc.getRunnable()).eit + ","
						+ this.cache.get(trc.getRunnable()).lit + ") ");
			}
			LOGGER.trace("{}", sb);
		}
	}


	private void resultToSwm() {
		getSwm().getProcessPrototypes().clear();
		for (final ProcessPrototype pp : this.result) {
			getSwm().getRunnables().addAll(new Helper().getRunnables(pp));
			getSwm().getProcessPrototypes().add(pp);
		}
	}


	private void retainRSCs(final ConstraintsModel cm2) {
		getCm().getRunnableSequencingConstraints().addAll(cm2.getRunnableSequencingConstraints());
		final Amalthea amodels = AmaltheaFactory.eINSTANCE.createAmalthea();
		amodels.setConstraintsModel(getCm());
		amodels.setSwModel(getSwm());
		setCm(new Helper().updateRSCs(amodels.getConstraintsModel(), amodels.getSwModel()));
		for (final Runnable r : getSwm().getRunnables()) {
			if (null != r.getFirstActivation() && !getSwm().getActivations().contains(r.getFirstActivation())) {
				getSwm().getActivations().add(r.getFirstActivation());
			}
		}
	}

	private final HashMap<Runnable, Long> runInstrCache = new HashMap<>();

	private long assignNewTrcs(final AmaltheaFactory swf, long cpl, final SWModel swm2, final ProcessPrototype ppt, long ttime) {
		for (final Runnable r : swm2.getRunnables()) {
			if (this.cache.get(r).lit > cpl) {
				cpl = this.cache.get(r).lit + this.runInstrCache.get(r);
			}
		}
		int slack = 0;
		while (ttime < cpl) {
			final List<Runnable> an = getAssignableNodes(ttime, swm2);
			switch (an.size()) {
				case 0:
					final long nextTime = getNextAssignableRunnablesRT(ttime, swm2);
					slack = (int) (nextTime >= ttime ? (nextTime - ttime) : 0);
					ttime = nextTime != ttime ? nextTime : ttime + 1;
					if (ttime <= 0) {
						ttime = cpl;
					}
					break;
				case 1:
					createAndAddRunCall(swf, ppt, ttime, slack, an.get(0));
					ttime += (this.runInstrCache.get(an.get(0)));
					slack = 0;
					break;
				default:
					final Runnable men = getMostEffectiveNode(an);
					if (men == null) {
						LOGGER.error("No most effective Runnable found - aborting");
						return 0;
					}
					createAndAddRunCall(swf, ppt, ttime, slack, men);
					ttime += (this.runInstrCache.get(men));
					slack = 0;
					break;
			}
		}
		return cpl;
	}


	private void createAndAddRunCall(final AmaltheaFactory swf, final ProcessPrototype ppt, final long ttime, final int slack,
			final Runnable men) {
		final RunnableCall trc2 = swf.createRunnableCall();
		trc2.setRunnable(men);
		if (slack > 0) {
			CustomPropertyUtil.customPut(trc2, "slack", slack);
		}
		ppt.getRunnableCalls().add(trc2);
		this.assignedNodes.add(men);
		updateTFs(men, ttime);
	}


	/**
	 * any assignment influences eit & lit values of unassigned nodes, this method corrects them
	 *
	 * @param r
	 *            the assigned runnable in order to determine suceeding and preceeding unassigned runnables
	 * @param ttime
	 *            the assigned time of assigend @param r, to determine a distance, that has to be added to eit or subtracted from lit values
	 * @throws Exception
	 */
	private void updateTFs(final Runnable r, final long ttime) {
		/*ttime does not include r ticks*/
		final long eitDistance = ttime - this.cache.get(r).eit;
		if (eitDistance > 0) {
			final TimeFrame rtf = this.cache.get(r);
			if (ttime <= rtf.lit) {
				rtf.eit = ttime;
			}
			else {
				LOGGER.error("Runnable {} is put at PP pos outside its time frame ", r.getName());
			}
			final Set<Runnable> srl = getSuceedingUnassignedRunnablesSingleLevel(r);
			for (final Runnable run : srl) {
				final TimeFrame ntf = this.cache.get(run);
				final long newttime = ttime + this.runInstrCache.get(r);
				if (newttime >= ntf.eit && newttime <= ntf.lit) {
					updateTFs(run, newttime);
				}
				else if (newttime >= ntf.eit && newttime > ntf.lit) {
					LOGGER.error("Runnable {} is put at PP pos outside its time frame ", run.getName());
				}
			}
		}
	}

	private Set<Runnable> getSuceedingUnassignedRunnablesSingleLevel(final Runnable r) {
		final Set<Runnable> targets = new HashSet<>();
		final Set<RunnableSequencingConstraint> targetsrsc = this.graph.outgoingEdgesOf(r);
		for (final RunnableSequencingConstraint rsc : targetsrsc) {
			if (!this.assignedNodes.contains(this.graph.getEdgeTarget(rsc))) {
				targets.add(this.graph.getEdgeTarget(rsc));
			}
		}
		return targets;
	}

	private long getNextAssignableRunnablesRT(final long time, final SWModel swm2) {
		List<Runnable> rl = swm2.getRunnables();
		rl.removeAll(this.assignedNodes);
		rl = rl.stream().filter(r -> this.cache.get(r).eit >= time).collect(Collectors.toList());
		final Runnable minRun = rl.stream().min((r1, r2) -> Long.compare(this.cache.get(r1).eit, this.cache.get(r2).eit)).orElse(null);
		return null == minRun ? 0 : this.cache.get(minRun).eit;
	}

	/**
	 * 1st half: determines earliest assignable node; 2nd half: determines a node within the given set of nodes @param an, that features the most restricted
	 * time frame and lowest communication overhead
	 *
	 * @return most effective node
	 */
	private Runnable getMostEffectiveNode(final List<Runnable> an) {
		/*
		 * from all assignable runnables, check preceding unassigned runnable instruction sums
		 */
		final HashMap<Runnable, Long> hm = new HashMap<>();
		final long cpl = this.globalCP.getPathLength();
		for (final Runnable r : an) {
			long prio = 1;
			prio += getCommunicationOverhead(r);
			final TimeFrame tF = this.cache.get(r);
			/*
			 * lit-eit --> the smaller the value the higher their prio e.g. cpl=20, lit-eit=5; prio is multiplied by shift factor 15
			 */
			prio *= cpl - (tF.lit - tF.eit > 0 ? tF.lit - tF.eit : 1);
			/* TBD consider runnable size */
			hm.put(r, prio);
		}

		float temp = 0;
		Runnable ret = null;
		for (final Entry<Runnable, Long> r : hm.entrySet()) {
			if (hm.get(r.getKey()) > temp) {
				ret = r.getKey();
				temp = hm.get(r.getKey());
			}
		}
		return ret;
	}

	/**
	 * computes the runnable's @param r communication overhead that is defined by the sum of accessed label size(s) in bits, shared with runnables of other
	 * processprototypes
	 *
	 * @return communication overhead = size of shared labels in bits (with other processprototypes)
	 */
	private long getCommunicationOverhead(final Runnable r) {
		int co = 0;
		/* Runnable is called by one processprototype */
		if (r.getRunnableCalls().size() > 1) {
			LOGGER.error("Runnable is called multiple times");
			return 0;
		}

		if (r.getRunnableCalls().isEmpty()) {
			return 0;
		}
		final RunnableCall trc = r.getRunnableCalls().get(0);
		if (!(trc.eContainer() instanceof ProcessPrototype)) {
			LOGGER.error("TRC neither task nor parocessPrototye");
			return 0;
		}
		for (final Label l : SoftwareUtil.getAccessedLabelSet(r, null)) {
			co += l.getSize() != null ? l.getSize().getNumberBits() : 1;
		}
		return co;
	}

	/**
	 * computes all assignable nodes according to a specific starttime and no order constraint violation with any assigned runnable
	 *
	 * @param tt
	 *            tasktime
	 * @param swm2
	 * @return all nodes, that can be computed at the given starttime tt
	 */
	private List<Runnable> getAssignableNodes(final long tt, final SWModel swm2) {
		final List<Runnable> an = swm2.getRunnables();
		an.removeAll(this.assignedNodes);
		return an.stream().filter(r -> this.cache.get(r).eit <= tt && this.cache.get(r).lit >= tt).collect(Collectors.toList());
	}


	public ConstraintsModel getCm() {
		return this.cm;
	}


	public void setCm(final ConstraintsModel cm) {
		this.cm = cm;
	}


	public SWModel getSwm() {
		return this.swm;
	}


	public DirectedGraph<Runnable, RunnableSequencingConstraint> getGraph() {
		return this.graph;
	}


	public CriticalPath getGlobalCP() {
		return this.globalCP;
	}
}
