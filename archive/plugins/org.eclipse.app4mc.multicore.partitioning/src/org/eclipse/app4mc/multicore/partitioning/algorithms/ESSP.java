/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.algorithms;

import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AccessPrecedenceSpec;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.RunnableSequencingConstraint;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil;
import org.eclipse.app4mc.multicore.partitioning.utils.Helper;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class forms an Earliest Start Schedule Partitioning mechanism, in oder
 * to distribute runnables to ProcessPrototypes with respect to a user defined
 * number of tasks. The mechanism iterates over all runnables beginning with the
 * one, that starts earliest and finishes its calculation after the smallest
 * amount of instructions. "Later" runnables are assigned to tasks with regard
 * to their dependency and to their instructions.
 */
public class ESSP {
	private static final Logger LOGGER = LoggerFactory.getLogger(ESSP.class);
	private final SWModel swm;
	private final ConstraintsModel cm;
	private final EList<ProcessPrototype> partitions = new BasicEList<>();
	private final EList<Runnable> assignedNodes = new BasicEList<>();
	private final Map<Runnable, Long> runPrecRTCache = new HashMap<>();
	private final Map<ProcessPrototype, Long> targetRT = new HashMap<>();
	private final CriticalPath cp;

	/**
	 * Constructor requires a @param swmi SWModel, @param cmi constraintsmodel
	 * and @param tasks the user defined number of tasks
	 */
	public ESSP(final SWModel swmi, final ConstraintsModel cmi, final int tasks, final CriticalPath... crpa) {
		this.swm = swmi;
		this.cm = cmi;
		if (swmi.getProcessPrototypes().size() > 1) {
			/* activation analsis performed starting ESSPe*/
			final ESSPe esspe = new ESSPe(swmi, cmi, tasks);
			esspe.build(new NullProgressMonitor());
		}
		final AmaltheaFactory swf = AmaltheaFactory.eINSTANCE;
		for (int i = 0; i < tasks; i++) {
			final ProcessPrototype pp = swf.createProcessPrototype();
			pp.setName("ESS" + i);
			this.partitions.add(pp);
			this.targetRT.put(pp, (long) 0);
		}
		if (tasks < getSwm().getProcessPrototypes().size()) {
			LOGGER.debug(
					"ESSP can't create less partitions than already present from independent graph groups (GGP) or activation groups (AA). ProcessPrototypes in file {} configured partitions:{}. ProcessPrototypes are resetted (AA/GGP is ignored).",
					getSwm().getProcessPrototypes().size(), tasks);

			getSwm().getProcessPrototypes().clear();
			final ProcessPrototype pp = swf.createProcessPrototype();
			pp.setName("AllRunnables");
			for (final Runnable r : getSwm().getRunnables()) {
				final RunnableCall trc = swf.createRunnableCall();
				trc.setRunnable(r);
				pp.getRunnableCalls().add(trc);
			}
			getSwm().getProcessPrototypes().add(pp);
		}

		this.cp = crpa.length > 0 ? crpa[0] : new CriticalPath(getSwm(), getCm());
		this.cp.getCP();
		for (final Runnable r : swmi.getRunnables()) {
			this.runPrecRTCache.put(r, this.cp.getLongestPrecRT(r));
		}
	}


	/**
	 * starts the ESSP mechanism. Requires runnables, dependencies
	 * (RunnableSequencingConstraints) and Instructions for each runnable
	 */
	public EList<ProcessPrototype> build(final IProgressMonitor monitor) {
		final Set<AccessPrecedenceSpec> aps = new HashSet<>();
		if (getSwm().getProcessPrototypes().size() > 1) {
			// If PPs are present from AA / GGP --> perform ESS partitioning via
			// ESSPe class
			return new ESSPe(getSwm(), getCm(), this.partitions.size()).build(monitor);
		}
		else if (getSwm().getProcessPrototypes().size() == 1) {
			aps.addAll(getSwm().getProcessPrototypes().get(0).getAccessPrecedenceSpec());
		}
		final Deque<Runnable> runs = createRunnableStack();

		final int temp = getSwm().getRunnables().size() - this.assignedNodes.size();
		monitor.beginTask("ESSP...", temp);
		while (this.assignedNodes.size() < getSwm().getRunnables().size()) {
			monitor.worked(1);
			final Runnable r = runs.pop();
			int taskIndex = 0;
			final ProcessPrototype ppsel = getLongestPPwithPred(r);
			if (ppsel == null) {
				taskIndex = getIndexOfEarliestTask();
			}
			else {
				taskIndex = this.partitions.indexOf(ppsel);
			}
			final AmaltheaFactory swf = AmaltheaFactory.eINSTANCE;
			final RunnableCall trc = swf.createRunnableCall();
			trc.setRunnable(r);
			if (this.slack > 0) {
				CustomPropertyUtil.customPut(trc, "slack", (int) this.slack);
			}
			this.partitions.get(taskIndex).getRunnableCalls().add(trc);
			this.assignedNodes.add(r);
			this.targetRT.put(this.partitions.get(taskIndex),
					this.targetRT.get(this.partitions.get(taskIndex)).longValue() + new Helper().getInstructions(r) + this.slack);
			this.slack = 0;
		}
		monitor.done();

		getSwm().getProcessPrototypes().clear();
		// user may have specified more partitions than ess created: delete
		// those
		for (final Iterator<ProcessPrototype> it = this.partitions.iterator(); it.hasNext();) {
			if (it.next().getRunnableCalls().isEmpty()) {
				it.remove();
			}
		}
		getSwm().getProcessPrototypes().addAll(this.partitions);

		// -----------------------------------------------------------
		for (final ProcessPrototype pp : getSwm().getProcessPrototypes()) {
			pp.setName("ESSP" + getSwm().getProcessPrototypes().indexOf(pp));
		}
		new Helper().updateRSCs(getCm(), getSwm());
		new Helper().updatePPsFirstLastActParams(getSwm());
		new Helper().assignAPs(aps);
		return getSwm().getProcessPrototypes();
	}

	/**
	 * Creates a Stack of Runnables all existing runnable in the swmodel, sorted
	 * by their graph position (sum of longest preceeding runnable's
	 * instructions; top = highest amount)
	 */
	private Deque<Runnable> createRunnableStack() {
		final List<Runnable> rl = new BasicEList<>();
		rl.addAll(getSwm().getRunnables());
		Collections.sort(rl,
				(final Runnable o1,
						final Runnable o2) -> ((this.runPrecRTCache.get(o2).intValue() - this.runPrecRTCache.get(o1).intValue()) == 0
								? (int) (new Helper().getInstructions(o1) - new Helper().getInstructions(o2))
								: (this.runPrecRTCache.get(o2).intValue() - this.runPrecRTCache.get(o1).intValue())));
		final Deque<Runnable> rs = new LinkedList<>();
		for (final Runnable r : rl) {
			rs.push(r);
		}
		return rs;
	}

	private long slack = 0;

	/**
	 * @return indexes of tasks, that possess a direct predecessor of @param r
	 *         at their last entry
	 */
	private ProcessPrototype getLongestPPwithPred(final Runnable r) {
		final Set<RunnableSequencingConstraint> rscs = this.cp.getGraph().incomingEdgesOf(r);
		if (rscs.isEmpty()) {
			return this.partitions.get(getIndexOfEarliestTask());
		}
		final Set<Runnable> rs = new HashSet<>();
		for (final RunnableSequencingConstraint rsc : rscs) {
			rs.add(rsc.getRunnableGroups().get(0).getRunnables().get(0));
		}
		long longestPred = -1;
		ProcessPrototype ppsel = null;
		for (final ProcessPrototype pp : this.partitions) {
			final long predt = getMaxPredTime(pp, rs);
			if (predt > longestPred) {
				ppsel = pp;
				longestPred = predt;
			}
		}
		if (ppsel == null) {
			LOGGER.error("No PP found");
		}

		/* is there a PP, of which the length is (> longestPred && < PP length of ppsel) then choose that one */
		long ppselLength = new Helper().getPPInstructions(ppsel);
		for (final ProcessPrototype pp : this.partitions) {
			final long currentPPLength = new Helper().getPPInstructions(pp);
			if (currentPPLength >= longestPred && currentPPLength < ppselLength) {
				ppsel = pp;
				ppselLength = currentPPLength;
			}
			/*check if there is PP, of which the length is < lengestPred and add slack to the runnable call*/
			if (currentPPLength < longestPred) {
				this.slack = longestPred - currentPPLength;
				ppsel = pp;
				ppselLength = currentPPLength;
			}
		}
		return ppsel;
	}

	private long getMaxPredTime(final ProcessPrototype pp, final Set<Runnable> rs) {
		if (!ppContains(pp, rs)) {
			return 0l;
		}
		final Set<Runnable> rsInPP = new HashSet<>();
		for (final RunnableCall rc : pp.getRunnableCalls()) {
			if (rs.contains(rc.getRunnable())) {
				rsInPP.add(rc.getRunnable());
			}
		}
		long count = 0;
		for (final RunnableCall rc : pp.getRunnableCalls()) {
			count += new Helper().getInstructions(rc.getRunnable());
			rsInPP.remove(rc.getRunnable());
			if (rsInPP.isEmpty()) {
				break;
			}
		}
		return count;
	}


	private boolean ppContains(final ProcessPrototype pp, final Set<Runnable> rs) {
		for (final RunnableCall rc : pp.getRunnableCalls()) {
			if (rs.contains(rc.getRunnable())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the index of the ProcessPrototype that features lowest
	 *         instructions defined by all containing runnables
	 */
	private int getIndexOfEarliestTask() {
		int index = 0;
		long min = Long.MAX_VALUE;
		for (final Entry<ProcessPrototype, Long> pp : this.targetRT.entrySet()) {
			if (this.targetRT.get(pp.getKey()).longValue() < min) {
				min = this.targetRT.get(pp.getKey()).longValue();
				index = this.partitions.indexOf(pp.getKey());
			}
		}
		return index;
	}


	public SWModel getSwm() {
		return this.swm;
	}


	public ConstraintsModel getCm() {
		return this.cm;
	}
}
