/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.algorithms;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.multicore.partitioning.utils.Helper;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public class Path {
	private Set<Runnable> runnables;
	private long runtime;

	public Path() {
		setRuntime(0);
		this.runnables = new HashSet<>();
	}

	public Path(final Set<Runnable> rl) {
		setRunnables(rl);
		updateRunTime();
		this.runnables = new HashSet<>();
	}

	public String getRunStr() {
		final StringBuilder sb = new StringBuilder();
		for (final Runnable r : getRunnables()) {
			sb.append(r.getName() + " ");
		}
		return sb.toString();
	}

	public void updateRunTime() {
		long i = 0;
		for (final Runnable r : getRunnables()) {
			i += new Helper().getInstructions(r);
		}
		this.runtime = i;
	}

	public Set<Runnable> getRunnables() {
		return this.runnables;
	}

	public EList<Runnable> getRunnablesL() {
		final EList<Runnable> rl = new BasicEList<>();
		for (final Runnable r : this.runnables) {
			rl.add(r);
		}
		return rl;
	}

	public void setRunnables(final Set<Runnable> rl) {
		this.runnables = rl;
		updateRunTime();
	}

	@SuppressWarnings("unchecked")
	public void setRunnables(final EList<Runnable> runnables) {
		this.runnables = (HashSet<Runnable>) runnables;
		updateRunTime();
	}

	public long getRuntime() {
		return this.runtime;
	}

	public void setRuntime(final long runtime) {
		this.runtime = runtime;
	}
}
