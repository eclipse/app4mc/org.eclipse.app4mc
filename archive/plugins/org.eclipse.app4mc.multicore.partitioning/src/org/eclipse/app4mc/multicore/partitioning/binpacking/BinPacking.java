/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.multicore.partitioning.binpacking;

import java.math.BigInteger;
import java.util.Arrays;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;


public class BinPacking {

	private final Amalthea amaltheaModel;

	public BinPacking(final Amalthea ama) {
		this.amaltheaModel = ama;
	}

	public void binPacking(final int nBPartitions, final int solveTimeInS) {

		/**************** 1. CONSTRUCT CHOCO MODEL ******************/
		final Model chocoModel = new Model("Partitioning_BinPacking");
		final int binSize = (getInstruSum() / nBPartitions)
				+ 2 * SoftwareUtil.getTicks(this.amaltheaModel.getSwModel().getRunnables().get(0), null).get(0).getDefault().getUpperBound().intValue();
		final int[] weights = new int[this.amaltheaModel.getSwModel().getRunnables().size()];
		for (final Runnable r : this.amaltheaModel.getSwModel().getRunnables()) {
			final int instr = (int) SoftwareUtil.getTicks(r, null).stream().mapToLong(ticks -> ticks.getDefault().getUpperBound()).sum();
			weights[this.amaltheaModel.getSwModel().getRunnables().indexOf(r)] = instr;
		}
		final IntVar[] binAssignment = chocoModel.intVarArray("binAssignment", this.amaltheaModel.getSwModel().getRunnables().size(), 0, nBPartitions - 1,
				false);
		/* max bin load definition */
		final IntVar[] binLoads = chocoModel.intVarArray("load", nBPartitions, 0, binSize, true);


		printBPvals(nBPartitions, binSize, weights);


		/**************** 2. APPLY CONSTRAINTS **********************/
		chocoModel.binPacking(binAssignment, weights, binLoads, 0).post();
		/* create minimum constraint */
		/* alternatively: IntVar minLoad = chocoModel.intVar("minLoad", 0, binSize, true) */
		/* alternatively: chocoModel.min(minLoad, binLoads).post() */
		/* create maximum constraint */
		final IntVar maxLoad = chocoModel.intVar("maxLoad", 0, binSize, true);
		chocoModel.max(maxLoad, binLoads).post();


		/**************** 3. SOLUTION PROCESS ***********************/
		chocoModel.setObjective(false, maxLoad);
		chocoModel.getSolver().limitTime(solveTimeInS * 1000l);
		final Solver solver = chocoModel.getSolver();
		final Solution fs = new Solution(chocoModel);
		while (solver.solve()) {
			fs.record();
		}
		solver.printStatistics();
		Logger.getLogger(this.getClass()).log(Level.INFO, fs.toString());
		for (int i = 0; i < nBPartitions; i++) {
			Logger.getLogger(this.getClass()).log(Level.INFO, String.format("BinLoad%3d %12d", i, fs.getIntVal(binLoads[i])));
		}


		/**************** 4. APPLY SOLUTION TO AMALTHEA MODEL *******/
		/* apply CSP solution to new PPs */
		if (solver.getSolutionCount() > 0) {
			for (int i = 0; i < nBPartitions; i++) {
				final ProcessPrototype pp = AmaltheaFactory.eINSTANCE.createProcessPrototype();
				pp.setName(Integer.toString(i));
				this.amaltheaModel.getSwModel().getProcessPrototypes().add(pp);
			}
			for (final Runnable r : this.amaltheaModel.getSwModel().getRunnables()) {
				final RunnableCall rc = AmaltheaFactory.eINSTANCE.createRunnableCall();
				rc.setRunnable(r);
				this.amaltheaModel.getSwModel().getProcessPrototypes()
						.get(fs.getIntVal(binAssignment[this.amaltheaModel.getSwModel().getRunnables().indexOf(r)])).getRunnableCalls().add(rc);
			}
		}

	}

	private void printBPvals(final int nBPartitions, final int binSize, final int[] weights) {
		Logger.getLogger(this.getClass()).log(Level.INFO,
				String.format("%10s %10d%n%10s %10d%n%10s %10d%n%10s %10d%n%10s %10d", "BinSize", binSize, "SumBins", (nBPartitions * binSize), "SumItems",
						Arrays.stream(weights).sum(), "MinItem", Arrays.stream(weights).min().getAsInt(), "MaxItem", Arrays.stream(weights).max().getAsInt()));
	}

	public Amalthea getAmaltheaModel() {
		return this.amaltheaModel;
	}

	private int getInstruSum() {
		BigInteger sum = BigInteger.valueOf(0);
		for (final Runnable r : this.amaltheaModel.getSwModel().getRunnables()) {
			sum = sum.add(BigInteger.valueOf(SoftwareUtil.getTicks(r, null).stream().mapToLong(ticks -> ticks.getDefault().getUpperBound()).sum()));
		}
		return sum.intValue();
	}

}
