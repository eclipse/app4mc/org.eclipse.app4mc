/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.utils;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.Activation;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.PeriodicActivation;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class creates ProcessPrototypes and including RunnableCalls according to Activations either referenced by tasks (Stimuli model - reverse engineered) or
 * within a SW model
 */
public class CheckActivations {

	private SWModel swmo;
	private StimuliModel stimu;
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckActivations.class);

	/**
	 * The createPP method including the stimuli model as a parameter creates ProcessPrototypes and Activations for each stimuli entry in the stimuli model, and
	 * assigns runnables correspondingly to the given activations
	 *
	 * @param swm
	 *            The SoftwareModel containing tasks, that reference activations from the stimuli model
	 * @param stimp
	 *            The StimuliModel containing the activationed referenced by the tasks in the SoftwareModel
	 */
	public void createPPs(final SWModel swm, final StimuliModel stimp, final IProgressMonitor monitor) {
		this.stimu = stimp;
		/*
		 * if there is a Stimuli model, there also must be tasks, that reference the activations; correspondingly, activations must be created and referenced by
		 * runnables
		 */
		for (final Stimulus stim : stimp.getStimuli()) {
			final PeriodicActivation act = AmaltheaFactory.eINSTANCE.createPeriodicActivation();
			act.setName(stim.getName());
			if (stim instanceof PeriodicStimulus) {
				final PeriodicStimulus per = (PeriodicStimulus) stim;
				if (null != per.getRecurrence()) {
					final Time t = AmaltheaFactory.eINSTANCE.createTime();
					t.setUnit(per.getRecurrence().getUnit());
					t.setValue(per.getRecurrence().getValue());
					act.setMax(t);
					final Time t2 = AmaltheaFactory.eINSTANCE.createTime();
					t2.setUnit(per.getRecurrence().getUnit());
					t2.setValue(per.getRecurrence().getValue());
					act.setMin(t2);
					final Time t3 = AmaltheaFactory.eINSTANCE.createTime();
					t3.setUnit(per.getRecurrence().getUnit());
					t3.setValue(per.getRecurrence().getValue());
					act.setRecurrence(t3);
				}
			}
			else if (stim instanceof InterProcessStimulus) {
				final InterProcessStimulus ips = (InterProcessStimulus) stim;
				final Task trigger = getFirstTrigger(ips);
				final PeriodicStimulus tris = (PeriodicStimulus) trigger.getStimuli().get(0);
				final Time t5 = AmaltheaFactory.eINSTANCE.createTime();
				t5.setUnit(tris.getRecurrence().getUnit());
				t5.setValue(tris.getRecurrence().getValue());
				act.setRecurrence(t5);
			}
			swm.getActivations().add(act);
		}
		updateRunActsByTaskAct(swm);
		createPPs(swm, monitor);
	}

	private void updateRunActsByTaskAct(final SWModel swm) {
		for (final Task t : swm.getTasks()) {
			for (final ActivityGraphItem geb : t.getActivityGraph().getItems()) {
				if (geb instanceof Group) {
					preGroupActUpdt(swm, t, geb);
				}
			}
		}
	}

	private void preGroupActUpdt(final SWModel swm, final Task t, final ActivityGraphItem geb) {
		Activation ref = null;
		for (final Activation a : swm.getActivations()) {
			if (a.getName().equals(t.getStimuli().get(0).getName())) {
				ref = a;
				break;
			}
		}
		for (final ActivityGraphItem csi : ((Group) geb).getItems()) {
			if (csi instanceof RunnableCall) {
				((RunnableCall) csi).getRunnable().getActivations().clear();
				((RunnableCall) csi).getRunnable().getActivations().add(ref);
			}
		}
	}

	/**
	 * The createPP method without any stimuli parameter creates ProcessPrototypes for each activation referenced by runnables within the softweare model
	 *
	 * @param swm
	 *            The SoftwareModel containing runnables, that reference activations within the software model
	 * @throws Exception
	 */
	public void createPPs(final SWModel swm, final IProgressMonitor monitor) {
		if (null != monitor) {
			monitor.beginTask("Acitvation Grouping", swm.getActivations().size());
		}
		if (!swm.getActivations().isEmpty()) {
			LOGGER.debug("There are {} activations.", swm.getActivations().size());
			for (final Activation act : swm.getActivations()) {
				if (null != monitor) {
					monitor.worked(1);
				}
				itrtOvrRnnblsGrpByAct(swm, act);
			}
			this.swmo = swm;
		}
		else {
			LOGGER.error("No activation found within software model!");
		}
	}

	private void itrtOvrRnnblsGrpByAct(final SWModel swm, final Activation act) {
		final AmaltheaFactory instance = AmaltheaFactory.eINSTANCE;
		final ProcessPrototype pp = instance.createProcessPrototype();
		pp.setName(act.getName());
		pp.setActivation(act);
		for (final Runnable r : swm.getRunnables()) {
			if (null != r.getFirstActivation()) {
				if (r.getFirstActivation().equals(act)) {
					final RunnableCall trc = instance.createRunnableCall();
					trc.setRunnable(r);
					pp.getRunnableCalls().add(trc);
				}
			}
			else {
				LOGGER.error("No activation reference found at runnable {}", r.getName());
			}
		}
		if (pp.getRunnableCalls().isEmpty()) {
			LOGGER.warn("There is an activation that is not referenced by any runnable (will be ignored) {}", pp.getActivation().getName());
		}
		else {
			swm.getProcessPrototypes().add(pp);
		}
	}

	public SWModel getSwmo() {
		return this.swmo;
	}

	public void setSwmo(final SWModel swmo) {
		this.swmo = swmo;
	}

	public StimuliModel getStimu() {
		return this.stimu;
	}

	public void setStimu(final StimuliModel stimu) {
		this.stimu = stimu;
	}

	private static Task getFirstTrigger(final InterProcessStimulus ips) {
		final Amalthea model = (Amalthea) ips.eContainer().eContainer();
		for (final Task t : model.getSwModel().getTasks()) {
			for (final ActivityGraphItem geb : t.getActivityGraph().getItems()) {
				final Task t2 = findIPSinGroup(ips, t, geb);
				if (null != t2) {
					return t2;
				}
			}
		}
		LOGGER.trace("No triggering task found for {}", ips.getName());
		return getTaskLongestPeriod(model);
	}

	private static Task findIPSinGroup(final InterProcessStimulus ips, final Task t, final ActivityGraphItem geb) {
		if (geb instanceof Group) {
			final Group cs = (Group) geb;
			for (final ActivityGraphItem csi : cs.getItems()) {
				if (csi instanceof InterProcessTrigger) {
					final InterProcessTrigger ipt = (InterProcessTrigger) csi;
					if (ipt.getStimulus().equals(ips) && t.getStimuli().get(0) instanceof PeriodicStimulus) {
						return t;
					}
				}
			}
		}
		return null;
	}

	private static Task getTaskLongestPeriod(final Amalthea model) {
		final List<Task> tl = model.getSwModel().getTasks().stream().filter(t -> t.getStimuli().get(0) instanceof PeriodicStimulus)
				.collect(Collectors.toList());
		Time maxPer = AmaltheaFactory.eINSTANCE.createTime();
		maxPer.setValue(BigInteger.ZERO);
		maxPer.setUnit(TimeUnit.PS);
		Task selT = tl.get(0);
		for (final Task t : tl) {
			if (((PeriodicStimulus) t.getStimuli().get(0)).getRecurrence().compareTo(maxPer) > 0) {
				maxPer = ((PeriodicStimulus) t.getStimuli().get(0)).getRecurrence();
				selT = t;
			}
		}
		return selT;
	}
}
