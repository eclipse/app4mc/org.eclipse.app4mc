/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.utils;

import java.util.HashMap;

import org.eclipse.app4mc.amalthea.model.AffinityConstraint;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.RunnableEntityGroup;
import org.eclipse.app4mc.amalthea.model.RunnablePairingConstraint;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.TargetCore;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public class RunnableCorePairingToPP {

	ConstraintsModel cm;
	SWModel swm;

	public RunnableCorePairingToPP(final SWModel sW, final ConstraintsModel cMp) {
		this.swm = sW;
		this.cm = cMp;
	}

	public void getPPsFromCorePairingsSplit() {
		this.swm.getProcessPrototypes().addAll(getPPsFromCorePairings());
	}

	private EList<ProcessPrototype> getPPsFromCorePairings() {
		final EList<ProcessPrototype> ppl = new BasicEList<>();
		final HashMap<ProcessingUnit, Integer> corePPIndexMap = new HashMap<>();

		for (final AffinityConstraint ac : this.cm.getAffinityConstraints()) {
			if (ac instanceof RunnablePairingConstraint) {
				final RunnablePairingConstraint rpc = (RunnablePairingConstraint) ac;
				if (rpc.getTarget() instanceof TargetCore) {
					final ProcessingUnit c = ((TargetCore) rpc.getTarget()).getCores().get(0);
					addOrCreatePPAddTrc(ppl, corePPIndexMap, rpc, c);
				}
			}
		}

		return ppl;
	}

	private void addOrCreatePPAddTrc(final EList<ProcessPrototype> ppl, final HashMap<ProcessingUnit, Integer> corePPIndexMap,
			final RunnablePairingConstraint rpc, final ProcessingUnit c) {
		if (!corePPIndexMap.containsKey(c)) {
			final ProcessPrototype pp = AmaltheaFactory.eINSTANCE.createProcessPrototype();
			pp.setName("CorePairing" + c.getName());
			final Tag tag = AmaltheaFactory.eINSTANCE.createTag();
			tag.setName("CorePairing" + c.getName());
			if (((TargetCore) rpc.getTarget()).getCores().size() > 1) {
				pp.setName(pp.getName() + "+");
				tag.setName(tag.getName() + "+");
			}
			pp.getTags().add(tag);
			for (final Runnable r : ((RunnableEntityGroup) rpc.getGroup()).getRunnables()) {
				final RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
				trc.setRunnable(r);
				pp.getRunnableCalls().add(trc);
				pp.getTags().add(tag);
			}
		}
		else {
			for (final Runnable r : ((RunnableEntityGroup) rpc.getGroup()).getRunnables()) {
				final RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
				trc.setRunnable(r);
				ppl.get(corePPIndexMap.get(c)).getRunnableCalls().add(trc);
			}
		}
	}

}
