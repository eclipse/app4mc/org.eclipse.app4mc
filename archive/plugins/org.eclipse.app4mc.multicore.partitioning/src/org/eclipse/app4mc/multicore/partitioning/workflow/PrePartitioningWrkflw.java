/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.workflow;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.workflow.core.Context;
import org.eclipse.app4mc.amalthea.workflow.core.WorkflowComponent;
import org.eclipse.app4mc.multicore.partitioning.algorithms.PrePartitioning;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrePartitioningWrkflw extends WorkflowComponent {
	private static final String RESULTSLOT = "prePartitioning";
	private boolean aa = false;
	private boolean ggp = false;
	private boolean effEdge = false;
	private boolean minimEdge = false;
	private static final Logger LOGGER = LoggerFactory.getLogger(PrePartitioningWrkflw.class);

	@Override
	protected void runInternal(final Context ctx) {
		Amalthea amodels = getAmaltheaModelCopy(ctx);

		final PrePartitioning prePart = new PrePartitioning(isAa(), isGgp(), isMinimEdge(), isEffEdge());
		amodels = prePart.performPrePartitioning(amodels, new NullProgressMonitor());

		if (amodels == null) {
			LOGGER.error("Prepartitioning failed");
			return;
		}
		LOGGER.debug("Setting result model in slot: {}", getResultSlot());
		ctx.set(getResultSlot(), amodels);
	}

	public String getResultSlot() {
		return PrePartitioningWrkflw.RESULTSLOT;
	}

	public boolean isAa() {
		return this.aa;
	}

	public boolean isGgp() {
		return this.ggp;
	}

	public boolean isEffEdge() {
		return this.effEdge;
	}

	public boolean isMinimEdge() {
		return this.minimEdge;
	}


	public void setAa(final boolean aa) {
		this.aa = aa;
	}


	public void setGgp(final boolean ggp) {
		this.ggp = ggp;
	}


	public void setEffEdge(final boolean effEdge) {
		this.effEdge = effEdge;
	}


	public void setMinimEdge(final boolean minimEdge) {
		this.minimEdge = minimEdge;
	}
}
