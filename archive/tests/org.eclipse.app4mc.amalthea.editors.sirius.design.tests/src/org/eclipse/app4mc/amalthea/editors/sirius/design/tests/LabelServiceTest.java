/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.editors.sirius.design.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.junit.Test;

/**
 * @author daniel.kunz@de.bosch.com
 *
 */
public class LabelServiceTest {

	private LabelService labelService = new LabelService();

	private SWModel createModelWithSWModel() {
		Amalthea model = AmaltheaFactory.eINSTANCE.createAmalthea();
		SWModel sw = AmaltheaFactory.eINSTANCE.createSWModel();
		model.setSwModel(sw);
		return sw;
	}

	private Runnable createRunnable(SWModel sw) {
		Runnable run = AmaltheaFactory.eINSTANCE.createRunnable();
		sw.getRunnables().add(run);
		return run;
	}

	private Label createLabel(SWModel sw) {
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		sw.getLabels().add(label);
		return label;
	}

	private LabelAccess createLabelAccess(Runnable run, Label label) {
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la.setData(label);
		run.getRunnableItems().add(la);
		return la;
	}


	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getRunnablesFromLabelAccesses(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromLabelAccessesNull() {
		List<Runnable> result = this.labelService.getRunnablesFromLabelAccesses(null);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getRunnablesFromLabelAccesses(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromLabelAccessesEmptyAccesses() {
		SWModel sw = createModelWithSWModel();
		Label label = createLabel(sw);

		List<Runnable> result = this.labelService.getRunnablesFromLabelAccesses(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getRunnablesFromLabelAccesses(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromLabelAccessesOneAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		createLabelAccess(runnable, label);

		List<Runnable> result = this.labelService.getRunnablesFromLabelAccesses(label);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(runnable, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getReadAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetReadAccessRunnableForLabelNull() {
		List<Runnable> result = this.labelService.getReadAccessRunnableForLabel(null);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getReadAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetReadAccessRunnableForLabelEmptyAccesses() {
		SWModel sw = createModelWithSWModel();
		Label label = createLabel(sw);

		List<Runnable> result = this.labelService.getReadAccessRunnableForLabel(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getReadAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetReadAccessRunnableForLabelReadAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		LabelAccess la = createLabelAccess(runnable, label);
		la.setAccess(LabelAccessEnum.READ);

		List<Runnable> result = this.labelService.getReadAccessRunnableForLabel(label);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(runnable, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getReadAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetReadAccessRunnableForLabelWriteAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		LabelAccess la = createLabelAccess(runnable, label);
		la.setAccess(LabelAccessEnum.WRITE);

		List<Runnable> result = this.labelService.getReadAccessRunnableForLabel(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getReadAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetReadAccessRunnableForLabelUndefinedAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		createLabelAccess(runnable, label);

		List<Runnable> result = this.labelService.getReadAccessRunnableForLabel(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getWriteAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetWriteAccessRunnableForLabelNull() {
		List<Runnable> result = this.labelService.getWriteAccessRunnableForLabel(null);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getWriteAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetWriteAccessRunnableForLabelEmptyAccesses() {
		SWModel sw = createModelWithSWModel();
		Label label = createLabel(sw);

		List<Runnable> result = this.labelService.getWriteAccessRunnableForLabel(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getWriteAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetWriteAccessRunnableForLabelReadAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		LabelAccess la = createLabelAccess(runnable, label);
		la.setAccess(LabelAccessEnum.READ);

		List<Runnable> result = this.labelService.getWriteAccessRunnableForLabel(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getWriteAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetWriteAccessRunnableForLabelWriteAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		LabelAccess la = createLabelAccess(runnable, label);
		la.setAccess(LabelAccessEnum.WRITE);

		List<Runnable> result = this.labelService.getWriteAccessRunnableForLabel(label);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(runnable, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.LabelService#getWriteAccessRunnableForLabel(org.eclipse.app4mc.amalthea.model.Label)}
	 * .
	 */
	@Test
	public void testGetWriteAccessRunnableForLabelUndefinedAccesses() {
		SWModel sw = createModelWithSWModel();
		Runnable runnable = createRunnable(sw);
		Label label = createLabel(sw);
		createLabelAccess(runnable, label);

		List<Runnable> result = this.labelService.getWriteAccessRunnableForLabel(label);

		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

}
