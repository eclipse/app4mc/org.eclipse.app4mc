/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.editors.sirius.design.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.app4mc.amalthea.editors.sirius.design.services.NameService;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Task;
import org.junit.Test;

/**
 * @author daniel.kunz@de.bosch.com
 *
 */
public class NameServiceTest {

	private NameService nameS = new NameService();

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.NameService#lengthOfName(org.eclipse.app4mc.amalthea.model.IReferable)}
	 * .
	 */
	@Test
	public void testLengthOfNameNull() {
		int result = this.nameS.lengthOfName(null);
		assertEquals(0, result);
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.NameService#lengthOfName(org.eclipse.app4mc.amalthea.model.IReferable)}
	 * .
	 */
	@Test
	public void testLengthOfNameEmptyObject() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		int result = this.nameS.lengthOfName(task);
		assertEquals(2, result);
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.NameService#lengthOfName(org.eclipse.app4mc.amalthea.model.IReferable)}
	 * .
	 */
	@Test
	public void testLengthOfNameObjectWithName() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		task.setName("name");
		int result = this.nameS.lengthOfName(task);
		assertEquals(6, result);
	}

}
