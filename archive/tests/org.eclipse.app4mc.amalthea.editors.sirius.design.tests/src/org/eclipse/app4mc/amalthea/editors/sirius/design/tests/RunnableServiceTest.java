/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.editors.sirius.design.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ExecutionNeed;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.util.InstructionsUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.junit.Test;

/**
 * @author daniel.kunz@de.bosch.com
 *
 */
public class RunnableServiceTest {

	private RunnableService runnS = new RunnableService();

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelWriteAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelWriteAccessesForRunnableNull() {
		List<Label> result = this.runnS.getLabelWriteAccessesForRunnable(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelWriteAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelWriteAccessesForRunnableEmpty() {
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		List<Label> result = this.runnS.getLabelWriteAccessesForRunnable(runnable);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelWriteAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelWriteAccessesForRunnableOneOtherRunnableItem() {
		Amalthea root = AmaltheaFactory.eINSTANCE.createAmalthea();
		SWModel sw = ModelUtil.getOrCreateSwModel(root);
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		sw.getRunnables().add(runnable);
		ExecutionNeed execNeed = InstructionsUtil.createExecutionNeedConstant(25);
		runnable.getRunnableItems().add(execNeed);
		List<Label> result = this.runnS.getLabelWriteAccessesForRunnable(runnable);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelWriteAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelWriteAccessesForRunnableOneLabelWrite() {
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		LabelAccess labelA = AmaltheaFactory.eINSTANCE.createLabelAccess();
		labelA.setData(label);
		labelA.setAccess(LabelAccessEnum.WRITE);
		runnable.getRunnableItems().add(labelA);
		Label label2 = AmaltheaFactory.eINSTANCE.createLabel();
		LabelAccess labelA2 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		labelA2.setData(label2);
		labelA2.setAccess(LabelAccessEnum.READ);
		runnable.getRunnableItems().add(labelA2);
		List<Label> result = this.runnS.getLabelWriteAccessesForRunnable(runnable);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(label, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelReadAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelReadAccessesForRunnableNull() {
		List<Label> result = this.runnS.getLabelReadAccessesForRunnable(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelReadAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelReadAccessesForRunnableEmpty() {
		Runnable runn = AmaltheaFactory.eINSTANCE.createRunnable();
		List<Label> result = this.runnS.getLabelReadAccessesForRunnable(runn);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelReadAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelReadAccessesForRunnableOtherItem() {
		Amalthea root = AmaltheaFactory.eINSTANCE.createAmalthea();
		SWModel sw = ModelUtil.getOrCreateSwModel(root);
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		sw.getRunnables().add(runnable);
		ExecutionNeed execNeed = InstructionsUtil.createExecutionNeedConstant(25);
		runnable.getRunnableItems().add(execNeed);
		List<Label> result = this.runnS.getLabelReadAccessesForRunnable(runnable);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getLabelReadAccessesForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetLabelReadAccessesForRunnableOneReadItem() {
		Amalthea root = AmaltheaFactory.eINSTANCE.createAmalthea();
		SWModel sw = ModelUtil.getOrCreateSwModel(root);
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		sw.getRunnables().add(runnable);
		ExecutionNeed execNeed = InstructionsUtil.createExecutionNeedConstant(25);
		runnable.getRunnableItems().add(execNeed);
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		LabelAccess labelA = AmaltheaFactory.eINSTANCE.createLabelAccess();
		labelA.setData(label);
		labelA.setAccess(LabelAccessEnum.WRITE);
		runnable.getRunnableItems().add(labelA);
		Label label2 = AmaltheaFactory.eINSTANCE.createLabel();
		LabelAccess labelA2 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		labelA2.setData(label2);
		labelA2.setAccess(LabelAccessEnum.READ);
		runnable.getRunnableItems().add(labelA2);
		List<Label> result = this.runnS.getLabelReadAccessesForRunnable(runnable);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(label2, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableNull() {
		List<Runnable> result = this.runnS.getCommunicationForRunnable(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableEmpty() {
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		List<Runnable> result = this.runnS.getCommunicationForRunnable(runnable);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableReadRead() {
		Runnable run1 = AmaltheaFactory.eINSTANCE.createRunnable();
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		Runnable run2 = AmaltheaFactory.eINSTANCE.createRunnable();
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la.setAccess(LabelAccessEnum.READ);
		la.setData(label);
		run1.getRunnableItems().add(la);
		LabelAccess la2 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la2.setAccess(LabelAccessEnum.READ);
		la2.setData(label);
		run2.getRunnableItems().add(la2);
		List<Runnable> result = this.runnS.getCommunicationForRunnable(run1);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableEmptyRead() {
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la.setAccess(LabelAccessEnum.WRITE);
		runnable.getRunnableItems().add(la);
		List<Runnable> result = this.runnS.getCommunicationForRunnable(runnable);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableReadWrite() {
		Runnable run1 = AmaltheaFactory.eINSTANCE.createRunnable();
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		Runnable run2 = AmaltheaFactory.eINSTANCE.createRunnable();
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la.setAccess(LabelAccessEnum.READ);
		la.setData(label);
		run1.getRunnableItems().add(la);
		LabelAccess la2 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la2.setAccess(LabelAccessEnum.WRITE);
		la2.setData(label);
		run2.getRunnableItems().add(la2);
		List<Runnable> result = this.runnS.getCommunicationForRunnable(run1);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableWriteWrite() {
		Runnable run1 = AmaltheaFactory.eINSTANCE.createRunnable();
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		Runnable run2 = AmaltheaFactory.eINSTANCE.createRunnable();
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la.setAccess(LabelAccessEnum.WRITE);
		la.setData(label);
		run1.getRunnableItems().add(la);
		LabelAccess la2 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la2.setAccess(LabelAccessEnum.WRITE);
		la2.setData(label);
		run2.getRunnableItems().add(la2);
		List<Runnable> result = this.runnS.getCommunicationForRunnable(run1);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.RunnableService#getCommunicationForRunnable(org.eclipse.app4mc.amalthea.model.Runnable)}
	 * .
	 */
	@Test
	public void testGetCommunicationForRunnableWriteRead() {
		Runnable run1 = AmaltheaFactory.eINSTANCE.createRunnable();
		Runnable run2 = AmaltheaFactory.eINSTANCE.createRunnable();
		Label label = AmaltheaFactory.eINSTANCE.createLabel();

		LabelAccess la1 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la1.setAccess(LabelAccessEnum.WRITE);
		la1.setData(label);
		run1.getRunnableItems().add(la1);

		LabelAccess la2 = AmaltheaFactory.eINSTANCE.createLabelAccess();
		la2.setAccess(LabelAccessEnum.READ);
		la2.setData(label);
		run2.getRunnableItems().add(la2);

		Amalthea model = AmaltheaFactory.eINSTANCE.createAmalthea();
		SWModel sw = ModelUtil.getOrCreateSwModel(model);
		sw.getRunnables().add(run1);
		sw.getRunnables().add(run2);
		sw.getLabels().add(label);

		List<Runnable> result = this.runnS.getCommunicationForRunnable(run1);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(run2, result.get(0));
	}

}
