/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.app4mc.multicore.partitioning.tests

import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.junit.Test
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.multicore.partitioning.binpacking.BinPacking
import static org.junit.Assert.assertTrue

class BinPackingTest {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	val int[] ia = #[2, 5, 3, 4, 12, 9, 1, 0, 5, 6, 2, 4]
	val int bins = 3
	
	
	@Test
	def void binPackingTest(){
		val model = amalthea [
			softwareModel[
				for (i : 0 ..<ia.length){
					runnables+=createRunnable(i)
				}
			]
		]
		
		var BinPacking bp = new BinPacking(model)
		bp.binPacking(bins,10)
		assertTrue(bp.amaltheaModel.swModel.processPrototypes.size==bins)
	}
	
	
	private def createRunnable(int id) {
		val r = AmaltheaFactory.eINSTANCE.createRunnable
		r.name=Integer.toString(id)
		val ag = AmaltheaFactory.eINSTANCE.createActivityGraph
		val ticks = AmaltheaFactory.eINSTANCE.createTicks
		ticks.^default = createDVC(ia.get(id))
		ag.items+=ticks
		r.activityGraph=ag
		r
	}
	
	private def DiscreteValueConstant createDVC(int upper) {
		val ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant
		ret.value=upper
		ret
	}
}