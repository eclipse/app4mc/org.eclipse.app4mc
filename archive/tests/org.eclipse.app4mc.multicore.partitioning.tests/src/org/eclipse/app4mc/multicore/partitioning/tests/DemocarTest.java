/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.multicore.partitioning.algorithms.CPP;
import org.eclipse.app4mc.multicore.partitioning.algorithms.ESSP;
import org.eclipse.app4mc.multicore.partitioning.utils.Helper;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.BeforeClass;
import org.junit.Test;

public class DemocarTest {

	@BeforeClass
	public static void init() {
		BasicConfigurator.configure();
	}

	@Test
	public void cppTest() {
		final Amalthea model = AmaltheaLoader.loadFromFileNamed("../../examples/app4mc.example.democar/democar.amxmi");
		assertNotNull(model);
		final CPP cpp = new CPP(model.getSwModel(), model.getConstraintsModel());
		cpp.build(new NullProgressMonitor());
		int runCalls = 0;
		for (final ProcessPrototype pp : cpp.getSwm().getProcessPrototypes()) {
			runCalls += pp.getRunnableCalls().size();
		}
		assertEquals(runCalls, cpp.getSwm().getRunnables().size());
		Logger.getLogger("DemocarTest").log(Level.DEBUG, new Helper().writePPs(cpp.getSwm().getProcessPrototypes()));
	}

	@Test
	public void esspTest() {
		final Amalthea model = AmaltheaLoader.loadFromFileNamed("../../examples/app4mc.example.democar/democar.amxmi");
		assertNotNull(model);
		final ESSP essp = new ESSP(model.getSwModel(), model.getConstraintsModel(), 6);
		essp.build(new NullProgressMonitor());
		assertEquals(6, essp.getSwm().getProcessPrototypes().size());
		int runCalls = 0;
		for (final ProcessPrototype pp : essp.getSwm().getProcessPrototypes()) {
			runCalls += pp.getRunnableCalls().size();
		}
		assertEquals(runCalls, model.getSwModel().getRunnables().size());
		Logger.getLogger("DemocarTest").log(Level.DEBUG, new Helper().writePPs(essp.getSwm().getProcessPrototypes()));
	}

}
