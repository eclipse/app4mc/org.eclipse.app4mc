/**
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 */
package org.eclipse.app4mc.multicore.partitioning.tests;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.multicore.partitioning.binpacking.BinPacking;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class BinPackingTest {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();
  
  private final int[] ia = { 2, 5, 3, 4, 12, 9, 1, 0, 5, 6, 2, 4 };
  
  private final int bins = 3;
  
  @Test
  public void binPackingTest() {
    final Procedure1<Amalthea> _function = (Amalthea it) -> {
      final Procedure1<SWModel> _function_1 = (SWModel it_1) -> {
        int _length = this.ia.length;
        ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length, true);
        for (final Integer i : _doubleDotLessThan) {
          EList<org.eclipse.app4mc.amalthea.model.Runnable> _runnables = it_1.getRunnables();
          org.eclipse.app4mc.amalthea.model.Runnable _createRunnable = this.createRunnable((i).intValue());
          _runnables.add(_createRunnable);
        }
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    BinPacking bp = new BinPacking(model);
    bp.binPacking(this.bins, 10);
    int _size = bp.getAmaltheaModel().getSwModel().getProcessPrototypes().size();
    boolean _equals = (_size == this.bins);
    Assert.assertTrue(_equals);
  }
  
  private org.eclipse.app4mc.amalthea.model.Runnable createRunnable(final int id) {
    org.eclipse.app4mc.amalthea.model.Runnable _xblockexpression = null;
    {
      final org.eclipse.app4mc.amalthea.model.Runnable r = AmaltheaFactory.eINSTANCE.createRunnable();
      r.setName(Integer.toString(id));
      final ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
      final Ticks ticks = AmaltheaFactory.eINSTANCE.createTicks();
      ticks.setDefault(this.createDVC(this.ia[id]));
      EList<ActivityGraphItem> _items = ag.getItems();
      _items.add(ticks);
      r.setActivityGraph(ag);
      _xblockexpression = r;
    }
    return _xblockexpression;
  }
  
  private DiscreteValueConstant createDVC(final int upper) {
    DiscreteValueConstant _xblockexpression = null;
    {
      final DiscreteValueConstant ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant();
      ret.setValue(upper);
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }
}
