/**
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 */
package org.eclipse.app4mc.multicore.partitioning.tests;

import com.google.common.base.Objects;
import java.math.BigInteger;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.multicore.partitioning.algorithms.CPP;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class CPPTest {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();
  
  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();
  
  @Test
  public void CPPTest() {
    final Procedure1<Amalthea> _function = (Amalthea it) -> {
      final Procedure1<ConstraintsModel> _function_1 = (ConstraintsModel it_1) -> {
      };
      this.b1.constraintsModel(it, _function_1);
      final Procedure1<SWModel> _function_2 = (SWModel it_1) -> {
        final Procedure1<Label> _function_3 = (Label it_2) -> {
          it_2.setName("l1");
          it_2.setSize(this.createDS(5));
        };
        this.b2.label(it_1, _function_3);
        final Procedure1<org.eclipse.app4mc.amalthea.model.Runnable> _function_4 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r1");
          final Procedure1<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Procedure1<Ticks> _function_6 = (Ticks it_4) -> {
              it_4.setDefault(this.createDVC(20l));
            };
            this.b2.ticks(it_3, _function_6);
            final Procedure1<LabelAccess> _function_7 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "l1"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_7);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.runnable(it_1, _function_4);
        final Procedure1<org.eclipse.app4mc.amalthea.model.Runnable> _function_5 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r2");
          final Procedure1<ActivityGraph> _function_6 = (ActivityGraph it_3) -> {
            final Procedure1<Ticks> _function_7 = (Ticks it_4) -> {
              it_4.setDefault(this.createDVC(30l));
            };
            this.b2.ticks(it_3, _function_7);
            final Procedure1<LabelAccess> _function_8 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "l1"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_8);
          };
          this.b2.activityGraph(it_2, _function_6);
        };
        this.b2.runnable(it_1, _function_5);
        final Procedure1<org.eclipse.app4mc.amalthea.model.Runnable> _function_6 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r3");
          final Procedure1<ActivityGraph> _function_7 = (ActivityGraph it_3) -> {
            final Procedure1<Ticks> _function_8 = (Ticks it_4) -> {
              it_4.setDefault(this.createDVC(10l));
            };
            this.b2.ticks(it_3, _function_8);
            final Procedure1<LabelAccess> _function_9 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "l1"));
              it_4.setAccess(LabelAccessEnum.READ);
            };
            this.b2.labelAccess(it_3, _function_9);
          };
          this.b2.activityGraph(it_2, _function_7);
        };
        this.b2.runnable(it_1, _function_6);
      };
      this.b1.softwareModel(it, _function_2);
    };
    final Amalthea model = this.b1.amalthea(_function);
    SWModel _swModel = model.getSwModel();
    ConstraintsModel _constraintsModel = model.getConstraintsModel();
    CPP cpp = new CPP(_swModel, _constraintsModel);
    NullProgressMonitor _nullProgressMonitor = new NullProgressMonitor();
    cpp.build(_nullProgressMonitor);
    int _size = cpp.getSwm().getProcessPrototypes().size();
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    int _size_1 = cpp.getSwm().getProcessPrototypes().get(0).getRunnableCalls().size();
    boolean _equals_1 = (_size_1 == 2);
    Assert.assertTrue(_equals_1);
    String _name = cpp.getSwm().getProcessPrototypes().get(1).getRunnableCalls().get(0).getRunnable().getName();
    boolean _equals_2 = Objects.equal(_name, "r1");
    Assert.assertTrue(_equals_2);
  }
  
  private DataSize createDS(final int i) {
    DataSize _xblockexpression = null;
    {
      final DataSize ret = AmaltheaFactory.eINSTANCE.createDataSize();
      ret.setValue(BigInteger.valueOf(i));
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }
  
  private DiscreteValueConstant createDVC(final long upper) {
    DiscreteValueConstant _xblockexpression = null;
    {
      final DiscreteValueConstant ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant();
      ret.setValue(upper);
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }
}
