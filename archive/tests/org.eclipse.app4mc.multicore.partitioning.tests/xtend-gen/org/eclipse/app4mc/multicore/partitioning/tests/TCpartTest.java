/**
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 */
package org.eclipse.app4mc.multicore.partitioning.tests;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
import org.eclipse.app4mc.amalthea.model.RunnableEntityGroup;
import org.eclipse.app4mc.amalthea.model.RunnableSequencingConstraint;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.multicore.partitioning.algorithms.TCbasedPart;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

@SuppressWarnings("all")
public class TCpartTest {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();
  
  private final String[] names = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
  
  private final int[] loads = { 1, 3, 4, 9, 1, 2, 5, 3, 2, 4 };
  
  private final int NBTASKS = 3;
  
  @BeforeClass
  public static void before() {
    BasicConfigurator.configure();
    Logger.getRootLogger().setLevel(Level.INFO);
  }
  
  @Test
  public void simpleModelTest() {
    final Procedure1<Amalthea> _function = (Amalthea it) -> {
      final Procedure1<SWModel> _function_1 = (SWModel it_1) -> {
        int _length = this.loads.length;
        ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length, true);
        for (final Integer i : _doubleDotLessThan) {
          EList<org.eclipse.app4mc.amalthea.model.Runnable> _runnables = it_1.getRunnables();
          org.eclipse.app4mc.amalthea.model.Runnable _createRunnable = this.createRunnable(this.names[(i).intValue()], this.loads[(i).intValue()]);
          _runnables.add(_createRunnable);
        }
      };
      this.b1.softwareModel(it, _function_1);
      final Procedure1<ConstraintsModel> _function_2 = (ConstraintsModel it_1) -> {
      };
      this.b1.constraintsModel(it, _function_2);
    };
    final Amalthea model = this.b1.amalthea(_function);
    ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, 9, true);
    for (final Integer i : _doubleDotLessThan) {
      EList<RunnableSequencingConstraint> _runnableSequencingConstraints = model.getConstraintsModel().getRunnableSequencingConstraints();
      RunnableSequencingConstraint _createRSC = this.createRSC(i, model);
      _runnableSequencingConstraints.add(_createRSC);
    }
    TCbasedPart tcp = new TCbasedPart(model);
    tcp.run(this.NBTASKS, 10, null);
    int _size = tcp.getAmaltheaModel().getSwModel().getProcessPrototypes().size();
    boolean _equals = (_size == this.NBTASKS);
    Assert.assertTrue(_equals);
  }
  
  public RunnableSequencingConstraint createRSC(final Integer i, final Amalthea model) {
    RunnableSequencingConstraint _xblockexpression = null;
    {
      final RunnableSequencingConstraint rsc = AmaltheaFactory.eINSTANCE.createRunnableSequencingConstraint();
      final RunnableEntityGroup rscg1 = AmaltheaFactory.eINSTANCE.createRunnableEntityGroup();
      final RunnableEntityGroup rscg2 = AmaltheaFactory.eINSTANCE.createRunnableEntityGroup();
      if (i != null) {
        switch (i) {
          case 0:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(0));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(4));
            rsc.setName("AE");
            break;
          case 1:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(1));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(5));
            rsc.setName("BF");
            break;
          case 2:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(1));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(6));
            rsc.setName("BG");
            break;
          case 3:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(1));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(9));
            rsc.setName("BJ");
            break;
          case 4:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(2));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(7));
            rsc.setName("CH");
            break;
          case 5:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(3));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(9));
            rsc.setName("DJ");
            break;
          case 6:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(4));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(7));
            rsc.setName("EH");
            break;
          case 7:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(7));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(8));
            rsc.setName("HI");
            break;
          case 8:
            rscg1.getRunnables().add(model.getSwModel().getRunnables().get(6));
            rscg2.getRunnables().add(model.getSwModel().getRunnables().get(8));
            rsc.setName("GI");
            break;
        }
      }
      rsc.getRunnableGroups().add(rscg1);
      rsc.getRunnableGroups().add(rscg2);
      _xblockexpression = rsc;
    }
    return _xblockexpression;
  }
  
  private org.eclipse.app4mc.amalthea.model.Runnable createRunnable(final String id, final int load) {
    org.eclipse.app4mc.amalthea.model.Runnable _xblockexpression = null;
    {
      final org.eclipse.app4mc.amalthea.model.Runnable r = AmaltheaFactory.eINSTANCE.createRunnable();
      r.setName(id);
      final ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
      final Ticks ticks = AmaltheaFactory.eINSTANCE.createTicks();
      ticks.setDefault(this.createDVC(load));
      EList<ActivityGraphItem> _items = ag.getItems();
      _items.add(ticks);
      r.setActivityGraph(ag);
      _xblockexpression = r;
    }
    return _xblockexpression;
  }
  
  private DiscreteValueConstant createDVC(final int upper) {
    DiscreteValueConstant _xblockexpression = null;
    {
      final DiscreteValueConstant ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant();
      ret.setValue(upper);
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }
}
