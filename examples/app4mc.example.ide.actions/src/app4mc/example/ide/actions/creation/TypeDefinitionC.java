/*********************************************************************************
 * Copyright (c) 2021-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.ide.actions.creation;

import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.Alias;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.BaseTypeDefinition;
import org.eclipse.app4mc.amalthea.model.DataSizeUnit;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.TypeDefinition;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.CreationService;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Type Definition | C Base Types (Demo)",
			"description = Creates C base type definitions with different aliases" },
		service = CreationService.class)

public class TypeDefinitionC implements CreationService {

	@Override
	public EStructuralFeature modelFeature() {
		return AmaltheaPackage.eINSTANCE.getSWModel_TypeDefinitions();
	}

	@PostConstruct
	public String create(SWModel swModel) {

		List<TypeDefinition> definitions = swModel.getTypeDefinitions();

		extendTypeDef(definitions, "boolean",	32,	"AUTOSAR:boolean",	"Simulink:boolean",	"ANSI C:unsigned long");

		extendTypeDef(definitions, "float32",	32,	"AUTOSAR:float32",	"Simulink:single",	"ANSI C:float");
		extendTypeDef(definitions, "float64",	64,	"AUTOSAR:float64",	"Simulink:double",	"ANSI C:double");

		extendTypeDef(definitions, "sint8",		 8,	"AUTOSAR:sint8",	"Simulink:int8",	"ANSI C:signed char");
		extendTypeDef(definitions, "sint16",	16,	"AUTOSAR:sint16",	"Simulink:int16",	"ANSI C:signed short");
		extendTypeDef(definitions, "sint32",	32,	"AUTOSAR:sint32",	"Simulink:int32",	"ANSI C:signed long");

		extendTypeDef(definitions, "uint8",		 8,	"AUTOSAR:uint8",	"Simulink:uint8",	"ANSI C:unsigned char");
		extendTypeDef(definitions, "uint16",	16,	"AUTOSAR:uint16",	"Simulink:uint16",	"ANSI C:unsigned short");
		extendTypeDef(definitions, "uint32",	32,	"AUTOSAR:uint32",	"Simulink:uint32",	"ANSI C:unsigned long");
		
		return null;	// do not show message dialog with status info
	}

	private void extendTypeDef(List<TypeDefinition> typeDefinitions, String typeName, int bits, String... aliasDefinitions) {

		// *** get or create base type definition

		BaseTypeDefinition typeDef = typeDefinitions.stream()
				.filter(BaseTypeDefinition.class::isInstance)
				.map(BaseTypeDefinition.class::cast)
				.filter(e -> typeName.equals(e.getName()))
				.findAny().orElse(null);

		if (typeDef == null) {
			// create new entry
			typeDef = AmaltheaFactory.eINSTANCE.createBaseTypeDefinition();
			typeDef.setName(typeName);
			typeDefinitions.add(typeDef);
		}

		// only set size if not already available
		if (typeDef.getSize() == null) {
			typeDef.setSize(FactoryUtil.createDataSize(bits, DataSizeUnit.BIT));			
		}

		// *** add aliases

		for (String aliasDef : aliasDefinitions) {
			String[] parts = aliasDef.split(":");
			String target = parts[0];
			String alias = parts[1];

			extendAliases(typeDef.getAliases(), target, alias);
		}

	}

	private void extendAliases(List<Alias> aliases, String target, String alias) {

		// *** get or create alias for the target system

		Alias aliasObject = aliases.stream()
				.filter(e -> target.equals(e.getTarget()))
				.findAny().orElse(null);

		if (aliasObject == null) {
			// create new alias
			aliasObject = AmaltheaFactory.eINSTANCE.createAlias();
			aliasObject.setTarget(target);
			aliases.add(aliasObject);
		}

		// set or overwrite
		aliasObject.setAlias(alias);
	}

}
