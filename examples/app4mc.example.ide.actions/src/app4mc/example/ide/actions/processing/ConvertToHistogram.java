/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.ide.actions.processing;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.math3.distribution.WeibullDistribution;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.DiscreteValueHistogram;
import org.eclipse.app4mc.amalthea.model.DiscreteValueHistogramEntry;
import org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.ProcessingService;
import org.eclipse.app4mc.amalthea.model.util.WeibullUtil;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Convert to histogram",
			"description = Converts a Weibull distribution to a histogram" })

public class ConvertToHistogram implements ProcessingService {

	@PostConstruct // provides dependency injection (additional @Inject is not required)
	public void convertToHistogram(
			DiscreteValueWeibullEstimatorsDistribution dist,
			IEventBroker broker // injected
		) {

		if (dist.getUpperBound() < dist.getLowerBound())
			// wrong bounds -> do nothing
			return;

		EObject container = dist.eContainer();
		EStructuralFeature feature = dist.eContainingFeature();

		if (container == null || feature == null || !feature.isChangeable() || feature.isMany())
			// only single valued changeable features are supported
			return;

		WeibullDistribution mathFunction = createMathFunction(dist);

		if (mathFunction == null)
			// no valid function
			return;

		// create new histogram and replace current Weibull distribution
		DiscreteValueHistogram histogram = createHistogram(dist, mathFunction);
		container.eSet(feature, histogram);

		// select the new element in the Amalthea model editor
		if (broker != null && histogram != null) {
			HashMap<String, Object> data = new HashMap<>();
			data.put("modelElements", List.of(histogram));
			broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
		}
	}

	private DiscreteValueHistogram createHistogram(DiscreteValueWeibullEstimatorsDistribution dist, WeibullDistribution mathFunction) {

		Long distLower = dist.getLowerBound();
		Long distUpper = dist.getUpperBound();
		final long range = distUpper - distLower;

		DiscreteValueHistogram histogram = AmaltheaFactory.eINSTANCE.createDiscreteValueHistogram();

		final long numBins = Math.min(range + 1, 20); // create 1 to 20 bins
		final double binSize = ((double) range) / numBins;

		for (int bin = 0; bin < numBins; bin++) {
			double border0 = bin * binSize;
			double border1 = (bin + 1) * binSize;

			long binLower = (bin == 0)
					? distLower // first bin
					: distLower + (long) (border0 + 1);
			long binUpper = (bin == numBins - 1)
					? distUpper // last bin
					: distLower + (long) border1;

			double y = mathFunction.probability(border0, border1);

			final DiscreteValueHistogramEntry entry = AmaltheaFactory.eINSTANCE.createDiscreteValueHistogramEntry();
			entry.setLowerBound(binLower);
			entry.setUpperBound(binUpper);
			entry.setOccurrences(Double.isFinite(y) ? Math.max((long) (y * numBins * 1000), 1) : numBins * 1000);
			histogram.getEntries().add(entry);
		}

		return histogram;
	}

	private WeibullDistribution createMathFunction(DiscreteValueWeibullEstimatorsDistribution dist) {
		final double lower = dist.getLowerBound().doubleValue();
		final double upper = dist.getUpperBound().doubleValue();
		final double average = dist.getAverage().doubleValue();
		final double remain = dist.getPRemainPromille();

		WeibullUtil.Parameters params = WeibullUtil.findParameters(lower, average, upper, remain);
		double shape = params.shape;
		double scale = params.scale;

		if (params.error != null)
			return null;

		return new WeibullDistribution(null, shape, scale);
	}

}
