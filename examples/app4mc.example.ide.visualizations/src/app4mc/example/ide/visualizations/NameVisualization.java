/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.ide.visualizations;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.osgi.service.component.annotations.Component;

@Component(property= {
		"name=Qualified Name (Demo)",
		"description=Shows the qualified name of the object (demo)"
})
public class NameVisualization implements Visualization {

	@PostConstruct
	public void createVisualization(INamed namedObject,Composite parent) {
		String name = namedObject.getQualifiedName();
		String delimiter = "======================= demo =======================";

		Label label = new Label(parent, SWT.None);
		label.setText("\n\n" + delimiter + "\n\n    " + name + "\n\n" + delimiter + "\n");
	}

}
