______________________________________________________________________________________________

WATERS 2016

International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems

https://waters2016.inria.fr/


WATERS 2016 community forum

https://www.ecrts.org/forum/viewforum.php?f=26

______________________________________________________________________________________________

FMTV 2016

Formal Methods for Timing Verification (FMTV) challenge

http://waters2016.inria.fr/challenge/


The 2016 FMTV Challenge (BOSCH)

http://ecrts.eit.uni-kl.de/forum/viewtopic.php?f=27&t=62

______________________________________________________________________________________________

The AMALTHEA model is migrated to APP4MC 1.0.0 with manual fixes.

Origin:
	ChallengeModel_updated.zip
	https://www.ecrts.org/forum/download/file.php?id=40
