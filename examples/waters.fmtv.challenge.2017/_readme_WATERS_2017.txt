______________________________________________________________________________________________

WATERS 2017

International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems

https://waters2017.inria.fr/


WATERS 2017 community forum

https://www.ecrts.org/forum/viewforum.php?f=31

______________________________________________________________________________________________

FMTV 2017

Formal Methods for Timing Verification (FMTV) challenge

https://waters2017.inria.fr/challenge/#Challenge17


The 2017 FMTV Challenge (BOSCH)

http://ecrts.eit.uni-kl.de/forum/viewtopic.php?f=32&t=85

______________________________________________________________________________________________

The AMALTHEA model is migrated to APP4MC 1.0.0 with manual fixes.

Origin:
	ChallengeModel_withCommImplementationTypev082.zip
	https://www.ecrts.org/forum/download/file.php?id=87


