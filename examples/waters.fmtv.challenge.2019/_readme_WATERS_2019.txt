______________________________________________________________________________________________

WATERS 2019

International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems

https://www.ecrts.org/archives/fileadmin/WebsitesArchiv/ecrts2019/waters


WATERS 2019 community forum

https://www.ecrts.org/forum/viewforum.php?f=42

______________________________________________________________________________________________

Description of the WATERS Industrial Challenge 2019

The 2019 FMTV Challenge (BOSCH)

https://www.ecrts.org/forum/viewtopic.php?f=43&t=124

______________________________________________________________________________________________

The AMALTHEA model is migrated to APP4MC 1.0.0 with manual fixes.

Origin:
	Forum
	https://www.ecrts.org/forum/viewtopic.php?f=43&t=126

	ChallengeModel_release_fix.zip
	https://www.ecrts.org/forum/download/file.php?id=117
