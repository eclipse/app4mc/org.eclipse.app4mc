
h2. Introduction

APP4MC comes with a predefined perspective available in the Eclipse menu under Window -> Open Perspective -> Other -> APP4MC. This perspective consists of the following elements:

* Project Explorer
* Editor
** Tree Editor showing the structure of the model content
** Standard Properties Tab is used to work on elements attributes
The following screenshot is showing this perspective and its contained elements.

!../pictures/user_guide_editor_structure.png!


h3. Steps to create a new AMALTHEA model

APP4MC provides a standard wizard to create a new AMALTHEA model from scratch.

p. *Step 1: Create a new general project*

bq..  The scope of an AMALTHEA model is defined by its enclosing container (project or folder).
      Therefore a project is required.

      !../pictures/user_guide_step1_create-project.png!

p. *Step 2: Create a new folder inside of the created project*

bq..  It is recommended to create a folder ( __although a project is also a possible container__ ).
  
      !../pictures/user_guide_step2_create-folder.png!

p. *Step 3: Create a new AMALTHEA model*

bq..  In the context menu (right mouse button) an entry for a new AMALTHEA model can be found.

      !../pictures/user_guide_step3_create-model.png!

      Another starting point is __File__ -> __New__ -> __Other__

      In the dialog you can select the parent folder and the file name.


h3. AMALTHEA Editor

The AMALTHEA Editor shows the entire model that contains sub models.
The next screenshot shows the "New Child" menu with all its possibilities.  

!(gray)../pictures/user_editor_with_central_model.png!

The AMALTHEA Editor has additional commands available in the editor toolbar.

!(gray)../pictures/user_guide_editor_commands.png!

h4. Show types of model elements

The Show types of elements button triggers the editor to show the direct type of the element in the tree editor using [element_type]. The following screenshot shows the toggle and the types marked with an underline.

!(gray)../pictures/user_guide_editor_showtypes.png!

h4. Search for model elements

The editor provides the possibility to search for model elements by using the available name attribute. For example this can be used to find all elements in the model with "ABS" at the beginning of their name. The result are shown in the Eclipse search result view.

!(gray)../pictures/user_guide_editor_search_input.png!

A double click on a result selects the element in the editor view.

!(gray)../pictures/user_guide_editor_search_result1.png!

An additional option is to toggle the search results to show them as a plain list instead of a tree grouped by type.

!(gray)../pictures/user_guide_editor_search_result2.png!


h3. Handling of multiple files (folder scope)

Amalthea model
* Amalthea files have the extension ".amxmi".
* Amalthea models support references to other model files in the same folder.


When the *first* Amalthea file in a folder is opened in the Amalthea editor
* all valid model files are loaded
* a common editing environment is established
* files and folder are decorated with markers
** loaded model files are decorated with a green dot
** model files that contain older versions are extended with the version
** the folder is marked as "is in use" and decorated with a construction barrier

!../pictures/user_guide_folder_scope.png!

When the *last* Amalthea editor of a folder is closed
* the common editing environment is released
* markers are removed

*Warning:*
Do not modify the contents of a folder while editors are open.
If you want to add or remove model files then first close all editors.


h3. AMALTHEA Examples

The AMALTHEA tool platform comes with several examples. This section will describe how a new project based on these examples can be created. 

p. *Step 1*

bq..  Click the "new" icon in the top left corner and select "Example..." or use the right mouse button.

      !../pictures/example-mapping/01-create-new-example.png!

p. *Step 2*

bq..  The "New Example" wizard will pop up and shows several examples.
      Select one examples and hit continue.

      !../pictures/example-mapping/02-select-democar-example.png!
 
p. *Step 3*

bq..  You will see a summary of the example projects that will be created.
      Click "Finish" to exit this dialog.
      
      !../pictures/example-mapping/03_democar-example-finish.png!

      You can now open the editor to inspect the models.
