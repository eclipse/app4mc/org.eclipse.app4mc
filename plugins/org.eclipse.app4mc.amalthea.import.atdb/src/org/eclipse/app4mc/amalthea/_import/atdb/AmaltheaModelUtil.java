/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

enum AmaltheaModelUtil {;

	static <T extends INamed> T getOrAddNew(final EObject amEObject, final EReference referenceFeature,
			final String name, final Class<T> clazz) {
		return getOrAddNew(amEObject, new TypedRoleInEObject<T>(referenceFeature, clazz, referenceFeature.getEReferenceType()), name);
	}

	static <T extends INamed> T getOrAddNew(final EObject amEObject, final TypedRoleInEObject<T> referenceRole, final String name) {
		@SuppressWarnings("unchecked")
		final List<T> values = referenceRole.role.isMany()
				? (List<T>) amEObject.eGet(referenceRole.role)
				: Arrays.asList(referenceRole.tClazz.cast(amEObject.eGet(referenceRole.role)));
		return values.stream().filter(Objects::nonNull).filter(p -> p.getName() != null && p.getName().equals(name)).findFirst().orElseGet(() -> {
			T ret;
			if (referenceRole.role.isContainment()) {
				ret = referenceRole.tClazz.cast(EcoreUtil.create(referenceRole.tEClazz));
				ret.setName(name);
			} else {
				if (IReferable.class.isAssignableFrom(referenceRole.tClazz)) {
					ret = AmaltheaIndex.getElements(amEObject, name, referenceRole.tClazz).stream().findFirst().orElse(null);
				} else {
					throw new IllegalArgumentException("For a reference the type must extend IReferable, but you gave me: " + referenceRole.tClazz);
				}
			}
			if (ret != null) {
				if (referenceRole.role.isMany()) {
					values.add(ret);
				} else {
					amEObject.eSet(referenceRole.role, ret);
				}
			}
			return ret;
		});
	}

	static <C extends EObject, T extends INamed> Entry<C, T> getOrAddNewWithContainer(final EObject eObject,
			final TypedRoleInEObject<C> containerRole, final TypedRoleInEObject<T> referenceRole, final String name) {
		@SuppressWarnings("unchecked")
		final Stream<C> allContainers = (containerRole.role.isMany()
				? ((List<C>) eObject.eGet(containerRole.role)).stream()
				: Stream.of(containerRole.tClazz.cast(eObject.eGet(containerRole.role)))
			).filter(containerRole.tClazz::isInstance);
		@SuppressWarnings("unchecked")
		final Stream<Entry<C, T>> allValues = allContainers.flatMap(containerEObject -> (referenceRole.role.isMany()
				? ((List<T>) containerEObject.eGet(referenceRole.role)).stream().map(ref -> new SimpleEntry<>(containerEObject, ref))
				: Stream.of(new SimpleEntry<>(containerEObject, referenceRole.tClazz.cast(containerEObject.eGet(referenceRole.role))))
			));

		return allValues.filter(p -> referenceRole.tClazz.isInstance(p.getValue()) && p.getValue().getName().equals(name))
				.findFirst().orElseGet(() -> {
			T ret = getOrCreateValue(eObject, referenceRole, name);
			if (ret != null) {
				// create and fill new container
				final C containerEObject = containerRole.tClazz.cast(EcoreUtil.create(containerRole.tEClazz));
				if (referenceRole.role.isMany()) {
					@SuppressWarnings("unchecked")
					final List<T> values = (List<T>)containerEObject.eGet(referenceRole.role);
					values.add(ret);
				} else {
					containerEObject.eSet(referenceRole.role, ret);
				}
				// add container to eObject
				if (containerRole.role.isMany()) {
					@SuppressWarnings("unchecked")
					final List<C> values = (List<C>)eObject.eGet(containerRole.role);
					values.add(containerEObject);
				} else {
					eObject.eSet(containerRole.role, containerEObject);
				}
				return new SimpleEntry<>(containerEObject, ret);
			}
			return null;
		});
	}
	
	private static <T extends INamed> T getOrCreateValue(final EObject eObject, final TypedRoleInEObject<T> referenceRole, final String name) {
		T ret;
		if (referenceRole.role.isContainment()) {
			ret = referenceRole.tClazz.cast(EcoreUtil.create(referenceRole.tEClazz));
			ret.setName(name);
		} else {
			if (IReferable.class.isAssignableFrom(referenceRole.tClazz)) {
				ret = AmaltheaIndex.getElements(eObject, name, referenceRole.tClazz).stream().findFirst().orElse(null);
			} else {
				throw new IllegalArgumentException("For a reference the type must extend IReferable, but you gave me: " + referenceRole.tClazz);
			}
		}
		return ret;
	}

	static Optional<Enumerator> getEnumLiteralByNameForFeature(final String enumLiteralName, final EStructuralFeature esf) {
		if (enumLiteralName == null || enumLiteralName.length() == 0 || (esf == null)) {
			return Optional.empty();
		}
		return Optional.of(esf).filter(EAttribute.class::isInstance).map(EAttribute.class::cast)
				.map(EAttribute::getEAttributeType).filter(Objects::nonNull)
				.map(EDataType::getInstanceClass).filter(Objects::nonNull).filter(Class::isEnum).flatMap(c ->
						Stream.of(c.getEnumConstants()).filter(Enumerator.class::isInstance).map(Enumerator.class::cast)
						.filter(e -> enumLiteralName.equals(e.getName())).findFirst());
	}
	
	static class TypedRoleInEObject<T extends EObject> {
		private final EReference role;
		private final Class<T> tClazz;
		private final EClass tEClazz;
		
		public TypedRoleInEObject(final EReference newRole, final Class<T> newTClazz, final EClass newTEClazz) {
			role = newRole;
			tClazz = newTClazz;
			tEClazz = newTEClazz;
		}
	}

}
