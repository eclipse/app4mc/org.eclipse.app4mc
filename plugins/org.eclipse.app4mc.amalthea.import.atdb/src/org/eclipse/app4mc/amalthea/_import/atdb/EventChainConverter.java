/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea._import.atdb.AmaltheaModelUtil.TypedRoleInEObject;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventChain;
import org.eclipse.app4mc.amalthea.model.EventChainContainer;
import org.eclipse.app4mc.amalthea.model.EventChainItemType;
import org.eclipse.app4mc.amalthea.model.EventChainReference;
import org.eclipse.app4mc.amalthea.model.SubEventChain;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.emf.ecore.EReference;

public class EventChainConverter extends AConverter {

	public EventChainConverter(final Amalthea model, final ATDBConnection con) {
		super(model, con, "event chains");
	}

	@Override
	protected void execute() throws SQLException {
		final ConstraintsModel constrModel = ModelUtil.getOrCreateConstraintsModel(model);

		final List<EventChain> eventChains = new ArrayList<>();
		con.getAllEventChains().forEach(ecName -> {
			final EventChain eventChain = AmaltheaModelUtil.getOrAddNew(constrModel, new TypedRoleInEObject<>(
					AmaltheaPackage.eINSTANCE.getConstraintsModel_EventChains(),
					EventChain.class,
					AmaltheaPackage.eINSTANCE.getEventChain()),
					ecName);
			eventChains.add(eventChain);
		});

		for(final EventChain eventChain:eventChains) {
			addEvent(eventChain, con.getEventChainStimulus(eventChain.getName()), AmaltheaPackage.eINSTANCE.getAbstractEventChain_Stimulus());
			addEvent(eventChain, con.getEventChainResponse(eventChain.getName()), AmaltheaPackage.eINSTANCE.getAbstractEventChain_Response());

			addItems(constrModel, eventChain, con.getEventChainItems(eventChain.getName()));

			final int minItemsComplete = con.getEventChainMinItemsCompleted(eventChain.getName());
			if (minItemsComplete > 0) {
				eventChain.setMinItemsCompleted(minItemsComplete);
				eventChain.setItemType(EventChainItemType.PARALLEL);
			} else {
				eventChain.setItemType(EventChainItemType.SEQUENCE);
			}
		}
	}
	
	private static void addEvent(final EventChain eventChain, final String eventName, final EReference roleInEventChain) {
		if (eventName != null && eventName.length() > 0) {
			AmaltheaModelUtil.getOrAddNew(eventChain, roleInEventChain, eventName, Event.class);
		}
	}
	
	private static void addItems(final ConstraintsModel constrModel, final EventChain eventChain, final List<String> items) {
		for(final String subECName:items) {
			if (subECName.length() == 0) continue;
			final boolean isReference = !AmaltheaIndex.getElements(constrModel, subECName, EventChain.class).isEmpty();
			if (isReference) {
				AmaltheaModelUtil.getOrAddNewWithContainer(eventChain,
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
								EventChainReference.class,
								AmaltheaPackage.eINSTANCE.getEventChainReference()),
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getEventChainReference_EventChain(),
								EventChain.class,
								AmaltheaPackage.eINSTANCE.getEventChain()),
						subECName);
			} else {
				AmaltheaModelUtil.getOrAddNewWithContainer(eventChain,
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
								EventChainContainer.class,
								AmaltheaPackage.eINSTANCE.getEventChainContainer()),
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getEventChainContainer_EventChain(),
								SubEventChain.class,
								AmaltheaPackage.eINSTANCE.getSubEventChain()),
						subECName);
			}
		}
	}

}
