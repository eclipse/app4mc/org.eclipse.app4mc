/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.eclipse.app4mc.amalthea._import.atdb.AmaltheaModelUtil.TypedRoleInEObject;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventModel;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class EventConverter extends AConverter {

	/**
	 * Same as {@link java.util.function.Supplier}, but may throw exception of type E.
	 * @param <T>
	 * @param <E>
	 */
	@FunctionalInterface
	public interface ThrowingSupplier<T, E extends Exception> {
	    T get() throws E;
	}

	private final Map<ThrowingSupplier<List<String>, SQLException>, EClass> eventSupplier2EClass;
	private static final Map<EClass, EReference> eClass2SourceReference = new LinkedHashMap<>();
	static {
		eClass2SourceReference.put(AmaltheaPackage.eINSTANCE.getRunnableEvent(), AmaltheaPackage.eINSTANCE.getRunnableEvent_Process());
		eClass2SourceReference.put(AmaltheaPackage.eINSTANCE.getProcessEvent(), AmaltheaPackage.eINSTANCE.getProcessEvent_ProcessingUnit());
		eClass2SourceReference.put(AmaltheaPackage.eINSTANCE.getLabelEvent(), AmaltheaPackage.eINSTANCE.getLabelEvent_Runnable());
	}

	public EventConverter(final Amalthea model, final ATDBConnection con) {
		super(model, con, "events");
		eventSupplier2EClass = new LinkedHashMap<>();
		eventSupplier2EClass.put(this.con::getAllRunnableEvents, AmaltheaPackage.eINSTANCE.getRunnableEvent());
		eventSupplier2EClass.put(this.con::getAllProcessEvents, AmaltheaPackage.eINSTANCE.getProcessEvent());
		eventSupplier2EClass.put(this.con::getAllLabelEvents, AmaltheaPackage.eINSTANCE.getLabelEvent());
		eventSupplier2EClass.put(this.con::getAllStimulusEvents, AmaltheaPackage.eINSTANCE.getStimulusEvent());
	}

	@Override
	protected void execute() throws SQLException {
		final EventModel evModel = ModelUtil.getOrCreateEventModel(model);

		final List<Event> events = new ArrayList<>();
		for(final Entry<ThrowingSupplier<List<String>, SQLException>, EClass> evSup2ECl: eventSupplier2EClass.entrySet()) {
			evSup2ECl.getKey().get().forEach(eventName ->
				events.add(AmaltheaModelUtil.getOrAddNew(evModel, new TypedRoleInEObject<>(
						AmaltheaPackage.eINSTANCE.getEventModel_Events(), Event.class, evSup2ECl.getValue()), eventName)));
		}

		for(final Event event: events) {
			updateEventType(event);
			updateEntity(event);
			updateSourceEntity(event);
		}
	}

	private void updateEventType(final Event event) throws SQLException {
		final EStructuralFeature eventTypeESF = event.eClass().getEStructuralFeature("eventType");
		final String eventType = con.getEventTypeForEvent(event.getName());
		final Optional<Enumerator> eventTypeEnumLiteral = AmaltheaModelUtil.getEnumLiteralByNameForFeature(eventType, eventTypeESF);
		eventTypeEnumLiteral.ifPresent(eLiteral -> event.eSet(eventTypeESF, eLiteral));
	}

	private void updateEntity(final Event event) throws SQLException {
		final EReference eventEntityESF = (EReference)event.eClass().getEStructuralFeature("entity");
		final String entityName = con.getEntityForEvent(event.getName());
		AmaltheaModelUtil.getOrAddNew(event, eventEntityESF, entityName, IReferable.class);
	}

	private void updateSourceEntity(final Event event) throws SQLException {
		final String sourceEntityName = con.getSourceEntityForEvent(event.getName());
		if (sourceEntityName.length() == 0) return;

		if (eClass2SourceReference.containsKey(event.eClass())) {
			AmaltheaModelUtil.getOrAddNew(event, eClass2SourceReference.get(event.eClass()), sourceEntityName, IReferable.class);
		}
	}

}
