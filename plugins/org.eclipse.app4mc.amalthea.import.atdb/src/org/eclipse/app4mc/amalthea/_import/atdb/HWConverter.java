/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea._import.atdb.AmaltheaModelUtil.TypedRoleInEObject;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.FrequencyUnit;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.StructureType;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;

public class HWConverter extends AConverter {

	public HWConverter(final Amalthea model, final ATDBConnection con) {
		super(model, con, "hardware model");
	}

	@Override
	protected void execute() throws SQLException {
		final HWModel hwModel = ModelUtil.getOrCreateHwModel(model);
		final HwStructure system = AmaltheaModelUtil.getOrAddNew(hwModel,
				AmaltheaPackage.eINSTANCE.getHWModel_Structures(), "System", HwStructure.class);
		system.setStructureType(StructureType.SYSTEM);

		final List<HwStructure> ecus = new ArrayList<>();
		con.getAllECUs().forEach(ecuName -> {
			final HwStructure ecu = AmaltheaModelUtil.getOrAddNew(system,
					AmaltheaPackage.eINSTANCE.getHwStructure_Structures(), ecuName, HwStructure.class);
			ecu.setStructureType(StructureType.ECU);
			ecus.add(ecu);
		});

		for(final HwStructure ecu:ecus) {
			final List<HwStructure> processors = new ArrayList<>();
			con.getProcessorsOfECU(ecu.getName()).forEach(processorName -> {
				final HwStructure processor = AmaltheaModelUtil.getOrAddNew(ecu,
						AmaltheaPackage.eINSTANCE.getHwStructure_Structures(), processorName, HwStructure.class);
				processor.setStructureType(StructureType.MICROCONTROLLER);
				processors.add(processor);
			});

			for(final HwStructure processor:processors) {
				final List<ProcessingUnit> cores = new ArrayList<>();
				con.getCoresOfProcessor(processor.getName()).forEach(coreName -> {
					final ProcessingUnit core = AmaltheaModelUtil.getOrAddNew(processor,
							new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getHwStructure_Modules(),
									ProcessingUnit.class, AmaltheaPackage.eINSTANCE.getProcessingUnit()),
							coreName);
					cores.add(core);
				});

				for(final ProcessingUnit core:cores) {
					final long frequencyInHz = con.getCoreFrequencyInHz(core.getName());
					if (frequencyInHz > 0) {
						try {
							final FrequencyDomain fd = AmaltheaModelUtil.getOrAddNew(hwModel,
									new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getHWModel_Domains(),
											FrequencyDomain.class, AmaltheaPackage.eINSTANCE.getFrequencyDomain()),
									core.getName() + "_FD");
							fd.setDefaultValue(FactoryUtil.createFrequency(frequencyInHz, FrequencyUnit.HZ));
							core.setFrequencyDomain(fd);
						} catch (NumberFormatException e) {
							// fail silently
						}
					}
				}
			}
		}
	}

}
