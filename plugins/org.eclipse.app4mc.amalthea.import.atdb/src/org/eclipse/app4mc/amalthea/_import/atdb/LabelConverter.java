/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.EntityProperty;

public class LabelConverter extends AConverter {

	public LabelConverter(final Amalthea model, final ATDBConnection con) {
		super(model, con, "labels");
	}

	@Override
	protected void execute() throws SQLException {
		final SWModel swModel = ModelUtil.getOrCreateSwModel(model);

		final List<Label> labels = new ArrayList<>();
		con.getAllLabels().forEach(labelName -> {
			final Label label = AmaltheaModelUtil.getOrAddNew(swModel,
					AmaltheaPackage.eINSTANCE.getSWModel_Labels(), labelName, Label.class);
			labels.add(label);
		});
		for(final Label label:labels) {
			final String initialValue = con.getLabelInitialValue(label.getName());
			if (initialValue.length() > 0) {
				CustomPropertyUtil.customPut(label, EntityProperty.INITIAL_VALUE.camelName, initialValue);
			}
		}
	}

}
