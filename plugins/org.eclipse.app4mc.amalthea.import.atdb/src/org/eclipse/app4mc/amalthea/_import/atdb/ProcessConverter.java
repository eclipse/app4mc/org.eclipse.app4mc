/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea._import.atdb.AmaltheaModelUtil.TypedRoleInEObject;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;

public class ProcessConverter extends AConverter {

	public ProcessConverter(final Amalthea model, final ATDBConnection con) {
		super(model, con, "processes");
	}

	@Override
	protected void execute() throws SQLException {
		final SWModel swModel = ModelUtil.getOrCreateSwModel(model);

		final List<Process> processes = new ArrayList<>();
		con.getAllTasks().forEach(taskName -> processes.add(AmaltheaModelUtil.getOrAddNew(swModel, AmaltheaPackage.eINSTANCE.getSWModel_Tasks(),
				taskName, Task.class)));
		con.getAllISRs().forEach(isrName -> processes.add(AmaltheaModelUtil.getOrAddNew(swModel, AmaltheaPackage.eINSTANCE.getSWModel_Isrs(),
				isrName, ISR.class)));

		for(final Process proc:processes) {
			// fill activity graph
			final ActivityGraph ag = ModelUtil.getOrCreateActivityGraph(proc);
			con.getRunnablesOfProcess(proc.getName()).forEach(runnableName ->
				AmaltheaModelUtil.getOrAddNewWithContainer(ag,
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getIActivityGraphItemContainer_Items(),
								RunnableCall.class,
								AmaltheaPackage.eINSTANCE.getRunnableCall()),
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getRunnableCall_Runnable(),
								Runnable.class,
								AmaltheaPackage.eINSTANCE.getRunnable()),
						runnableName).getKey());

			// connect stimuli
			con.getStimuliOfProcess(proc.getName()).forEach(stimulusName ->
				AmaltheaModelUtil.getOrAddNew(proc, AmaltheaPackage.eINSTANCE.getProcess_Stimuli(), stimulusName, Stimulus.class));
		}
	}

}
