/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea._import.atdb.AmaltheaModelUtil.TypedRoleInEObject;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ITimeDeviation;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.SingleStimulus;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.MetricAggregation;

public class StimulusConverter extends AConverter {

	private static final String A2A = "activateToActivate_";

	public StimulusConverter(final Amalthea model, final ATDBConnection con) {
		super(model, con, "stimuli");
	}

	@Override
	protected void execute() throws SQLException {
		final StimuliModel stimuliModel = ModelUtil.getOrCreateStimuliModel(model);
		final TimeUnit timeBase = TimeUnit.getByName(con.getTimeBase().toLowerCase());

		final List<PeriodicStimulus> stimuli = new ArrayList<>();
		for(final String stimulusName:con.getAllStimuli()) {
			if (con.getAllInstancesForEntity(stimulusName).size() == 1) {
				// only one instance => single stimulus
				try {
					final long occurrence = Long.parseLong(con.getValueForMetricAndEntity(stimulusName, A2A + MetricAggregation.Min));
					final SingleStimulus singStimme = AmaltheaModelUtil.getOrAddNew(stimuliModel,
							new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getStimuliModel_Stimuli(),
									SingleStimulus.class, AmaltheaPackage.eINSTANCE.getSingleStimulus()),
							stimulusName);
					final Time occurrenceTime = FactoryUtil.createTime(occurrence, timeBase).adjustUnit();
					singStimme.setOccurrence(occurrenceTime);
				} catch (NumberFormatException e) {
					// fail silently
				}
			} else {
				// periodic otherwise
				final PeriodicStimulus stimulus = AmaltheaModelUtil.getOrAddNew(stimuliModel,
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getStimuliModel_Stimuli(),
								PeriodicStimulus.class, AmaltheaPackage.eINSTANCE.getPeriodicStimulus()),
						stimulusName);
				stimuli.add(stimulus);
			}
		}
		
		setPeriodicStimuliParameters(stimuli, timeBase);
	}
	
	private void setPeriodicStimuliParameters(final List<PeriodicStimulus> stimuli, final TimeUnit timeBase) throws SQLException {
		for(final PeriodicStimulus stimulus:stimuli) {
			double periodAvg = -1;
			double periodStDev = -1;
			try {
				periodAvg = Double.parseDouble(con.getValueForMetricAndEntity(stimulus.getName(), A2A + MetricAggregation.Avg));
				periodStDev = Double.parseDouble(con.getValueForMetricAndEntity(stimulus.getName(), A2A + MetricAggregation.StDev));
			} catch (NumberFormatException e) {
				// fail silently
			}
			try {
				final long periodMin = Long.parseLong(con.getValueForMetricAndEntity(stimulus.getName(), A2A + MetricAggregation.Min));
				final long periodMax = Long.parseLong(con.getValueForMetricAndEntity(stimulus.getName(), A2A + MetricAggregation.Max));
				if (periodMin == periodMax) {
					final Time recurrence = FactoryUtil.createTime(periodMin, timeBase).adjustUnit();
					stimulus.setRecurrence(recurrence);
				} else {
					if (periodAvg <= -1) {
						periodAvg = (periodMin + periodMax) / 2d;
					}
					final Time avgRecurrence = FactoryUtil.createTime(periodAvg, timeBase).adjustUnit();
					stimulus.setRecurrence(avgRecurrence);
					final Time minJitter = FactoryUtil.createTime(periodMin - periodAvg, timeBase).adjustUnit();
					final Time maxJitter = FactoryUtil.createTime(periodMax - periodAvg, timeBase).adjustUnit();
					ITimeDeviation jitterDev;
					if (periodStDev >= 0) {
						final Time stDevJitter = FactoryUtil.createTime(periodStDev, timeBase).adjustUnit();
						final Time avgZeroJitter = FactoryUtil.createTime(0, timeBase).adjustUnit();
						jitterDev = FactoryUtil.createTimeGaussDistribution(avgZeroJitter, stDevJitter, minJitter, maxJitter);
					} else {
						jitterDev = FactoryUtil.createTimeBoundaries(minJitter, maxJitter);
					}
					stimulus.setJitter(jitterDev);
				}
			} catch (NumberFormatException e) {
				// fail silently
			}
		}
	}

}
