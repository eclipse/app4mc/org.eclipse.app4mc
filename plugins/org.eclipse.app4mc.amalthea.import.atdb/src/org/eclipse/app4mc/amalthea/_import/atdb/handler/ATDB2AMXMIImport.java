/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb.handler;

import java.util.List;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea._import.atdb.wizard.ImportWizard;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;

public class ATDB2AMXMIImport {

	@Execute
	public void execute(final @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection, final IWorkbenchWindow runnableContext) {
		final List<?> selectionList = selection.toList();
		final String atdbSource = selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
				.filter(f -> f.getFileExtension().equalsIgnoreCase("atdb")).map(f -> f.getLocation().toString())
				.findFirst().orElseThrow(() -> new IllegalArgumentException("Selection does not contain an *.atdb file!"));
		final IFile amxmiTarget = selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
				.filter(f -> f.getFileExtension().equalsIgnoreCase("amxmi")).findFirst().orElseGet(() -> {
					// use the file name of the atdb
					final int from = Math.max(atdbSource.lastIndexOf('/'), atdbSource.lastIndexOf('\\'));
					final int to = atdbSource.lastIndexOf(".atdb"); //$NON-NLS-1$
					final String name = atdbSource.substring(from + 1, to);
					return selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
							.filter(f -> f.getFileExtension().equalsIgnoreCase("atdb")).map(IResource::getParent)
							.map(c -> c.getProject().getFile(name + ".amxmi"))
							.findFirst().orElseThrow(() -> new IllegalArgumentException("Selected *.atdb file is not part of a Eclipse workspace!"));
				});
		if ((atdbSource.length() == 0) || (runnableContext == null)) {
			return;
		}

		ImportWizard.importAndOpenInIDE(atdbSource, amxmiTarget, true, true, true, runnableContext);
	}

}
