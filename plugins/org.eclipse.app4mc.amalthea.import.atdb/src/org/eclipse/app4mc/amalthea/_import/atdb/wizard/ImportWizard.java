/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb.wizard;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.app4mc.amalthea._import.atdb.ImportTransformation;
import org.eclipse.app4mc.amalthea._import.atdb.handler.ATDB2AMXMIImport;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.ide.IDE;
import org.osgi.framework.FrameworkUtil;

public class ImportWizard extends Wizard implements IImportWizard {

	private ImportPage mainPage;
	private IStructuredSelection selection;

	@Override
	public void addPages() {
		super.addPages();
		mainPage = new ImportPage(selection);
		addPage(mainPage);
	}

	@Override
	public void init(final IWorkbench workbench, final IStructuredSelection selection) {
		this.selection = selection;

		final List<?> selectedResources = IDE.computeSelectedResources(selection);
		if (!selectedResources.isEmpty()) {
			this.selection = new StructuredSelection(selectedResources);
		}

		setWindowTitle(Messages.ImportWizard_title);
		setNeedsProgressMonitor(true);
	}

	@Override
	public boolean performFinish() {
		final String atdbSource = mainPage.getATDBSource();
		final IFile amxmiTarget = mainPage.getAMXMITarget();
		final boolean extractLabelsAndAccesses = mainPage.isExtractLabelsAndAccesses();
		final boolean extractRunnableRuntimes = mainPage.isExtractRunnableRuntimes();
		final boolean overwriteExisting = mainPage.isOverwriteExisting();
		return importAndOpenInIDE(atdbSource, amxmiTarget, extractLabelsAndAccesses, extractRunnableRuntimes, overwriteExisting, getContainer());
	}

	public static boolean importAndOpenInIDE(final String atdbSource, final IFile amxmiTarget,
			final boolean extractLabelsAndAccesses,	final boolean extractRunnableRuntimes, final boolean overwriteExisting,
			final IRunnableContext runnableContext) {

		final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {

			@Override
			protected void execute(final IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException, CoreException {
				final IRunnableWithProgress transformer = new ImportTransformation(atdbSource, amxmiTarget.getLocation().toString(),
						extractLabelsAndAccesses, extractRunnableRuntimes, overwriteExisting);
				transformer.run(progressMonitor);
				// refresh workspace
				amxmiTarget.refreshLocal(1, progressMonitor);
				progressMonitor.done();
			}
		};

		try {
			runnableContext.run(true, true, operation);
		} catch (InvocationTargetException e1) {
			Platform.getLog(ATDB2AMXMIImport.class).log(new Status(IStatus.ERROR,
					FrameworkUtil.getBundle(ATDB2AMXMIImport.class).getSymbolicName(), e1.getLocalizedMessage(), e1));
			return false;
		} catch (InterruptedException e2) {
			Thread.currentThread().interrupt();
			return false;
		}

		// open new amxmi file in editor
		final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			IDE.openEditor(page, amxmiTarget);
		}
		catch (final PartInitException e) {
			Platform.getLog(ATDB2AMXMIImport.class).log(new Status(IStatus.ERROR,
					FrameworkUtil.getBundle(ATDB2AMXMIImport.class).getSymbolicName(), e.getLocalizedMessage(), e));
			return false;
		}

		return true;
	}

}
