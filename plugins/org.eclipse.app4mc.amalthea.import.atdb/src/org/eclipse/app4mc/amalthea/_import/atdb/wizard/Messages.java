/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb.wizard;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.app4mc.amalthea._import.atdb.wizard.messages"; //$NON-NLS-1$
	public static String ImportPage_title;
	public static String ImportPage_message;
	public static String ImportPage_selectFile;
	public static String ImportPage_fromATDB;
	public static String ImportPage_optionExtractLabelsAndAccesses;
	public static String ImportPage_optionExtractRunableRuntimes;
	public static String ImportWizard_title;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}

}
