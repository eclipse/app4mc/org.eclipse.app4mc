/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.hw.container;

import java.util.Collection;

import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;

public class HwConnectionContainerIP extends TransientItemProvider {

	public HwConnectionContainerIP(final AdapterFactory adapterFactory, final HwStructure parent) {
		super(adapterFactory);
		parent.eAdapters().add(this);
	}

	@Override
	public EStructuralFeature myFeature() {
		return myPackage().getHwStructure_Connections();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getText(java.lang.Object)
	 */
	@Override
	public String getText(final Object object) {
		final StringBuilder buffer = new StringBuilder();
		buffer.append("Connections ("); //$NON-NLS-1$
		buffer.append(((HwStructure) getTarget()).getConnections().size());
		buffer.append(")"); //$NON-NLS-1$

		return buffer.toString();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#collectNewChildDescriptors(java.util.Collection,
	 *      java.lang.Object)
	 */
	@Override
	protected void collectNewChildDescriptors(final Collection<Object> newChildDescriptors, final Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
		newChildDescriptors.add(createChildParameter(myFeature(), myFactory().createHwConnection()));
	}

}