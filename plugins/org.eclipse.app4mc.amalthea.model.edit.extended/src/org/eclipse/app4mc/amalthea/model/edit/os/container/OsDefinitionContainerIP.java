/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.os.container;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ViewerNotification;

public class OsDefinitionContainerIP extends TransientItemProvider {

	public OsDefinitionContainerIP(final AdapterFactory adapterFactory, final OSModel parent) {
		super(adapterFactory);
		parent.eAdapters().add(this);
	}


	// ********** Standard TransientItemProvider section **********


	@Override
	public List<EStructuralFeature> myFeatures() {
		return List.of(
				myPackage().getOSModel_SchedulerDefinitions(),
				myPackage().getOSModel_SchedulingParameterDefinitions(),
				myPackage().getOSModel_OsOverheads());
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getText(java.lang.Object)
	 */
	@Override
	public String getText(final Object object) {
		final StringBuilder buffer = new StringBuilder();
		buffer.append("Definitions ("); //$NON-NLS-1$
		OSModel osModel = (OSModel) getTarget();
		buffer.append(osModel.getSchedulerDefinitions().size()
				+ osModel.getSchedulingParameterDefinitions().size()
				+ osModel.getOsOverheads().size());
		buffer.append(")"); //$NON-NLS-1$

		return buffer.toString();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#collectNewChildDescriptors(java.util.Collection,
	 *      java.lang.Object)
	 */
	@Override
	protected void collectNewChildDescriptors(final Collection<Object> newChildDescriptors, final Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
		newChildDescriptors.add(createChildParameter(myPackage().getOSModel_SchedulerDefinitions(), myFactory().createSchedulerDefinition()));
		newChildDescriptors.add(createChildParameter(myPackage().getOSModel_SchedulingParameterDefinitions(), myFactory().createSchedulingParameterDefinition()));
		newChildDescriptors.add(createChildParameter(myPackage().getOSModel_OsOverheads(), myFactory().createOsOverhead()));
	}


	// ********** Standard section for contained ItemProviderContainers **********


	private static final Set<EStructuralFeature> SPECIAL_FEATURES = Set.of(
				AmaltheaPackage.eINSTANCE.getOSModel_SchedulerDefinitions(), // OS_MODEL__SCHEDULER_DEFINITIONS
				AmaltheaPackage.eINSTANCE.getOSModel_SchedulingParameterDefinitions(), // OS_MODEL__SCHEDULING_PARAMETER_DEFINITIONS
				AmaltheaPackage.eINSTANCE.getOSModel_OsOverheads() // OS_MODEL__OS_OVERHEADS
			);

	private static final Set<Integer> SPECIAL_FEATURE_IDS =
			SPECIAL_FEATURES.stream()
				.mapToInt(EStructuralFeature::getFeatureID).boxed()
				.collect(Collectors.toCollection(HashSet::new));

	// Container item providers
	protected SchedulerDefinitionContainerIP schedulerDefinitionCIP;
	protected SchedulingParameterDefinitionContainerIP schedulingParameterDefinitionCIP;
	protected OsOverheadContainerIP osOverheadCIP;

	public SchedulerDefinitionContainerIP getSchedulerDefinitionsContainerIP(final OSModel osModel) {
		if (this.schedulerDefinitionCIP == null) {
			this.schedulerDefinitionCIP = new SchedulerDefinitionContainerIP(this.adapterFactory, osModel);
		}
		return this.schedulerDefinitionCIP;
	}

	public SchedulingParameterDefinitionContainerIP getSchedulingParameterDefinitionsContainerIP(final OSModel osModel) {
		if (this.schedulingParameterDefinitionCIP == null) {
			this.schedulingParameterDefinitionCIP = new SchedulingParameterDefinitionContainerIP(this.adapterFactory, osModel);
		}
		return this.schedulingParameterDefinitionCIP;
	}

	public OsOverheadContainerIP getOsOverheadsContainerIP(final OSModel osModel) {
		if (this.osOverheadCIP == null) {
			this.osOverheadCIP = new OsOverheadContainerIP(this.adapterFactory, osModel);
		}
		return this.osOverheadCIP;
	}

	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {

		for (EStructuralFeature feature : super.getChildrenFeatures(object)) {
			if (isValidValue(object, child, feature)) {
				return feature;
			}
		}
		return null;
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(final Object object) {
		// Do NOT modify cached collection!
		Set<? extends EStructuralFeature> result = new HashSet<>(super.getChildrenFeatures(object));

		// reduce result
		result.removeAll(SPECIAL_FEATURES);

		return result;
	}

	@Override
	public Collection<?> getChildren(final Object object) {
		final List<Object> children = new ArrayList<>(super.getChildren(object));
		final OSModel osModel = (OSModel) getTarget(); // class is transient item providers

		// only display virtual folders if not empty (on top of the list)
		if (!osModel.getOsOverheads().isEmpty())
			children.add(0, getOsOverheadsContainerIP(osModel));
		if (!osModel.getSchedulingParameterDefinitions().isEmpty())
			children.add(0, getSchedulingParameterDefinitionsContainerIP(osModel));
		if (!osModel.getSchedulerDefinitions().isEmpty())
			children.add(0, getSchedulerDefinitionsContainerIP(osModel));
		return children;
	}

	@Override
	protected Command createAddCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection, final int index) {
		return createWrappedCommand(super.createAddCommand(domain, owner, feature, collection, index), owner, feature);
	}

	@Override
	protected Command createRemoveCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection) {
		return createWrappedCommand(super.createRemoveCommand(domain, owner, feature, collection), owner, feature);
	}

	protected Command createWrappedCommand(final Command command, final EObject owner, final EStructuralFeature feature) {
		int featureID = feature.getFeatureID();

		if (!SPECIAL_FEATURE_IDS.contains(featureID)) {
			return command;
		}

		return new CommandWrapper(command) {
			@Override
			public Collection<?> getAffectedObjects() {
				Collection<?> affected = super.getAffectedObjects();
				if (affected.contains(owner)) {
					if (featureID == AmaltheaPackage.OS_MODEL__SCHEDULER_DEFINITIONS) {
						affected = Collections.singleton(getSchedulerDefinitionsContainerIP((OSModel) owner));
					} else if (featureID == AmaltheaPackage.OS_MODEL__SCHEDULING_PARAMETER_DEFINITIONS) {
						affected = Collections.singleton(getSchedulingParameterDefinitionsContainerIP((OSModel) owner));
					} else if (featureID == AmaltheaPackage.OS_MODEL__OS_OVERHEADS) {
						affected = Collections.singleton(getOsOverheadsContainerIP((OSModel) owner));
					}
				}
				return affected;
			}
		};
	}

	@Override
	public void dispose() {
		if (this.schedulerDefinitionCIP != null) {
			this.schedulerDefinitionCIP.dispose();
		}
		if (this.schedulingParameterDefinitionCIP != null) {
			this.schedulingParameterDefinitionCIP.dispose();
		}
		if (this.osOverheadCIP != null) {
			this.osOverheadCIP.dispose();
		}

		super.dispose();
	}

	@Override
	public void notifyChanged(final Notification notification) {
		updateChildren(notification);

		int featureID = notification.getFeatureID(HWModel.class);

		if (SPECIAL_FEATURE_IDS.contains(featureID)) {
			// update virtual folder labels
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, true));
		} else {
			// default
			super.notifyChanged(notification);
		}
	}

}
