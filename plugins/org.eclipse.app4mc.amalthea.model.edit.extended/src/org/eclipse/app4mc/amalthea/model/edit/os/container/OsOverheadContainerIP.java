/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.os.container;

import java.util.Collection;

import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;

public class OsOverheadContainerIP extends TransientItemProvider {

	public OsOverheadContainerIP(final AdapterFactory adapterFactory, final OSModel parent) {
		super(adapterFactory);
		parent.eAdapters().add(this);
	}

	@Override
	public EStructuralFeature myFeature() {
		return myPackage().getOSModel_OsOverheads();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getText(java.lang.Object)
	 */
	@Override
	public String getText(final Object object) {
		final StringBuilder buffer = new StringBuilder();
		buffer.append("OS Overheads ("); //$NON-NLS-1$
		buffer.append(((OSModel) getTarget()).getOsOverheads().size());
		buffer.append(")"); //$NON-NLS-1$

		return buffer.toString();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#collectNewChildDescriptors(java.util.Collection,
	 *      java.lang.Object)
	 */
	@Override
	protected void collectNewChildDescriptors(final Collection<Object> newChildDescriptors, final Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
		newChildDescriptors.add(createChildParameter(myPackage().getOSModel_OsOverheads(), myFactory().createOsOverhead()));
	}

}
