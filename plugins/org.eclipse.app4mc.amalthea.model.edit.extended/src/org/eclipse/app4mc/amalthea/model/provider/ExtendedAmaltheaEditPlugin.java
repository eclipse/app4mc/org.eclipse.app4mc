/**
 * *******************************************************************************
 *  Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.provider;

import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.ResourceLocator;

public final class ExtendedAmaltheaEditPlugin extends EMFPlugin {

	public static final ExtendedAmaltheaEditPlugin INSTANCE = new ExtendedAmaltheaEditPlugin();

	private static Implementation plugin;

	public ExtendedAmaltheaEditPlugin() {
		super
		  (new ResourceLocator [] {
		   });
	}

	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}

	public static Implementation getPlugin() {
		return plugin;
	}

	public static class Implementation extends EclipsePlugin {

		public Implementation() {
			super();

			// Remember the static instance.
			//
			plugin = this;
		}
	}

}
