/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SchedulerDefinitionItemProvider extends OsDefinitionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchedulerDefinitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDescriptionPropertyDescriptor(object);
			addAlgorithmParametersPropertyDescriptor(object);
			addProcessParametersPropertyDescriptor(object);
			addRequiresParentSchedulerPropertyDescriptor(object);
			addPassesParametersUpwardsPropertyDescriptor(object);
			addHasExactlyOneChildPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_IDescription_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_IDescription_description_feature", "_UI_IDescription_type"),
				 AmaltheaPackage.eINSTANCE.getIDescription_Description(),
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Process Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessParametersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SchedulerDefinition_processParameters_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SchedulerDefinition_processParameters_feature", "_UI_SchedulerDefinition_type"),
				 AmaltheaPackage.eINSTANCE.getSchedulerDefinition_ProcessParameters(),
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_ParametersPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Algorithm Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAlgorithmParametersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SchedulerDefinition_algorithmParameters_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SchedulerDefinition_algorithmParameters_feature", "_UI_SchedulerDefinition_type"),
				 AmaltheaPackage.eINSTANCE.getSchedulerDefinition_AlgorithmParameters(),
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_ParametersPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Requires Parent Scheduler feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequiresParentSchedulerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SchedulerDefinition_requiresParentScheduler_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SchedulerDefinition_requiresParentScheduler_feature", "_UI_SchedulerDefinition_type"),
				 AmaltheaPackage.eINSTANCE.getSchedulerDefinition_RequiresParentScheduler(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 getString("_UI_OptionsPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Passes Parameters Upwards feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPassesParametersUpwardsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SchedulerDefinition_passesParametersUpwards_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SchedulerDefinition_passesParametersUpwards_feature", "_UI_SchedulerDefinition_type"),
				 AmaltheaPackage.eINSTANCE.getSchedulerDefinition_PassesParametersUpwards(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 getString("_UI_OptionsPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Has Exactly One Child feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasExactlyOneChildPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SchedulerDefinition_hasExactlyOneChild_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SchedulerDefinition_hasExactlyOneChild_feature", "_UI_SchedulerDefinition_type"),
				 AmaltheaPackage.eINSTANCE.getSchedulerDefinition_HasExactlyOneChild(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 getString("_UI_OptionsPropertyCategory"),
				 null));
	}

	/**
	 * This returns SchedulerDefinition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SchedulerDefinition"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SchedulerDefinition)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_SchedulerDefinition_type") :
			getString("_UI_SchedulerDefinition_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SchedulerDefinition.class)) {
			case AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION:
			case AmaltheaPackage.SCHEDULER_DEFINITION__REQUIRES_PARENT_SCHEDULER:
			case AmaltheaPackage.SCHEDULER_DEFINITION__PASSES_PARAMETERS_UPWARDS:
			case AmaltheaPackage.SCHEDULER_DEFINITION__HAS_EXACTLY_ONE_CHILD:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
