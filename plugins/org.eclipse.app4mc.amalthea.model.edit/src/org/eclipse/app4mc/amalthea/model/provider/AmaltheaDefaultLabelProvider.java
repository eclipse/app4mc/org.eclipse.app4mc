/*
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.provider;

import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Simple label provider for Amalthea objects
 * 
 * (makes use of generated EMF label providers to get text and image)
 * 
 */
public class AmaltheaDefaultLabelProvider extends LabelProvider {
	AmaltheaItemProviderAdapterFactory providerFactory;

	public AmaltheaDefaultLabelProvider() {
		// use Amalthea default adapter factory
		providerFactory = new AmaltheaItemProviderAdapterFactory();
	}

	private IItemLabelProvider getProviderFor(Object object) {
		return (IItemLabelProvider) providerFactory.adapt(object, IItemLabelProvider.class);
	}

	@Override
	public String getText(Object object) {
		IItemLabelProvider provider = getProviderFor(object);
		if (provider == null)
			return "";

		return provider.getText(object);
	}

	@Override
	public Image getImage(Object object) {
		IItemLabelProvider provider = getProviderFor(object);
		if (provider == null)
			return null;

		return ExtendedImageRegistry.INSTANCE.getImage(provider.getImage(object));
	}

}
