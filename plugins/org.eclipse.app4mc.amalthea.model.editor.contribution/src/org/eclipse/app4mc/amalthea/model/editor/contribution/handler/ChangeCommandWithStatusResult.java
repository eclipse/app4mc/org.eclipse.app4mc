/*********************************************************************************
 * Copyright (c) 2022-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.handler;

import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.editor.contribution.registry.RegistryServiceWrapper;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.change.util.ChangeRecorder;
import org.eclipse.emf.edit.command.ChangeCommand;

class ChangeCommandWithStatusResult<T> extends ChangeCommand {

	IEclipseContext context;
	String childContextName;
	RegistryServiceWrapper<T> service;
	List<EObject> objects;

	Object status;

	public ChangeCommandWithStatusResult(
			Notifier notifier, IEclipseContext context, String childContextName, 
			RegistryServiceWrapper<T> service, List<EObject> objects) {

		super(new ChangeRecorder(), notifier);
		this.context = context;
		this.childContextName = childContextName;
		this.service = service;
		this.objects = objects;
	}

	@Override
	protected void doExecute() {
		// *** execute specific processing actions here ***
		IEclipseContext activeContext = context.createChild(childContextName);
		activeContext.set(service.getType(), objects.get(0));
		activeContext.set(List.class, objects);

		try {
			this.status = ContextInjectionFactory.invoke(service.getServiceInstance(), PostConstruct.class, activeContext);
		} finally {
			// dispose the context after the execution to avoid memory leaks
			activeContext.dispose();
		}
	}

}
