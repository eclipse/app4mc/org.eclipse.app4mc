/*********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.menu;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.editor.contribution.registry.CreationServiceRegistry;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;

/**
 * Imperative expression implementation that checks if the current active selection is an Amalthea model element.
 */

public class CreationServiceExpression extends AmaltheaModelElementExpression {

	@Evaluate
	public boolean evaluate(
			@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
			@Optional @Service CreationServiceRegistry registry) {

		// only single object selections can have creation services
		if (selection == null || selection.isEmpty() || selection.size() > 1)
			return false;

		return (selection.getFirstElement() instanceof TransientItemProvider || super.evaluate(selection))
				&& registry.hasServices(selection.toList());
	}

}
