/**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.actions;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.ui.ProfileDialog;
import org.eclipse.app4mc.validation.ui.ProfileDialogSettings;
import org.eclipse.app4mc.validation.ui.ValidationUIPlugin;
import org.eclipse.app4mc.validation.ui.util.ValidationExecutorWithMonitor;
import org.eclipse.app4mc.validation.ui.util.ValidationMarkerHelper;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.MarkerHelper;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidateAction extends Action {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidateAction.class);

	private IEditorPart editorPart;

	public ValidateAction() {
		super("Validate", AS_PUSH_BUTTON);

		// image taken from platform:/plugin/org.eclipse.wb.core/icons/test.png
		Bundle bundle = FrameworkUtil.getBundle(ProfileDialogSettings.class);
		if (bundle != null) {
			setImageDescriptor(ResourceLocator.imageDescriptorFromBundle(bundle.getSymbolicName(),
					"/icons/full/obj16/test.png").orElse(null)); //$NON-NLS-1$
		}
		setToolTipText("Validate model");
	}

	public void setActiveEditor(IEditorPart editor) {
		editorPart = editor;
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		IEclipseContext windowCtx = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getService(IEclipseContext.class);
		ProfileDialogSettings settings = ContextInjectionFactory.make(ProfileDialogSettings.class, windowCtx);

		// retrieve stored settings (last selection)
		IDialogSettings store = ValidationUIPlugin.getDefault().getDialogSettings();
		settings.loadFrom(store);

		// open profile selection dialog
		ProfileDialog dialog = new ProfileDialog(
				Display.getDefault().getActiveShell(),
				settings);

		if (dialog.open() == Window.OK && settings.getDialogResults() != null) {
			final Set<Class<? extends IProfile>> selectedProfiles = new HashSet<>(settings.getDialogResults());
			if (selectedProfiles.isEmpty()) return;

			// store settings (last selection)
			settings.saveTo(store);

			// debugging
			if (LOGGER.isInfoEnabled()) {
				StringBuilder builder = new StringBuilder("Selected profiles:");
				for (Class<? extends IProfile> sc : selectedProfiles) {
					builder.append("\n\t- ");
					builder.append(sc.getName());
				}
				LOGGER.info(builder.toString());
				LOGGER.info("Available processors: {}", Runtime.getRuntime().availableProcessors());
			}

			// try to get model objects to validate
			final Resource resource = AmaltheaEditorUtil.getResource(editorPart.getEditorInput());
			final List<EObject> rootObjects = computeRootObjects(resource, settings.getScope());

			if (rootObjects.isEmpty())
				return;

			// Run the check validation operation in a workspace job
			final ValidationExecutorWithMonitor executor = new ValidationExecutorWithMonitor(selectedProfiles);

			final Job job = new Job("Model Validation") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					return executor.validateWithMonitor(rootObjects, monitor);
				}
			};
			if (LOGGER.isDebugEnabled()) {
				job.addJobChangeListener(logValidationResults(executor));
			}
			job.addJobChangeListener(updateMarkersWhenDone(executor, rootObjects));
			job.addJobChangeListener(showMessageDialogWhenDone(executor));
			job.schedule(); // start as soon as possible

		}
	}

	private List<EObject> computeRootObjects(Resource resource, String scope) {
		List<EObject> rootObjects = new ArrayList<>();

		if (resource == null)
			return rootObjects;

		if (ProfileDialogSettings.SCOPE_FILE.equals(scope)) {
			Amalthea model = AmaltheaEditorUtil.getModel(resource);
			if (model != null) {
				rootObjects.add(model);
			}
		}
		else if (ProfileDialogSettings.SCOPE_FOLDER.equals(scope)) {
			List<Amalthea> models = AmaltheaEditorUtil.getModels(resource.getResourceSet());
			if (models != null) {
				rootObjects.addAll(models);
			}
		}

		return rootObjects;
	}

	private JobChangeAdapter logValidationResults(final ValidationExecutor executor) {
		return new JobChangeAdapter() {
			@Override
			public void done(IJobChangeEvent event) {
				// *** Log validation results (for debugging) ***

				if (event.getResult().isOK() && LOGGER.isDebugEnabled()) {
					ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
					PrintStream printStream = new PrintStream(byteStream, true);
					executor.dumpResultMap(printStream);
					LOGGER.debug(byteStream.toString());
				}
			}
		};
	}

	private JobChangeAdapter showMessageDialogWhenDone(final ValidationExecutor executor) {
		return new JobChangeAdapter() {
			@Override
			public void done(IJobChangeEvent event) {
				if (event.getResult().isOK() && EMFPlugin.IS_RESOURCES_BUNDLE_AVAILABLE) {
					// *** Show message dialog ***

					// count results
					int errors = 0;
					int warnings = 0;
					for (ValidationDiagnostic diag : executor.getResults()) {
						switch (diag.getSeverityLevel()) {
						case ERROR: errors++; break;
						case WARNING: warnings++; break;
						default: break;
						}
					}

					String message;
					if (errors == 0 && warnings == 0) {
						message = "No errors or warnings.";
					} else {
						message = "Found " + errors + " errors and " + warnings + " warnings."
								+ "\r\r See problems view for further details.";
					}
					final String displayString = "Validation finished !" + "\r\r" + message;

					Display.getDefault().asyncExec( () -> MessageDialog.openInformation(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"AMALTHEA Model Validation",
						displayString) );
				}
			}
		};
	}

	private JobChangeAdapter updateMarkersWhenDone(final ValidationExecutor executor, final List<EObject> objects) {
		return new JobChangeAdapter() {
			@Override
			public void done(IJobChangeEvent event) {
				if (event.getResult().isOK() && EMFPlugin.IS_RESOURCES_BUNDLE_AVAILABLE) {
					// *** Update problems view ***

					// convert results
					BasicDiagnostic diagnostics = new BasicDiagnostic();
					for (ValidationDiagnostic diag : executor.getResults()) {
						diagnostics.add(diag);
					}

					// collect files (IFile)
					Set<IFile> files = new HashSet<>();
					for (EObject eObject : objects) {
						IFile file = AmaltheaEditorUtil.getIFile(eObject);
						if (file != null) {
							files.add(file);
						}
					}

					// update problem markers
					MarkerHelper markerHelper = new ValidationMarkerHelper();
					for (IFile file : files) {
						markerHelper.deleteMarkers(file);
					}

					try {
						markerHelper.createMarkers(diagnostics);
					} catch (CoreException e) {
						LOGGER.error("Error on updating problem markers", e);
					}
				}
			}
		};
	}

}
