/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.addon;

import javax.inject.Inject;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.e4.core.contexts.IEclipseContext;

public class ModelEditAddon {

	@Inject
	public void init(IEclipseContext context) {
		// Extracting namespace from AmaltheaPackage
		String nsURI = AmaltheaPackage.eNS_URI;

		// Extracting AMALTHEA metamodel version
		String modelVersion = nsURI.lastIndexOf('/') != -1
				? nsURI.substring(nsURI.lastIndexOf('/') + 1)
				: nsURI;

		context.set("APP4MC_MODEL_VERSION", modelVersion);
	}

}
