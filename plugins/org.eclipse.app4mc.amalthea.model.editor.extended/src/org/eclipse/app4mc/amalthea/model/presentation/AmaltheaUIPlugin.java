/**
 ********************************************************************************
 * Copyright (c) 2013 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.presentation;

import org.eclipse.ui.plugin.AbstractUIPlugin;


public class AmaltheaUIPlugin extends AbstractUIPlugin {

	/**
	 * The shared instance
	 */
	private static AmaltheaUIPlugin plugin = new AmaltheaUIPlugin();

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static AmaltheaUIPlugin getDefault() {
		return plugin;
	}

	/**
	 * @return the plugin's id.
	 */
	public static String getPluginId() {
		return plugin.getBundle().getSymbolicName();
	}

}
