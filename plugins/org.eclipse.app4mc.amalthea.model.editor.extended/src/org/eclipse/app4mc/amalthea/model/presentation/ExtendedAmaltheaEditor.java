/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.presentation;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.app4mc.amalthea.model.editor.container.AmaltheaModelContainer;
import org.eclipse.app4mc.amalthea.model.editor.providers.ExtendedAmaltheaLabelProvider;
import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.ui.viewer.ColumnViewerInformationControlToolTipSupport;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.ui.celleditor.AdapterFactoryTreeEditor;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.provider.DiagnosticDecorator;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.util.Util;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IDecoratorManager;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.osgi.service.event.EventHandler;

public class ExtendedAmaltheaEditor extends AmaltheaEditor {

	public static final String AMALTHEA_MODEL_EDITOR = "AMALTHEA Model Editor";

	private boolean showTypes;

	private EventHandler selectionListener = event -> {
		// we need to set the focus in order to trigger a selection event
		// that also updates the properties view
		final IWorkbenchPartSite site = getSite();
		if (site == null) return;

		final IWorkbenchPage page = site.getPage();
		if (page == null) return;

		IEditorPart editorPart = page.getActiveEditor();
		if (!(editorPart instanceof ExtendedAmaltheaEditor)) return;

		editorPart.setFocus();

		Object modelElements = event.getProperty("modelElements");
		setSelectionToViewer((Collection<?>) modelElements);
	};

	public boolean isShowTypesEnabled() {
		return showTypes;
	}

	public void enableShowTypes(boolean enabled) {
		showTypes = enabled;
	}

	@Override
	protected void initializeEditingDomain() {
		// Initialize later:
		//  - shared AdapterFactory, EditingDomain and CommandStack are created by AmaltheaModelContainer
		//  - a local CommandStackListener is added in the init method
	}

	/**
	 * Initializes the editor.
	 *
	 * Additions:
	 * <ul>
	 * <li>register editor at common model container</li>
	 * <li>use common editing domain, resource set and command stack</li>
	 * </ul>
	 */
	@Override
	public void init(IEditorSite site, IEditorInput editorInput) {
		super.init(site, editorInput);

		showTypes = InstanceScope.INSTANCE.getNode("org.eclipse.app4mc.platform.ide").getBoolean("app4mc.showTypes", false);
		
		AmaltheaModelContainer amContainer = AmaltheaEditorUtil.getModelContainer(editorInput);
		if (amContainer != null) {
			// use common model container to register editor
			amContainer.addEditor(this);

			// initialize common editing domain
			adapterFactory = amContainer.getAdapterFactory();
			editingDomain = amContainer.getEditingDomain();
			addCommandStackListener(editingDomain.getCommandStack());
		}
	}

	private void addCommandStackListener(CommandStack commandStack) {
		// Add a listener to set the most recent command's affected objects to be the
		// selection of the viewer with focus.
		//
		commandStack.addCommandStackListener(event -> {
			final Composite container = getContainer();
			if (container == null || container.isDisposed()) return;

			final Display display = container.getDisplay();
			if (display == null || display.isDisposed()) return;

			display.asyncExec(() -> {
				firePropertyChange(IEditorPart.PROP_DIRTY);

				// Try to select the affected objects.
				//
				Command mostRecentCommand = ((CommandStack) event.getSource()).getMostRecentCommand();
				if (mostRecentCommand != null) {
					setSelectionToViewer(mostRecentCommand.getAffectedObjects());
				}
				for (Iterator<PropertySheetPage> i = propertySheetPages.iterator(); i.hasNext();) {
					PropertySheetPage propertySheetPage = i.next();
					if (propertySheetPage.getControl() == null || propertySheetPage.getControl().isDisposed()) {
						i.remove();
					} else {
						propertySheetPage.refresh();
					}
				}
			});
		});
	}

	/**
	 * This is the method used by the framework to install your own controls.
	 *
	 * Modifications (compared to superclass):
	 * <ul>
	 * <li>find corresponding resource of editorInput</li>
	 * <li>set resource as input (instead of resourceSet)</li>
	 * <li>add label provider that can show additional type info</li>
	 * <li>add selection subscriber</li>
	 * </ul>
	 */
	@Override
	public void createPages() {
		// Creates the model from the editor input
		//
		createModel();

		// *** find corresponding resource of editorInput
		URI uri = EditUIUtil.getURI(getEditorInput());
		Resource resource = editingDomain.getResourceSet().getResource(uri, false);

		// Only creates the other pages if there is something that can be edited
		//
		if (resource != null && !resource.getContents().isEmpty()) {
			// Create a page for the selection tree view.
			//
			Tree tree = new Tree(getContainer(), SWT.MULTI);
			selectionViewer = new TreeViewer(tree);
			setCurrentViewer(selectionViewer);

			selectionViewer.setUseHashlookup(true);
			selectionViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));

			// *** add label provider that can show additional type info
			IDialogSettings dialogSettings = AmaltheaEditorPlugin.getPlugin().getDialogSettings();
			selectionViewer.setLabelProvider(new ExtendedAmaltheaLabelProvider(
					new AdapterFactoryLabelProvider(adapterFactory),
					new DiagnosticDecorator(editingDomain, selectionViewer, dialogSettings),
					this));

			// *** set resource as input
			selectionViewer.setInput(resource);
			selectionViewer.setSelection(new StructuredSelection(resource), true);

			new AdapterFactoryTreeEditor(selectionViewer.getTree(), adapterFactory);
			new ColumnViewerInformationControlToolTipSupport(selectionViewer,
					new DiagnosticDecorator.EditingDomainLocationListener(editingDomain, selectionViewer));

			createContextMenuFor(selectionViewer);
			int pageIndex = addPage(tree);
			setPageText(pageIndex, "Selection");

			getSite().getShell().getDisplay().asyncExec
				(() -> {
					 if (!getContainer().isDisposed()) {
						 setActivePage(0);
					 }
				 });
		}

		// Ensures that this editor will only display the page's tab area if there are more than one page
		//
		getContainer().addControlListener
			(new ControlAdapter() {
				boolean guard = false;
				@Override
				public void controlResized(ControlEvent event) {
					if (!guard) {
						guard = true;
						hideTabs();
						guard = false;
					}
				}
			 });

		getSite().getShell().getDisplay().asyncExec(this::updateProblemIndication);

		// *** add selection subscriber
		addSelectionSubscriber();
	}

	@Override
	protected void hideTabs() {
		// implemented here to provide a valid method called from ControlAdapter
		super.hideTabs();
	}

	@Override
	protected void handleChangedResources() {
		if (!changedResources.isEmpty() && (!isDirty() || handleDirtyConflict())) {
			ResourceSet resourceSet = editingDomain.getResourceSet();
			if (isDirty()) {
				changedResources.addAll(resourceSet.getResources());
			}

			// Do NOT unload and load the model

			editingDomain.getCommandStack().flush();

			updateProblemIndication = false;

			if (AdapterFactoryEditingDomain.isStale(editorSelection)) {
				setSelection(StructuredSelection.EMPTY);
			}

			updateProblemIndication = true;
			updateProblemIndication();
		}
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public boolean isSaveOnCloseNeeded() {
		if (super.isSaveOnCloseNeeded()) {
			// use common model container to get the number of open editors
			AmaltheaModelContainer amContainer = AmaltheaEditorUtil.getModelContainer(getEditorInput());

			// save on close is only needed for last open editor
			return amContainer == null || amContainer.getOpenEditors().size() == 1;
		}

		return false;
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor) {
		super.doSave(progressMonitor);

		// use common model container to update the state of open editors
		AmaltheaModelContainer amContainer = AmaltheaEditorUtil.getModelContainer(getEditorInput());
		if (amContainer != null) {
			amContainer.updateDirtyStateOfEditors();
		}

		// update compressed decorator
		IDecoratorManager decoratorMgr = PlatformUI.getWorkbench().getDecoratorManager();
		if (decoratorMgr != null) {
			decoratorMgr.update("org.eclipse.app4mc.amalthea.file.compressed.decorator");
		}
	}

	/**
	 * Called to indicate that the editor has been made dirty
	 * or the changes have been saved.
	 */
	public void editorDirtyStateChanged() {
		firePropertyChange(PROP_DIRTY);
	}

	@Override
	public boolean isDirty() {
		ResourceSet resourceSet = getEditingDomain().getResourceSet();
		boolean anyModified = resourceSet.getResources().stream().anyMatch(Resource::isModified);
		return anyModified || super.isDirty();
	}

	@Override
	public void dispose() {
		removeSelectionSubscriber();

		// use common model container to unregister editor
		AmaltheaModelContainer amContainer = AmaltheaEditorUtil.getModelContainer(getEditorInput());
		if (amContainer != null) {
			amContainer.removeEditor(this);
		}

		super.dispose();
	}

	/**
	 * Register a subscriber that listens for events that trigger a selection.
	 */
	private void addSelectionSubscriber() {
		IWorkbenchPartSite site = getSite();
		if (site == null)
			return;

		IEventBroker broker = site.getService(IEventBroker.class);
		if (broker == null)
			return;

		broker.subscribe("org/eclipse/app4mc/amalthea/editor/SELECT", selectionListener);
	}

	/**
	 * Unregister subscriber that listens for events that trigger a selection.
	 */
	private void removeSelectionSubscriber() {
		IWorkbenchPartSite site = getSite();
		if (site == null)
			return;

		IEventBroker broker = site.getService(IEventBroker.class);
		if (broker == null)
			return;

		// not really necessary as there is one EventBroker instance per EclipseContext
		// but it is a good practice to clean up what we have registered
		broker.unsubscribe(selectionListener);
	}

	/*
	 * Workaround for empty viewer after click on already selected item
	 *
	 * On Windows some SWT bug avoids repainting of non selected elements. Since the related SWT issue
	 * has been discussed for years and no real fix was found, we try to limit the negative effects.
	 * See Bugzilla entries: 491737, 458015, 433858.
	 *
	 * If OS is Windows then an additional redraw is called explicitly.
	 * Users may notice a minor flickering on selection.
	 * However, that is surely preferable to not seeing unselected elements at all.
	 */
	@Override
	public void setSelection(ISelection selection) {
		super.setSelection(selection);

		if (!Util.isWindows()) return; // No Windows OS -> no workaround required

		// *** Workaround: additional redraw ***
		Viewer viewer = getViewer();
		if (viewer != null) {
			Control control = viewer.getControl();
			if (control != null) {
				control.redraw();
			}
		}
	}

}
