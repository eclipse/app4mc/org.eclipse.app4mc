/**
 * *******************************************************************************
 *  Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.presentation;


import java.util.Collection;
import java.util.Collections;

/**
 * This is a simple wizard for creating a new model file.
 */
public class ExtendedAmaltheaModelWizard extends AmaltheaModelWizard {

	/**
	 * Returns the names of the types that can be created as the root object.
	 */
	@Override
	protected Collection<String> getInitialObjectNames() {
		return Collections.singletonList("Amalthea");
	}

}
