
h2. Software Model

The AMALTHEA software model is central accessible through the __SWModel__ element. 


h3. Memory Information

Analyzing and mapping the software structure to available memories needs additional information of the included elements. This type of information targets the consumed size of memory of an element, represented by the __size__ attribute of type __DataUnit__. The element __AbstractMemoryElement__ is a generalized element that provides this data. The following image shows the structure and also the elements of the software model that are extending __AbstractMemoryElement__ (the overview picture is only showing the hierarchy and not possible relationships between the elements):

!(scale)../pictures/model_sw_memory_inf.png!


h3. Labels

The label element represents a data element. It is directly located in a defined area of a given memory.
It can be used as a parameter, a temporarily existing variable or representing a  constant value.

!../pictures/model_sw_label.png!

The following attributes are describing a label:

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name  |_. Description |
| __name__ | The name represented as String value (derived from __AbstractElementMemoryInformation__) |
| __displayName__ | In addition to the name attribute, which must be unique, a label can also be described by an additional and optional display name. The displayName attribute must not be unique. It can be used for example to represent specification data, which can be different from the unique name (like a symbol) of an available software build. |
| __dataType__ | Reference to the data type definition |
| __constant__ | Boolean value to represent, if label is representing a constant value |
| __bVolatile__ | Boolean value to mark a label as volatile |
| __dataStability__ | Enumeration value to represent the data stability needs of the label. If set, it overwrites the global settings stated by the __OsDataConsistency__, otherwise it inherits them (see <notextile><a href="./model_os.html#os-data-consistency">OS Data Consistency</a></notextile>). |
| __stabilityLevel__ | Enumeration value to represent the data stability level of the label. If set, it overwrites the global settings stated by the __OsDataConsistency__, otherwise it inherits them (see <notextile><a href="./model_os.html#os-data-consistency">OS Data Stability</a></notextile>). |

h3. Channels

The channel element has two different characteristics: it represents a data element in memory and also a way how runnables transmit larger amounts of data. A typical applications is the streaming of video data where a continuous sequence of images is sent in smaller chunks.

!../pictures/model_sw_channel.png!

The following attributes are describing a label:

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name  |_. Description |
| __name__ | The name represented as String value (derived from __AbstractElementMemoryInformation__) |
| __displayName__ | In addition to the name attribute, which must be unique, a label can also be described by an additional and optional display name. The displayName attribute must not be unique. It can be used for example to represent specification data, which can be different from the unique name (like a symbol) of an available software build. |
| __elementType__ | Reference to the data type definition of a single element |
| __defaultElements__ | Number of elements initially in the channel (at start-up) |
| __maxElements__ | Depth of channel (maximum number of elements that may be stored) |


h3. Data Types

h4. General Information

The AMALTHEA data model supports meta information for data types. Therefore the element __TypeDefinition__ exists in the software part of the model. It consists of the name and size to define a data type. __BaseTypeDefinition__ has the additional possibility to define the representation of these data types in a target environment (e.g. AUTOSAR), represented by the __Alias__ element.

The compound data types are data structures, based on given or defined base types.
In the literature they are also often named composite or derived types "see Wiki":https://en.wikipedia.org/wiki/Data_type.
The result of this type of definition is an own data type, which can be used as base data types.
They can consist of static structures or dynamic ones, like arrays or stacks.

!../pictures/model_sw_type_system.png!

The following compound data type definitions are currently supported:

* Pointer: Holds a reference to another type using its memory address
* Array: Contains a number of elements of the same data type. The size of an array can be fixed or expandable.
* Struct: Contains other data types in a structured way, often called fields or members.  The fields can be accessed by their name.

h4. Sample

In the picture below a small sample is modeled containing the following information:

* Boolean type with size of 8 bits and alias for AR (Boolean) and C (bool)
* Char16 type with size of 16 bits and alias for AR (Char16) and C (short)
* charArray consists of 4 elements of type Char16
* "hello world struct" with the attribute name (charArray) and valid (Boolean)

Note: The picture shows the element types for better understanding!

!(gray)../pictures/model_sw_type_sample.png!


h3. Activations

Activations are used to specify the intended activation behavior of __Runnables__ and __ProcessPrototypes__. Typically they are defined before the creation of tasks (and the runnable to task mappings). So if there are no tasks defined, or if the mapping of runnables to tasks is not done, this is a way to document when the runnables should be executed.  
Activations are independent of other top level elements in the AMALTHEA model. Single, periodic, sporadic, event or custom (free textual description only, no predefined semantic) activation pattern can be defined. This information can be used in the following development steps to create tasks, stimuli and the mappings to dedicated hardware.

!../pictures/model_sw_activation.png!

*(validation-rule) _PeriodicActivation_: The _Time_ object in the role of _min_ must not contain a negative value!
*(validation-rule) _PeriodicActivation_: The _Time_ object in the role of _max_ must not contain a negative value!
*(validation-rule) _PeriodicActivation_: The _Time_ object in the role of _offset_ must not contain a negative value!
*(validation-rule) _PeriodicActivation_: The _Time_ object in the role of _deadline_ must not contain a negative value!
*(validation-rule) _SingleActivation_: The _Time_ object in the role of _min_ must not contain a negative value!
*(validation-rule) _SingleActivation_: The _Time_ object in the role of _max_ must not contain a negative value!


h3. Tasks / ISR

The following figure shows the structure which describes __Tasks__ and Interrupt Service Routines (__ISR__) in the AMALTHEA software model. The abstract class __Process__ generalizes ISRs and Tasks and provides the common attributes like the activation that is at runtime level represented by a __Stimulus__. A __Task__ or __ISR__ contains __calls__ either to other Tasks (via inter process activation) or Runnables. These types of __calls__ are included in the __activityGraph__ attribute.

!(scale)../pictures/model_sw_task_isr.png!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __name__ | Name of the process |
| __stimuli__ | Reference to one or more __Stimuli__ representing the different types of possible activations of this __Process__|
| __activityGraph__ | List of activities (e.g. runnable calls, label accesses). This specifies the behavior of the process. The objects will be executed in the same order they are stored in the list. |
| __preemption__ | Defines the preemption of a task by higher priority tasks. |
| __multipleTaskActivationLimit__ | Defines the maximum number of existing instances from the same task at any time, which is checked for load limitation reasons when a task is activated. If the multipleTaskActivationLimit is exeeded, the activation is ignored. See the "BTF Example" (User Guide > Examples) for an multipleTaskActivationLimit of 3.|


h3. Runnables and Services

Both elements, runnables and services, are an abstraction of an executable entity. They are both represented by the __Runnable__ element and are distinguished by using the service attribute of type boolean.
Unlike tasks, which are activated in the context for the operating system, runnables are called by tasks or other runnables.

Runnables and Services in the AMALTHEA model can have call parameters. It is possible to specify the arguments of a service call and potential data dependencies. Based on these types of information, an additional analysis can be performed.


h3. Runnables

The next picture shows the general structure of the __Runnable__ element.

!../pictures/model_sw_runnable_overview.png!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __callback__ | __True__ if a runnable is used as a callback |
| __service__ | __True__ if __Runnable__ element can be seen in a service or global function manner. In more detail, the __Runnable__ is only called from other __Runnables__ and not directly from a __Task__ context. |
| __activations__ | Although runnables in the simulation can only be called by tasks, it is possible to specify activation patterns for a runnable. |
| __executionCondition__ | It is possible to specify an execution condition for a runnable (depending on modes).|
| __activityGraph__ | Top level object containing __ActivityGraphItem__ objects, representing runtime, label accesses, other __Runnable__ accesses. The possibilities are described below. |
| __tags__ | Can be used to annotate or group special kind of __Runnables__ |


h3. Activity Graph

A __ActivityGraph__ is the root container of __ActivityGraphItems__. In addition some of the items can also contain sub items. These items are implementing the interface __IActivityGraphItemContainer__.

!../pictures/model_sw_activitygraph_item_container.png!

The following picture shows the structure of a __Activity Graph__. The __Activity Graph Items__ are used to define detailed behavior. With the __Switches__ it is possible to define alternative execution paths. The elements of the graph are executed in the order they are stored in the data model. 

!../pictures/model_sw_activitygraph_structure.png!



h3. Activity Graph Items

The __ActivityGraphItems__ are describing the detailed behavior of a __Process__ or __Runnable__. This can be either an abstraction of runtime or representing access to other elements like __Runnables__, __Labels__, and __Semaphores__. An overview of the different possibilities is given in the next pictures.

!../pictures/model_sw_activity_graph_items.svg!


h4. Groups

A __Group__ is used to structure the __ActivityGraphItems__. 

!../pictures/model_sw_activity_graph_items__group.svg!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __ordered__ | __boolean__ attribute that indicates if the execution order of the contained elements is +fixed+ (default) or random. |
| __interruptible__ | __boolean__ attribute that specifies whether the group +can be interrupted+ (default).<br/>If set to false the group cannot be interrupted (neither by interrupts nor by other higher priority tasks). |

Some hints regarding +non-interruptible+ groups:
* they usually should be very short, so the system remains responsive
* they may not have nested groups among their contained items (this would result in a validation error)
* they should not be used for resource access synchronization across cores - there are Semaphores for that purpose
* they directly correspond to the sections between __SuspendAllInterrupts__ and __ResumeAllInterrupts__ calls in AUTOSAR

h4. While Loop

A __WhileLoop__ enables to represent repetitions of activity graph items within an activity graph. These repeated items are contained in __items__ (from __ActivityGraphItemContainer__). They will be repeated as long as the __condition__ is evaluated to be *true*. In order to enter the while loop at all, the condition must initially be *true*.

!../pictures/model_sw_activity_graph_items__whileloop.svg!


h4. Switch

Switches can be used to define different execution paths within an activity graph. A __Switch__ corresponds to __if-else__ or __switch-case__ statements in a programming language. It uses the evaluated __ConditionDisjunction__ to decide which entry will be executed. The first fulfilled condition determines the path to execute.
A __SwitchEntry__ object is used to represent an execution path. A condition is defined for each entry (via __ConditionDisjunction__).
At the __Switch__ it is also possible to define a default path, which is executed if none of the conditions of the __SwitchEntries__ is true.

!../pictures/model_sw_activity_graph_items__switch.svg!


h4. Probability Switch

Each entry (path) of a __ProbabilitySwitch__ has a probability-value. As the name indicates, this is the value that defines the probability that the path is executed. It is computed as a fraction of its value divided by the sum of all __ProbabilitySwitchEntries__ in the surrounding switch.

!../pictures/model_sw_activity_graph_items__probability_switch.png!


h4. Ticks

__Ticks__ allow to specify the required execution "time" in a basic way. They are the equivalent of cycles in the hardware domain and the execution time can easily be calculated if the frequency of the executing __ProcessingUnit__ (PU) is known.
Detailed definition: see <notextile><a href="./model_basics.html#basics-ticks">Ticks</a></notextile>


h4. Execution Need

__ExecutionNeed__ allows to specify the required execution "time" in a more abstract way. Multiple __NeedEntries__ can be used to specify execution characteristics. There is a map of default entries and (optional) maps of entries for specific hardware types (e.g. FPGA). These extended entries are also implemented as a map with a processing unit definition as the key.

!../pictures/model_sw_activity_graph_items__execution_need.png!

A simple approach (equivalent to the __RunnableInstructions__ in older versions of the AMALTHEA model) is the specification of the "number of generic instructions" that have to be executed. Together with the hardware feature "instructions per cycle (IPC)" and the frequency of a processing unit it permits the calculation of the execution time on a specific processing unit.

!../pictures/model_sw_hw_features_executionneed.png!

With the generic concept of __ExecutionNeeds__ it is also possible to describe more detailed characteristics, e.g. instruction mixes of floating point operations, integer operations, load/store operations, etc., or any other relevant aspect.


h4. Calls and AUTOSAR communication

!(scale)../pictures/model_sw_activity_graph_items__calls.png!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __RunnableCall__ | The activation of another __Runnable__.
*Counters* with prescalers and offsets are optional to define the ratio of __RunnableCalls__. Since runnables are can be called in different contexts, the value of the counter is always local in the context of the caller. Global counters should be modelled with counting modes |
| __SenderReceiverCommunication__ | An abstract description for sender-receiver-communication (it can be read or write). |
| __ServerCall__ | An abstract description for client/server communication. It refers to a required runnable that describes the called server operation |
| __SynchronousServerCall__ | A synchronous server call with defined waiting behaviour. |
| __AsynchronousServerCall__ | An asynchronous server call with (optional) specification of result runnable. |
| __GetResultsServerCall__ | Get the result of a previous asynchronous server call (with defined blocking behaviour). |


h4. Label Access

!(scale)../pictures/model_sw_activity_graph_items__label_access.png!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __access__ | The type of access is represented using the values of __LabelAccessEnum__. |
| __data__ | Describes the access to an existent __Label__ (set as reference). |
| __dataStability__ | Describes the data stability needs. If set, it overwrites the label settings, otherwise it inherits them (see <notextile><a href="./model_os.html#os-data-consistency">OS Data Consistency</a></notextile>). |
| __implementation__ | Describes how a label access is implemented:	<ul><li>*explicit*: also known as "direct" </li><li>*implicit*: also known as "optimized"</li><li>*timed*</li></ul> |
| __statistic__ | Defines the frequency of the label access. |
| __transmissionPolicy__ | The following attributes reflect the computing demand (time) depending on data: <ul><li> *chunkSize*: Size of a part of an element, maximum is the element size. </li><li> *chunkProcessingTicks*: Number of ticks that will be executed to process one chunk (algorithmic overhead). </li></ul> The next attribute specifies the amount of data actually accessed by a runnable (required to analyze memory bandwidth demands): <ul><li> *transmitRatio*: Specify the ratio of each element that is actually transmitted by the runnable in percent. Value must be between [0, 1], default value is 1.0 . </li></ul> |


h4. Channel Access

!../pictures/model_sw_activity_graph_items__channel_access.png!

Common attributes:

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __data__ | Describes the access (__ChannelSend__ or __ChannelReceive__) to an existent __Channel__ (set as reference). |
| __elements__ | Maximum number of elements that are transmitted. |
| __transmissionPolicy__ | The following attributes reflect computing demand (time) depending on data: <ul><li> *chunkSize*: Size of a part of an element, maximum is the element size. </li><li> *chunkProcessingTicks*: Number of ticks that will be executed to process one chunk (algorithmic overhead). </li></ul> The next attribute specifies the amount of data actually accessed by a runnable (required to analyze memory bandwidth demands): <ul><li> *transmitRatio*: Specify the ratio of each element that is actually transmitted by the runnable in percent. Value must be between [0, 1], default value is 1.0 . </li></ul> |

Receive attributes:

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __receiveOperation__ | The type of an operation is defined by LIFO or FIFO, Read or Take: <ul><li> *LIFO*: last-in, first-out </li><li> *FIFO*: first-in, first-out </li><li> *Read*: reads elements (without modifying the channel content) </li><li> *Take*: removes the received elements </li></ul> |
| __elementIndex__ | Position (index) in channel at which the operation is effective. Zero is the default and denotes the oldest (FIFO) or newest element (LIFO) in the channel. |
| __lowerBound__ | Minimum number of elements returned by the operation. The value must be in the range [0,n], with n is the maximum number of elements that are received. Default value is n. |
| __dataMustBeNew__ | Specify if the operation must only return elements that are not previously read by the runnable. Default value is false. |


h4. Semaphore Access

!../pictures/model_sw_activity_graph_items__semaphore_access.svg!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __SemaphoreAccess__ | Represents an access of a Semaphore. The type of access is defined using the __SemaphoreAccessEnum__ values. The __Semaphore__ itself is set as a reference to an existent one. |
| __access__ | Defines the type of access (request, exclusive, release). Note that for spinlocks and mutexes, a request access is always exclusive, so __request__ means the same as __exclusive__ in these cases. |
| __waitingBehaviour__ | Defines if the process is blocking the core when it waits (active) or not (passive). Note that for spinlock accesses, only active waiting can be used. |

*(validation-rule)  _SemaphoreAccess_: The _waitingBehaviour_ can only be _active_ if the accessed semaphore is a spinlock!


h4. Mode Label Access

!../pictures/model_sw_activity_graph_items__mode_label_access.png!

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __ModeLabelAccess__ | Describes the access to an existing __ModeLabel__ (set as reference). The type of access is represented using the values of __ModeLabelAccessEnum__: <ul><li>*read*: indicates that the behavior of the runnable is influenced by the current value of the __ModeLabel__.</li><li>*set*: represents the change of a mode label. The defined __value__ is set.</li><li>*increment* / *decrement*: changes a mode label. The value is increased / decreased by __step__. In case of an __EnumMode__ the next / previous literal is set (according to __step__ limited by the range of literals).</li></ul> |

*(validation-rule)  _ModeLabelAccess_: The _value_ string must be a valid element of the corresponding _Mode_!


h4. Custom Event Trigger

The __Custom Event Trigger__ references an event of type __Custom Event__. The execution of a __Custom Event Trigger__ entry triggers the corresponding event that can be observed by an __Event Stimulus__.  

!../pictures/model_sw_activity_graph_items__triggers.png!


h4. Enforced Migration

Each task is controlled by at least one task scheduler. A task scheduler is the resource owner of one or multiple processor cores (The task scheduler decides on which of its cores the task is executed). The __Enforced Migration__ forces the task to switch to another task scheduler. Therefore the __Enforced Migration__ entry contains a reference to the new task scheduler.

This element is used as a short notation to simplify the simulation of different design possibilities. It should normally be modeled as two separate tasks and an inter process trigger.


h4. Inter Process Trigger

The __Inter Process Trigger__ references a stimulus of type __Inter Process Stimulus__. The execution of an __Inter Process Trigger__ entry triggers the processes that are mapped to this stimulus.  


h4. Schedule Point

At a __Schedule Point__, the process calls the scheduler that currently administrates it. This is used for cooperative task scheduling (see OSEK Specification 2.2.3, 2005).


h4. Terminate Process

If a __Terminate Process__ is reached during the execution of a __Activity Graph__, the __Task__ or __ISR__ terminates immediately. It is not required to insert this element at the end of a __Activity Graph__. It can be used to define an execution path (by using __Switches__) that terminates a process.   


h4. Wait/Clear/Set Event

The AMALTHEA Software Model contains a list of __OS-Event__ objects. These can be used for task synchronization. Elements are  __Wait Event__, __Clear Event__ and __Set Event__. 

!../pictures/model_sw_osevents.png!

With __Wait Event__ the process waits for a number of events (defined in the __Event Mask__) to be set. Here it can be defined if the process waits just for one of the __OS-Events__ (maskType = OR) or for all of them (maskType = AND). The __waiting behaviour__ defines if the process is blocking the core when it waits (active) or not (passive). 

__Set Event__ sets/fires a list of __OS-Events__. Here it is possible to define a context for the __OS-Event__. If a process is referenced, then the __OS-Events__ are set just for this process.

The execution of a __Clear Event__ entry unsets all referenced __OS-Events__. 

For more information about OS-Events, see the OSEK Specification 2.2.3, 2005. 


h4. Statistical Values

!../pictures/model_sw_statistic.png!

It is possible to add to different elements (as shown in the picture above) different types of statistical values. These values can be used to describe in more detail the following behaviors:

* __RunEntityCallStatistic__: Can be used to describe in more detail the dynamic call behavior of a __RunnableCall__. The value is representing how often the call is performed.
* __LabelAccessStatistic__: Describes in more detail the dynamic behavior accessing a __Label__ by having cache misses or how often a read/write is performed.



h3. Modes

The next diagram shows the overall picture, the sub chapters describe the details.

!(scale)../pictures/model_sw_modes_overview.png!

The actual state of a Mode is represented by a __ModeLabel__ or a __LocalModeLabel__.
The (global) mode labels are mainly used in __Mode Conditions__ to describe the conditional execution of a __Runnable__ or a __Stimulus__.
Local mode labels are defined in the context of an executable and can be modified in the activity graph.
Both types of mode labels can be used to determine which path should be executed in a __Switch__ statement or how often the body of a __WhileLoop__ should be repeated.



h3. Mode Labels

!(scale)../pictures/model_sw_modes.png!

The __Mode__ element denotes the type and the possible values of a specific system state. The subclasses __EnumMode__ and __NumericMode__ describe the values that are allowed for the corresponding __ModeLabel__. In case of a __EnumMode__ the __initialValue__ has to be the name of one of the contained __ModeLiterals__. For __NumericMode__ the __initialValue__ has to be the string representation of an integer. 

Both __Mode Label__ elements are a concrete representation of a specific __Mode__. They are described using the following attributes:

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __name__ | Name of the mode label |
| __displayName__ | In addition to the name attribute, which must be unique, a mode label can also be described by an additional and optional display name. The displayName attribute must not be unique. It can be used for example to represent specification data, which can be different from the unique name (like a symbol) of an available software build. |
| __mode__ | Reference to the corresponding __Mode__ definition |
| __initialValue__ | String representation of the initial value (of a mode label) |
| __defaultValue__ | String representation of the default value (of a local mode label) |

h4. Mode Changes

The value of a (global) mode label can be changed using the set-mode-value-list of a __Stimulus__.

Another possibility is a write access within an __Activity Graph__ via a __Mode Label Access__ or __Local Mode Label Assignment__.

h4. Mode Conditions

Mode conditions allow to specify dedicated states of the overall system. 
Depending on the condition evaluation switches can be executed or stimuli can be influenced.
The ModeValueMap represents the system state that has to be provided as an external input, e.g. from a simulation or analysis tool.

!(scale)../pictures/model_sw_modecondition.png!



h3. Local Mode Labels

Local mode labels are defined in the context of an executable (Task, ISR or Runnable).

The class diagram shows the abstract class *LocalModeValue* that provides the connection between a local mode label and a value source.

!(scale)../pictures/model_sw_local_mode_value.png!

Possible value sources:

* Constants (integer values or mode literals)
* Values of other mode labels
* Fill levels of channels
* Arithmetic expressions

The subclasses are used for comparison and assignment.

!(scale)../pictures/model_sw_local_mode_value_hierarchy.png!


h4. Local Mode Label Assignment

The __Local Mode Label Assignment__ offers a number of possibilities to change the value of a local mode label. Values can be constant, the copied value of another mode label or even the result of an arithmetic expression.

The __Mode Label Assignment__ simply provides a way to copy the content of a local mode label to a global one.

!(scale)../pictures/model_sw_mode_assignment.png!


h4. Local Mode Conditions

Local mode conditions are available where local mode labels are accessible. That means they can be used in __Switch__ or  __WhileLoop__ statements in the activity graph of an executable.

!(scale)../pictures/model_sw_local_mode_condition.png!


h4. Local mode label examples

The following example shows a local mode label "loop1" that represents a numeric mode called "Counter". It is defined in the scope of the runnable "ABSCalculation". This scope is also seen in all references to the local mode label where the qualified name "ABSCalculation.loop1" has to be used. The value of the local mode label is modified and checked by several items of the activity graph: the value is set, checked in the loop condition of a while loop and decremented in the loop body. The last line shows that it is also possible to store the value of a local mode label in a (global) mode label.

!(scale)../pictures/model_sw_local_mode_label_counter.png!

The local mode labels of an executable are available only during the execution. The next part of the example shows how to initialize local mode labels of the called runnable as part of the runnable call. This feature allows to set the context of a specific invocation and the runnable can react accordingly (e.g. by executing different paths of the activity graph).

!(scale)../pictures/model_sw_local_mode_label_call_context.png!




h3. Process Prototypes

!(scale)../pictures/model_sw_process_prototype.png!

In addition to the Task elements, the AMALTHEA model contains an element process prototype.
This prototype can be used to define raw data of a task. It can be used to specify access to
labels (read, write) or other runnables/functions as possible with the normal task, but not the
order of the access. These prototypes are then processed by different algorithms. The algorithms are creating the tasks, are filling, verifying or modifying the data based on their different checks. The final result of this processing are tasks, which are corresponding to the data of the prototypes.

!../pictures/process_prototypes.png!

These tasks are representing the current state and can be further processed, for example to generate code or further simulation. With the process prototypes available in the model, it is possible to define the structure of the software in an early development phase. The implementation at that moment is open and not yet completed, but the general idea can be verified.
Another issue can be the distribution to a multi-core system, coming from a single-core system. Therefore the activity graph can be analyzed and computed to get the right order and parallelization of the elements and dependencies.


h3. Process Chains

The following figure shows the structure which describes __Process Chains__ in the AMALTHEA software model. A process chain is used to group task and isrs together which are executing after each other and represent an end-to-end data processing path. The processes inside a process chain are connected via __Inter Process Activations__.

!../pictures/model_sw_process_chain.png!

The following attributes are describing a process chain:

table(minimal){padding:10px; border:1px solid black; background:#f8f8f8}. 
|_. Name |_. Description |
| __name__ | Name of the process chain |
| __processes__ | List of tasks and isrs which are included in the process chain |


h3. Custom Entities

!../pictures/model_custom_entity.png!

The __CustomEntity__ element defines a way to add not yet available elements of the software model in a generic way. The only contained attribute defines the type of the entity by setting it as a __String__. Additional properties can be set using the __CustomAttributes__.


h3. Section

Section (Virtual Memory Section) is used to group the memory elements (Labels, Runnables). This is achieved by associating the *Section* element to *Label* & *Runnable* elements
 
Below are properties of Section element:

* name
* asilLevel

!../pictures/model_section.png!

With this type of information available, the complexity of mapping software components to memories can be reduced. The next picture gives an overview about the general idea of the mapping with Sections.

!../pictures/model_section_memory.png!


h3. Data Dependencies and Runnable Parameters

h4. Overview

The purpose of following model elements is to describe a high level data flow. Parameters of runnables (or services) and specific call arguments allow to model a behavior that depends on the call tree but the detailed computation and the control flow within a runnable are not taken into account. Therefore only "potential" influences are modeled. The following picture shows this data flow with dashed lines.

!../pictures/model_sw_dataflow_overview.png!

h4. Elements with data dependency

The data flow is specified with "depends on" references. Elements that can specify a dependency are:

* __**LabelAccess**__ of type "WRITE"
* __**CallArgument**__ that refers to an "IN" (or "INOUT") parameter
* __**RunnableParameter**__ with direction "OUT" (or "INOUT")

!../pictures/model_sw_depends_on.png!

h4. Data Dependency

A data dependency defines the elements that can influence a value that is written or transferred. Possible origins are:

* __**Label**__
* __**RunnableParameter**__ with direction "IN" (or "INOUT")
* __**CallArgument**__ that refers to an "OUT" (or "INOUT") parameter

!../pictures/model_sw_data_dependency.png!

