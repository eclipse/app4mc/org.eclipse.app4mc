/**
 ********************************************************************************
 * Copyright (c) 2018-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.CommonElements;
import org.eclipse.app4mc.amalthea.model.CoreClassifier;
import org.eclipse.app4mc.amalthea.model.MemoryClassifier;
import org.eclipse.app4mc.amalthea.model.Namespace;
import org.eclipse.app4mc.amalthea.model.Tag;

public class CommonElementsBuilder {

	public CommonElements commonElementsRoot(final Consumer<CommonElements> initializer) {
		final CommonElements obj = AmaltheaFactory.eINSTANCE.createCommonElements();
		initializer.accept(obj);
		return obj;
	}

	// ********** Top level elements **********

	public void memoryClassifier(final CommonElements container, final Consumer<MemoryClassifier> initializer) {
		final MemoryClassifier obj = AmaltheaFactory.eINSTANCE.createMemoryClassifier();
		container.getMemoryClassifiers().add(obj);
		initializer.accept(obj);
	}

	public void coreClassifier(final CommonElements container, final Consumer<CoreClassifier> initializer) {
		final CoreClassifier obj = AmaltheaFactory.eINSTANCE.createCoreClassifier();
		container.getCoreClassifiers().add(obj);
		initializer.accept(obj);
	}

	public void tag(final CommonElements container, final Consumer<Tag> initializer) {
		final Tag obj = AmaltheaFactory.eINSTANCE.createTag();
		container.getTags().add(obj);
		initializer.accept(obj);
	}

	public void namespace(final CommonElements container, final Consumer<Namespace> initializer) {
		final Namespace obj = AmaltheaFactory.eINSTANCE.createNamespace();
		container.getNamespaces().add(obj);
		initializer.accept(obj);
	}

	// ********** Namespace segments **********

	public void segment(final Namespace container, final Consumer<Namespace> initializer) {
		final Namespace obj = AmaltheaFactory.eINSTANCE.createNamespace();
		container.getNextSegments().add(obj);
		initializer.accept(obj);
	}
}
