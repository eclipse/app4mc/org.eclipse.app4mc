/**
 ********************************************************************************
 * Copyright (c) 2018-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.MemoryMapping;
import org.eclipse.app4mc.amalthea.model.PhysicalSectionMapping;
import org.eclipse.app4mc.amalthea.model.RunnableAllocation;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.Value;

public class MappingBuilder {

	public MappingModel mappingModelRoot(final Consumer<MappingModel> initializer) {
		final MappingModel obj = AmaltheaFactory.eINSTANCE.createMappingModel();
		initializer.accept(obj);
		return obj;
	}

	// ********** Top level elements **********

	public void isrAllocation(final MappingModel container, final Consumer<ISRAllocation> initializer) {
		final ISRAllocation obj = AmaltheaFactory.eINSTANCE.createISRAllocation();
		container.getIsrAllocation().add(obj);
		initializer.accept(obj);
	}

	public void taskAllocation(final MappingModel container, final Consumer<TaskAllocation> initializer) {
		final TaskAllocation obj = AmaltheaFactory.eINSTANCE.createTaskAllocation();
		container.getTaskAllocation().add(obj);
		initializer.accept(obj);
	}

	public void runnableAllocation(final MappingModel container, final Consumer<RunnableAllocation> initializer) {
		final RunnableAllocation obj = AmaltheaFactory.eINSTANCE.createRunnableAllocation();
		container.getRunnableAllocation().add(obj);
		initializer.accept(obj);
	}

	public void schedulerAllocation(final MappingModel container, final Consumer<SchedulerAllocation> initializer) {
		final SchedulerAllocation obj = AmaltheaFactory.eINSTANCE.createSchedulerAllocation();
		container.getSchedulerAllocation().add(obj);
		initializer.accept(obj);
	}

	public void memoryMapping(final MappingModel container, final Consumer<MemoryMapping> initializer) {
		final MemoryMapping obj = AmaltheaFactory.eINSTANCE.createMemoryMapping();
		container.getMemoryMapping().add(obj);
		initializer.accept(obj);
	}

	public void physicalSectionMapping(final MappingModel container, final Consumer<PhysicalSectionMapping> initializer) {
		final PhysicalSectionMapping obj = AmaltheaFactory.eINSTANCE.createPhysicalSectionMapping();
		container.getPhysicalSectionMapping().add(obj);
		initializer.accept(obj);
	}

	// ********** Parameters for task allocation **********

	public void schedulingParameter(final TaskAllocation container, final String parameterName,
			final Value parameterValue) {
		final Scheduler sched = container.getScheduler();
		final SchedulerDefinition sd = sched.getDefinition();
		sd.getProcessParameters().stream().filter(pad -> pad.getName().equals(parameterName)).findFirst()
				.ifPresent(spd -> container.getSchedulingParameters().put(spd, parameterValue));
	}

}
