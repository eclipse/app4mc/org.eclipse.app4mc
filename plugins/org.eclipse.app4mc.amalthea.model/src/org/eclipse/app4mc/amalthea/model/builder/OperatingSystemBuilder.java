/**
 ********************************************************************************
 * Copyright (c) 2018-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.OsAPIOverhead;
import org.eclipse.app4mc.amalthea.model.OsDataConsistency;
import org.eclipse.app4mc.amalthea.model.OsISROverhead;
import org.eclipse.app4mc.amalthea.model.OsOverhead;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.VendorOperatingSystem;

public class OperatingSystemBuilder {

	public OSModel osModelRoot(final Consumer<OSModel> initializer) {
		final OSModel obj = AmaltheaFactory.eINSTANCE.createOSModel();
		initializer.accept(obj);
		return obj;
	}

	// ********** Top level elements **********

	public void operatingSystem(final OSModel container, final Consumer<OperatingSystem> initializer) {
		final OperatingSystem obj = AmaltheaFactory.eINSTANCE.createOperatingSystem();
		container.getOperatingSystems().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void operatingSystem_Vendor(final OSModel container, final Consumer<VendorOperatingSystem> initializer) {
		final VendorOperatingSystem obj = AmaltheaFactory.eINSTANCE.createVendorOperatingSystem();
		container.getOperatingSystems().add(obj);
		initializer.accept(obj);
	}

	public void semaphore(final OSModel container, final Consumer<Semaphore> initializer) {
		final Semaphore obj = AmaltheaFactory.eINSTANCE.createSemaphore();
		container.getSemaphores().add(obj);
		initializer.accept(obj);
	}

	public void osOverhead(final OSModel container, final Consumer<OsOverhead> initializer) {
		final OsOverhead obj = AmaltheaFactory.eINSTANCE.createOsOverhead();
		container.getOsOverheads().add(obj);
		initializer.accept(obj);
	}

	public void schedulerDefinition(final OSModel container, final Consumer<SchedulerDefinition> initializer) {
		final SchedulerDefinition obj = AmaltheaFactory.eINSTANCE.createSchedulerDefinition();
		container.getSchedulerDefinitions().add(obj);
		initializer.accept(obj);
	}

	public void parameterDefinition(final OSModel container, final Consumer<SchedulingParameterDefinition> initializer) {
		final SchedulingParameterDefinition obj = AmaltheaFactory.eINSTANCE.createSchedulingParameterDefinition();
		container.getSchedulingParameterDefinitions().add(obj);
		initializer.accept(obj);
	}

	// ********** Operating system **********

	public void taskScheduler(final OperatingSystem container, final Consumer<TaskScheduler> initializer) {
		final TaskScheduler obj = AmaltheaFactory.eINSTANCE.createTaskScheduler();
		container.getTaskSchedulers().add(obj);
		initializer.accept(obj);
	}

	public void schedulingParameter(final Scheduler container, final String parameterName, final Value parameterValue) {
		final SchedulerDefinition sd = container.getDefinition();
		sd.getAlgorithmParameters().stream().filter(pad -> pad.getName().equals(parameterName)).findFirst()
				.ifPresent(spd -> container.getSchedulingParameters().put(spd, parameterValue));
	}

	public void interruptController(final OperatingSystem container,
			final Consumer<InterruptController> initializer) {
		final InterruptController obj = AmaltheaFactory.eINSTANCE.createInterruptController();
		container.getInterruptControllers().add(obj);
		initializer.accept(obj);
	}

	public void dataConsistency(final OperatingSystem container, final Consumer<OsDataConsistency> initializer) {
		final OsDataConsistency obj = AmaltheaFactory.eINSTANCE.createOsDataConsistency();
		container.setOsDataConsistency(obj);
		initializer.accept(obj);
	}

	public void labelAccess(final Scheduler container, final Consumer<LabelAccess> initializer) {
		final LabelAccess obj = AmaltheaFactory.eINSTANCE.createLabelAccess();
		container.getComputationItems().add(obj);
		initializer.accept(obj);
	}

	public void parentAssociation(final TaskScheduler container, final Consumer<SchedulerAssociation> initializer) {
		final SchedulerAssociation obj = AmaltheaFactory.eINSTANCE.createSchedulerAssociation();
		container.setParentAssociation(obj);
		initializer.accept(obj);
	}

	public void schedulingParameter(final SchedulerAssociation container, final String parameterName,
			final Value parameterValue) {
		Scheduler sched = container.getParent();
		final SchedulerDefinition sd = sched.getDefinition();
		sd.getProcessParameters().stream().filter(pad -> pad.getName().equals(parameterName)).findFirst()
				.ifPresent(spd -> container.getSchedulingParameters().put(spd, parameterValue));
	}

	// ********** OS instructions (overhead) **********

	public void apiOverhead(final OsOverhead container, final Consumer<OsAPIOverhead> initializer) {
		final OsAPIOverhead obj = AmaltheaFactory.eINSTANCE.createOsAPIOverhead();
		container.setApiOverhead(obj);
		initializer.accept(obj);
	}

	public void isrOverheadCat1(final OsOverhead container, final Consumer<OsISROverhead> initializer) {
		final OsISROverhead obj = AmaltheaFactory.eINSTANCE.createOsISROverhead();
		container.setIsrCategory1Overhead(obj);
		initializer.accept(obj);
	}

	public void isrOverheadCat2(final OsOverhead container, final Consumer<OsISROverhead> initializer) {
		final OsISROverhead obj = AmaltheaFactory.eINSTANCE.createOsISROverhead();
		container.setIsrCategory2Overhead(obj);
		initializer.accept(obj);
	}

}
