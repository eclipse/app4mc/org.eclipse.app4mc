/**
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AsynchronousServerCall;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.ClearEvent;
import org.eclipse.app4mc.amalthea.model.ConditionConjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.CustomActivation;
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
import org.eclipse.app4mc.amalthea.model.EnforcedMigration;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.EventActivation;
import org.eclipse.app4mc.amalthea.model.EventMask;
import org.eclipse.app4mc.amalthea.model.ExecutionNeed;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabelCondition;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.ModeValueCondition;
import org.eclipse.app4mc.amalthea.model.NumericMode;
import org.eclipse.app4mc.amalthea.model.OsEvent;
import org.eclipse.app4mc.amalthea.model.PeriodicActivation;
import org.eclipse.app4mc.amalthea.model.ProbabilitySwitch;
import org.eclipse.app4mc.amalthea.model.ProbabilitySwitchEntry;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.ProcessPrototype;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.RunnableParameter;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulePoint;
import org.eclipse.app4mc.amalthea.model.Section;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccessEnum;
import org.eclipse.app4mc.amalthea.model.SetEvent;
import org.eclipse.app4mc.amalthea.model.SingleActivation;
import org.eclipse.app4mc.amalthea.model.SporadicActivation;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchDefault;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TerminateProcess;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.VariableRateActivation;
import org.eclipse.app4mc.amalthea.model.WaitEvent;

public class SoftwareBuilder {
	public SWModel softwareModelRoot(final Consumer<SWModel> initializer) {
		final SWModel obj = AmaltheaFactory.eINSTANCE.createSWModel();
		initializer.accept(obj);
		return obj;
	}

	// ********** Top level elements **********

	public void isr(final SWModel container, final Consumer<ISR> initializer) {
		final ISR obj = AmaltheaFactory.eINSTANCE.createISR();
		container.getIsrs().add(obj);
		initializer.accept(obj);
	}

	public void task(final SWModel container, final Consumer<Task> initializer) {
		final Task obj = AmaltheaFactory.eINSTANCE.createTask();
		container.getTasks().add(obj);
		initializer.accept(obj);
	}

	public void runnable(final SWModel container, final Consumer<Runnable> initializer) {
		final Runnable obj = AmaltheaFactory.eINSTANCE.createRunnable();
		container.getRunnables().add(obj);
		initializer.accept(obj);
	}

	public void label(final SWModel container, final Consumer<Label> initializer) {
		final Label obj = AmaltheaFactory.eINSTANCE.createLabel();
		container.getLabels().add(obj);
		initializer.accept(obj);
	}

	public void channel(final SWModel container, final Consumer<Channel> initializer) {
		final Channel obj = AmaltheaFactory.eINSTANCE.createChannel();
		container.getChannels().add(obj);
		initializer.accept(obj);
	}

	public void processPrototype(final SWModel container, final Consumer<ProcessPrototype> initializer) {
		final ProcessPrototype obj = AmaltheaFactory.eINSTANCE.createProcessPrototype();
		container.getProcessPrototypes().add(obj);
		initializer.accept(obj);
	}

	public void section(final SWModel container, final Consumer<Section> initializer) {
		final Section obj = AmaltheaFactory.eINSTANCE.createSection();
		container.getSections().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void activation_Single(final SWModel container, final Consumer<SingleActivation> initializer) {
		final SingleActivation obj = AmaltheaFactory.eINSTANCE.createSingleActivation();
		container.getActivations().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void activation_Sporadic(final SWModel container, final Consumer<SporadicActivation> initializer) {
		final SporadicActivation obj = AmaltheaFactory.eINSTANCE.createSporadicActivation();
		container.getActivations().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void activation_Periodic(final SWModel container, final Consumer<PeriodicActivation> initializer) {
		final PeriodicActivation obj = AmaltheaFactory.eINSTANCE.createPeriodicActivation();
		container.getActivations().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void activation_Event(final SWModel container, final Consumer<EventActivation> initializer) {
		final EventActivation obj = AmaltheaFactory.eINSTANCE.createEventActivation();
		container.getActivations().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void activation_VariableRate(final SWModel container, final Consumer<VariableRateActivation> initializer) {
		final VariableRateActivation obj = AmaltheaFactory.eINSTANCE.createVariableRateActivation();
		container.getActivations().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void activation_Custom(final SWModel container, final Consumer<CustomActivation> initializer) {
		final CustomActivation obj = AmaltheaFactory.eINSTANCE.createCustomActivation();
		container.getActivations().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void mode_Enum(final SWModel container, final Consumer<EnumMode> initializer) {
		final EnumMode obj = AmaltheaFactory.eINSTANCE.createEnumMode();
		container.getModes().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void mode_Numeric(final SWModel container, final Consumer<NumericMode> initializer) {
		final NumericMode obj = AmaltheaFactory.eINSTANCE.createNumericMode();
		container.getModes().add(obj);
		initializer.accept(obj);
	}

	public void modeLabel(final SWModel container, final Consumer<ModeLabel> initializer) {
		final ModeLabel obj = AmaltheaFactory.eINSTANCE.createModeLabel();
		container.getModeLabels().add(obj);
		initializer.accept(obj);
	}

	public void osEvent(final SWModel container, final Consumer<OsEvent> initializer) {
		final OsEvent obj = AmaltheaFactory.eINSTANCE.createOsEvent();
		container.getEvents().add(obj);
		initializer.accept(obj);
	}

	// ********** Mode Literals **********

	public void literal(final EnumMode container, final Consumer<ModeLiteral> initializer) {
		final ModeLiteral obj = AmaltheaFactory.eINSTANCE.createModeLiteral();
		container.getLiterals().add(obj);
		initializer.accept(obj);
	}

	// ********** Activity Graph **********

	public void activityGraph(final Process container, final Consumer<ActivityGraph> initializer) {
		final ActivityGraph obj = AmaltheaFactory.eINSTANCE.createActivityGraph();
		container.setActivityGraph(obj);
		initializer.accept(obj);
	}

	public void activityGraph(final Runnable container, final Consumer<ActivityGraph> initializer) {
		final ActivityGraph obj = AmaltheaFactory.eINSTANCE.createActivityGraph();
		container.setActivityGraph(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - ModeSwitch **********

	public void conditionalSwitch(final IActivityGraphItemContainer container, final Consumer<Switch> initializer) {
		final Switch obj = AmaltheaFactory.eINSTANCE.createSwitch();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void defaultEntry(final Switch container, final Consumer<SwitchDefault> initializer) {
		final SwitchDefault obj = AmaltheaFactory.eINSTANCE.createSwitchDefault();
		container.setDefaultEntry(obj);
		initializer.accept(obj);
	}

	public void entry(final Switch container, final Consumer<SwitchEntry> initializer) {
		final SwitchEntry obj = AmaltheaFactory.eINSTANCE.createSwitchEntry();
		container.getEntries().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void condition_OR(final SwitchEntry container, final Consumer<ConditionDisjunction> initializer) {
		final ConditionDisjunction obj = AmaltheaFactory.eINSTANCE.createConditionDisjunction();
		container.setCondition(obj);
		initializer.accept(obj);
	}

	public void condition(final ConditionDisjunction container, final ModeLabel label, final RelationalOperator relation, final String value) {
		final ModeValueCondition obj = AmaltheaFactory.eINSTANCE.createModeValueCondition();
		obj.setLabel(label);
		obj.setRelation(relation);
		obj.setValue(value);
		container.getEntries().add(obj);
	}

	public void condition(final ConditionDisjunction container, final ModeLabel label1, final RelationalOperator relation, final ModeLabel label2) {
		final ModeLabelCondition obj = AmaltheaFactory.eINSTANCE.createModeLabelCondition();
		obj.setLabel1(label1);
		obj.setRelation(relation);
		obj.setLabel2(label2);
		container.getEntries().add(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void condition_AND(final ConditionDisjunction container, final Consumer<ConditionConjunction> initializer) {
		final ConditionConjunction obj = AmaltheaFactory.eINSTANCE.createConditionConjunction();
		container.getEntries().add(obj);
		initializer.accept(obj);
	}

	public void condition(final ConditionConjunction container, final ModeLabel label, final RelationalOperator relation, final String value) {
		final ModeValueCondition obj = AmaltheaFactory.eINSTANCE.createModeValueCondition();
		obj.setLabel(label);
		obj.setRelation(relation);
		obj.setValue(value);
		container.getEntries().add(obj);
	}

	public void condition(final ConditionConjunction container, final ModeLabel label1, final RelationalOperator relation, final ModeLabel label2) {
		final ModeLabelCondition obj = AmaltheaFactory.eINSTANCE.createModeLabelCondition();
		obj.setLabel1(label1);
		obj.setRelation(relation);
		obj.setLabel2(label2);
		container.getEntries().add(obj);
	}

	// ********** IActivityGraphItemContainer - Items - ProbabilitySwitch **********

	public void probabilitySwitch(final IActivityGraphItemContainer container, final Consumer<ProbabilitySwitch> initializer) {
		final ProbabilitySwitch obj = AmaltheaFactory.eINSTANCE.createProbabilitySwitch();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void entry(final ProbabilitySwitch container, final Consumer<ProbabilitySwitchEntry> initializer) {
		final ProbabilitySwitchEntry obj = AmaltheaFactory.eINSTANCE.createProbabilitySwitchEntry();
		container.getEntries().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - SchedulePoint **********

	public void schedulePoint(final IActivityGraphItemContainer container, final Consumer<SchedulePoint> initializer) {
		final SchedulePoint obj = AmaltheaFactory.eINSTANCE.createSchedulePoint();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - InterProcessTrigger **********

	public void interProcessTrigger(final IActivityGraphItemContainer container, final Consumer<InterProcessTrigger> initializer) {
		final InterProcessTrigger obj = AmaltheaFactory.eINSTANCE.createInterProcessTrigger();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - EnforcedMigration **********

	public void enforcedMigration(final IActivityGraphItemContainer container, final Consumer<EnforcedMigration> initializer) {
		final EnforcedMigration obj = AmaltheaFactory.eINSTANCE.createEnforcedMigration();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - TerminateProcess **********

	public void terminateProcess(final IActivityGraphItemContainer container, final Consumer<TerminateProcess> initializer) {
		final TerminateProcess obj = AmaltheaFactory.eINSTANCE.createTerminateProcess();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - OS events **********

	public void clearEvent(final IActivityGraphItemContainer container, final Consumer<ClearEvent> initializer) {
		final ClearEvent obj = AmaltheaFactory.eINSTANCE.createClearEvent();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void waitEvent(final IActivityGraphItemContainer container, final Consumer<WaitEvent> initializer) {
		final WaitEvent obj = AmaltheaFactory.eINSTANCE.createWaitEvent();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void setEvent(final IActivityGraphItemContainer container, final Consumer<SetEvent> initializer) {
		final SetEvent obj = AmaltheaFactory.eINSTANCE.createSetEvent();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void eventMask(final ClearEvent clearEvent, final OsEvent...osEvents) {
		final EventMask eventMask = AmaltheaFactory.eINSTANCE.createEventMask();
		for(OsEvent osEvent:osEvents) {
			if (osEvent != null) {
				eventMask.getEvents().add(osEvent);
			}
		}
		clearEvent.setEventMask(eventMask);
	}

	public void eventMask(final WaitEvent waitEvent, final OsEvent...osEvents) {
		final EventMask eventMask = AmaltheaFactory.eINSTANCE.createEventMask();
		for(OsEvent osEvent:osEvents) {
			if (osEvent != null) {
				eventMask.getEvents().add(osEvent);
			}
		}
		waitEvent.setEventMask(eventMask);
	}

	public void eventMask(final SetEvent setEvent, final OsEvent...osEvents) {
		final EventMask eventMask = AmaltheaFactory.eINSTANCE.createEventMask();
		for(OsEvent osEvent:osEvents) {
			if (osEvent != null) {
				eventMask.getEvents().add(osEvent);
			}
		}
		setEvent.setEventMask(eventMask);
	}

	// ********** Runnable Parameters **********

	public void parameter(final Runnable container, final Consumer<RunnableParameter> initializer) {
		final RunnableParameter obj = AmaltheaFactory.eINSTANCE.createRunnableParameter();
		container.getParameters().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - Group **********

	public void group(final IActivityGraphItemContainer container, final Consumer<Group> initializer) {
		final Group obj = AmaltheaFactory.eINSTANCE.createGroup();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - LabelAccess **********

	public void labelAccess(final IActivityGraphItemContainer container, final Consumer<LabelAccess> initializer) {
		final LabelAccess obj = AmaltheaFactory.eINSTANCE.createLabelAccess();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void modeLabelAccess(final IActivityGraphItemContainer container, final Consumer<ModeLabelAccess> initializer) {
		final ModeLabelAccess obj = AmaltheaFactory.eINSTANCE.createModeLabelAccess();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - ChannelAccess **********

	public void channelSend(final IActivityGraphItemContainer container, final Consumer<ChannelSend> initializer) {
		final ChannelSend obj = AmaltheaFactory.eINSTANCE.createChannelSend();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void channelReceive(final IActivityGraphItemContainer container, final Consumer<ChannelReceive> initializer) {
		final ChannelReceive obj = AmaltheaFactory.eINSTANCE.createChannelReceive();
		container.getItems().add(obj);
		initializer.accept(obj);
	}
	
	// ********** IActivityGraphItemContainer - Items - SemaphoreAccess **********
	
	public void semaphoreRequest(final IActivityGraphItemContainer container, final Consumer<SemaphoreAccess> initializer) {
		final SemaphoreAccess obj = AmaltheaFactory.eINSTANCE.createSemaphoreAccess();
		obj.setAccess(SemaphoreAccessEnum.REQUEST);
		container.getItems().add(obj);
		initializer.accept(obj);
	}
	
	public void semaphoreExclusive(final IActivityGraphItemContainer container, final Consumer<SemaphoreAccess> initializer) {
		final SemaphoreAccess obj = AmaltheaFactory.eINSTANCE.createSemaphoreAccess();
		obj.setAccess(SemaphoreAccessEnum.EXCLUSIVE);
		container.getItems().add(obj);
		initializer.accept(obj);
	}
	
	public void semaphoreRelease(final IActivityGraphItemContainer container, final Consumer<SemaphoreAccess> initializer) {
		final SemaphoreAccess obj = AmaltheaFactory.eINSTANCE.createSemaphoreAccess();
		obj.setAccess(SemaphoreAccessEnum.RELEASE);
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - RunnableCall **********

	public void runnableCall(final IActivityGraphItemContainer container, final Consumer<RunnableCall> initializer) {
		final RunnableCall obj = AmaltheaFactory.eINSTANCE.createRunnableCall();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - ServerCall **********

	public void asynchronousServerCall(final IActivityGraphItemContainer container, final Consumer<AsynchronousServerCall> initializer) {
		final AsynchronousServerCall obj = AmaltheaFactory.eINSTANCE.createAsynchronousServerCall();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	// ********** IActivityGraphItemContainer - Items - ExecutionNeed **********

	public void execNeed(final IActivityGraphItemContainer container, final Consumer<ExecutionNeed> initializer) {
		final ExecutionNeed obj = AmaltheaFactory.eINSTANCE.createExecutionNeed();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void need(final ExecutionNeed container, final String key, final IDiscreteValueDeviation need) {
		container.getNeeds().put(key, need);
	}

	// ********** IActivityGraphItemContainer - Items - Ticks **********

	public void ticks(final IActivityGraphItemContainer container, final Consumer<Ticks> initializer) {
		final Ticks obj = AmaltheaFactory.eINSTANCE.createTicks();
		container.getItems().add(obj);
		initializer.accept(obj);
	}

	public void defaultDeviation(final Ticks container, final IDiscreteValueDeviation ticks) {
		container.setDefault(ticks);
	}

	public void defaultConstant(final Ticks container, final long ticks) {
		final DiscreteValueConstant obj = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant();
		obj.setValue(ticks);
		container.setDefault(obj);
	}

	public void extended(final Ticks container, final ProcessingUnitDefinition puDef, final IDiscreteValueDeviation ticks) {
		container.getExtended().put(puDef, ticks);
	}

	// ********** IActivityGraphItemContainer - Items - to be extended **********

}
