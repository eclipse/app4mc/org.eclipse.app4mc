/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.emf;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

public class AmaltheaEObjectImpl extends MinimalEObjectImpl.Container {

	//	Overwrite methods if special behavior is required

}
