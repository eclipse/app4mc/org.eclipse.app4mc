/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.emf;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class AmaltheaResourceSetImpl extends ResourceSetImpl {

	/**
	 * Try to find the object in all contained resources
	 */
	@Override
	public EObject getEObject(URI uri, boolean loadOnDemand) {
		if (uri == null)
			return null;

		for (Resource resource : resources) {
			EObject eObject = resource.getEObject(uri.fragment());
			if (eObject != null)
				return eObject;
		}

		return null;
	}

}
