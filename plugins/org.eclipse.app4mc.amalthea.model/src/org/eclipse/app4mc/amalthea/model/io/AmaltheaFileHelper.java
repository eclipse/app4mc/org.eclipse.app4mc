/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AmaltheaFileHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(AmaltheaFileHelper.class);

	public static final String INVALID = "invalid";
	public static final String MODEL_FILE_EXTENSION = "amxmi";

	// Suppress default constructor
	private AmaltheaFileHelper() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Check if the given file is a zip archive.
	 *
	 * @param file The {@link File} to check.
	 * @return <code>true</code> if the given file is a zip archive,
	 *         <code>false</code> if not.
	 */
	public static boolean isZipFile(File file) {
		boolean result = false;

		if (file != null) {
			try (ZipFile f = new ZipFile(file)) {
				// zipped file detected
				result = true;
			} catch (IOException e) {
				// IOException includes ZipException -> not a zip file
			}
		}

		return result;
	}

	/**
	 * Extract the model namespace of the given input file.
	 *
	 * @param inputFile The file for which the model namespace is
	 *                       requested.
	 * @return The model namespace of the given input file or <i>invalid</i> if
	 *         an error occurs on parsing or the model namespace is invalid.
	 */
	public static String getNamespace(File inputFile) {
		String result = INVALID;

		if (inputFile != null && inputFile.exists() && inputFile.getName().endsWith(MODEL_FILE_EXTENSION)) {
			Path inputPath = Paths.get(inputFile.toURI());
			if (isZipFile(inputFile)) {
				return getNamespaceFromZip(inputPath);
			} else {
				return getNamespaceFromFile(inputPath);
			}
		}

		return result;
	}

	/**
	 * Extract the model namespace of the given input file.
	 *
	 * @param inputPath The {@link Path} of the model file for which the model
	 *                  namespace is requested.
	 * @return The model namespace of the given input file or <i>invalid</i> if an
	 *         error occurs on parsing or the model namespace is invalid.
	 */
	public static String getNamespaceFromFile(Path inputPath) {
		try (InputStream input = Files.newInputStream(inputPath)) {
			return getNamespaceFromStream(input);
		} catch (IOException | XMLStreamException e) {
			LOGGER.error("Error on parsing input model file for namespace", e);
		}

		return INVALID;
	}

	/**
	 * Extract the model namespace of the given input file.
	 *
	 * @param inputPath The {@link Path} of the model file for which the model
	 *                  namespace is requested.
	 * @return The model namespace of the given input file or <i>invalid</i> if an
	 *         error occurs on parsing or the model namespace is invalid.
	 */
	public static String getNamespaceFromZip(Path inputPath) {
		try (ZipInputStream input = new ZipInputStream(Files.newInputStream(inputPath))) {
			ZipEntry zipEntry = input.getNextEntry();
			if (zipEntry != null) {
				return getNamespaceFromStream(input);
			}
		} catch (IOException | XMLStreamException e) {
			LOGGER.error("Error on parsing input model file for namespace", e);
		}

		return INVALID;
	}

	private static String getNamespaceFromStream(InputStream input) throws XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		xmlInputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		xmlInputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

		XMLEventReader reader = xmlInputFactory.createXMLEventReader(input);
		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();
				if ("am".equals(startElement.getName().getPrefix())) {
					return startElement.getNamespaceURI("am");
				}
			}
		}

		return INVALID;
	}

	/**
	 * Check if the model namespace of the provided file is of the current
	 * supported model version.
	 *
	 * @param toCheck The model file to check.
	 * @return <code>true</code> if the model namespace is the current supported model
	 *         namespace, <code>false</code> if not.
	 */
	public static boolean isNamespaceValid(File toCheck) {
		String namespaceFromFile = getNamespace(toCheck);
		return ModelUtil.MODEL_NAMESPACE.equals(namespaceFromFile);
	}

	/**
	 * Extract the model version of the given input model file.
	 *
	 * @param inputModelFile The model file for which the model version is
	 *                       requested.
	 * @return The model version of the given input model file or <i>invalid</i> if
	 *         an error occurs on parsing or the model version is invalid.
	 */
	public static String getModelVersion(File inputModelFile) {
		return getVersionFromNamespace(getNamespace(inputModelFile));
	}

	/**
	 * Extract the model version of the given input model file.
	 *
	 * @param inputPath The {@link Path} to the model file.
	 * @return The model version of the given input model file or <i>invalid</i> if
	 *         an error occurs on parsing or the model version is invalid.
	 */
	public static String getModelVersionFromFile(Path inputPath) {
		return getVersionFromNamespace(getNamespaceFromFile(inputPath));
	}

	/**
	 * Extract the model version of the given zipped input model file.
	 *
	 * @param inputPath The {@link Path} to the model file.
	 * @return The model version of the given input model file or <i>invalid</i> if
	 *         an error occurs on parsing or the model version is invalid.
	 */
	public static String getModelVersionFromZip(Path inputPath) {
		return getVersionFromNamespace(getNamespaceFromZip(inputPath));
	}

	private static String getVersionFromNamespace(String url) {
		if (url == null || url.equals(INVALID))
			return INVALID;

		return url.lastIndexOf('/') == -1
				? url
				: url.substring(url.lastIndexOf('/') + 1);
	}

	/**
	 * Check if the model version of the provided file is of the current
	 * supported model version.
	 *
	 * @param toCheck The model file to check.
	 * @return <code>true</code> if the model version is the current supported model
	 *         version, <code>false</code> if not.
	 */
	public static boolean isModelVersionValid(File toCheck) {
		String versionFromFile = getModelVersion(toCheck);
		return ModelUtil.MODEL_VERSION.equals(versionFromFile);
	}

	/**
	 * Checks if the file with the given filename is an Amalthea model file by
	 * checking the file extension.
	 *
	 * @param filename the name of the file to check.
	 * @return <code>true</code> if the given file is an Amalthea model file,
	 *         <code>false</code> if not.
	 */
	public static boolean isModelFile(String filename) {
		String fileExtension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
		return isModelFileExtension(fileExtension);
	}

	/**
	 * Checks if the given file extension is an Amalthea model file extension.
	 *
	 * @param fileExtension the file extension to check.
	 * @return <code>true</code> if the given file extension is an Amalthea model
	 *         file extension, <code>false</code> if not.
	 */
	public static boolean isModelFileExtension(String fileExtension) {
		return MODEL_FILE_EXTENSION.equals(fileExtension);
	}

}
