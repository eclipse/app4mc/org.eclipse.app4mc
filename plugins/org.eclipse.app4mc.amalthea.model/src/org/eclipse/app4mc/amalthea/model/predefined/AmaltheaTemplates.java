/**
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch - initial API and implementation
 */

package org.eclipse.app4mc.amalthea.model.predefined;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.ParameterType;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Parameter;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;

public class AmaltheaTemplates {

	// Suppress default constructor
	private AmaltheaTemplates() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Adds a standard scheduler definition with required parameters to the OSModel.
	 *
	 * If elements with the same unique name are already defined then the creation of that part is skipped.
	 *
	 * @param os
	 * @param standardScheduler
	 * @return The just added or existing scheduler definition. <code>NULL</code> if some error occurred.
	 */
	public static SchedulerDefinition addStandardSchedulerDefinition(OSModel os, StandardSchedulers.Algorithm standardScheduler) {
		if (os == null || standardScheduler == null) return null;

		String schedulerName = standardScheduler.getAlgorithmName();
		if (schedulerName == null) return null;

		@SuppressWarnings("null")
		Set<SchedulerDefinition> existingSchedulerDefs =
					AmaltheaIndex.getElements(os, schedulerName, SchedulerDefinition.class);
		@SuppressWarnings("null")
		Map<String, SchedulingParameterDefinition> existingParameterDefs =
					AmaltheaIndex.getElements(os, SchedulingParameterDefinition.class).stream()
						.collect(Collectors.toMap(SchedulingParameterDefinition::getName, Function.identity()));

		// create scheduler definition (only if not already available)
		if (existingSchedulerDefs.isEmpty()) {
			// create definition
			SchedulerDefinition schedulerDef = AmaltheaFactory.eINSTANCE.createSchedulerDefinition();
			schedulerDef.setName(standardScheduler.getAlgorithmName());
			schedulerDef.setDescription(standardScheduler.getDescription());
			schedulerDef.setHasExactlyOneChild(standardScheduler.hasExactlyOneChild());
			schedulerDef.setPassesParametersUpwards(standardScheduler.passesParametersUpwards());
			schedulerDef.setRequiresParentScheduler(standardScheduler.requiresParentScheduler());
			os.getSchedulerDefinitions().add(schedulerDef);

			// link to algorithm parameter definition (and create if necessary)
			for (Parameter stdParam : standardScheduler.getAlgorithmParameters()) {
				SchedulingParameterDefinition paramDef = existingParameterDefs.get(stdParam.getParameterName());
				if (paramDef == null) {
					paramDef = addNewParameterDefinition(os, stdParam);
				}
				schedulerDef.getAlgorithmParameters().add(paramDef);
			}
			// link to process parameter definition (and create if necessary)
			for (Parameter stdParam : standardScheduler.getProcessParameters()) {
				SchedulingParameterDefinition paramDef = existingParameterDefs.get(stdParam.getParameterName());
				if (paramDef == null) {
					paramDef = addNewParameterDefinition(os, stdParam);
				}
				schedulerDef.getProcessParameters().add(paramDef);
			}
			return schedulerDef;
		} else {
			return existingSchedulerDefs.iterator().next();
		}
	}

	/**
	 * Adds a standard scheduling parameter definition to the OSModel.
	 *
	 * If an element with the same unique name is already defined then the creation is skipped.
	 *
	 * @param os
	 * @param standardParameter
	 * @return The just added or existing scheduling parameter definition. <code>NULL</code> if some error occurred.
	 */
	public static SchedulingParameterDefinition addStandardSchedulingParameterDefinition(OSModel os, StandardSchedulers.Parameter standardParameter) {
		if (os == null || standardParameter == null) return null;

		String parameterName = standardParameter.getParameterName();
		if (parameterName == null) return null;

		@SuppressWarnings("null")
		Set<SchedulingParameterDefinition> existingParameterDefs =
			AmaltheaIndex.getElements(os, parameterName, SchedulingParameterDefinition.class);

		// create scheduling parameter definition (only if not already available)
		if (existingParameterDefs.isEmpty()) {
			// create definition
			return addNewParameterDefinition(os, standardParameter);
		} else {
			return existingParameterDefs.iterator().next();
		}
	}

	private static SchedulingParameterDefinition addNewParameterDefinition(OSModel os, Parameter stdParam) {
		// create parameter definition
		SchedulingParameterDefinition paramDef = AmaltheaFactory.eINSTANCE.createSchedulingParameterDefinition();

		// set simple attributes
		paramDef.setName(stdParam.getParameterName());
		paramDef.setMandatory(stdParam.isMandatory());
		paramDef.setMany(stdParam.isMany());

		// set type and default value (if available)
		String value = stdParam.getDefaultValue();
		switch (stdParam.getType()) {
		case BOOL:
			paramDef.setType(ParameterType.BOOL);
			if (value != null) {
				paramDef.setDefaultValue(FactoryUtil.createBooleanObject(Boolean.parseBoolean(value)));
			}
			break;
		case INTEGER:
			paramDef.setType(ParameterType.INTEGER);
			if (value != null) {
				paramDef.setDefaultValue(FactoryUtil.createIntegerObject(Integer.parseInt(value)));
			}
			break;
		case FLOAT:
			paramDef.setType(ParameterType.FLOAT);
			if (value != null) {
				paramDef.setDefaultValue(FactoryUtil.createFloatObject(Float.parseFloat(value)));
			}
			break;
		case TIME:
			paramDef.setType(ParameterType.TIME);
			if (value != null) {
				paramDef.setDefaultValue(FactoryUtil.createTime(value));
			}
			break;
		case STRING:
			paramDef.setType(ParameterType.STRING);
			if (value != null) {
				paramDef.setDefaultValue(FactoryUtil.createStringObject(value));
			}
			break;
		default:
			break;
		}

		// add definition to model
		os.getSchedulingParameterDefinitions().add(paramDef);

		return paramDef;
	}

}
