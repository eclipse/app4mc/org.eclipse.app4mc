/**
 ********************************************************************************
 * Copyright (c) 2022 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.stimuli;

import java.math.BigInteger;

import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;
import org.eclipse.emf.common.util.EMap;

/**
 * Event Model functions for APP4MC's {@link RelativePeriodicStimulus}.
 * <p>
 * For a full documentation of the underlying functions, please refer to the
 * <a href="https://doi.org/10.1016/j.sysarc.2021.102343">related
 * publication</a>, Section 3.2.
 */
public class EMPeriodicRelative implements IEventModel {
	/**
	 * Upper bound (i.e., maximum) distance between two subsequent activation
	 * events.
	 */
	private Time upperBoundDistance;
	/**
	 * Lower bound (i.e., minimum) distance between two subsequent activation
	 * events.
	 */
	private Time lowerBoundDistance;

	public EMPeriodicRelative() {
		// Empty on purpose
	}

	@Override
	public long etaPlus(final Time dt) {
		if (dt.getValue().compareTo(BigInteger.ZERO) == 0) {
			return 0;
		}

		return (long) Math.ceil(dt.divide(this.lowerBoundDistance));
	}

	@Override
	public long etaMinus(final Time dt) {
		if (dt.getValue().compareTo(BigInteger.ZERO) == 0) {
			return 0;
		}

		return (long) Math.floor(dt.divide(this.upperBoundDistance));
	}

	@Override
	public Time deltaPlus(final long n) {
		// Function is only valid for n >= 2, return null on invalid parameter
		if (n < 2) {
			return null;
		}

		return this.upperBoundDistance.multiply(n - 1);
	}

	@Override
	public Time deltaMinus(final long n) {
		// Function is only valid for n >= 2, return null on invalid parameter
		if (n < 2) {
			return null;
		}

		return this.lowerBoundDistance.multiply(n - 1);
	}

	@Override
	public double getUtilization(Process process, TimeType tt, EMap<ModeLabel, String> modes) {
		final Time time = RuntimeUtil.getExecutionTimeForProcess(process, modes, tt);
		if (tt.equals(TimeType.BCET)) {
			// Minimum load occurs when activation events occur with maximum distance
			return time.divide(this.upperBoundDistance);
		} else if (tt.equals(TimeType.WCET)) {
			// Maximum load occurs when activation events occur with minimum distance
			return time.divide(this.lowerBoundDistance);
		} else if (tt.equals(TimeType.ACET)) {
			final Time averageDistance = this.lowerBoundDistance.add(this.upperBoundDistance).multiply(0.5);
			return time.divide(averageDistance);
		}
		// Unsupported configuration
		return -1;
	}

	public Time getUpperBoundDistance() {
		return upperBoundDistance;
	}

	public void setUpperBoundDistance(Time upperBoundDistance) {
		this.upperBoundDistance = upperBoundDistance;
	}

	public Time getLowerBoundDistance() {
		return lowerBoundDistance;
	}

	public void setLowerBoundDistance(Time lowerBoundDistance) {
		this.lowerBoundDistance = lowerBoundDistance;
	}
}
