/**
 ********************************************************************************
 * Copyright (c) 2022 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.stimuli;

import java.math.BigInteger;

import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.SingleStimulus;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;
import org.eclipse.emf.common.util.EMap;

/**
 * Event Model functions for APP4MC's {@link SingleStimulus}.
 * <p>
 * For a full documentation of the underlying functions, please refer to the
 * <a href="https://doi.org/10.1016/j.sysarc.2021.102343">related
 * publication</a>, Section 3.5.
 */
public class EMSingle implements IEventModel {
	// To the average human being, 292 Billion years should feel like infinity.
	private static final Time INFINITY = FactoryUtil.createTime(Long.MAX_VALUE, TimeUnit.S);

	public EMSingle() {
		// Empty on purpose
	}

	@Override
	public long etaPlus(final Time dt) {
		if (dt.getValue().compareTo(BigInteger.ZERO) == 0) {
			return 0;
		}
		return 1;
	}

	@Override
	public long etaMinus(final Time dt) {
		return 0;
	}

	@Override
	public Time deltaPlus(final long n) {
		return INFINITY;
	}

	@Override
	public Time deltaMinus(final long n) {
		return INFINITY;
	}

	@Override
	public double getUtilization(Process process, TimeType tt, EMap<ModeLabel, String> modes) {
		// The utilization of a process that is only activated once throughout an
		// application's execution will converge towards 0.
		return 0.0;
	}
}
