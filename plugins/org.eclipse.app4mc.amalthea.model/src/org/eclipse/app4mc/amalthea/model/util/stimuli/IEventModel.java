/**
 ********************************************************************************
 * Copyright (c) 2022 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.stimuli;

import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType;
import org.eclipse.emf.common.util.EMap;

public interface IEventModel {
	/**
	 * Returns the upper bound on the number of times a process is released in the
	 * <a href="#{@link}">{@link Time}</a> interval dt.
	 *
	 * @param dt size of the time interval
	 * @return maximum number of times a process is released
	 */
	public long etaPlus(Time dt);

	/**
	 * Returns the lower bound on the number of times a process is released in the
	 * <a href="#{@link}">{@link Time}</a> interval dt.
	 *
	 * @param dt size of the time interval
	 * @return minimum number of times a process is released
	 */
	public long etaMinus(Time dt);

	/**
	 * Returns the upper bound on the distance between n activation events.
	 *
	 * @param n number of activation events
	 * @return maximum distance between activation events
	 */
	public Time deltaPlus(long n);

	/**
	 * Returns the lower bound on the distance between n activation events.
	 *
	 * @param n number of activation events
	 * @return minimum distance between activation events
	 */
	public Time deltaMinus(long n);

	/**
	 * Returns the {@link TimeType} utilization of the given {@link Process} when
	 * activated using the given {@link IEventModel}.
	 * 
	 * @param process The process for which utilization shall be calculated for.
	 * @param tt      Type of the utilization bound (worst, average, or best case
	 *                assumption) to calculate.
	 * @param modes   What modes to consider during utilization calculation (use
	 *                {@code null} to disable filtering and consider all operation
	 *                modes)
	 * @return
	 */
	public double getUtilization(Process process, TimeType tt, EMap<ModeLabel, String> modes);
}
