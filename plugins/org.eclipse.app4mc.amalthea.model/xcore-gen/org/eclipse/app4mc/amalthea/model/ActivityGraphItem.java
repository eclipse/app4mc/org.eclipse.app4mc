/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Graph Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An abstract item of a ActivityGraph.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingExecutable <em>Containing Executable</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingProcess <em>Containing Process</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingRunnable <em>Containing Runnable</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingActivityGraph <em>Containing Activity Graph</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getActivityGraphItem()
 * @model abstract="true"
 * @generated
 */
public interface ActivityGraphItem extends BaseObject {
	/**
	 * Returns the value of the '<em><b>Containing Executable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @since 1.2
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Executable</em>' reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getActivityGraphItem_ContainingExecutable()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	IExecutable getContainingExecutable();

	/**
	 * Returns the value of the '<em><b>Containing Process</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @deprecated Use getContainingExecutable() instead.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Process</em>' reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getActivityGraphItem_ContainingProcess()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	@Deprecated
	org.eclipse.app4mc.amalthea.model.Process getContainingProcess();

	/**
	 * Returns the value of the '<em><b>Containing Runnable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * @deprecated Use getContainingExecutable() instead.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Runnable</em>' reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getActivityGraphItem_ContainingRunnable()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	@Deprecated
	org.eclipse.app4mc.amalthea.model.Runnable getContainingRunnable();

	/**
	 * Returns the value of the '<em><b>Containing Activity Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Activity Graph</em>' reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getActivityGraphItem_ContainingActivityGraph()
	 * @model resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	ActivityGraph getContainingActivityGraph();

} // ActivityGraphItem
