/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arithmetic Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ArithmeticExpression#getOperand1 <em>Operand1</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ArithmeticExpression#getOperand2 <em>Operand2</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ArithmeticExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getArithmeticExpression()
 * @model
 * @generated
 */
public interface ArithmeticExpression extends ILocalModeValueSource {
	/**
	 * Returns the value of the '<em><b>Operand1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand1</em>' containment reference.
	 * @see #setOperand1(ILocalModeValueSource)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getArithmeticExpression_Operand1()
	 * @model containment="true"
	 * @generated
	 */
	ILocalModeValueSource getOperand1();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ArithmeticExpression#getOperand1 <em>Operand1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand1</em>' containment reference.
	 * @see #getOperand1()
	 * @generated
	 */
	void setOperand1(ILocalModeValueSource value);

	/**
	 * Returns the value of the '<em><b>Operand2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operand2</em>' containment reference.
	 * @see #setOperand2(ILocalModeValueSource)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getArithmeticExpression_Operand2()
	 * @model containment="true"
	 * @generated
	 */
	ILocalModeValueSource getOperand2();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ArithmeticExpression#getOperand2 <em>Operand2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operand2</em>' containment reference.
	 * @see #getOperand2()
	 * @generated
	 */
	void setOperand2(ILocalModeValueSource value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.app4mc.amalthea.model.ArithmeticOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see org.eclipse.app4mc.amalthea.model.ArithmeticOperator
	 * @see #setOperator(ArithmeticOperator)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getArithmeticExpression_Operator()
	 * @model unique="false"
	 * @generated
	 */
	ArithmeticOperator getOperator();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ArithmeticExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see org.eclipse.app4mc.amalthea.model.ArithmeticOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ArithmeticOperator value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	boolean isNumeric();

} // ArithmeticExpression
