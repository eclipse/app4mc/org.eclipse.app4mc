/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentInterface#getDataType <em>Data Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentInterface#getSubInterfaces <em>Sub Interfaces</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentInterface()
 * @model abstract="true"
 * @generated
 */
public interface ComponentInterface extends ReferableBaseObject, ITaggable {
	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' reference.
	 * @see #setDataType(TypeDefinition)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentInterface_DataType()
	 * @model
	 * @generated
	 */
	TypeDefinition getDataType();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ComponentInterface#getDataType <em>Data Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' reference.
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(TypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Sub Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.SubInterface}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.SubInterface#getContainingInterface <em>Containing Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Interfaces</em>' containment reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentInterface_SubInterfaces()
	 * @see org.eclipse.app4mc.amalthea.model.SubInterface#getContainingInterface
	 * @model opposite="containingInterface" containment="true"
	 * @generated
	 */
	EList<SubInterface> getSubInterfaces();

} // ComponentInterface
