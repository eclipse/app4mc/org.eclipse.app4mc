/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentPort#getContainingComponent <em>Containing Component</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentPort#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentPort#getInterface <em>Interface</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentPort()
 * @model
 * @generated
 */
public interface ComponentPort extends ReferableBaseObject, ITaggable {
	/**
	 * Returns the value of the '<em><b>Containing Component</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.Component#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Component</em>' container reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentPort_ContainingComponent()
	 * @see org.eclipse.app4mc.amalthea.model.Component#getPorts
	 * @model opposite="ports" transient="false" changeable="false"
	 * @generated
	 */
	Component getContainingComponent();

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.app4mc.amalthea.model.InterfaceKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.app4mc.amalthea.model.InterfaceKind
	 * @see #setKind(InterfaceKind)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentPort_Kind()
	 * @model unique="false"
	 * @generated
	 */
	InterfaceKind getKind();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ComponentPort#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.app4mc.amalthea.model.InterfaceKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(InterfaceKind value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(ComponentInterface)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentPort_Interface()
	 * @model
	 * @generated
	 */
	ComponentInterface getInterface();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ComponentPort#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(ComponentInterface value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	EList<String> getNamePrefixSegments();

} // ComponentPort
