/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Structure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentStructure#getStructureType <em>Structure Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentStructure#getSubStructures <em>Sub Structures</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentStructure#getMemberObjects <em>Member Objects</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentStructure()
 * @model
 * @generated
 */
public interface ComponentStructure extends ReferableObject {
	/**
	 * Returns the value of the '<em><b>Structure Type</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Structure Type</em>' attribute.
	 * @see #setStructureType(String)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentStructure_StructureType()
	 * @model default="" unique="false"
	 * @generated
	 */
	String getStructureType();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ComponentStructure#getStructureType <em>Structure Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Structure Type</em>' attribute.
	 * @see #getStructureType()
	 * @generated
	 */
	void setStructureType(String value);

	/**
	 * Returns the value of the '<em><b>Sub Structures</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.ComponentStructure}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Structures</em>' containment reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentStructure_SubStructures()
	 * @model containment="true"
	 * @generated
	 */
	EList<ComponentStructure> getSubStructures();

	/**
	 * Returns the value of the '<em><b>Member Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.IComponentStructureMember}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p><b>Returns an <em>immutable</em> list of objects (IComponentStructureMember) referring to this structure.</b></p>
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Member Objects</em>' reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentStructure_MemberObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<IComponentStructureMember> getMemberObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	ComponentStructure getContainingStructure();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	String getDefaultNameSeparator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	EList<String> getNamePrefixSegments();

} // ComponentStructure
