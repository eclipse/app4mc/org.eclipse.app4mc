/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Connector#getContainingSystem <em>Containing System</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Connector#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Connector#getTargetPort <em>Target Port</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Connector#getImplementedInterfaces <em>Implemented Interfaces</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getConnector()
 * @model
 * @generated
 */
public interface Connector extends BaseObject, INamed, ITaggable {
	/**
	 * Returns the value of the '<em><b>Containing System</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.ISystem#getConnectors <em>Connectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing System</em>' container reference.
	 * @see #setContainingSystem(ISystem)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getConnector_ContainingSystem()
	 * @see org.eclipse.app4mc.amalthea.model.ISystem#getConnectors
	 * @model opposite="connectors" transient="false"
	 * @generated
	 */
	ISystem getContainingSystem();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.Connector#getContainingSystem <em>Containing System</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing System</em>' container reference.
	 * @see #getContainingSystem()
	 * @generated
	 */
	void setContainingSystem(ISystem value);

	/**
	 * Returns the value of the '<em><b>Source Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Port</em>' containment reference.
	 * @see #setSourcePort(QualifiedPort)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getConnector_SourcePort()
	 * @model containment="true"
	 * @generated
	 */
	QualifiedPort getSourcePort();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.Connector#getSourcePort <em>Source Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Port</em>' containment reference.
	 * @see #getSourcePort()
	 * @generated
	 */
	void setSourcePort(QualifiedPort value);

	/**
	 * Returns the value of the '<em><b>Target Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Port</em>' containment reference.
	 * @see #setTargetPort(QualifiedPort)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getConnector_TargetPort()
	 * @model containment="true"
	 * @generated
	 */
	QualifiedPort getTargetPort();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.Connector#getTargetPort <em>Target Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Port</em>' containment reference.
	 * @see #getTargetPort()
	 * @generated
	 */
	void setTargetPort(QualifiedPort value);

	/**
	 * Returns the value of the '<em><b>Implemented Interfaces</b></em>' map.
	 * The key is of type {@link org.eclipse.app4mc.amalthea.model.ComponentInterface},
	 * and the value is of type {@link org.eclipse.app4mc.amalthea.model.Channel},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implemented Interfaces</em>' map.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getConnector_ImplementedInterfaces()
	 * @model mapType="org.eclipse.app4mc.amalthea.model.InterfaceChannel&lt;org.eclipse.app4mc.amalthea.model.ComponentInterface, org.eclipse.app4mc.amalthea.model.Channel&gt;"
	 * @generated
	 */
	EMap<ComponentInterface, Channel> getImplementedInterfaces();

} // Connector
