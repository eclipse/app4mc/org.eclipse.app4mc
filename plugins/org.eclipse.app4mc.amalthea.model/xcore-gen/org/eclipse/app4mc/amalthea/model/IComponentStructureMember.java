/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IComponent Structure Member</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.IComponentStructureMember#getStructure <em>Structure</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getIComponentStructureMember()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IComponentStructureMember extends EObject {
	/**
	 * Returns the value of the '<em><b>Structure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Structure</em>' reference.
	 * @see #setStructure(ComponentStructure)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getIComponentStructureMember_Structure()
	 * @model
	 * @generated
	 */
	ComponentStructure getStructure();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.IComponentStructureMember#getStructure <em>Structure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Structure</em>' reference.
	 * @see #getStructure()
	 * @generated
	 */
	void setStructure(ComponentStructure value);

} // IComponentStructureMember
