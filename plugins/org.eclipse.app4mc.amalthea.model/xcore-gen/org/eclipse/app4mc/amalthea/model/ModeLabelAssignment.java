/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mode Label Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ModeLabelAssignment#getGlobalLabel <em>Global Label</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ModeLabelAssignment#getLocalLabel <em>Local Label</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getModeLabelAssignment()
 * @model
 * @generated
 */
public interface ModeLabelAssignment extends ActivityGraphItem {
	/**
	 * Returns the value of the '<em><b>Global Label</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Label</em>' reference.
	 * @see #setGlobalLabel(ModeLabel)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getModeLabelAssignment_GlobalLabel()
	 * @model required="true"
	 * @generated
	 */
	ModeLabel getGlobalLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ModeLabelAssignment#getGlobalLabel <em>Global Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Global Label</em>' reference.
	 * @see #getGlobalLabel()
	 * @generated
	 */
	void setGlobalLabel(ModeLabel value);

	/**
	 * Returns the value of the '<em><b>Local Label</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Label</em>' reference.
	 * @see #setLocalLabel(LocalModeLabel)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getModeLabelAssignment_LocalLabel()
	 * @model required="true"
	 * @generated
	 */
	LocalModeLabel getLocalLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ModeLabelAssignment#getLocalLabel <em>Local Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Label</em>' reference.
	 * @see #getLocalLabel()
	 * @generated
	 */
	void setLocalLabel(LocalModeLabel value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" diagnosticsUnique="false" contextUnique="false"
	 * @generated
	 */
	boolean validateInvariants(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ModeLabelAssignment
