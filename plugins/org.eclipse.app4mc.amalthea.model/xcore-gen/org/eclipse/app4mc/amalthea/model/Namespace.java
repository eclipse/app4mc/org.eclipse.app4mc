/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Namespace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Namespace#getNextSegments <em>Next Segments</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Namespace#getPreviousSegment <em>Previous Segment</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Namespace#getMemberObjects <em>Member Objects</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getNamespace()
 * @model
 * @generated
 */
public interface Namespace extends ReferableObject {
	/**
	 * Returns the value of the '<em><b>Next Segments</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.Namespace}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.Namespace#getPreviousSegment <em>Previous Segment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Segments</em>' containment reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getNamespace_NextSegments()
	 * @see org.eclipse.app4mc.amalthea.model.Namespace#getPreviousSegment
	 * @model opposite="previousSegment" containment="true"
	 * @generated
	 */
	EList<Namespace> getNextSegments();

	/**
	 * Returns the value of the '<em><b>Previous Segment</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.Namespace#getNextSegments <em>Next Segments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Segment</em>' container reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getNamespace_PreviousSegment()
	 * @see org.eclipse.app4mc.amalthea.model.Namespace#getNextSegments
	 * @model opposite="nextSegments" transient="false" changeable="false"
	 * @generated
	 */
	Namespace getPreviousSegment();

	/**
	 * Returns the value of the '<em><b>Member Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.INamespaceMember}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * <p><b>Returns an <em>immutable</em> list of objects (INamespaceMember) referring to this namespace.</b></p>
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Member Objects</em>' reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getNamespace_MemberObjects()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<INamespaceMember> getMemberObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	EList<String> getNamePrefixSegments();

} // Namespace
