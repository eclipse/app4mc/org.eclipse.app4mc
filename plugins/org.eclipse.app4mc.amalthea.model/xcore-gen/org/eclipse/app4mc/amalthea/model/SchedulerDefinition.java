/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scheduler Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#getAlgorithmParameters <em>Algorithm Parameters</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#getProcessParameters <em>Process Parameters</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#isRequiresParentScheduler <em>Requires Parent Scheduler</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#isPassesParametersUpwards <em>Passes Parameters Upwards</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#isHasExactlyOneChild <em>Has Exactly One Child</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSchedulerDefinition()
 * @model
 * @generated
 */
public interface SchedulerDefinition extends OsDefinition, IDescription {
	/**
	 * Returns the value of the '<em><b>Algorithm Parameters</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Algorithm Parameters</em>' reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSchedulerDefinition_AlgorithmParameters()
	 * @model
	 * @generated
	 */
	EList<SchedulingParameterDefinition> getAlgorithmParameters();

	/**
	 * Returns the value of the '<em><b>Process Parameters</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Process Parameters</em>' reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSchedulerDefinition_ProcessParameters()
	 * @model
	 * @generated
	 */
	EList<SchedulingParameterDefinition> getProcessParameters();

	/**
	 * Returns the value of the '<em><b>Requires Parent Scheduler</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requires Parent Scheduler</em>' attribute.
	 * @see #setRequiresParentScheduler(boolean)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSchedulerDefinition_RequiresParentScheduler()
	 * @model default="false" unique="false"
	 * @generated
	 */
	boolean isRequiresParentScheduler();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#isRequiresParentScheduler <em>Requires Parent Scheduler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requires Parent Scheduler</em>' attribute.
	 * @see #isRequiresParentScheduler()
	 * @generated
	 */
	void setRequiresParentScheduler(boolean value);

	/**
	 * Returns the value of the '<em><b>Passes Parameters Upwards</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Passes Parameters Upwards</em>' attribute.
	 * @see #setPassesParametersUpwards(boolean)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSchedulerDefinition_PassesParametersUpwards()
	 * @model default="false" unique="false"
	 * @generated
	 */
	boolean isPassesParametersUpwards();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#isPassesParametersUpwards <em>Passes Parameters Upwards</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Passes Parameters Upwards</em>' attribute.
	 * @see #isPassesParametersUpwards()
	 * @generated
	 */
	void setPassesParametersUpwards(boolean value);

	/**
	 * Returns the value of the '<em><b>Has Exactly One Child</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Exactly One Child</em>' attribute.
	 * @see #setHasExactlyOneChild(boolean)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSchedulerDefinition_HasExactlyOneChild()
	 * @model default="false" unique="false"
	 * @generated
	 */
	boolean isHasExactlyOneChild();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.SchedulerDefinition#isHasExactlyOneChild <em>Has Exactly One Child</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Exactly One Child</em>' attribute.
	 * @see #isHasExactlyOneChild()
	 * @generated
	 */
	void setHasExactlyOneChild(boolean value);

} // SchedulerDefinition
