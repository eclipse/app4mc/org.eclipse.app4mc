/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A switch in the ActivityGraph, the selected path depends on the value of the provided conditions.
 * 
 * @since 2.0
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Switch#getEntries <em>Entries</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.Switch#getDefaultEntry <em>Default Entry</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSwitch()
 * @model
 * @generated
 */
public interface Switch extends ActivityGraphItem {
	/**
	 * Returns the value of the '<em><b>Entries</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.SwitchEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entries</em>' containment reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSwitch_Entries()
	 * @model containment="true"
	 * @generated
	 */
	EList<SwitchEntry> getEntries();

	/**
	 * Returns the value of the '<em><b>Default Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Entry</em>' containment reference.
	 * @see #setDefaultEntry(SwitchDefault)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSwitch_DefaultEntry()
	 * @model containment="true"
	 * @generated
	 */
	SwitchDefault getDefaultEntry();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.Switch#getDefaultEntry <em>Default Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Entry</em>' containment reference.
	 * @see #getDefaultEntry()
	 * @generated
	 */
	void setDefaultEntry(SwitchDefault value);

} // Switch
