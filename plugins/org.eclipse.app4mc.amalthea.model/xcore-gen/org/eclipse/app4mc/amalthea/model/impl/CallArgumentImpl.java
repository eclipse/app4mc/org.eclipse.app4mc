/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.CallArgument;
import org.eclipse.app4mc.amalthea.model.DataDependency;
import org.eclipse.app4mc.amalthea.model.IDependsOn;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.ReferableObject;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.RunnableParameter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.xcore.lib.XcoreCollectionLiterals;
import org.eclipse.xtext.xbase.lib.StringExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Argument</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.CallArgumentImpl#getDependsOn <em>Depends On</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.CallArgumentImpl#getContainingCall <em>Containing Call</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.CallArgumentImpl#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallArgumentImpl extends ReferableObjectImpl implements CallArgument {
	/**
	 * The cached value of the '{@link #getDependsOn() <em>Depends On</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependsOn()
	 * @generated
	 * @ordered
	 */
	protected DataDependency dependsOn;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected RunnableParameter parameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallArgumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getCallArgument();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataDependency getDependsOn() {
		return dependsOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDependsOn(DataDependency newDependsOn, NotificationChain msgs) {
		DataDependency oldDependsOn = dependsOn;
		dependsOn = newDependsOn;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON, oldDependsOn, newDependsOn);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDependsOn(DataDependency newDependsOn) {
		if (newDependsOn != dependsOn) {
			NotificationChain msgs = null;
			if (dependsOn != null)
				msgs = ((InternalEObject)dependsOn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON, null, msgs);
			if (newDependsOn != null)
				msgs = ((InternalEObject)newDependsOn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON, null, msgs);
			msgs = basicSetDependsOn(newDependsOn, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON, newDependsOn, newDependsOn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RunnableCall getContainingCall() {
		if (eContainerFeatureID() != AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL) return null;
		return (RunnableCall)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RunnableCall basicGetContainingCall() {
		if (eContainerFeatureID() != AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL) return null;
		return (RunnableCall)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RunnableParameter getParameter() {
		if (parameter != null && parameter.eIsProxy()) {
			InternalEObject oldParameter = (InternalEObject)parameter;
			parameter = (RunnableParameter)eResolveProxy(oldParameter);
			if (parameter != oldParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.CALL_ARGUMENT__PARAMETER, oldParameter, parameter));
			}
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RunnableParameter basicGetParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParameter(RunnableParameter newParameter) {
		RunnableParameter oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.CALL_ARGUMENT__PARAMETER, oldParameter, parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		String _xifexpression = null;
		boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(super.getName());
		if (_isNullOrEmpty) {
			_xifexpression = "access-1";
		}
		else {
			_xifexpression = super.getName();
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getNamePrefixSegments() {
		final BasicEList<String> segments = XcoreCollectionLiterals.<String>newBasicEList();
		RunnableCall _containingCall = this.getContainingCall();
		ActivityGraph _containingActivityGraph = null;
		if (_containingCall!=null) {
			_containingActivityGraph=_containingCall.getContainingActivityGraph();
		}
		EObject _eContainer = null;
		if (_containingActivityGraph!=null) {
			_eContainer=_containingActivityGraph.eContainer();
		}
		final IReferable runnableOrProcess = ((IReferable) _eContainer);
		if ((runnableOrProcess != null)) {
			segments.addAll(runnableOrProcess.getQualifiedNameSegments());
		}
		segments.add("calls");
		List<String> _elvis = null;
		RunnableCall _containingCall_1 = this.getContainingCall();
		org.eclipse.app4mc.amalthea.model.Runnable _runnable = null;
		if (_containingCall_1!=null) {
			_runnable=_containingCall_1.getRunnable();
		}
		EList<String> _qualifiedNameSegments = null;
		if (_runnable!=null) {
			_qualifiedNameSegments=_runnable.getQualifiedNameSegments();
		}
		if (_qualifiedNameSegments != null) {
			_elvis = _qualifiedNameSegments;
		} else {
			_elvis = java.util.Collections.<String>unmodifiableList(org.eclipse.xtext.xbase.lib.CollectionLiterals.<String>newArrayList(""));
		}
		segments.addAll(_elvis);
		segments.add("param");
		String _elvis_1 = null;
		RunnableParameter _parameter = this.getParameter();
		String _name = null;
		if (_parameter!=null) {
			_name=_parameter.getName();
		}
		if (_name != null) {
			_elvis_1 = _name;
		} else {
			_elvis_1 = "";
		}
		segments.add(_elvis_1);
		return segments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return eBasicSetContainer(otherEnd, AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON:
				return basicSetDependsOn(null, msgs);
			case AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL:
				return eBasicSetContainer(null, AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL:
				return eInternalContainer().eInverseRemove(this, AmaltheaPackage.RUNNABLE_CALL__ARGUMENTS, RunnableCall.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON:
				return getDependsOn();
			case AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL:
				if (resolve) return getContainingCall();
				return basicGetContainingCall();
			case AmaltheaPackage.CALL_ARGUMENT__PARAMETER:
				if (resolve) return getParameter();
				return basicGetParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON:
				setDependsOn((DataDependency)newValue);
				return;
			case AmaltheaPackage.CALL_ARGUMENT__PARAMETER:
				setParameter((RunnableParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON:
				setDependsOn((DataDependency)null);
				return;
			case AmaltheaPackage.CALL_ARGUMENT__PARAMETER:
				setParameter((RunnableParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON:
				return dependsOn != null;
			case AmaltheaPackage.CALL_ARGUMENT__CONTAINING_CALL:
				return basicGetContainingCall() != null;
			case AmaltheaPackage.CALL_ARGUMENT__PARAMETER:
				return parameter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDependsOn.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON: return AmaltheaPackage.IDEPENDS_ON__DEPENDS_ON;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDependsOn.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.IDEPENDS_ON__DEPENDS_ON: return AmaltheaPackage.CALL_ARGUMENT__DEPENDS_ON;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.INAMED___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.CALL_ARGUMENT___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == IReferable.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.IREFERABLE___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.CALL_ARGUMENT___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == ReferableObject.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.REFERABLE_OBJECT___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.CALL_ARGUMENT___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == IDependsOn.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AmaltheaPackage.CALL_ARGUMENT___GET_NAME:
				return getName();
			case AmaltheaPackage.CALL_ARGUMENT___GET_NAME_PREFIX_SEGMENTS:
				return getNamePrefixSegments();
		}
		return super.eInvoke(operationID, arguments);
	}

} //CallArgumentImpl
