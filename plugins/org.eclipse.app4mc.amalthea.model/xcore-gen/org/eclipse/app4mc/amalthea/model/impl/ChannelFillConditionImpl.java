/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.ChannelFillCondition;

import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Channel Fill Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ChannelFillConditionImpl#getChannel <em>Channel</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ChannelFillConditionImpl#getRelation <em>Relation</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ChannelFillConditionImpl#getFillLevel <em>Fill Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChannelFillConditionImpl extends BaseObjectImpl implements ChannelFillCondition {
	/**
	 * The cached value of the '{@link #getChannel() <em>Channel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChannel()
	 * @generated
	 * @ordered
	 */
	protected Channel channel;

	/**
	 * The default value of the '{@link #getRelation() <em>Relation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected static final RelationalOperator RELATION_EDEFAULT = RelationalOperator._UNDEFINED_;

	/**
	 * The cached value of the '{@link #getRelation() <em>Relation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected RelationalOperator relation = RELATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFillLevel() <em>Fill Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFillLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int FILL_LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFillLevel() <em>Fill Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFillLevel()
	 * @generated
	 * @ordered
	 */
	protected int fillLevel = FILL_LEVEL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChannelFillConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getChannelFillCondition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Channel getChannel() {
		if (channel != null && channel.eIsProxy()) {
			InternalEObject oldChannel = (InternalEObject)channel;
			channel = (Channel)eResolveProxy(oldChannel);
			if (channel != oldChannel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.CHANNEL_FILL_CONDITION__CHANNEL, oldChannel, channel));
			}
		}
		return channel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Channel basicGetChannel() {
		return channel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setChannel(Channel newChannel) {
		Channel oldChannel = channel;
		channel = newChannel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.CHANNEL_FILL_CONDITION__CHANNEL, oldChannel, channel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RelationalOperator getRelation() {
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRelation(RelationalOperator newRelation) {
		RelationalOperator oldRelation = relation;
		relation = newRelation == null ? RELATION_EDEFAULT : newRelation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.CHANNEL_FILL_CONDITION__RELATION, oldRelation, relation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getFillLevel() {
		return fillLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFillLevel(int newFillLevel) {
		int oldFillLevel = fillLevel;
		fillLevel = newFillLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.CHANNEL_FILL_CONDITION__FILL_LEVEL, oldFillLevel, fillLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__CHANNEL:
				if (resolve) return getChannel();
				return basicGetChannel();
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__RELATION:
				return getRelation();
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__FILL_LEVEL:
				return getFillLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__CHANNEL:
				setChannel((Channel)newValue);
				return;
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__RELATION:
				setRelation((RelationalOperator)newValue);
				return;
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__FILL_LEVEL:
				setFillLevel((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__CHANNEL:
				setChannel((Channel)null);
				return;
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__RELATION:
				setRelation(RELATION_EDEFAULT);
				return;
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__FILL_LEVEL:
				setFillLevel(FILL_LEVEL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__CHANNEL:
				return channel != null;
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__RELATION:
				return relation != RELATION_EDEFAULT;
			case AmaltheaPackage.CHANNEL_FILL_CONDITION__FILL_LEVEL:
				return fillLevel != FILL_LEVEL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (relation: ");
		result.append(relation);
		result.append(", fillLevel: ");
		result.append(fillLevel);
		result.append(')');
		return result.toString();
	}

} //ChannelFillConditionImpl
