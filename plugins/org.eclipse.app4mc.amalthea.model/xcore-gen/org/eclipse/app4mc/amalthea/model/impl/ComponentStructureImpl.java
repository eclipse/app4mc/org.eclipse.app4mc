/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ComponentStructure;
import org.eclipse.app4mc.amalthea.model.IComponentStructureMember;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.ReferableObject;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.xcore.lib.XcoreCollectionLiterals;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Structure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentStructureImpl#getStructureType <em>Structure Type</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentStructureImpl#getSubStructures <em>Sub Structures</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentStructureImpl#getMemberObjects <em>Member Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentStructureImpl extends ReferableObjectImpl implements ComponentStructure {
	/**
	 * The default value of the '{@link #getStructureType() <em>Structure Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStructureType()
	 * @generated
	 * @ordered
	 */
	protected static final String STRUCTURE_TYPE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getStructureType() <em>Structure Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStructureType()
	 * @generated
	 * @ordered
	 */
	protected String structureType = STRUCTURE_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubStructures() <em>Sub Structures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubStructures()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentStructure> subStructures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentStructureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getComponentStructure();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStructureType() {
		return structureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStructureType(String newStructureType) {
		String oldStructureType = structureType;
		structureType = newStructureType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.COMPONENT_STRUCTURE__STRUCTURE_TYPE, oldStructureType, structureType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ComponentStructure> getSubStructures() {
		if (subStructures == null) {
			subStructures = new EObjectContainmentEList<ComponentStructure>(ComponentStructure.class, this, AmaltheaPackage.COMPONENT_STRUCTURE__SUB_STRUCTURES);
		}
		return subStructures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IComponentStructureMember> getMemberObjects() {
		EReference _iComponentStructureMember_Structure = AmaltheaPackage.eINSTANCE.getIComponentStructureMember_Structure();
		return AmaltheaIndex.<IComponentStructureMember>getInverseReferences(this, AmaltheaPackage.eINSTANCE.getComponentStructure_MemberObjects(), 
			java.util.Collections.<EReference>unmodifiableSet(org.eclipse.xtext.xbase.lib.CollectionLiterals.<EReference>newHashSet(_iComponentStructureMember_Structure)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ComponentStructure getContainingStructure() {
		ComponentStructure _xifexpression = null;
		EObject _eContainer = this.eContainer();
		if ((_eContainer instanceof ComponentStructure)) {
			EObject _eContainer_1 = this.eContainer();
			_xifexpression = ((ComponentStructure) _eContainer_1);
		}
		else {
			_xifexpression = null;
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDefaultNameSeparator() {
		return "::";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getNamePrefixSegments() {
		EList<String> _elvis = null;
		ComponentStructure _containingStructure = this.getContainingStructure();
		EList<String> _qualifiedNameSegments = null;
		if (_containingStructure!=null) {
			_qualifiedNameSegments=_containingStructure.getQualifiedNameSegments();
		}
		if (_qualifiedNameSegments != null) {
			_elvis = _qualifiedNameSegments;
		} else {
			BasicEList<String> _newBasicEList = XcoreCollectionLiterals.<String>newBasicEList();
			_elvis = _newBasicEList;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENT_STRUCTURE__SUB_STRUCTURES:
				return ((InternalEList<?>)getSubStructures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENT_STRUCTURE__STRUCTURE_TYPE:
				return getStructureType();
			case AmaltheaPackage.COMPONENT_STRUCTURE__SUB_STRUCTURES:
				return getSubStructures();
			case AmaltheaPackage.COMPONENT_STRUCTURE__MEMBER_OBJECTS:
				return getMemberObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENT_STRUCTURE__STRUCTURE_TYPE:
				setStructureType((String)newValue);
				return;
			case AmaltheaPackage.COMPONENT_STRUCTURE__SUB_STRUCTURES:
				getSubStructures().clear();
				getSubStructures().addAll((Collection<? extends ComponentStructure>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENT_STRUCTURE__STRUCTURE_TYPE:
				setStructureType(STRUCTURE_TYPE_EDEFAULT);
				return;
			case AmaltheaPackage.COMPONENT_STRUCTURE__SUB_STRUCTURES:
				getSubStructures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENT_STRUCTURE__STRUCTURE_TYPE:
				return STRUCTURE_TYPE_EDEFAULT == null ? structureType != null : !STRUCTURE_TYPE_EDEFAULT.equals(structureType);
			case AmaltheaPackage.COMPONENT_STRUCTURE__SUB_STRUCTURES:
				return subStructures != null && !subStructures.isEmpty();
			case AmaltheaPackage.COMPONENT_STRUCTURE__MEMBER_OBJECTS:
				return !getMemberObjects().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.INAMED___GET_DEFAULT_NAME_SEPARATOR: return AmaltheaPackage.COMPONENT_STRUCTURE___GET_DEFAULT_NAME_SEPARATOR;
				case AmaltheaPackage.INAMED___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.COMPONENT_STRUCTURE___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == IReferable.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.IREFERABLE___GET_DEFAULT_NAME_SEPARATOR: return AmaltheaPackage.COMPONENT_STRUCTURE___GET_DEFAULT_NAME_SEPARATOR;
				case AmaltheaPackage.IREFERABLE___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.COMPONENT_STRUCTURE___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == ReferableObject.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.REFERABLE_OBJECT___GET_DEFAULT_NAME_SEPARATOR: return AmaltheaPackage.COMPONENT_STRUCTURE___GET_DEFAULT_NAME_SEPARATOR;
				case AmaltheaPackage.REFERABLE_OBJECT___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.COMPONENT_STRUCTURE___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AmaltheaPackage.COMPONENT_STRUCTURE___GET_CONTAINING_STRUCTURE:
				return getContainingStructure();
			case AmaltheaPackage.COMPONENT_STRUCTURE___GET_DEFAULT_NAME_SEPARATOR:
				return getDefaultNameSeparator();
			case AmaltheaPackage.COMPONENT_STRUCTURE___GET_NAME_PREFIX_SEGMENTS:
				return getNamePrefixSegments();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (structureType: ");
		result.append(structureType);
		result.append(')');
		return result.toString();
	}

} //ComponentStructureImpl
