/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.AmaltheaValidations;
import org.eclipse.app4mc.amalthea.model.LocalModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabelAssignment;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mode Label Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ModeLabelAssignmentImpl#getGlobalLabel <em>Global Label</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ModeLabelAssignmentImpl#getLocalLabel <em>Local Label</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModeLabelAssignmentImpl extends ActivityGraphItemImpl implements ModeLabelAssignment {
	/**
	 * The cached value of the '{@link #getGlobalLabel() <em>Global Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGlobalLabel()
	 * @generated
	 * @ordered
	 */
	protected ModeLabel globalLabel;

	/**
	 * The cached value of the '{@link #getLocalLabel() <em>Local Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalLabel()
	 * @generated
	 * @ordered
	 */
	protected LocalModeLabel localLabel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModeLabelAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getModeLabelAssignment();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModeLabel getGlobalLabel() {
		if (globalLabel != null && globalLabel.eIsProxy()) {
			InternalEObject oldGlobalLabel = (InternalEObject)globalLabel;
			globalLabel = (ModeLabel)eResolveProxy(oldGlobalLabel);
			if (globalLabel != oldGlobalLabel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.MODE_LABEL_ASSIGNMENT__GLOBAL_LABEL, oldGlobalLabel, globalLabel));
			}
		}
		return globalLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeLabel basicGetGlobalLabel() {
		return globalLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGlobalLabel(ModeLabel newGlobalLabel) {
		ModeLabel oldGlobalLabel = globalLabel;
		globalLabel = newGlobalLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.MODE_LABEL_ASSIGNMENT__GLOBAL_LABEL, oldGlobalLabel, globalLabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LocalModeLabel getLocalLabel() {
		if (localLabel != null && localLabel.eIsProxy()) {
			InternalEObject oldLocalLabel = (InternalEObject)localLabel;
			localLabel = (LocalModeLabel)eResolveProxy(oldLocalLabel);
			if (localLabel != oldLocalLabel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.MODE_LABEL_ASSIGNMENT__LOCAL_LABEL, oldLocalLabel, localLabel));
			}
		}
		return localLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalModeLabel basicGetLocalLabel() {
		return localLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLocalLabel(LocalModeLabel newLocalLabel) {
		LocalModeLabel oldLocalLabel = localLabel;
		localLabel = newLocalLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.MODE_LABEL_ASSIGNMENT__LOCAL_LABEL, oldLocalLabel, localLabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean validateInvariants(final DiagnosticChain diagnostics, final Map<Object, Object> context) {
		return AmaltheaValidations.validateInvariants(this, diagnostics);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__GLOBAL_LABEL:
				if (resolve) return getGlobalLabel();
				return basicGetGlobalLabel();
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__LOCAL_LABEL:
				if (resolve) return getLocalLabel();
				return basicGetLocalLabel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__GLOBAL_LABEL:
				setGlobalLabel((ModeLabel)newValue);
				return;
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__LOCAL_LABEL:
				setLocalLabel((LocalModeLabel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__GLOBAL_LABEL:
				setGlobalLabel((ModeLabel)null);
				return;
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__LOCAL_LABEL:
				setLocalLabel((LocalModeLabel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__GLOBAL_LABEL:
				return globalLabel != null;
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT__LOCAL_LABEL:
				return localLabel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AmaltheaPackage.MODE_LABEL_ASSIGNMENT___VALIDATE_INVARIANTS__DIAGNOSTICCHAIN_MAP:
				return validateInvariants((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ModeLabelAssignmentImpl
