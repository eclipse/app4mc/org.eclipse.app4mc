/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import com.google.common.base.Objects;
import java.lang.reflect.InvocationTargetException;

import java.util.Map;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.AmaltheaValidations;
import org.eclipse.app4mc.amalthea.model.Condition;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunctionEntry;
import org.eclipse.app4mc.amalthea.model.ModeCondition;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabelCondition;
import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mode Label Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ModeLabelConditionImpl#getRelation <em>Relation</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ModeLabelConditionImpl#getLabel1 <em>Label1</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ModeLabelConditionImpl#getLabel2 <em>Label2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModeLabelConditionImpl extends BaseObjectImpl implements ModeLabelCondition {
	/**
	 * The default value of the '{@link #getRelation() <em>Relation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected static final RelationalOperator RELATION_EDEFAULT = RelationalOperator._UNDEFINED_;

	/**
	 * The cached value of the '{@link #getRelation() <em>Relation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected RelationalOperator relation = RELATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLabel1() <em>Label1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel1()
	 * @generated
	 * @ordered
	 */
	protected ModeLabel label1;

	/**
	 * The cached value of the '{@link #getLabel2() <em>Label2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel2()
	 * @generated
	 * @ordered
	 */
	protected ModeLabel label2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModeLabelConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getModeLabelCondition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RelationalOperator getRelation() {
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRelation(RelationalOperator newRelation) {
		RelationalOperator oldRelation = relation;
		relation = newRelation == null ? RELATION_EDEFAULT : newRelation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.MODE_LABEL_CONDITION__RELATION, oldRelation, relation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModeLabel getLabel1() {
		if (label1 != null && label1.eIsProxy()) {
			InternalEObject oldLabel1 = (InternalEObject)label1;
			label1 = (ModeLabel)eResolveProxy(oldLabel1);
			if (label1 != oldLabel1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.MODE_LABEL_CONDITION__LABEL1, oldLabel1, label1));
			}
		}
		return label1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeLabel basicGetLabel1() {
		return label1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLabel1(ModeLabel newLabel1) {
		ModeLabel oldLabel1 = label1;
		label1 = newLabel1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.MODE_LABEL_CONDITION__LABEL1, oldLabel1, label1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModeLabel getLabel2() {
		if (label2 != null && label2.eIsProxy()) {
			InternalEObject oldLabel2 = (InternalEObject)label2;
			label2 = (ModeLabel)eResolveProxy(oldLabel2);
			if (label2 != oldLabel2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.MODE_LABEL_CONDITION__LABEL2, oldLabel2, label2));
			}
		}
		return label2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeLabel basicGetLabel2() {
		return label2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLabel2(ModeLabel newLabel2) {
		ModeLabel oldLabel2 = label2;
		label2 = newLabel2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.MODE_LABEL_CONDITION__LABEL2, oldLabel2, label2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSatisfiedBy(final EMap<ModeLabel, String> context) {
		String _get = null;
		if (context!=null) {
			_get=context.get(this.getLabel1());
		}
		final String labelValue1 = _get;
		String _get_1 = null;
		if (context!=null) {
			_get_1=context.get(this.getLabel2());
		}
		final String labelValue2 = _get_1;
		return (((labelValue1 == null) || (labelValue2 == null)) || Objects.equal(labelValue1, labelValue2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean validateInvariants(final DiagnosticChain diagnostics, final Map<Object, Object> context) {
		return AmaltheaValidations.validateInvariants(this, diagnostics);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_CONDITION__RELATION:
				return getRelation();
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL1:
				if (resolve) return getLabel1();
				return basicGetLabel1();
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL2:
				if (resolve) return getLabel2();
				return basicGetLabel2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_CONDITION__RELATION:
				setRelation((RelationalOperator)newValue);
				return;
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL1:
				setLabel1((ModeLabel)newValue);
				return;
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL2:
				setLabel2((ModeLabel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_CONDITION__RELATION:
				setRelation(RELATION_EDEFAULT);
				return;
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL1:
				setLabel1((ModeLabel)null);
				return;
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL2:
				setLabel2((ModeLabel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.MODE_LABEL_CONDITION__RELATION:
				return relation != RELATION_EDEFAULT;
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL1:
				return label1 != null;
			case AmaltheaPackage.MODE_LABEL_CONDITION__LABEL2:
				return label2 != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ConditionDisjunctionEntry.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Condition.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ModeCondition.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.MODE_LABEL_CONDITION__RELATION: return AmaltheaPackage.MODE_CONDITION__RELATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ConditionDisjunctionEntry.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Condition.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ModeCondition.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.MODE_CONDITION__RELATION: return AmaltheaPackage.MODE_LABEL_CONDITION__RELATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AmaltheaPackage.MODE_LABEL_CONDITION___IS_SATISFIED_BY__EMAP:
				return isSatisfiedBy((EMap<ModeLabel, String>)arguments.get(0));
			case AmaltheaPackage.MODE_LABEL_CONDITION___VALIDATE_INVARIANTS__DIAGNOSTICCHAIN_MAP:
				return validateInvariants((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (relation: ");
		result.append(relation);
		result.append(')');
		return result.toString();
	}

} //ModeLabelConditionImpl
