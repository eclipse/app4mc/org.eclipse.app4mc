/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.INamespaceMember;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.Namespace;
import org.eclipse.app4mc.amalthea.model.ReferableObject;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.xcore.lib.XcoreCollectionLiterals;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Namespace</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.NamespaceImpl#getNextSegments <em>Next Segments</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.NamespaceImpl#getPreviousSegment <em>Previous Segment</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.NamespaceImpl#getMemberObjects <em>Member Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NamespaceImpl extends ReferableObjectImpl implements Namespace {
	/**
	 * The cached value of the '{@link #getNextSegments() <em>Next Segments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextSegments()
	 * @generated
	 * @ordered
	 */
	protected EList<Namespace> nextSegments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NamespaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getNamespace();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Namespace> getNextSegments() {
		if (nextSegments == null) {
			nextSegments = new EObjectContainmentWithInverseEList<Namespace>(Namespace.class, this, AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS, AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT);
		}
		return nextSegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Namespace getPreviousSegment() {
		if (eContainerFeatureID() != AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT) return null;
		return (Namespace)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Namespace basicGetPreviousSegment() {
		if (eContainerFeatureID() != AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT) return null;
		return (Namespace)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<INamespaceMember> getMemberObjects() {
		EReference _iNamespaceMember_Namespace = AmaltheaPackage.eINSTANCE.getINamespaceMember_Namespace();
		return AmaltheaIndex.<INamespaceMember>getInverseReferences(this, AmaltheaPackage.eINSTANCE.getNamespace_MemberObjects(), 
			java.util.Collections.<EReference>unmodifiableSet(org.eclipse.xtext.xbase.lib.CollectionLiterals.<EReference>newHashSet(_iNamespaceMember_Namespace)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getNamePrefixSegments() {
		EList<String> _elvis = null;
		Namespace _previousSegment = this.getPreviousSegment();
		EList<String> _qualifiedNameSegments = null;
		if (_previousSegment!=null) {
			_qualifiedNameSegments=_previousSegment.getQualifiedNameSegments();
		}
		if (_qualifiedNameSegments != null) {
			_elvis = _qualifiedNameSegments;
		} else {
			BasicEList<String> _newBasicEList = XcoreCollectionLiterals.<String>newBasicEList();
			_elvis = _newBasicEList;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNextSegments()).basicAdd(otherEnd, msgs);
			case AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return eBasicSetContainer(otherEnd, AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS:
				return ((InternalEList<?>)getNextSegments()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT:
				return eBasicSetContainer(null, AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT:
				return eInternalContainer().eInverseRemove(this, AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS, Namespace.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS:
				return getNextSegments();
			case AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT:
				if (resolve) return getPreviousSegment();
				return basicGetPreviousSegment();
			case AmaltheaPackage.NAMESPACE__MEMBER_OBJECTS:
				return getMemberObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS:
				getNextSegments().clear();
				getNextSegments().addAll((Collection<? extends Namespace>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS:
				getNextSegments().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.NAMESPACE__NEXT_SEGMENTS:
				return nextSegments != null && !nextSegments.isEmpty();
			case AmaltheaPackage.NAMESPACE__PREVIOUS_SEGMENT:
				return basicGetPreviousSegment() != null;
			case AmaltheaPackage.NAMESPACE__MEMBER_OBJECTS:
				return !getMemberObjects().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.INAMED___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.NAMESPACE___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == IReferable.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.IREFERABLE___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.NAMESPACE___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == ReferableObject.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.REFERABLE_OBJECT___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.NAMESPACE___GET_NAME_PREFIX_SEGMENTS;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AmaltheaPackage.NAMESPACE___GET_NAME_PREFIX_SEGMENTS:
				return getNamePrefixSegments();
		}
		return super.eInvoke(operationID, arguments);
	}

} //NamespaceImpl
