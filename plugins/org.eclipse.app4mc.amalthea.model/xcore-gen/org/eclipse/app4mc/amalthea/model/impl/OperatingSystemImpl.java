/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import org.eclipse.app4mc.amalthea.model.AmaltheaExtensions;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.Namespace;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.OsDataConsistency;
import org.eclipse.app4mc.amalthea.model.OsOverhead;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.ecore.xcore.lib.XcoreCollectionLiterals;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operating System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.OperatingSystemImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.OperatingSystemImpl#getQualifiedName <em>Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.OperatingSystemImpl#getOverhead <em>Overhead</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.OperatingSystemImpl#getTaskSchedulers <em>Task Schedulers</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.OperatingSystemImpl#getInterruptControllers <em>Interrupt Controllers</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.OperatingSystemImpl#getOsDataConsistency <em>Os Data Consistency</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperatingSystemImpl extends BaseObjectImpl implements OperatingSystem {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getQualifiedName() <em>Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOverhead() <em>Overhead</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverhead()
	 * @generated
	 * @ordered
	 */
	protected OsOverhead overhead;

	/**
	 * The cached value of the '{@link #getTaskSchedulers() <em>Task Schedulers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskSchedulers()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskScheduler> taskSchedulers;

	/**
	 * The cached value of the '{@link #getInterruptControllers() <em>Interrupt Controllers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptControllers()
	 * @generated
	 * @ordered
	 */
	protected EList<InterruptController> interruptControllers;

	/**
	 * The cached value of the '{@link #getOsDataConsistency() <em>Os Data Consistency</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOsDataConsistency()
	 * @generated
	 * @ordered
	 */
	protected OsDataConsistency osDataConsistency;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatingSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getOperatingSystem();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.OPERATING_SYSTEM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getQualifiedName() {
		return AmaltheaExtensions.toPlainString(this.getQualifiedNameSegments(), this.getDefaultNameSeparator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OsOverhead getOverhead() {
		if (overhead != null && overhead.eIsProxy()) {
			InternalEObject oldOverhead = (InternalEObject)overhead;
			overhead = (OsOverhead)eResolveProxy(oldOverhead);
			if (overhead != oldOverhead) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.OPERATING_SYSTEM__OVERHEAD, oldOverhead, overhead));
			}
		}
		return overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OsOverhead basicGetOverhead() {
		return overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOverhead(OsOverhead newOverhead) {
		OsOverhead oldOverhead = overhead;
		overhead = newOverhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.OPERATING_SYSTEM__OVERHEAD, oldOverhead, overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TaskScheduler> getTaskSchedulers() {
		if (taskSchedulers == null) {
			taskSchedulers = new EObjectContainmentEList<TaskScheduler>(TaskScheduler.class, this, AmaltheaPackage.OPERATING_SYSTEM__TASK_SCHEDULERS);
		}
		return taskSchedulers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<InterruptController> getInterruptControllers() {
		if (interruptControllers == null) {
			interruptControllers = new EObjectContainmentEList<InterruptController>(InterruptController.class, this, AmaltheaPackage.OPERATING_SYSTEM__INTERRUPT_CONTROLLERS);
		}
		return interruptControllers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OsDataConsistency getOsDataConsistency() {
		return osDataConsistency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOsDataConsistency(OsDataConsistency newOsDataConsistency, NotificationChain msgs) {
		OsDataConsistency oldOsDataConsistency = osDataConsistency;
		osDataConsistency = newOsDataConsistency;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY, oldOsDataConsistency, newOsDataConsistency);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOsDataConsistency(OsDataConsistency newOsDataConsistency) {
		if (newOsDataConsistency != osDataConsistency) {
			NotificationChain msgs = null;
			if (osDataConsistency != null)
				msgs = ((InternalEObject)osDataConsistency).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY, null, msgs);
			if (newOsDataConsistency != null)
				msgs = ((InternalEObject)newOsDataConsistency).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY, null, msgs);
			msgs = basicSetOsDataConsistency(newOsDataConsistency, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY, newOsDataConsistency, newOsDataConsistency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INamed getNamedContainer() {
		INamed _xifexpression = null;
		EObject _eContainer = this.eContainer();
		if ((_eContainer instanceof INamed)) {
			EObject _eContainer_1 = this.eContainer();
			_xifexpression = ((INamed) _eContainer_1);
		}
		else {
			_xifexpression = null;
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNamePrefix() {
		String _xifexpression = null;
		Namespace _namespace = this.getNamespace();
		boolean _tripleEquals = (_namespace == null);
		if (_tripleEquals) {
			return "";
		}
		else {
			_xifexpression = AmaltheaExtensions.toPlainString(this.getNamePrefixSegments(), this.getDefaultNameSeparator());
		}
		return _xifexpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getQualifiedNameSegments() {
		final EList<String> segments = this.getNamePrefixSegments();
		String _name = this.getName();
		boolean _tripleNotEquals = (_name != null);
		if (_tripleNotEquals) {
			segments.add(this.getName());
		}
		return segments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDefaultNameSeparator() {
		return ".";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Namespace getNamespace() {
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getNamePrefixSegments() {
		EList<String> _elvis = null;
		Namespace _namespace = this.getNamespace();
		EList<String> _qualifiedNameSegments = null;
		if (_namespace!=null) {
			_qualifiedNameSegments=_namespace.getQualifiedNameSegments();
		}
		if (_qualifiedNameSegments != null) {
			_elvis = _qualifiedNameSegments;
		} else {
			BasicEList<String> _newBasicEList = XcoreCollectionLiterals.<String>newBasicEList();
			_elvis = _newBasicEList;
		}
		return _elvis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.OPERATING_SYSTEM__TASK_SCHEDULERS:
				return ((InternalEList<?>)getTaskSchedulers()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.OPERATING_SYSTEM__INTERRUPT_CONTROLLERS:
				return ((InternalEList<?>)getInterruptControllers()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY:
				return basicSetOsDataConsistency(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.OPERATING_SYSTEM__NAME:
				return getName();
			case AmaltheaPackage.OPERATING_SYSTEM__QUALIFIED_NAME:
				return getQualifiedName();
			case AmaltheaPackage.OPERATING_SYSTEM__OVERHEAD:
				if (resolve) return getOverhead();
				return basicGetOverhead();
			case AmaltheaPackage.OPERATING_SYSTEM__TASK_SCHEDULERS:
				return getTaskSchedulers();
			case AmaltheaPackage.OPERATING_SYSTEM__INTERRUPT_CONTROLLERS:
				return getInterruptControllers();
			case AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY:
				return getOsDataConsistency();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.OPERATING_SYSTEM__NAME:
				setName((String)newValue);
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__OVERHEAD:
				setOverhead((OsOverhead)newValue);
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__TASK_SCHEDULERS:
				getTaskSchedulers().clear();
				getTaskSchedulers().addAll((Collection<? extends TaskScheduler>)newValue);
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__INTERRUPT_CONTROLLERS:
				getInterruptControllers().clear();
				getInterruptControllers().addAll((Collection<? extends InterruptController>)newValue);
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY:
				setOsDataConsistency((OsDataConsistency)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.OPERATING_SYSTEM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__OVERHEAD:
				setOverhead((OsOverhead)null);
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__TASK_SCHEDULERS:
				getTaskSchedulers().clear();
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__INTERRUPT_CONTROLLERS:
				getInterruptControllers().clear();
				return;
			case AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY:
				setOsDataConsistency((OsDataConsistency)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.OPERATING_SYSTEM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case AmaltheaPackage.OPERATING_SYSTEM__QUALIFIED_NAME:
				return QUALIFIED_NAME_EDEFAULT == null ? getQualifiedName() != null : !QUALIFIED_NAME_EDEFAULT.equals(getQualifiedName());
			case AmaltheaPackage.OPERATING_SYSTEM__OVERHEAD:
				return overhead != null;
			case AmaltheaPackage.OPERATING_SYSTEM__TASK_SCHEDULERS:
				return taskSchedulers != null && !taskSchedulers.isEmpty();
			case AmaltheaPackage.OPERATING_SYSTEM__INTERRUPT_CONTROLLERS:
				return interruptControllers != null && !interruptControllers.isEmpty();
			case AmaltheaPackage.OPERATING_SYSTEM__OS_DATA_CONSISTENCY:
				return osDataConsistency != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.OPERATING_SYSTEM__NAME: return AmaltheaPackage.INAMED__NAME;
				case AmaltheaPackage.OPERATING_SYSTEM__QUALIFIED_NAME: return AmaltheaPackage.INAMED__QUALIFIED_NAME;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.INAMED__NAME: return AmaltheaPackage.OPERATING_SYSTEM__NAME;
				case AmaltheaPackage.INAMED__QUALIFIED_NAME: return AmaltheaPackage.OPERATING_SYSTEM__QUALIFIED_NAME;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == INamed.class) {
			switch (baseOperationID) {
				case AmaltheaPackage.INAMED___GET_NAMED_CONTAINER: return AmaltheaPackage.OPERATING_SYSTEM___GET_NAMED_CONTAINER;
				case AmaltheaPackage.INAMED___GET_NAME_PREFIX: return AmaltheaPackage.OPERATING_SYSTEM___GET_NAME_PREFIX;
				case AmaltheaPackage.INAMED___GET_QUALIFIED_NAME_SEGMENTS: return AmaltheaPackage.OPERATING_SYSTEM___GET_QUALIFIED_NAME_SEGMENTS;
				case AmaltheaPackage.INAMED___GET_DEFAULT_NAME_SEPARATOR: return AmaltheaPackage.OPERATING_SYSTEM___GET_DEFAULT_NAME_SEPARATOR;
				case AmaltheaPackage.INAMED___GET_NAMESPACE: return AmaltheaPackage.OPERATING_SYSTEM___GET_NAMESPACE;
				case AmaltheaPackage.INAMED___GET_NAME_PREFIX_SEGMENTS: return AmaltheaPackage.OPERATING_SYSTEM___GET_NAME_PREFIX_SEGMENTS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AmaltheaPackage.OPERATING_SYSTEM___GET_NAMED_CONTAINER:
				return getNamedContainer();
			case AmaltheaPackage.OPERATING_SYSTEM___GET_NAME_PREFIX:
				return getNamePrefix();
			case AmaltheaPackage.OPERATING_SYSTEM___GET_QUALIFIED_NAME_SEGMENTS:
				return getQualifiedNameSegments();
			case AmaltheaPackage.OPERATING_SYSTEM___GET_DEFAULT_NAME_SEPARATOR:
				return getDefaultNameSeparator();
			case AmaltheaPackage.OPERATING_SYSTEM___GET_NAMESPACE:
				return getNamespace();
			case AmaltheaPackage.OPERATING_SYSTEM___GET_NAME_PREFIX_SEGMENTS:
				return getNamePrefixSegments();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //OperatingSystemImpl
