/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.util.Collection;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.IExecutable;
import org.eclipse.app4mc.amalthea.model.LocalModeLabel;
import org.eclipse.app4mc.amalthea.model.Stimulus;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ProcessImpl#getLocalLabels <em>Local Labels</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ProcessImpl#getActivityGraph <em>Activity Graph</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ProcessImpl#getStimuli <em>Stimuli</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ProcessImpl extends AbstractProcessImpl implements org.eclipse.app4mc.amalthea.model.Process {
	/**
	 * The cached value of the '{@link #getLocalLabels() <em>Local Labels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalLabels()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalModeLabel> localLabels;

	/**
	 * The cached value of the '{@link #getActivityGraph() <em>Activity Graph</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivityGraph()
	 * @generated
	 * @ordered
	 */
	protected ActivityGraph activityGraph;

	/**
	 * The cached value of the '{@link #getStimuli() <em>Stimuli</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStimuli()
	 * @generated
	 * @ordered
	 */
	protected EList<Stimulus> stimuli;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getProcess();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LocalModeLabel> getLocalLabels() {
		if (localLabels == null) {
			localLabels = new EObjectContainmentWithInverseEList<LocalModeLabel>(LocalModeLabel.class, this, AmaltheaPackage.PROCESS__LOCAL_LABELS, AmaltheaPackage.LOCAL_MODE_LABEL__CONTAINING_EXECUTABLE);
		}
		return localLabels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityGraph getActivityGraph() {
		return activityGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActivityGraph(ActivityGraph newActivityGraph, NotificationChain msgs) {
		ActivityGraph oldActivityGraph = activityGraph;
		activityGraph = newActivityGraph;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AmaltheaPackage.PROCESS__ACTIVITY_GRAPH, oldActivityGraph, newActivityGraph);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActivityGraph(ActivityGraph newActivityGraph) {
		if (newActivityGraph != activityGraph) {
			NotificationChain msgs = null;
			if (activityGraph != null)
				msgs = ((InternalEObject)activityGraph).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AmaltheaPackage.PROCESS__ACTIVITY_GRAPH, null, msgs);
			if (newActivityGraph != null)
				msgs = ((InternalEObject)newActivityGraph).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AmaltheaPackage.PROCESS__ACTIVITY_GRAPH, null, msgs);
			msgs = basicSetActivityGraph(newActivityGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.PROCESS__ACTIVITY_GRAPH, newActivityGraph, newActivityGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Stimulus> getStimuli() {
		if (stimuli == null) {
			stimuli = new EObjectResolvingEList<Stimulus>(Stimulus.class, this, AmaltheaPackage.PROCESS__STIMULI);
		}
		return stimuli;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.PROCESS__LOCAL_LABELS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLocalLabels()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.PROCESS__LOCAL_LABELS:
				return ((InternalEList<?>)getLocalLabels()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.PROCESS__ACTIVITY_GRAPH:
				return basicSetActivityGraph(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.PROCESS__LOCAL_LABELS:
				return getLocalLabels();
			case AmaltheaPackage.PROCESS__ACTIVITY_GRAPH:
				return getActivityGraph();
			case AmaltheaPackage.PROCESS__STIMULI:
				return getStimuli();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.PROCESS__LOCAL_LABELS:
				getLocalLabels().clear();
				getLocalLabels().addAll((Collection<? extends LocalModeLabel>)newValue);
				return;
			case AmaltheaPackage.PROCESS__ACTIVITY_GRAPH:
				setActivityGraph((ActivityGraph)newValue);
				return;
			case AmaltheaPackage.PROCESS__STIMULI:
				getStimuli().clear();
				getStimuli().addAll((Collection<? extends Stimulus>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.PROCESS__LOCAL_LABELS:
				getLocalLabels().clear();
				return;
			case AmaltheaPackage.PROCESS__ACTIVITY_GRAPH:
				setActivityGraph((ActivityGraph)null);
				return;
			case AmaltheaPackage.PROCESS__STIMULI:
				getStimuli().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.PROCESS__LOCAL_LABELS:
				return localLabels != null && !localLabels.isEmpty();
			case AmaltheaPackage.PROCESS__ACTIVITY_GRAPH:
				return activityGraph != null;
			case AmaltheaPackage.PROCESS__STIMULI:
				return stimuli != null && !stimuli.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IExecutable.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.PROCESS__LOCAL_LABELS: return AmaltheaPackage.IEXECUTABLE__LOCAL_LABELS;
				case AmaltheaPackage.PROCESS__ACTIVITY_GRAPH: return AmaltheaPackage.IEXECUTABLE__ACTIVITY_GRAPH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IExecutable.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.IEXECUTABLE__LOCAL_LABELS: return AmaltheaPackage.PROCESS__LOCAL_LABELS;
				case AmaltheaPackage.IEXECUTABLE__ACTIVITY_GRAPH: return AmaltheaPackage.PROCESS__ACTIVITY_GRAPH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ProcessImpl
