/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.Value;

import org.eclipse.app4mc.amalthea.model.emf.AmaltheaEObjectImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduler Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerAssociationImpl#getSchedulingParameters <em>Scheduling Parameters</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerAssociationImpl#getChild <em>Child</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerAssociationImpl#getParent <em>Parent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SchedulerAssociationImpl extends AmaltheaEObjectImpl implements SchedulerAssociation {
	/**
	 * The cached value of the '{@link #getSchedulingParameters() <em>Scheduling Parameters</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulingParameters()
	 * @generated
	 * @ordered
	 */
	protected EMap<SchedulingParameterDefinition, Value> schedulingParameters;

	/**
	 * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParent()
	 * @generated
	 * @ordered
	 */
	protected TaskScheduler parent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchedulerAssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getSchedulerAssociation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TaskScheduler getChild() {
		if (eContainerFeatureID() != AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD) return null;
		return (TaskScheduler)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskScheduler basicGetChild() {
		if (eContainerFeatureID() != AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD) return null;
		return (TaskScheduler)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TaskScheduler getParent() {
		if (parent != null && parent.eIsProxy()) {
			InternalEObject oldParent = (InternalEObject)parent;
			parent = (TaskScheduler)eResolveProxy(oldParent);
			if (parent != oldParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.SCHEDULER_ASSOCIATION__PARENT, oldParent, parent));
			}
		}
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskScheduler basicGetParent() {
		return parent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(TaskScheduler newParent) {
		TaskScheduler oldParent = parent;
		parent = newParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.SCHEDULER_ASSOCIATION__PARENT, oldParent, parent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<SchedulingParameterDefinition, Value> getSchedulingParameters() {
		if (schedulingParameters == null) {
			schedulingParameters = new EcoreEMap<SchedulingParameterDefinition,Value>(AmaltheaPackage.eINSTANCE.getSchedulingParameter(), SchedulingParameterImpl.class, this, AmaltheaPackage.SCHEDULER_ASSOCIATION__SCHEDULING_PARAMETERS);
		}
		return schedulingParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return eBasicSetContainer(otherEnd, AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__SCHEDULING_PARAMETERS:
				return ((InternalEList<?>)getSchedulingParameters()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD:
				return eBasicSetContainer(null, AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD:
				return eInternalContainer().eInverseRemove(this, AmaltheaPackage.TASK_SCHEDULER__PARENT_ASSOCIATION, TaskScheduler.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__SCHEDULING_PARAMETERS:
				if (coreType) return getSchedulingParameters();
				else return getSchedulingParameters().map();
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD:
				if (resolve) return getChild();
				return basicGetChild();
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__PARENT:
				if (resolve) return getParent();
				return basicGetParent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__SCHEDULING_PARAMETERS:
				((EStructuralFeature.Setting)getSchedulingParameters()).set(newValue);
				return;
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__PARENT:
				setParent((TaskScheduler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__SCHEDULING_PARAMETERS:
				getSchedulingParameters().clear();
				return;
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__PARENT:
				setParent((TaskScheduler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__SCHEDULING_PARAMETERS:
				return schedulingParameters != null && !schedulingParameters.isEmpty();
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__CHILD:
				return basicGetChild() != null;
			case AmaltheaPackage.SCHEDULER_ASSOCIATION__PARENT:
				return parent != null;
		}
		return super.eIsSet(featureID);
	}

} //SchedulerAssociationImpl
