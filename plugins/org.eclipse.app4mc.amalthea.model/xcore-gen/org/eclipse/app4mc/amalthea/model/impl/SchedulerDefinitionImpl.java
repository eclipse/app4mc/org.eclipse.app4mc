/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.util.Collection;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.IDescription;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduler Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerDefinitionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerDefinitionImpl#getAlgorithmParameters <em>Algorithm Parameters</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerDefinitionImpl#getProcessParameters <em>Process Parameters</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerDefinitionImpl#isRequiresParentScheduler <em>Requires Parent Scheduler</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerDefinitionImpl#isPassesParametersUpwards <em>Passes Parameters Upwards</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerDefinitionImpl#isHasExactlyOneChild <em>Has Exactly One Child</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SchedulerDefinitionImpl extends OsDefinitionImpl implements SchedulerDefinition {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAlgorithmParameters() <em>Algorithm Parameters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlgorithmParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<SchedulingParameterDefinition> algorithmParameters;

	/**
	 * The cached value of the '{@link #getProcessParameters() <em>Process Parameters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<SchedulingParameterDefinition> processParameters;

	/**
	 * The default value of the '{@link #isRequiresParentScheduler() <em>Requires Parent Scheduler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRequiresParentScheduler()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REQUIRES_PARENT_SCHEDULER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRequiresParentScheduler() <em>Requires Parent Scheduler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRequiresParentScheduler()
	 * @generated
	 * @ordered
	 */
	protected boolean requiresParentScheduler = REQUIRES_PARENT_SCHEDULER_EDEFAULT;

	/**
	 * The default value of the '{@link #isPassesParametersUpwards() <em>Passes Parameters Upwards</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPassesParametersUpwards()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PASSES_PARAMETERS_UPWARDS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPassesParametersUpwards() <em>Passes Parameters Upwards</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPassesParametersUpwards()
	 * @generated
	 * @ordered
	 */
	protected boolean passesParametersUpwards = PASSES_PARAMETERS_UPWARDS_EDEFAULT;

	/**
	 * The default value of the '{@link #isHasExactlyOneChild() <em>Has Exactly One Child</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasExactlyOneChild()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_EXACTLY_ONE_CHILD_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasExactlyOneChild() <em>Has Exactly One Child</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasExactlyOneChild()
	 * @generated
	 * @ordered
	 */
	protected boolean hasExactlyOneChild = HAS_EXACTLY_ONE_CHILD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchedulerDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getSchedulerDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SchedulingParameterDefinition> getProcessParameters() {
		if (processParameters == null) {
			processParameters = new EObjectResolvingEList<SchedulingParameterDefinition>(SchedulingParameterDefinition.class, this, AmaltheaPackage.SCHEDULER_DEFINITION__PROCESS_PARAMETERS);
		}
		return processParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SchedulingParameterDefinition> getAlgorithmParameters() {
		if (algorithmParameters == null) {
			algorithmParameters = new EObjectResolvingEList<SchedulingParameterDefinition>(SchedulingParameterDefinition.class, this, AmaltheaPackage.SCHEDULER_DEFINITION__ALGORITHM_PARAMETERS);
		}
		return algorithmParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isRequiresParentScheduler() {
		return requiresParentScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRequiresParentScheduler(boolean newRequiresParentScheduler) {
		boolean oldRequiresParentScheduler = requiresParentScheduler;
		requiresParentScheduler = newRequiresParentScheduler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.SCHEDULER_DEFINITION__REQUIRES_PARENT_SCHEDULER, oldRequiresParentScheduler, requiresParentScheduler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isPassesParametersUpwards() {
		return passesParametersUpwards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPassesParametersUpwards(boolean newPassesParametersUpwards) {
		boolean oldPassesParametersUpwards = passesParametersUpwards;
		passesParametersUpwards = newPassesParametersUpwards;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.SCHEDULER_DEFINITION__PASSES_PARAMETERS_UPWARDS, oldPassesParametersUpwards, passesParametersUpwards));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isHasExactlyOneChild() {
		return hasExactlyOneChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHasExactlyOneChild(boolean newHasExactlyOneChild) {
		boolean oldHasExactlyOneChild = hasExactlyOneChild;
		hasExactlyOneChild = newHasExactlyOneChild;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.SCHEDULER_DEFINITION__HAS_EXACTLY_ONE_CHILD, oldHasExactlyOneChild, hasExactlyOneChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION:
				return getDescription();
			case AmaltheaPackage.SCHEDULER_DEFINITION__ALGORITHM_PARAMETERS:
				return getAlgorithmParameters();
			case AmaltheaPackage.SCHEDULER_DEFINITION__PROCESS_PARAMETERS:
				return getProcessParameters();
			case AmaltheaPackage.SCHEDULER_DEFINITION__REQUIRES_PARENT_SCHEDULER:
				return isRequiresParentScheduler();
			case AmaltheaPackage.SCHEDULER_DEFINITION__PASSES_PARAMETERS_UPWARDS:
				return isPassesParametersUpwards();
			case AmaltheaPackage.SCHEDULER_DEFINITION__HAS_EXACTLY_ONE_CHILD:
				return isHasExactlyOneChild();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__ALGORITHM_PARAMETERS:
				getAlgorithmParameters().clear();
				getAlgorithmParameters().addAll((Collection<? extends SchedulingParameterDefinition>)newValue);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__PROCESS_PARAMETERS:
				getProcessParameters().clear();
				getProcessParameters().addAll((Collection<? extends SchedulingParameterDefinition>)newValue);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__REQUIRES_PARENT_SCHEDULER:
				setRequiresParentScheduler((Boolean)newValue);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__PASSES_PARAMETERS_UPWARDS:
				setPassesParametersUpwards((Boolean)newValue);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__HAS_EXACTLY_ONE_CHILD:
				setHasExactlyOneChild((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__ALGORITHM_PARAMETERS:
				getAlgorithmParameters().clear();
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__PROCESS_PARAMETERS:
				getProcessParameters().clear();
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__REQUIRES_PARENT_SCHEDULER:
				setRequiresParentScheduler(REQUIRES_PARENT_SCHEDULER_EDEFAULT);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__PASSES_PARAMETERS_UPWARDS:
				setPassesParametersUpwards(PASSES_PARAMETERS_UPWARDS_EDEFAULT);
				return;
			case AmaltheaPackage.SCHEDULER_DEFINITION__HAS_EXACTLY_ONE_CHILD:
				setHasExactlyOneChild(HAS_EXACTLY_ONE_CHILD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case AmaltheaPackage.SCHEDULER_DEFINITION__ALGORITHM_PARAMETERS:
				return algorithmParameters != null && !algorithmParameters.isEmpty();
			case AmaltheaPackage.SCHEDULER_DEFINITION__PROCESS_PARAMETERS:
				return processParameters != null && !processParameters.isEmpty();
			case AmaltheaPackage.SCHEDULER_DEFINITION__REQUIRES_PARENT_SCHEDULER:
				return requiresParentScheduler != REQUIRES_PARENT_SCHEDULER_EDEFAULT;
			case AmaltheaPackage.SCHEDULER_DEFINITION__PASSES_PARAMETERS_UPWARDS:
				return passesParametersUpwards != PASSES_PARAMETERS_UPWARDS_EDEFAULT;
			case AmaltheaPackage.SCHEDULER_DEFINITION__HAS_EXACTLY_ONE_CHILD:
				return hasExactlyOneChild != HAS_EXACTLY_ONE_CHILD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IDescription.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION: return AmaltheaPackage.IDESCRIPTION__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IDescription.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.IDESCRIPTION__DESCRIPTION: return AmaltheaPackage.SCHEDULER_DEFINITION__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", requiresParentScheduler: ");
		result.append(requiresParentScheduler);
		result.append(", passesParametersUpwards: ");
		result.append(passesParametersUpwards);
		result.append(", hasExactlyOneChild: ");
		result.append(hasExactlyOneChild);
		result.append(')');
		return result.toString();
	}

} //SchedulerDefinitionImpl
