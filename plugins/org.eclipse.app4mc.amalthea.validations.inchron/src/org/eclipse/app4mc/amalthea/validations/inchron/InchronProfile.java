/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron;

import org.eclipse.app4mc.amalthea.validations.standard.AmaltheaProfile;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ProfileGroup;
import org.eclipse.app4mc.validation.core.IProfileConfiguration;
import org.osgi.service.component.annotations.Component;

@Profile(
		name = "Inchron Validations",
		description = "Validation for Amalthea models used in Inchron Toolsuite"
	)

@ProfileGroup(
		profiles = {
				AmaltheaProfile.class,
				InchronOsProfile.class,
				InchronHWProfile.class,
				InchronSoftwareProfile.class,
				InchronConstraintsProfile.class,
				InchronStimuliProfile.class
		}
	)

@Component
public class InchronProfile implements IProfileConfiguration {
	// Do nothing
}