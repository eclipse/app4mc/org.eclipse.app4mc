/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron;

import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWInvalidRunnableAllocation;
import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWInvalidTaskAllocation;
import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWRunnableAllocationCheck;
import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWRunnableMustHaveActivityGraph;
import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWTaskEnforcedMigrationCheck;
import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWTaskMustHaveActivityGraph;
import org.eclipse.app4mc.amalthea.validations.standard.SoftwareProfile;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ProfileGroup;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Software Validations (INCHRON)")

@ProfileGroup(
		// Amalthea standard software profile
		profiles = SoftwareProfile.class
	)

@ValidationGroup(
		severity = Severity.ERROR,
		validations =  {
				InchronSWInvalidRunnableAllocation.class,
				InchronSWRunnableMustHaveActivityGraph.class,
				InchronSWInvalidTaskAllocation.class,
				InchronSWTaskMustHaveActivityGraph.class,
				InchronSWTaskEnforcedMigrationCheck.class,
				InchronSWRunnableAllocationCheck.class
		}
	)


public class InchronSoftwareProfile  implements IProfile {
	// Do nothing
}
