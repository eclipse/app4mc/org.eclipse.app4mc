/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim;

import org.eclipse.app4mc.amalthea.validations.sim.basic.SimBasicIdentifiers;
import org.eclipse.app4mc.amalthea.validations.sim.basic.SimSupportedModelElements;
import org.eclipse.app4mc.amalthea.validations.ta.basic.TABasicContinuousValueGaussDistribution;
import org.eclipse.app4mc.amalthea.validations.ta.basic.TABasicDiscreteValueGaussDistribution;
import org.eclipse.app4mc.amalthea.validations.ta.basic.TABasicTimeGaussDistribution;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Basic Validations (APP4MC.sim)")

@ValidationGroup(
	severity = Severity.ERROR,
	validations =  {
		SimBasicIdentifiers.class,
		TABasicContinuousValueGaussDistribution.class,
		TABasicDiscreteValueGaussDistribution.class,
		TABasicTimeGaussDistribution.class
	}
)

@ValidationGroup(
		severity = Severity.WARNING,
		validations =  {
			SimSupportedModelElements.class,
		}
)

public class SimBasicProfile implements IProfile {
	// Do nothing
}
