/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim;

import org.eclipse.app4mc.amalthea.validations.inchron.os.InchronOsPUAllocationMustBeDisjunct;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSMandatorySchedulingParametersSet;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSSchedulingParameterMultiplicityMatchesDefinition;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSSchedulingParameterValueTypeMatchesDefinedType;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSStandardSchedulerDefinitionConformance;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSStandardSchedulingParameterDefinitionConformance;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "OS model Validations (APP4MC.sim)")

@ValidationGroup(
	severity = Severity.ERROR,
	validations =  {
			AmOSSchedulingParameterValueTypeMatchesDefinedType.class,
			AmOSSchedulingParameterMultiplicityMatchesDefinition.class,
			AmOSMandatorySchedulingParametersSet.class,
			AmOSStandardSchedulerDefinitionConformance.class,
			AmOSStandardSchedulingParameterDefinitionConformance.class
	}
)
@ValidationGroup(
	severity = Severity.INFO,
	validations = {
		InchronOsPUAllocationMustBeDisjunct.class
	}
)
public class SimOsProfile implements IProfile {
	// Do nothing
}
