/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim;

import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWRunnableMustHaveActivityGraph;
import org.eclipse.app4mc.amalthea.validations.inchron.sw.InchronSWTaskMustHaveActivityGraph;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelAccessFeasibility;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelElements;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelMapped;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelAccessFeasibility;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelAccessType;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelMapped;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelAccessFeasibility;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelAccessType;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelMapped;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareProcess;
import org.eclipse.app4mc.amalthea.validations.ta.software.TASoftwareModeConditionConjunctionAlwaysFalse;
import org.eclipse.app4mc.amalthea.validations.ta.software.TASoftwareModeConditionDisjunctionAlwaysTrue;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Software Validations (APP4MC.sim)")

@ValidationGroup(
	severity = Severity.ERROR,
	validations = {
		SimSoftwareProcess.class,
		SimSoftwareLabelAccessFeasibility.class,
		SimSoftwareChannelAccessFeasibility.class,
		SimSoftwareLabelMapped.class,
		SimSoftwareChannelElements.class,
		SimSoftwareChannelMapped.class,
		SimSoftwareLabelAccessType.class,
		SimSoftwareModeLabelAccessType.class
	}
)

@ValidationGroup(
	severity = Severity.WARNING,
	validations = {
		SimSoftwareModeLabelAccessFeasibility.class,
		SimSoftwareModeLabelMapped.class,
		InchronSWTaskMustHaveActivityGraph.class,
		TASoftwareModeConditionConjunctionAlwaysFalse.class,
		TASoftwareModeConditionDisjunctionAlwaysTrue.class
	}
)

@ValidationGroup(
	severity = Severity.INFO,
	validations = {
		InchronSWRunnableMustHaveActivityGraph.class,
	}
)

public class SimSoftwareProfile implements IProfile {
	// Do nothing
}
