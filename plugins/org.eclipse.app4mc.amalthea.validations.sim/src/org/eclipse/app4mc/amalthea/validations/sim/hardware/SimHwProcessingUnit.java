/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.hardware;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of ProcessingUnit definitions
 *
 * <ul>
 * <li>ProcessingUnit definition must be set</li>
 * </ul>
 */

@Validation(
		id = "Sim-HW-ProcessingUnit",
		checks = {	"ProcessingUnit definition must be set" })

public class SimHwProcessingUnit extends AmaltheaValidation {

	public static final String MISSING_DEFINITION = ": missing definition";
	
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getProcessingUnit();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof ProcessingUnit) {
			ProcessingUnit pu = (ProcessingUnit) eObject;
			if (pu.getDefinition() == null) {
				addIssue(results, pu, ePackage.getProcessingUnit_Definition(),
						objectInfo(pu) + MISSING_DEFINITION);
			}
		}
	}

}
