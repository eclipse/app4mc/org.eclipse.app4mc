/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard;

import org.eclipse.app4mc.amalthea.validations.standard.basic.AmBasicCounter;
import org.eclipse.app4mc.amalthea.validations.standard.basic.AmBasicCustomPropertyKey;
import org.eclipse.app4mc.amalthea.validations.standard.basic.AmBasicDataSize;
import org.eclipse.app4mc.amalthea.validations.standard.basic.AmBasicFrequency;
import org.eclipse.app4mc.amalthea.validations.standard.basic.AmBasicQuantity;
import org.eclipse.app4mc.amalthea.validations.standard.basic.AmBasicTimeRange;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Basic Validations")

@ValidationGroup(
		severity = Severity.ERROR,
		validations =  {
				AmBasicCounter.class,
				AmBasicDataSize.class,
				AmBasicFrequency.class,
				AmBasicQuantity.class,
				AmBasicTimeRange.class
		}
)

@ValidationGroup(
		severity = Severity.WARNING,
		validations =  {
				AmBasicCustomPropertyKey.class
		}
)

public class BasicProfile implements IProfile {
    // Do nothing
}
