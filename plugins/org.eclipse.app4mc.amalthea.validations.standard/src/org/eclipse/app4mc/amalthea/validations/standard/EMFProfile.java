/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard;

import org.eclipse.app4mc.amalthea.validations.standard.emf.AmEmfIntrinsic;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfileConfiguration;
import org.eclipse.app4mc.validation.core.Severity;

/**
 * Generated EMF validations for AMALTHEA
 */

@Profile(
	name = "Amalthea EMF Validations",
	description = "Standard EMF validations for AMALTHEA models (generated)."
)

@ValidationGroup(
		severity = Severity.UNDEFINED,
		validations =  {
				AmEmfIntrinsic.class
		}
)

public class EMFProfile implements IProfileConfiguration {
	// Do nothing
}
