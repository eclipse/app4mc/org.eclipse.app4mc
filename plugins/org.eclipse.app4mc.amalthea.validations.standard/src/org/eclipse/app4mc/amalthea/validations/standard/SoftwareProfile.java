/**
 ********************************************************************************
 * Copyright (c) 2019-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard;

import org.eclipse.app4mc.amalthea.validations.standard.software.AmSwCallArgument;
import org.eclipse.app4mc.amalthea.validations.standard.software.AmSwDataDependency;
import org.eclipse.app4mc.amalthea.validations.standard.software.AmSwGroup;
import org.eclipse.app4mc.amalthea.validations.standard.software.AmSwSemaphoreAccess;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Software Validations")

@ValidationGroup(
		severity = Severity.ERROR,
		validations =  {
				AmSwCallArgument.class,
				AmSwDataDependency.class,
				AmSwGroup.class,
				AmSwSemaphoreAccess.class
		}
)

public class SoftwareProfile implements IProfile {
    // Do nothing
}
