/**
 ********************************************************************************
 * Copyright (c) 2021-2024 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.ParameterType;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.xbase.lib.Pair;

/**
 * Checks for each scheduler, if mandatory parameters are set.
 *
 * <ul>
 * <li>Mandatory algorithm scheduling parameters must be set.</li>
 * <li>Mandatory process scheduling parameters for task and ISR allocations must be set.</li>
 * <li>Mandatory process scheduling parameters for scheduler associations must be set.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Mandatory-Scheduling-Parameters-Set",
		checks = { "Mandatory scheduling parameters must be set" })

public class AmOSMandatorySchedulingParametersSet extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getScheduler();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof Scheduler sc) {

			SchedulerDefinition scDef = sc.getDefinition();

			if (scDef == null)
				return; // no definition -> missing reference

			// collect immutable lists of mandatory scheduling parameters

			final List<SchedulingParameterDefinition> mandatoryAlgorithmParamDefs = scDef.getAlgorithmParameters()
					.stream().filter(SchedulingParameterDefinition::isMandatory).toList();
			final List<SchedulingParameterDefinition> mandatoryProcessParamDefs = scDef.getProcessParameters()
					.stream().filter(SchedulingParameterDefinition::isMandatory).toList();

			if (mandatoryAlgorithmParamDefs.isEmpty() && mandatoryProcessParamDefs.isEmpty())
				return; // no mandatory parameters -> nothing to check


			final Map<Pair<EObject, EStructuralFeature>, List<SchedulingParameterDefinition>> missingSPs = new LinkedHashMap<>();

			// check algorithm parameters (mandatory but not included)

			final List<SchedulingParameterDefinition> missingAlgorithmParamDefs = mandatoryAlgorithmParamDefs
					.stream().filter(e -> !sc.getSchedulingParameters().containsKey(e)).toList();

			missingSPs.put(new Pair<>(sc, ePackage.getISchedulingParameterContainer_SchedulingParameters()), missingAlgorithmParamDefs);

			if (sc instanceof TaskScheduler ts) {
				// check task allocation parameters
				for(final TaskAllocation ta : ts.getTaskAllocations()) {
					final List<SchedulingParameterDefinition> perTAMissing = new ArrayList<>(mandatoryProcessParamDefs);
					perTAMissing.removeAll(ta.getSchedulingParameters().keySet());
					// TODO: consider grouping schedulers - look for parent scheduler parameters
					missingSPs.put(new Pair<>(ta, ePackage.getISchedulingParameterContainer_SchedulingParameters()), perTAMissing);
				}

				// check scheduler association parameters
				for(final SchedulerAssociation sa : ts.getChildAssociations()) {
					final List<SchedulingParameterDefinition> perSAMissing = new ArrayList<>(mandatoryProcessParamDefs);
					perSAMissing.removeAll(sa.getSchedulingParameters().keySet());
					// TODO: consider grouping schedulers - look for child scheduler parameters
					missingSPs.put(new Pair<>(sa, ePackage.getISchedulingParameterContainer_SchedulingParameters()), perSAMissing);
				}
			} else if (sc instanceof InterruptController ic) {
				// check ISR allocation parameters
				for(final ISRAllocation ia : ic.getIsrAllocations()) {
					final List<SchedulingParameterDefinition> perIAMissing = mandatoryProcessParamDefs.stream()
							.filter(spd -> !spd.getName().equals("priority") || spd.getType() != ParameterType.INTEGER).collect(Collectors.toList());
					// maybe support scheduling parameters in ISRAllocations?
					missingSPs.put(new Pair<>(ia, ePackage.getISRAllocation_Priority()), perIAMissing);
				}
			}

			missingSPs.forEach((eo, missingList) -> missingList.forEach(missing ->
				addIssue(results, eo.getKey(), eo.getValue(), "Mandatory scheduling parameter \"" + missing.getName()
						+ "\" is not set for " + objectInfo(eo.getKey()))
			));
		}
	}

}
