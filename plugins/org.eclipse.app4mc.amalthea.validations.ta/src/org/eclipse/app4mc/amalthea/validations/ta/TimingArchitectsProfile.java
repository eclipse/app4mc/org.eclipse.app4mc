/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.validations.ta;

import org.eclipse.app4mc.amalthea.validations.standard.AmaltheaProfile;
import org.eclipse.app4mc.amalthea.validations.ta.misc.TAMiscSemaphore;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ProfileGroup;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfileConfiguration;
import org.eclipse.app4mc.validation.core.Severity;
import org.osgi.service.component.annotations.Component;

/**
 * Validations for AMALTHEA models used in a Timing Architects Simulation
 */
@Profile(
	name = "Timing Architects Validations",
	description = "Validations for AMALTHEA models used in a Timing Architects Simulation."
)

@ProfileGroup(
	profiles = {
		AmaltheaProfile.class,
		TABasicProfile.class,
		TAConstraintsProfile.class,
		TAHardwareProfile.class,
		TASoftwareProfile.class,
		TAStimuliProfile.class
	}
)
@ValidationGroup(
	severity = Severity.ERROR,
	validations = {
		TAMiscSemaphore.class
	}
)

@Component
public class TimingArchitectsProfile implements IProfileConfiguration {
    // Do nothing
}
