/**
 ********************************************************************************
 * Copyright (c) 2020-2022 DLR e. V., OFFIS e. V. and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     OFFIS e. V. - initial API and implementation
 *     DLR e. V.   - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.visualization.runnabledependency;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.eclipse.app4mc.visualization.ui.VisualizationParameters;

/**
 * Configuration of the visualization
 * 
 * @author Jan Steffen Becker (jan.becker@dlr.de)
 *
 */
public class RunnableDependencyConfig {
	private static final String HORIZONTAL_LAYOUT_KEY = "HorizontalLayout";
	private static final String HORIZONTAL_LAYOUT_DEFAULT = "true";

	private static final String SHOW_CALL_DEPENDENCIES_KEY = "ShowCallDependencies";
	private static final String SHOW_CALL_DEPENDENCIES_DEFAULT = "true";

	private static final String SHOW_LABEL_DEPENDENCIES_KEY = "ShowLabelDependencies";
	private static final String SHOW_LABEL_DEPENDENCIES_DEFAULT = "true";

	private static final String SHOW_LABELS_KEY = "ShowLabels";
	private static final String SHOW_LABELS_DEFAULT = "true";

	private static final String SHOW_TASKS_KEY = "ShowTasks";
	private static final String SHOW_TASKS_DEFAULT = "true";

	private static final String SCALE_KEY = "Scale";
	private static final String SCALE_DEFAULT = "80"; // percent

	private final VisualizationParameters parameters;
	private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

	public RunnableDependencyConfig(VisualizationParameters viewParameters) {
		parameters = viewParameters;
	}

	public boolean isHorizontalLayout() {
		return Boolean.parseBoolean(parameters.getOrDefault(HORIZONTAL_LAYOUT_KEY, HORIZONTAL_LAYOUT_DEFAULT));
	}

	public void setHorizontalLayout(boolean horizontal) {
		parameters.put(HORIZONTAL_LAYOUT_KEY, Boolean.toString(horizontal));
		firePropertyChange("parameter1", null, horizontal);
	}

	public boolean isShowCallDependencies() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_CALL_DEPENDENCIES_KEY, SHOW_CALL_DEPENDENCIES_DEFAULT));
	}

	public void setShowCallDependencies(boolean showCallDependencies) {
		parameters.put(SHOW_CALL_DEPENDENCIES_KEY, Boolean.toString(showCallDependencies));
		firePropertyChange("parameter2", null, showCallDependencies);
	}

	public boolean isShowLabelDependencies() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_LABEL_DEPENDENCIES_KEY, SHOW_LABEL_DEPENDENCIES_DEFAULT));
	}

	public void setShowLabelDependencies(boolean showLabelDependencies) {
		parameters.put(SHOW_LABEL_DEPENDENCIES_KEY, Boolean.toString(showLabelDependencies));
		firePropertyChange("parameter3", null, showLabelDependencies);
	}

	public boolean isShowLabels() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_LABELS_KEY, SHOW_LABELS_DEFAULT));
	}

	public void setShowLabels(boolean showLabels) {
		parameters.put(SHOW_LABELS_KEY, Boolean.toString(showLabels));
		firePropertyChange("parameter4", null, showLabels);
	}

	public boolean isShowTasks() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_TASKS_KEY, SHOW_TASKS_DEFAULT));
	}

	public void setShowTasks(boolean showTasks) {
		parameters.put(SHOW_TASKS_KEY, Boolean.toString(showTasks));
		firePropertyChange("parameter5", null, showTasks);
	}

	public int getScale() {
		return Integer.parseInt(parameters.getOrDefault(SCALE_KEY, SCALE_DEFAULT));
	}

	/**
	 * Sets a new scale value (if the new value is different and within the bounds [10, 200])
	 * 
	 * @param newScale
	 * @return true if value was changed
	 */
	public boolean setScale(int newScale) {
		int oldScale = getScale();
		if (oldScale == newScale || newScale < 10 || newScale > 200) {
			return false;	
		}

		parameters.put(SCALE_KEY, Integer.toString(newScale));
		firePropertyChange("scale", oldScale, newScale);
		return true;
	}

	public boolean decrementScale() {
		return setScale(Math.max(10, getScale() - 10)); // minimum 10 %
	}

	public boolean incrementScale() {
		return setScale(Math.min(200, getScale() + 10)); // maximum 200 %
	}

	// property change handling

	public void addChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removeChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		changeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}

}
