/*********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.javafx;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.visualization.ui.VisualizationParameters;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property= {
		"name=Shared Runnable Label Dependencies",
		"description=Visualize all Runnables of the Software Model and their shared Labels"
})
public class SWModelRunnableSharedDependencyVisualization implements Visualization {

	@Reference
	RunnableSharedDependencyVisualization visualization;

	@PostConstruct
	public void createVisualization(SWModel model, VisualizationParameters parameters, Composite parent, IEventBroker broker) {
		visualization.createVisualization(model.getRunnables(), parameters, parent, broker);
	}

}
