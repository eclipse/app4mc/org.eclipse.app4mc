/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.visualizations.javafx;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.embed.swt.FXCanvas;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

@Component(property= {
		"name=SWModel Statistics Visualization",
		"description=Some simple statistics on the SWModel"
})
public class SoftwareModelVisualization implements Visualization {

	@PostConstruct
	public void createVisualization(SWModel model, Composite parent) {
		parent.setLayout(new GridLayout());

		FXCanvas canvas = new FXCanvas(parent, SWT.NONE);

		GridDataFactory
		    .fillDefaults()
		    .grab(true, true)
		    .applyTo(canvas);

		// create the root layout pane
		BorderPane layout = new BorderPane();
		layout.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

		// create a Scene instance
		// set the layout container as root
		// set the background fill to the background color of the shell
		Scene scene = new Scene(layout);

		// set the Scene to the FXCanvas
		canvas.setScene(scene);

		Label output = new Label();
		layout.setCenter(output);

		RotateTransition rotateTransition = new RotateTransition(Duration.seconds(1), output);
		rotateTransition.setByAngle(360);

		ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1), output);
		scaleTransition.setFromX(1.0);
		scaleTransition.setFromY(1.0);
		scaleTransition.setToX(4.0);
		scaleTransition.setToY(4.0);

		output.setText("SWModel");

		ParallelTransition parallelTransition = new ParallelTransition(rotateTransition, scaleTransition);

        parallelTransition.setOnFinished(e -> {
        	TranslateTransition trans = new TranslateTransition(Duration.seconds(0.5), output);
        	trans.setFromY(output.getBoundsInLocal().getMinY());
        	trans.setToY(-1 * ((scene.getHeight() / 2) - output.getHeight()) + 25);

        	trans.setOnFinished(event -> {

        		GridPane grid = new GridPane();
        		grid.setHgap(10);

        		ColumnConstraints leftCol = new ColumnConstraints();
        		leftCol.setHgrow(Priority.NEVER);
        		ColumnConstraints rightCol = new ColumnConstraints();
        		rightCol.setHgrow(Priority.NEVER);
        		rightCol.setHalignment(HPos.RIGHT);
        		ColumnConstraints spacerCol = new ColumnConstraints();
        		spacerCol.setHgrow(Priority.ALWAYS);

        		grid.getColumnConstraints().addAll(leftCol, rightCol, spacerCol);

        		Font defaultFont = Font.getDefault();
        		Font boldFont = Font.font(defaultFont.getFamily(), FontWeight.BOLD, defaultFont.getSize());

        		Text headlineText = new Text(output.getText());
        		headlineText.setFont(Font.font(defaultFont.getFamily(), defaultFont.getSize() * 4));
        		grid.add(headlineText, 0, 0);
        		GridPane.setColumnSpan(headlineText, 3);
        		GridPane.setHalignment(headlineText, HPos.CENTER);
        		GridPane.setValignment(headlineText, VPos.CENTER);

        		grid.add(new Text(), 0, 1);
        		grid.add(new Text(), 1, 1);
        		grid.add(new Text(), 2, 1);

        		addLine(grid, defaultFont, boldFont, "Activations:", model.getActivations().size(), 2);
        		addLine(grid, defaultFont, boldFont, "Channels:", model.getChannels().size(), 3);
        		addLine(grid, defaultFont, boldFont, "Custom Entities:", model.getCustomEntities().size(), 4);
        		addLine(grid, defaultFont, boldFont, "Custom Properties:", model.getCustomProperties().size(), 5);
        		addLine(grid, defaultFont, boldFont, "Events:", model.getEvents().size(), 6);
        		addLine(grid, defaultFont, boldFont, "ISRs:", model.getIsrs().size(), 7);
        		addLine(grid, defaultFont, boldFont, "Labels:", model.getLabels().size(), 8);
        		addLine(grid, defaultFont, boldFont, "Mode Labels:", model.getModeLabels().size(), 9);
        		addLine(grid, defaultFont, boldFont, "Modes:", model.getModes().size(), 10);
        		addLine(grid, defaultFont, boldFont, "Process Chains:", model.getProcessChains().size(), 11);
        		addLine(grid, defaultFont, boldFont, "Process Prototypes:", model.getProcessPrototypes().size(), 12);
        		addLine(grid, defaultFont, boldFont, "Runnables:", model.getRunnables().size(), 13);
        		addLine(grid, defaultFont, boldFont, "Sections:", model.getSections().size(), 14);
        		addLine(grid, defaultFont, boldFont, "Tasks:", model.getTasks().size(), 15);
        		addLine(grid, defaultFont, boldFont, "Type Definitions:", model.getTypeDefinitions().size(), 16);

        		layout.setCenter(grid);
        	});

        	trans.play();
        });

		parallelTransition.play();
	}

	private void addLine(GridPane grid, Font defaultFont, Font boldFont, String label, int size, int row) {
		Text activationsText = new Text(label);
		activationsText.setFont(size > 0 ? boldFont : defaultFont);
		grid.add(activationsText, 0, row);
		Text activationsNumber = new Text("" + size);
		activationsNumber.setFont(size > 0 ? boldFont : defaultFont);
		grid.add(activationsNumber, 1, row);
	}

	@PreDestroy
	public void dispose() {
		// for debugging: System.out.println("Destroy resources")
	}

}
