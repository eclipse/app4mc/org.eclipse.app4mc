/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.visualizations.javafx.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class RunnablePainter {

	private static final double MIN_UNIT_HEIGHT = 20;
	private static final FontMetrics MIN_BOLD_FONT_METRICS = new FontMetrics(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 12));
	private static final FontMetrics MIN_FONT_METRICS = new FontMetrics(Font.font(Font.getDefault().getFamily(), 12));

	private final Runnable runnable;
	private List<LabelAccess> readItems;
	private List<LabelAccess> writeItems;

	private int itemMax = 0;

	private double scaleFactor = 1.0d;
	private double maxScaleFactor = 0d;
	private boolean autoscale = true;

	private Rectangle runnableRectangle;
	private HashMap<Label, Rectangle> readAccessRectangles = new HashMap<>();
	private HashMap<Label, Rectangle> writeAccessRectangles = new HashMap<>();

	private Color runnableStroke = Color.GREEN;
	private Color runnableHeaderFillColor = Color.LIGHTGREEN;
	private Color runnableBodyFillColor = null;
	private Color runnableHeaderFontFill = Color.BLACK;
	private Color labelFontFill = Color.GREEN;

	/**
	 * Create a new painter for the given {@link Runnable} with enable autoscaling.
	 *
	 * @param runnable The {@link Runnable} for which the painter should be created.
	 */
	public RunnablePainter(Runnable runnable) {
		this(runnable, true);
	}

	/**
	 * Create a new painter for the given {@link Runnable}.
	 *
	 * @param runnable  The {@link Runnable} for which the painter should be
	 *                  created.
	 * @param autoscale <code>true</code> if the rendering should be automatically
	 *                  scaled for the available bounds, <code>false</code> if the
	 *                  default dimensions should be used.
	 */
	public RunnablePainter(Runnable runnable, boolean autoscale) {
		this.runnable = runnable;
		this.autoscale = autoscale;

		readItems = SoftwareUtil.getReadLabelAccessList(runnable, null);
		writeItems = SoftwareUtil.getWriteLabelAccessList(runnable, null);

		itemMax = Math.max(readItems.size(), writeItems.size());
		if (itemMax == 0) {
			// if there are no items yet, we use 1 to have at least a reasonable rendering
			itemMax = 1;
		}
	}

	/**
	 *
	 * @return The preferred minimum height using a unit height of 20.
	 */
	public double getPreferredMinimumHeight() {
		// number of units needed for height calculation
		// number of items + number of gaps between items + gap on top and bottom of box + box header + padding to border
		int units = itemMax + (itemMax - 1) + 2 + 2 + 2;
		return MIN_UNIT_HEIGHT * scaleFactor * units;
	}

	/**
	 *
	 * @return the preferred minimum width using a font size of 12.
	 */
	public double getPreferredMinimumWidth() {
		double longestRead = 0;
		for (LabelAccess item : readItems) {
			String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
			longestRead = Math.max(longestRead, MIN_FONT_METRICS.computeStringWidth(name));
		}

		double longestWrite = 0;
		for (LabelAccess item : writeItems) {
			String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
			longestWrite = Math.max(longestWrite, MIN_FONT_METRICS.computeStringWidth(name));
		}

		double textWidth = MIN_BOLD_FONT_METRICS.computeStringWidth(runnable.getName());

		// padding left/ right + longestRead + longestWrite + padding label access to rectangle + rectangle width
		return ((6 * MIN_UNIT_HEIGHT) + longestRead + longestWrite + textWidth) * scaleFactor;
	}

	/**
	 * Paint the {@link Runnable} on the given {@link GraphicsContext}.
	 *
	 * @param gc           The {@link GraphicsContext} that should be used for
	 *                     rendering.
	 * @param parentBounds The bounds on which the {@link Runnable} should be
	 *                     painted.
	 */
	public void paint(GraphicsContext gc, Rectangle parentBounds) {

		// number of units needed for height calculation
		// number of items + number of gaps between items + gap on top and bottom of box + box header + padding to border
		int units = itemMax + (itemMax - 1) + 2 + 2 + 2;

		// the minimum item height is 20
		double unitHeight = MIN_UNIT_HEIGHT * scaleFactor;
		double minHeight = units * unitHeight;

		if (autoscale && (parentBounds.getHeight() > minHeight)) {
			unitHeight = parentBounds.getHeight() / units;
		}

		// calculate with font metrics
		Font defaultFont = gc.getFont();
		double fontSize = 12 * (Double.valueOf(unitHeight) / 20);

		Font headlineFont = Font.font(defaultFont.getFamily(), FontWeight.BOLD, fontSize);
		Font labelAccessFont = Font.font(defaultFont.getFamily(), fontSize);

		// calculate start x positions
		gc.setFont(headlineFont);
		FontMetrics headlineMetrics = new FontMetrics(headlineFont);
		double textWidth = headlineMetrics.computeStringWidth(runnable.getName());

		// check the longest read access label
		gc.setFont(labelAccessFont);
		FontMetrics labelAccessMetrics = new FontMetrics(gc.getFont());
		double longestRead = 0;
		for (LabelAccess item : readItems) {
			String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
			longestRead = Math.max(longestRead, labelAccessMetrics.computeStringWidth(name));
		}

		double longestWrite = 0;
		for (LabelAccess item : writeItems) {
			String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
			longestWrite = Math.max(longestWrite, labelAccessMetrics.computeStringWidth(name));
		}

		// padding left/ right + longestRead + longestWrite + padding label access to rectangle + rectangle width
		double runnableWidth = (2 * unitHeight) + longestRead + longestWrite + (2 * unitHeight) + textWidth + (2 * unitHeight);
		if (autoscale && (runnableWidth > parentBounds.getWidth())) {
			double factor = parentBounds.getWidth() / runnableWidth;
			unitHeight *= factor;

			fontSize = 12 * (Double.valueOf(unitHeight) / 20);

			headlineFont = Font.font(defaultFont.getFamily(), FontWeight.BOLD, fontSize);
			labelAccessFont = Font.font(defaultFont.getFamily(), fontSize);

			headlineMetrics = new FontMetrics(headlineFont);
			textWidth = headlineMetrics.computeStringWidth(runnable.getName());

			// check the longest read access label
			gc.setFont(labelAccessFont);
			labelAccessMetrics = new FontMetrics(gc.getFont());
			longestRead = 0;
			for (LabelAccess item : readItems) {
				String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
				longestRead = Math.max(longestRead, labelAccessMetrics.computeStringWidth(name));
			}
		}

		// calculate start x positions
		double rectX = parentBounds.getX() + (2 * unitHeight) + longestRead;
		double runnableX = rectX + unitHeight;

		// draw runnable
		gc.setFont(headlineFont);
		gc.setStroke(runnableStroke);

		// first draw the body
		if (runnableBodyFillColor != null) {
			gc.setFill(runnableBodyFillColor);
			gc.fillRect(
					rectX,
					parentBounds.getY() + unitHeight,
					textWidth + (2 * unitHeight),
					(units - 2) * unitHeight);
		}
		gc.strokeRect(
				rectX,
				parentBounds.getY() + unitHeight,
				textWidth + (2 * unitHeight),
				(units - 2) * unitHeight);

		// now draw the runnable header
		gc.setFill(runnableHeaderFillColor);
		headlineMetrics = new FontMetrics(headlineFont);

		runnableRectangle = new Rectangle(
				rectX,
				parentBounds.getY() + unitHeight,
				textWidth + (2 * unitHeight),
				2 * unitHeight);
		gc.fillRect(
				rectX,
				parentBounds.getY() + unitHeight,
				textWidth + (2 * unitHeight),
				2 * unitHeight);

		gc.setFill(runnableHeaderFontFill);
		gc.fillText(
				runnable.getName(),
				runnableX,
				parentBounds.getY() + (unitHeight / 2) + (2 * unitHeight) - ((unitHeight - headlineMetrics.lineHeight) / 2) - headlineMetrics.descent);

		gc.setFont(labelAccessFont);
		gc.setStroke(runnableStroke);
		labelAccessMetrics = new FontMetrics(gc.getFont());

		// draw read access
		double yStart = parentBounds.getY() + 4 * unitHeight;
		double readX = rectX - (unitHeight / 2);
		for (LabelAccess item : readItems) {
			gc.setFill(Color.WHITE);
			gc.fillRoundRect(
					readX,
					yStart,
					unitHeight,
					unitHeight,
					10,
					10);
			gc.strokeRoundRect(
					readX,
					yStart,
					unitHeight,
					unitHeight,
					10,
					10);

			readAccessRectangles.put(item.getData(), new Rectangle(
					readX,
					yStart,
					unitHeight,
					unitHeight));

			gc.setFill(labelFontFill);
			String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
			gc.fillText(
					name,
					readX - labelAccessMetrics.computeStringWidth(name) - (unitHeight / 2),
					yStart + unitHeight - ((unitHeight - labelAccessMetrics.lineHeight) / 2) - labelAccessMetrics.descent);

			yStart += (2 * unitHeight);
		}

		// draw write access
		yStart = parentBounds.getY() + 4 * unitHeight;
		double writeX = rectX + textWidth + (2 * unitHeight) - (unitHeight / 2);
		for (LabelAccess item : writeItems) {
			gc.setFill(Color.WHITE);
			gc.fillRoundRect(
					writeX,
					yStart,
					unitHeight,
					unitHeight,
					10,
					10);
			gc.strokeRoundRect(
					writeX,
					yStart,
					unitHeight,
					unitHeight,
					10,
					10);

			writeAccessRectangles.put(item.getData(), new Rectangle(
					writeX,
					yStart,
					unitHeight,
					unitHeight));

			gc.setFill(labelFontFill);
			String name = item.getData().getDisplayName() != null ? item.getData().getDisplayName() : item.getData().getName();
			gc.fillText(
					name,
					writeX + unitHeight + (unitHeight / 2),
					yStart + unitHeight - ((unitHeight - labelAccessMetrics.lineHeight) / 2) - labelAccessMetrics.descent);

			yStart += (2 * unitHeight);
		}
	}

	/**
	 *
	 * @return The {@link Runnable} that this painter is assigned to.
	 */
	public Runnable getRunnable() {
		return runnable;
	}

	/**
	 * Get the {@link Rectangle} for the read label access of the given label.
	 * <p>
	 * <b>Note: </b> The {@link Rectangle} is only available AFTER
	 * {@link #paint(GraphicsContext, Rectangle)} was executed.
	 * </p>
	 *
	 * @param label The label for which the {@link Rectangle} is requested.
	 * @return The {@link Rectangle} for the read label access of the given label,
	 *         or <code>null</code> if no {@link Rectangle} is available.
	 */
	public Rectangle getReadRectangle(Label label) {
		return readAccessRectangles.get(label);
	}

	/**
	 * Get the {@link Rectangle}s for the read label access of this painters
	 * {@link Runnable}.
	 * <p>
	 * <b>Note: </b> The {@link Rectangle}s are only available AFTER
	 * {@link #paint(GraphicsContext, Rectangle)} was executed.
	 * </p>
	 *
	 * @return The {@link Label} to {@link Rectangle} mapping for the read label
	 *         access of this painters {@link Runnable}.
	 */
	public Map<Label, Rectangle> getReadRectangles() {
		return readAccessRectangles;
	}

	/**
	 * Get the {@link Rectangle} for the write label access of the given label.
	 * <p>
	 * <b>Note: </b> The {@link Rectangle} is only available AFTER
	 * {@link #paint(GraphicsContext, Rectangle)} was executed.
	 * </p>
	 *
	 * @param label The label for which the {@link Rectangle} is requested.
	 * @return The {@link Rectangle} for the write label access of the given label,
	 *         or <code>null</code> if no {@link Rectangle} is available.
	 */
	public Rectangle getWriteRectangle(Label label) {
		return writeAccessRectangles.get(label);
	}

	/**
	 * Get the {@link Rectangle}s for the write label access of this painters
	 * {@link Runnable}.
	 * <p>
	 * <b>Note: </b> The {@link Rectangle}s are only available AFTER
	 * {@link #paint(GraphicsContext, Rectangle)} was executed.
	 * </p>
	 *
	 * @return The {@link Label} to {@link Rectangle} mapping for the write label
	 *         access of this painters {@link Runnable}.
	 */
	public Map<Label, Rectangle> getWriteRectangles() {
		return writeAccessRectangles;
	}

	/**
	 * Get the {@link Rectangle}s for the runnable header of this painters
	 * {@link Runnable}.
	 * <p>
	 * <b>Note: </b> The {@link Rectangle} is only available AFTER
	 * {@link #paint(GraphicsContext, Rectangle)} was executed.
	 * </p>
	 *
	 * @return The {@link Rectangle} for the runnable header of this painters
	 *         {@link Runnable}.
	 */
	public Rectangle getRunnableRectangle() {
		return runnableRectangle;
	}

	/**
	 *
	 * @param fill The color that should be used to fill the runnable rectangle.
	 */
	public void setRunnableHeaderFill(Color fill) {
		runnableHeaderFillColor = fill;
	}

	/**
	 *
	 * @param fill The color that should be used to fill the runnable body
	 *             rectangle. Setting it to <code>null</code> will result in a
	 *             transparent body.
	 */
	public void setRunnableBodyFill(Color fill) {
		runnableBodyFillColor = fill;
	}

	/**
	 *
	 * @param stroke The color that should be used for the border of the rectangle.
	 */
	public void setRunnableStroke(Color stroke) {
		runnableStroke = stroke;
	}

	/**
	 *
	 * @param fill The color that should be used for the runnable header text.
	 */
	public void setRunnableHeaderFontFill(Color fill) {
		runnableHeaderFontFill = fill;
	}

	/**
	 *
	 * @param fill The color that should be used for the label access labels.
	 */
	public void setLabelFontFill(Color fill) {
		labelFontFill = fill;
	}

	/**
	 * Set the color for the runnable. This includes the rectangle border, the label
	 * access labels and the header background.
	 *
	 * @param color The color to set.
	 */
	public void setRunnableColor(Color color) {
		runnableStroke = color;
		runnableHeaderFillColor = color;
		labelFontFill = color;
	}

	/**
	 *
	 * @return The scaling factor applied to the rendering of this painter.
	 */
	public double getScaleFactor() {
		return scaleFactor;
	}

	/**
	 *
	 * @param scaleFactor The scaling factor that should be applied to the rendering of this painter.
	 */
	public void setScaleFactor(double scaleFactor) {
		if (scaleFactor <= maxScaleFactor && scaleFactor >= 0.1) {
			this.scaleFactor = scaleFactor;
		}
	}

	/**
	 *
	 * @return The maximum supported scale factor.
	 */
	public double getMaxScaleFactor() {
		return maxScaleFactor;
	}

	/**
	 *
	 * @param maxScaleFactor The maximum supported scale factor.
	 */
	public void setMaxScaleFactor(double maxScaleFactor) {
		this.maxScaleFactor = Math.floor(maxScaleFactor * 10d) / 10d;
		// ensure to never scale below 0.1
		if (this.maxScaleFactor < 0.1) {
			this.maxScaleFactor = 0.1d;
		}
		if (this.maxScaleFactor < scaleFactor) {
			scaleFactor = this.maxScaleFactor;
		}
	}

	/**
	 * Increase the scaling factor to zoom in.
	 */
	public void zoomIn() {
		if (scaleFactor <= maxScaleFactor) {
			double newFactor = scaleFactor + 0.1d;
			newFactor = Math.round(newFactor * 10d) / 10d;

			scaleFactor = Math.min(maxScaleFactor, newFactor);
		}
	}

	/**
	 * Decrease the scaling factor to zoom out.
	 */
	public void zoomOut() {
		double newFactor = scaleFactor - 0.1d;
		newFactor = Math.round(newFactor * 10d) / 10d;
		scaleFactor = Math.max(newFactor, 0.1d);
	}
}
