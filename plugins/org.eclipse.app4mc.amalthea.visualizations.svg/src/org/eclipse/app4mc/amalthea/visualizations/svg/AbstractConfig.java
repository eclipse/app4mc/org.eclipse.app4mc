/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.eclipse.app4mc.visualization.ui.VisualizationParameters;

public abstract class AbstractConfig {

	private static final String SCALE_KEY = "Scale";
	private static final String SCALE_DEFAULT = "100";

	private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

	protected final VisualizationParameters parameters;

	protected AbstractConfig(VisualizationParameters viewParameters) {
		parameters = viewParameters;
	}

	public int getScale() {
		return Integer.parseInt(parameters.getOrDefault(SCALE_KEY, SCALE_DEFAULT));
	}

	/**
	 * Sets a new scale value (if the new value is different and within the bounds [10, 200])
	 * 
	 * @param newScale
	 * @return true if value was changed
	 */
	public boolean setScale(int newScale) {
		int oldScale = getScale();
		if (oldScale == newScale || newScale < 10 || newScale > 200) {
			return false;	
		}
	
		parameters.put(SCALE_KEY, Integer.toString(newScale));
		firePropertyChange("scale", oldScale, newScale);
		return true;
	}

	public boolean decrementScale() {
		return setScale(Math.max(10, getScale() - 10)); // minimum 10 %
	}

	public boolean incrementScale() {
		return setScale(Math.min(200, getScale() + 10)); // maximum 200 %
	}

	// property change handling

	public void addChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removeChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		changeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}

}
