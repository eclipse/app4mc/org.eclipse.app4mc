/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import org.eclipse.app4mc.visualization.util.svg.AbstractDiagram;

/**
 * Generic container for all relevant inputs of the visualization
 * 
 * This object is handed over to the updateDiagram() method
 * and is passed to a specific generator by a subclass
 *
 */
public final class Context {
	public final Object object;
	public final AbstractConfig config;
	public final AbstractDiagram diagram;

	public Context(Object object, AbstractConfig config, AbstractDiagram diagram) {
		this.object = object;
		this.config = config;
		this.diagram = diagram;
	}

}
