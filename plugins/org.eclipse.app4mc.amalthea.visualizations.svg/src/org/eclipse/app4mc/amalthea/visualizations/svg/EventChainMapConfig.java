/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import org.eclipse.app4mc.visualization.ui.VisualizationParameters;

/**
 * Configuration of the visualization
 *
 */
public class EventChainMapConfig extends AbstractConfig {
	private static final String SHOW_ALL_EVENTS_KEY = "ShowAllEvents";
	private static final String SHOW_ALL_EVENTS_DEFAULT = "true";

	private static final String SHOW_LINKS_KEY = "ShowLinks";
	private static final String SHOW_LINKS_DEFAULT = "true";

	private static final String EXPAND_SUBCHAIN_REFERENCES_KEY = "ExpandSubchainReferences";
	private static final String EXPAND_SUBCHAIN_REFERENCES_DEFAULT = "false";

	private static final String SHOW_REPEATING_EVENTS_GRAYED_KEY = "ShowRepeatingEventsGrayed";
	private static final String SHOW_REPEATING_EVENTS_GRAYED_DEFAULT = "false";
	
	public EventChainMapConfig(VisualizationParameters viewParameters) {
		super(viewParameters);
	}

	public boolean isShowAllEvents() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_ALL_EVENTS_KEY, SHOW_ALL_EVENTS_DEFAULT));
	}

	public void setShowAllEvents(boolean showAllEvents) {
		parameters.put(SHOW_ALL_EVENTS_KEY, Boolean.toString(showAllEvents));
		firePropertyChange("parameter1", null, showAllEvents);
	}

	public boolean isShowLinks() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_LINKS_KEY, SHOW_LINKS_DEFAULT));
	}

	public void setShowLinks(boolean showLinks) {
		parameters.put(SHOW_LINKS_KEY, Boolean.toString(showLinks));
		firePropertyChange("parameter2", null, showLinks);
	}

	public boolean isExpandSubchainReferences() {
		return Boolean.parseBoolean(parameters.getOrDefault(EXPAND_SUBCHAIN_REFERENCES_KEY, EXPAND_SUBCHAIN_REFERENCES_DEFAULT));
	}

	public void setExpandSubchainReferences(boolean expandReferences) {
		parameters.put(EXPAND_SUBCHAIN_REFERENCES_KEY, Boolean.toString(expandReferences));
		firePropertyChange("parameter3", null, expandReferences);
	}

	public boolean isShowRepeatingEventsGrayed() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_REPEATING_EVENTS_GRAYED_KEY, SHOW_REPEATING_EVENTS_GRAYED_DEFAULT));
	}

	public void setShowRepeatingEventsGrayed(boolean showEventsGrayed) {
		parameters.put(SHOW_REPEATING_EVENTS_GRAYED_KEY, Boolean.toString(showEventsGrayed));
		firePropertyChange("parameter4", null, showEventsGrayed);
	}

}
