/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import org.eclipse.app4mc.visualization.ui.VisualizationParameters;

/**
 * Configuration of the visualization
 *
 */
public class HardwareStructureConfig extends AbstractConfig {
	private static final String SHOW_MODULES_KEY = "ShowModules";
	private static final String SHOW_MODULES_DEFAULT = "true";

	private static final String SHOW_PHYSICAL_CONNECTIONS_KEY = "ShowPhysicalConnections";
	private static final String SHOW_PHYSICAL_CONNECTIONS_DEFAULT = "true";

	private static final String SHOW_LOGICAL_CONNECTIONS_KEY = "ShowLogicalConnections";
	private static final String SHOW_LOGICAL_CONNECTIONS_DEFAULT = "true";
	
	public HardwareStructureConfig(VisualizationParameters viewParameters) {
		super(viewParameters);
	}

	public boolean isShowModules() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_MODULES_KEY, SHOW_MODULES_DEFAULT));
	}

	public void setShowModules(boolean showModules) {
		parameters.put(SHOW_MODULES_KEY, Boolean.toString(showModules));
		firePropertyChange("parameter1", null, showModules);
	}

	public boolean isShowPhysicalConnections() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_PHYSICAL_CONNECTIONS_KEY, SHOW_PHYSICAL_CONNECTIONS_DEFAULT));
	}

	public void setShowPhysicalConnections(boolean showPhysicalConnections) {
		parameters.put(SHOW_PHYSICAL_CONNECTIONS_KEY, Boolean.toString(showPhysicalConnections));
		firePropertyChange("parameter2", null, showPhysicalConnections);
	}

	public boolean isShowLogicalConnections() {
		return Boolean.parseBoolean(parameters.getOrDefault(SHOW_LOGICAL_CONNECTIONS_KEY, SHOW_LOGICAL_CONNECTIONS_DEFAULT));
	}

	public void setShowLogicalConnections(boolean showLogicalConnections) {
		parameters.put(SHOW_LOGICAL_CONNECTIONS_KEY, Boolean.toString(showLogicalConnections));
		firePropertyChange("parameter3", null, showLogicalConnections);
	}

}
