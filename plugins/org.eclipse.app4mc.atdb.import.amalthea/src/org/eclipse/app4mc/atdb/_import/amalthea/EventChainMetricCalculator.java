/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.MetricAggregation;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class EventChainMetricCalculator implements IRunnableWithProgress {
	
	private static final Map<String, String> metricCalculationStmts = new LinkedHashMap<>();
	static {
		Stream.of("Age", "Reaction").forEach(latencyType -> {
			final String latencyName = latencyType.toLowerCase() + "Latency";
			metricCalculationStmts.put(latencyName,
					"INSERT OR IGNORE INTO entityInstanceMetricValue SELECT entityId, entityInstance, (SELECT id FROM metric WHERE "//
					+ "name = '" + latencyName + "'), responseTimestamp - stimulusTimestamp FROM eventChainInstanceInfo "//
					+ "WHERE is" + latencyType + ";");
			Stream.of(MetricAggregation.values()).forEach(kind -> {
				final String metricName = latencyName + "_" + kind;
				final String aggregateFunction = kind.getSQLStr("value");
				metricCalculationStmts.put(metricName, "INSERT OR IGNORE INTO entityMetricValue SELECT entityId, (SELECT id FROM metric WHERE name = '"//
						+ metricName + "'), " + aggregateFunction + " FROM entityInstanceMetricValue WHERE (SELECT is" + latencyType + " FROM "//
						+ "eventChainInstanceInfo WHERE eventChainInstanceInfo.entityId = entityInstanceMetricValue.entityId AND "//
						+ "eventChainInstanceInfo.entityInstance = entityInstanceMetricValue.entityInstance) "//
						+ "GROUP BY entityId;");
			});
		});
	}

	private final ATDBConnection con;

	public EventChainMetricCalculator(final ATDBConnection con) {
		this.con = con;
	}

	@Override
	public void run(IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		final SubMonitor subMon = SubMonitor.convert(progressMonitor, "Calculating event chain metrics...", 3);
		try (final Statement mStmt = con.createStatement()) {
			// create event chain instances with separate sql program (a sequence of queries)
			String ecInstCalculationSQL;
			final Bundle bundle = FrameworkUtil.getBundle(EventChainMetricCalculator.class);
			final String pathInBundle = EventChainMetricCalculator.class.getPackage().getName().replace('.', '/') + "/ecinststatements.sql";
			final URL fileURL = bundle.getResource(pathInBundle);
			ecInstCalculationSQL = loadStringFromFile(fileURL);
			if (ecInstCalculationSQL.length() == 0) return;
			con.executeUpdate(ecInstCalculationSQL);
			subMon.worked(1);

			con.executeBatchUpdate(atdbCon -> {
				// calculate event chain metrics min max avg for age and reaction latency values
				for(final Entry<String,String> metricCalculationStmt:metricCalculationStmts.entrySet()) {
					atdbCon.insertMetric(metricCalculationStmt.getKey(), "time");
					mStmt.addBatch(metricCalculationStmt.getValue());
				}
			});
			subMon.worked(1);

			con.executeBatchStatements(mStmt);
			subMon.worked(1);
		} catch (SQLException e) {
			throw new InvocationTargetException(e);
		}
	}

	private static String loadStringFromFile(final URL fileURL) {
		try (final InputStream fileIS = fileURL.openStream();
			 final ByteArrayOutputStream bAOS = new ByteArrayOutputStream();) {
			byte[] buffer = new byte[1024];
			int length;
			while ((length = fileIS.read(buffer)) != -1) {
			    bAOS.write(buffer, 0, length);
			}
			return bAOS.toString();
		} catch (IOException e) {
			// fail silently
		}
		return "";
	}

}
