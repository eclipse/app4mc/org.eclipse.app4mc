/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.atdb.ATDBBuilder;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.DBConnection.AccessMode;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.google.common.io.Files;

public class ImportTransformation implements IRunnableWithProgress {

	private final String amxmiSource;
	private final String atdbSource;
	private final String atdbFile;
	private final boolean copySource;

	public ImportTransformation(final String amxmiSource, final String atdbSource, final String atdbFile, final boolean copySource) {
		this.amxmiSource = amxmiSource;
		this.atdbSource = atdbSource;
		this.atdbFile = atdbFile;
		this.copySource = copySource;
	}

	@Override
	public void run(IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		if (amxmiSource.isEmpty() || atdbFile.isEmpty()) {
			return;
		}
		final SubMonitor subMon = SubMonitor.convert(progressMonitor, "Inserting AMALTHEA model specification parts to ATDB...", 4);
		// copy atdb source if not empty
		if (copySource && !atdbSource.isEmpty() && !atdbSource.equals(atdbFile)) {
			try {
				Files.copy(new File(atdbSource), new File(atdbFile));
			} catch (IOException e) {
				throw new InvocationTargetException(e);
			}
		}
		final SubMonitor openAMXMI_ATDBMonitor = subMon.split(1);
		openAMXMI_ATDBMonitor.beginTask("Opening AMXMI and ATDB files...", 1);
		try (final ATDBConnection con = new ATDBConnection(atdbFile, AccessMode.ReadWrite)) {
			final Amalthea model = AmaltheaLoader.loadFromFileNamed(amxmiSource);

			// ensure that we have a minimal data base
			new ATDBBuilder(con).createBasicDBStructure().createBasicViews();

			// import events
			final SubMonitor eventImportMonitor = subMon.split(1);
			final EventImporter eventImporter = new EventImporter(con, model);
			eventImporter.run(eventImportMonitor);

			// import event chains
			final SubMonitor ecImportMonitor = subMon.split(1);
			final EventChainImporter ecImporter = new EventChainImporter(con, model);
			ecImporter.run(ecImportMonitor);

			// calculate event chain instances and metrics
			final SubMonitor ecInstMetricMonitor = subMon.split(1);
			if (con.tableExists("traceEvent")) {
				final EventChainMetricCalculator ecmc = new EventChainMetricCalculator(con);
				ecmc.run(ecInstMetricMonitor);
			}

		} catch (SQLException e) {
			throw new InvocationTargetException(e);
		} finally {
			progressMonitor.done();
		}
	}

}
