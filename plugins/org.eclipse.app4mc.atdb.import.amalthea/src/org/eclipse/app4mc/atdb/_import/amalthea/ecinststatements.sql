DROP TABLE IF EXISTS ecStimuliInstance;
CREATE TEMPORARY TABLE ecStimuliInstance AS SELECT
  entity.id,
  traceEvent.timestamp,
  traceEvent.sqcnr
FROM entity, event, traceEvent WHERE
  entity.entityTypeId = (SELECT id FROM entityType WHERE entityType.name = 'EC') AND
  NOT EXISTS(SELECT value FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems')) AND
  event.id = (SELECT CAST(value AS INTEGER) FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecStimulus')) AND
  traceEvent.entityId = event.entityId AND traceEvent.eventTypeId = event.eventTypeId ORDER BY entity.id, traceEvent.timestamp DESC;

DROP TABLE IF EXISTS ecResponsesInstance;
CREATE TEMPORARY TABLE ecResponsesInstance AS SELECT
  entity.id,
  traceEvent.timestamp,
  traceEvent.sqcnr
FROM entity, event, traceEvent WHERE
  entity.entityTypeId = (SELECT id FROM entityType WHERE entityType.name = 'EC') AND
  NOT EXISTS(SELECT value FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems')) AND
  event.id = (SELECT CAST(value AS INTEGER) FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecResponse')) AND
  traceEvent.entityId = event.entityId AND traceEvent.eventTypeId = event.eventTypeId ORDER BY entity.id, traceEvent.timestamp;

-- collect all event chain instances (no sqcnr number yet)
DROP TABLE IF EXISTS tempECInstanceNoNr;
CREATE TEMPORARY TABLE tempECInstanceNoNr AS
WITH
  ecReactionInstance(ecId, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr) AS (SELECT
    ecStimuliInstance.id,
    ecStimuliInstance.timestamp,
    ecStimuliInstance.sqcnr,
    ecResponsesInstance.timestamp,
    ecResponsesInstance.sqcnr
  FROM ecResponsesInstance, ecStimuliInstance WHERE
    ecStimuliInstance.id = ecResponsesInstance.id AND
    ecResponsesInstance.timestamp > ecStimuliInstance.timestamp AND
    IFNULL(ecResponsesInstance.timestamp <= (SELECT timestamp FROM ecStimuliInstance AS nextEcStimuliInstance WHERE nextEcStimuliInstance.id = ecStimuliInstance.id AND nextEcStimuliInstance.rowid = ecStimuliInstance.rowid - 1), 1)
  GROUP BY ecStimuliInstance.id, ecStimuliInstance.timestamp, ecStimuliInstance.sqcnr),

  ecAgeInstance(ecId, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr) AS (SELECT
    ecResponsesInstance.id,
    ecStimuliInstance.timestamp,
    ecStimuliInstance.sqcnr,
    ecResponsesInstance.timestamp,
    ecResponsesInstance.sqcnr
  FROM ecResponsesInstance, ecStimuliInstance WHERE
    ecStimuliInstance.id = ecResponsesInstance.id AND
    ecResponsesInstance.timestamp > ecStimuliInstance.timestamp AND
    IFNULL(ecStimuliInstance.timestamp >= (SELECT timestamp FROM ecResponsesInstance AS previousEcResponsesInstance WHERE previousEcResponsesInstance.id = ecResponsesInstance.id AND previousEcResponsesInstance.rowid = ecResponsesInstance.rowid - 1), 1)
  GROUP BY ecResponsesInstance.id, ecResponsesInstance.timestamp, ecResponsesInstance.sqcnr)

SELECT ecId, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr, MAX(age) AS isAge, MAX(reaction) AS isReaction FROM
(SELECT *, 1 AS reaction, 0 AS age FROM ecReactionInstance UNION ALL SELECT *, 0 AS reaction, 1 AS age FROM ecAgeInstance)
GROUP BY ecId, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr;

-- calculate sqcnrs for each event chain instance
DROP TABLE IF EXISTS tempECInstance;
CREATE TEMPORARY TABLE tempECInstance AS
WITH ecInstanceOffset(ecId, sqcnrOffset) AS (SELECT ecId, MIN(rowid) FROM tempECInstanceNoNr GROUP BY ecId)
SELECT ecId, rowid - (SELECT sqcnrOffset FROM ecInstanceOffset WHERE ecId = tempECInstanceNoNr.ecId) AS sqcnr, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr, isAge, isReaction FROM tempECInstanceNoNr;

-- copy from the temporary table to the entity instance
INSERT OR IGNORE INTO entityInstance SELECT ecId, sqcnr FROM tempECInstance WHERE stimulusTimestamp > -1 AND responseTimestamp > -1;
-- also derive instance infos
INSERT OR IGNORE INTO eventChainInstanceInfo SELECT * FROM tempECInstance WHERE stimulusTimestamp > -1 AND responseTimestamp > -1;


-- now do the same for composite event chains (only one level down supported)
DROP TABLE IF EXISTS tempCompositeECInstance;
CREATE TEMPORARY TABLE tempCompositeECInstance AS

WITH RECURSIVE
  compositeEc2ItemCount(ecId, itemCount) AS (
  SELECT
    entity.id,
    (SELECT COUNT(value) FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems'))
  FROM entity WHERE
    entity.entityTypeId = (SELECT id FROM entityType WHERE entityType.name = 'EC') AND
    EXISTS(SELECT value FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems'))
  ),

  compositeEcReactionInstance(ecId, ecInstance, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr, itemEcId, itemEcInstance, itemIndex) AS (
  SELECT
    entity.id,
    eventChainInstanceInfo.entityInstance,
    eventChainInstanceInfo.stimulusTimestamp,
    eventChainInstanceInfo.stimulusSqcnr,
    eventChainInstanceInfo.responseTimestamp,
    eventChainInstanceInfo.responseSqcnr,
    eventChainInstanceInfo.entityId,
    eventChainInstanceInfo.entityInstance,
    0
  FROM entity, eventChainInstanceInfo WHERE
    entity.entityTypeId = (SELECT id FROM entityType WHERE entityType.name = 'EC') AND
    EXISTS(SELECT value FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems')) AND
    eventChainInstanceInfo.entityId = (SELECT CAST(value AS INTEGER) FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems') AND propertyValue.sqcnr = 0) AND
    eventChainInstanceInfo.isReaction
  UNION ALL
  SELECT
    compositeEcReactionInstance.ecId,
    compositeEcReactionInstance.ecInstance,
    eventChainInstanceInfo.stimulusTimestamp,
    eventChainInstanceInfo.stimulusSqcnr,
    eventChainInstanceInfo.responseTimestamp,
    eventChainInstanceInfo.responseSqcnr,
    eventChainInstanceInfo.entityId,
    eventChainInstanceInfo.entityInstance,
    compositeEcReactionInstance.itemIndex + 1
  FROM compositeEcReactionInstance, eventChainInstanceInfo WHERE
    eventChainInstanceInfo.entityId = (SELECT CAST(value AS INTEGER) FROM propertyValue WHERE entityId = compositeEcReactionInstance.ecId AND propertyId = (SELECT id FROM property WHERE name = 'ecItems') AND propertyValue.sqcnr = compositeEcReactionInstance.itemIndex + 1) AND
    eventChainInstanceInfo.stimulusTimestamp = (SELECT responseTimestamp FROM eventChainInstanceInfo AS previousEcItem WHERE previousEcItem.entityId = compositeEcReactionInstance.itemEcId AND previousEcItem.entityInstance = compositeEcReactionInstance.itemEcInstance) AND
    eventChainInstanceInfo.isReaction
  ),

  compositeEcAgeInstance(ecId, ecInstance, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr, itemEcId, itemEcInstance, itemIndex) AS (
  SELECT
    entity.id,
    eventChainInstanceInfo.entityInstance,
    eventChainInstanceInfo.stimulusTimestamp,
    eventChainInstanceInfo.stimulusSqcnr,
    eventChainInstanceInfo.responseTimestamp,
    eventChainInstanceInfo.responseSqcnr,
    eventChainInstanceInfo.entityId,
    eventChainInstanceInfo.entityInstance,
    0
  FROM entity, eventChainInstanceInfo WHERE
    entity.entityTypeId = (SELECT id FROM entityType WHERE entityType.name = 'EC') AND
    EXISTS(SELECT value FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems')) AND
    eventChainInstanceInfo.entityId = (SELECT CAST(value AS INTEGER) FROM propertyValue WHERE entityId = entity.id AND propertyId = (SELECT id FROM property WHERE name = 'ecItems') AND propertyValue.sqcnr = 0) AND
    eventChainInstanceInfo.isAge
  UNION ALL
  SELECT
    compositeEcAgeInstance.ecId,
    compositeEcAgeInstance.ecInstance,
    eventChainInstanceInfo.stimulusTimestamp,
    eventChainInstanceInfo.stimulusSqcnr,
    eventChainInstanceInfo.responseTimestamp,
    eventChainInstanceInfo.responseSqcnr,
    eventChainInstanceInfo.entityId,
    eventChainInstanceInfo.entityInstance,
    compositeEcAgeInstance.itemIndex + 1
  FROM compositeEcAgeInstance, eventChainInstanceInfo WHERE
    eventChainInstanceInfo.entityId = (SELECT CAST(value AS INTEGER) FROM propertyValue WHERE entityId = compositeEcAgeInstance.ecId AND propertyId = (SELECT id FROM property WHERE name = 'ecItems') AND propertyValue.sqcnr = compositeEcAgeInstance.itemIndex + 1) AND
    eventChainInstanceInfo.stimulusTimestamp = (SELECT responseTimestamp FROM eventChainInstanceInfo AS previousEcItem WHERE previousEcItem.entityId = compositeEcAgeInstance.itemEcId AND previousEcItem.entityInstance = compositeEcAgeInstance.itemEcInstance) AND
    eventChainInstanceInfo.isAge
  )

SELECT ecId, ecInstance, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr, MAX(age) AS isAge, MAX(reaction) AS isReaction FROM (
SELECT ecId, ecInstance,
  MAX(CASE WHEN itemIndex = 0 THEN stimulusTimestamp ELSE -1 END) AS stimulusTimestamp,
  MAX(CASE WHEN itemIndex = 0 THEN stimulusSqcnr ELSE -1 END) AS stimulusSqcnr,
  MAX(CASE WHEN itemIndex = (SELECT itemCount - 1 FROM compositeEc2ItemCount WHERE compositeEc2ItemCount.ecId = compositeEcReactionInstance.ecId) THEN responseTimestamp ELSE -1 END) AS responseTimestamp,
  MAX(CASE WHEN itemIndex = (SELECT itemCount - 1 FROM compositeEc2ItemCount WHERE compositeEc2ItemCount.ecId = compositeEcReactionInstance.ecId) THEN responseSqcnr ELSE -1 END) AS responseSqcnr,
  1 AS reaction, 0 AS age FROM compositeEcReactionInstance GROUP BY ecId, ecInstance UNION ALL
SELECT ecId, ecInstance,
  MAX(CASE WHEN itemIndex = 0 THEN stimulusTimestamp ELSE -1 END) AS stimulusTimestamp,
  MAX(CASE WHEN itemIndex = 0 THEN stimulusSqcnr ELSE -1 END) AS stimulusSqcnr,
  MAX(CASE WHEN itemIndex = (SELECT itemCount - 1 FROM compositeEc2ItemCount WHERE compositeEc2ItemCount.ecId = compositeEcAgeInstance.ecId) THEN responseTimestamp ELSE -1 END) AS responseTimestamp,
  MAX(CASE WHEN itemIndex = (SELECT itemCount - 1 FROM compositeEc2ItemCount WHERE compositeEc2ItemCount.ecId = compositeEcAgeInstance.ecId) THEN responseSqcnr ELSE -1 END) AS responseSqcnr,
  0 AS reaction, 1 AS age FROM compositeEcAgeInstance GROUP BY ecId, ecInstance
) GROUP BY ecId, ecInstance, stimulusTimestamp, stimulusSqcnr, responseTimestamp, responseSqcnr ORDER BY ecId, ecInstance;

-- copy from temporary table to entityInstance
INSERT OR IGNORE INTO entityInstance SELECT DISTINCT ecId, ecInstance FROM tempCompositeECInstance WHERE stimulusTimestamp > -1 AND responseTimestamp > -1;
-- also derive instance infos
INSERT OR IGNORE INTO eventChainInstanceInfo SELECT * FROM tempCompositeECInstance WHERE stimulusTimestamp > -1 AND responseTimestamp > -1;
