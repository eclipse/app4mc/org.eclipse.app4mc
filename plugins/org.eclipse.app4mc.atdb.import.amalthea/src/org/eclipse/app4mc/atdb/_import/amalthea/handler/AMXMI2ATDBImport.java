/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea.handler;

import java.util.List;

import javax.inject.Named;

import org.eclipse.app4mc.atdb._import.amalthea.wizard.ImportWizard;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;

public class AMXMI2ATDBImport {

	@Execute
	public void execute(final @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection, final IWorkbenchWindow runnableContext) {
		final List<?> selectionList = selection.toList();
		final String amxmiSource = selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
				.filter(f -> f.getFileExtension().equalsIgnoreCase("amxmi")).map(f -> f.getLocation().toString())
				.findFirst().orElseThrow(() -> new IllegalArgumentException("Selection does not contain an *.amxmi file!"));
		final String atdbSource = selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
				.filter(f -> f.getFileExtension().equalsIgnoreCase("atdb")).map(f -> f.getLocation().toString())
				.findFirst().orElseThrow(() -> new IllegalArgumentException("Selection does not contain an *.atdb file!"));
		final IFile atdbTarget = selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
				.filter(f -> f.getFileExtension().equalsIgnoreCase("amxmi")).map(IResource::getParent).map(c -> {
					// use the file name of the amxmi
					final int from = Math.max(amxmiSource.lastIndexOf('/'), amxmiSource.lastIndexOf('\\'));
					final int to = amxmiSource.lastIndexOf(".amxmi"); //$NON-NLS-1$
					final String name = amxmiSource.substring(from + 1, to);
					return c.getProject().getFile(name + ".atdb");
				}).findFirst().orElseThrow(() -> new IllegalArgumentException("Selected *.amxmi file is not part of a Eclipse workspace!"));
		if (amxmiSource.length() == 0 || atdbSource.length() == 0 || (runnableContext == null)) {
			return;
		}

		ImportWizard.importAndOpenInIDE(amxmiSource, atdbSource, atdbTarget, true, runnableContext);
	}

}
