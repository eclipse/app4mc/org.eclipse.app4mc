/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea.wizard;

import java.io.File;

import org.eclipse.core.resources.IContainer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.WizardResourceImportPage;
import org.eclipse.ui.internal.ide.IDEWorkbenchMessages;

@SuppressWarnings("restriction")
class ImportPage extends WizardResourceImportPage {

	private Text amxmiNameField;
	private Label atdbGroupLabel;
	private Text atdbNameField;
	private Button atdbBrowseButton;
	private Button updateExistingATDBFile;

	ImportPage(final IStructuredSelection selection) {
		this("AMALTHEAImportPage", selection); //$NON-NLS-1$
		setTitle(Messages.ImportPage_title);
		this.setMessage(Messages.ImportPage_message);
	}

	ImportPage(final String name, final IStructuredSelection selection) {
		super(name, selection);
	}

	@Override
	protected void createSourceGroup(final Composite parent) {
		final Composite sourceContainerGroup = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		sourceContainerGroup.setLayout(layout);
		sourceContainerGroup.setFont(parent.getFont());
		sourceContainerGroup.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));

		// amxmi label
		final Label amxmiGroupLabel = new Label(sourceContainerGroup, SWT.NONE);
		amxmiGroupLabel.setText(Messages.ImportPage_fromAmxmi);
		amxmiGroupLabel.setFont(parent.getFont());

		// amxmi name entry field
		amxmiNameField = new Text(sourceContainerGroup, SWT.READ_ONLY | SWT.BORDER);
		final GridData amxmiData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);
		amxmiData.widthHint = SIZING_TEXT_FIELD_WIDTH;
		amxmiNameField.setLayoutData(amxmiData);
		amxmiNameField.setFont(parent.getFont());

		// amxmi browse button
		final Button amxmiBrowseButton = new Button(sourceContainerGroup, SWT.PUSH);
		amxmiBrowseButton.setText(IDEWorkbenchMessages.WizardImportPage_browse2);
		amxmiBrowseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				selectAMXMIFile();
				update();
			}
		});
		amxmiBrowseButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		amxmiBrowseButton.setFont(parent.getFont());
		setButtonLayoutData(amxmiBrowseButton);

		// atdb label
		atdbGroupLabel = new Label(sourceContainerGroup, SWT.NONE);
		atdbGroupLabel.setText(Messages.ImportPage_fromATDB);
		atdbGroupLabel.setFont(parent.getFont());
		atdbGroupLabel.setEnabled(false);

		// atdb name entry field
		atdbNameField = new Text(sourceContainerGroup, SWT.READ_ONLY | SWT.BORDER);
		final GridData atdbData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);
		atdbData.widthHint = SIZING_TEXT_FIELD_WIDTH;
		atdbNameField.setLayoutData(atdbData);
		atdbNameField.setFont(parent.getFont());
		atdbNameField.setEnabled(false);

		// atdb browse button
		atdbBrowseButton = new Button(sourceContainerGroup, SWT.PUSH);
		atdbBrowseButton.setText(IDEWorkbenchMessages.WizardImportPage_browse2);
		atdbBrowseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				selectATDBFile();
				update();
			}
		});
		atdbBrowseButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		atdbBrowseButton.setFont(parent.getFont());
		atdbBrowseButton.setEnabled(false);
		setButtonLayoutData(atdbBrowseButton);
	}

	@Override
	protected void createOptionsGroupButtons(Group optionsGroup) {
		updateExistingATDBFile = new Button(optionsGroup, SWT.CHECK);
		updateExistingATDBFile.setFont(optionsGroup.getFont());
		updateExistingATDBFile.setText(Messages.ImportPage_optionUpdateATDB);
		updateExistingATDBFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				update();
			}
		});
	}

	@Override
	protected ITreeContentProvider getFileProvider() {
		return null;
	}

	@Override
	protected ITreeContentProvider getFolderProvider() {
		return null;
	}

	@Override
	protected boolean determinePageCompletion() {
		boolean result = super.determinePageCompletion();

		final String amxmiPath = amxmiNameField.getText();
		final File amxmiFile = new File(amxmiPath);
		result &= amxmiFile.exists();

		if (updateExistingATDBFile != null && updateExistingATDBFile.getSelection()) {
			final String atdbPath = atdbNameField.getText();
			final File atdbFile = new File(atdbPath);
			result &= atdbFile.exists();
		}

		return result;
	}

	private void selectAMXMIFile() {
		final FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
		fileDialog.setText(Messages.ImportPage_selectFile);
		fileDialog.setFilterExtensions(new String[] { "*.amxmi" }); //$NON-NLS-1$
		final String open = fileDialog.open();
		if (open != null) {
			amxmiNameField.setText(open);
			update();
		}
	}

	private void selectATDBFile() {
		final FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
		fileDialog.setText(Messages.ImportPage_selectFile);
		fileDialog.setFilterExtensions(new String[] { "*.atdb" }); //$NON-NLS-1$
		final String open = fileDialog.open();
		if (open != null) {
			atdbNameField.setText(open);
			update();
		}
	}

	private void update() {
		setPageComplete(determinePageCompletion());
		atdbGroupLabel.setEnabled(updateExistingATDBFile.getSelection());
		atdbBrowseButton.setEnabled(updateExistingATDBFile.getSelection());
		atdbNameField.setEnabled(updateExistingATDBFile.getSelection());
	}

	String getAMXMISource() {
		return amxmiNameField.getText();
	}

	String getATDBSource() {
		return atdbNameField.getText();
	}

	IContainer getTargetContainer() {
		return getSpecifiedContainer();
	}

	boolean isUpdateExistingATDB() {
		return updateExistingATDBFile.getSelection();
	}

}
