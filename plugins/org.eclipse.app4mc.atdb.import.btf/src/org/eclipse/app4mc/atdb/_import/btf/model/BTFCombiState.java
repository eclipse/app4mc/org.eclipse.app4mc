/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.model;

import java.util.EnumSet;

public enum BTFCombiState {

	coreExecution(BTFEntityState.running, BTFEntityState.polling),
	netExecution(BTFEntityState.running, BTFEntityState.polling, BTFEntityState.waiting),
	grossExecution(BTFEntityState.running, BTFEntityState.polling, BTFEntityState.parking, BTFEntityState.waiting, BTFEntityState.ready);

	private final EnumSet<BTFEntityState> states;

	BTFCombiState(final BTFEntityState firstState, final BTFEntityState... otherStates) {
		states = EnumSet.of(firstState, otherStates);
	}

	public EnumSet<BTFEntityState> getStates() {
		return states;
	}

}
