/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.ProcessEventType;
import org.eclipse.app4mc.amalthea.model.RunnableEventType;

public enum BTFCountMetric {

	activations(ProcessEventType.ACTIVATE),
	preemptions(ProcessEventType.PREEMPT),
	suspensions(RunnableEventType.SUSPEND);

	public final Enum<?> eventToCount;
	public final BTFEntityType entityType;

	BTFCountMetric(final Enum<?> eventToCount) {
		this.eventToCount = eventToCount;
		String entityTypeName = eventToCount.getClass().getSimpleName();
		entityTypeName = entityTypeName.substring(0, entityTypeName.indexOf("EventType")).toLowerCase();
		entityType = BTFEntityType.getForName(entityTypeName);
	}

	public static List<Enum<?>> getInvolvedBTFEventsForEntityType(final BTFEntityType entityType) {
		return Stream.of(BTFCountMetric.values()).filter(cm -> cm.entityType == entityType)
				.map(cm -> cm.eventToCount).collect(Collectors.toList());
	}

}
