/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.model;

import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.ProcessEventType;
import org.eclipse.app4mc.amalthea.model.RunnableEventType;
import org.eclipse.emf.common.util.Enumerator;

public enum BTFInterInstanceMetric {

	activateToActivate(ProcessEventType.ACTIVATE, ProcessEventType.ACTIVATE),
	startToStart(new SimpleEntry<>(ProcessEventType.START, ProcessEventType.START), new SimpleEntry<>(RunnableEventType.START, RunnableEventType.START)),
	endToEnd(new SimpleEntry<>(ProcessEventType.TERMINATE, ProcessEventType.TERMINATE), new SimpleEntry<>(RunnableEventType.TERMINATE, RunnableEventType.TERMINATE)),
	endToStart(new SimpleEntry<>(ProcessEventType.TERMINATE, ProcessEventType.START), new SimpleEntry<>(RunnableEventType.TERMINATE, RunnableEventType.START)),
	slack(ProcessEventType.TERMINATE, ProcessEventType.ACTIVATE); // switch the sign of this metric to get lateness

	public final Map<BTFEntityType, Entry<Enumerator, Enumerator>> entityType2FirstAndSecond;

	BTFInterInstanceMetric(final Enumerator firstInstEvent, final Enumerator secondInstEvent) {
		this(new SimpleEntry<>(firstInstEvent, secondInstEvent));
	}

	@SafeVarargs BTFInterInstanceMetric(final Entry<Enumerator, Enumerator>...firstAndSecondInstEvent) {
		entityType2FirstAndSecond = new LinkedHashMap<>();
		Stream.of(firstAndSecondInstEvent).forEach(entry -> {
			String entityTypeName = entry.getKey().getClass().getSimpleName();
			entityTypeName = entityTypeName.substring(0, entityTypeName.indexOf("EventType")).toLowerCase();
			entityType2FirstAndSecond.put(BTFEntityType.getForName(entityTypeName), entry);
		});
	}

}
