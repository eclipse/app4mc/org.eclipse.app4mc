/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.wizard;

import java.io.File;

import org.eclipse.core.resources.IContainer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.WizardResourceImportPage;
import org.eclipse.ui.internal.ide.IDEWorkbenchMessages;

@SuppressWarnings("restriction")
class ImportPage extends WizardResourceImportPage {

	private Text sourceNameField;
	private Button calculateMetrics;
	private Button persistTraceEvents;
	private Button doInMemoryDBImport;

	ImportPage(final IStructuredSelection selection) {
		this("btfImportPage", selection); //$NON-NLS-1$
		setTitle(Messages.ImportPage_title);
		this.setMessage(Messages.ImportPage_message);
	}

	ImportPage(final String name, final IStructuredSelection selection) {
		super(name, selection);
	}

	@Override
	protected void createSourceGroup(final Composite parent) {
		final Composite sourceContainerGroup = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		sourceContainerGroup.setLayout(layout);
		sourceContainerGroup.setFont(parent.getFont());
		sourceContainerGroup.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));

		// source label
		final Label groupLabel = new Label(sourceContainerGroup, SWT.NONE);
		groupLabel.setText(Messages.ImportPage_fromBTF);
		groupLabel.setFont(parent.getFont());

		// source name entry field
		sourceNameField = new Text(sourceContainerGroup, SWT.READ_ONLY | SWT.BORDER);
		final GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);
		data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		sourceNameField.setLayoutData(data);
		sourceNameField.setFont(parent.getFont());

		// source browse button
		final Button sourceBrowseButton = new Button(sourceContainerGroup, SWT.PUSH);
		sourceBrowseButton.setText(IDEWorkbenchMessages.WizardImportPage_browse2);
		sourceBrowseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				selectFile();
				update();
			}
		});
		sourceBrowseButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		sourceBrowseButton.setFont(parent.getFont());
		setButtonLayoutData(sourceBrowseButton);
	}

	@Override
	protected void createOptionsGroupButtons(Group optionsGroup) {
		calculateMetrics = new Button(optionsGroup, SWT.CHECK);
		calculateMetrics.setFont(optionsGroup.getFont());
		calculateMetrics.setText(Messages.ImportPage_optionCalculateMetrics);
		calculateMetrics.setSelection(true);

		persistTraceEvents = new Button(optionsGroup, SWT.CHECK);
		persistTraceEvents.setFont(optionsGroup.getFont());
		persistTraceEvents.setText(Messages.ImportPage_optionPersistTraceEvents);
		persistTraceEvents.setSelection(true);

		doInMemoryDBImport = new Button(optionsGroup, SWT.CHECK);
		doInMemoryDBImport.setFont(optionsGroup.getFont());
		doInMemoryDBImport.setText(Messages.ImportPage_optionDoInMemoryDBImport);
		doInMemoryDBImport.setSelection(true);
	}

	@Override
	protected ITreeContentProvider getFileProvider() {
		return null;
	}

	@Override
	protected ITreeContentProvider getFolderProvider() {
		return null;
	}

	@Override
	protected boolean determinePageCompletion() {
		boolean result = super.determinePageCompletion();

		final String path = sourceNameField.getText();
		final File file = new File(path);
		result &= file.exists();

		return result;
	}

	private void selectFile() {
		final FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
		fileDialog.setText(Messages.ImportPage_selectFile);
		fileDialog.setFilterExtensions(new String[] { "*.btf" }); //$NON-NLS-1$
		final String open = fileDialog.open();
		if (open != null) {
			sourceNameField.setText(open);
			update();
		}
	}

	private void update() {
		setPageComplete(determinePageCompletion());
	}

	String getSource() {
		return sourceNameField.getText();
	}

	IContainer getTargetContainer() {
		return getSpecifiedContainer();
	}

	boolean isCalculateMetrics() {
		return calculateMetrics.getSelection();
	}

	boolean isPersistTraceEvents() {
		return persistTraceEvents.getSelection();
	}

	boolean isDoInMemoryDBImport() {
		return doInMemoryDBImport.getSelection();
	}

}
