/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb.metrics;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.app4mc.atdb.MetricAggregation;
import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.app4mc.atdb.metrics.messages"; //$NON-NLS-1$
	public static String DBViewer_fileErrorMessage;
	public static String DBViewer_fileErrorTitle;
	public static String DBViewer_immutableResultError;
	public static String DBViewer_processTitle;
	public static String DBViewer_runnableTitle;
	public static String DBViewer_eventChainTitle;
	public static String DBViewer_nameTitle;
	public static String DBViewer_metricColumnTitle;
	public static String DBViewer_processTimeMetricsTitle;
	public static String DBViewer_runnableTimeMetricsTitle;
	public static String DBViewer_eventChainTimeMetricsTitle;
	public static String MetricAggregation_minimum;
	public static String MetricAggregation_maximum;
	public static String MetricAggregation_average;
	public static String MetricAggregation_stanDev;

	static Map<MetricAggregation, String> metricAggr2UILabel = new LinkedHashMap<>();
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
		metricAggr2UILabel.put(MetricAggregation.Min, MetricAggregation_minimum);
		metricAggr2UILabel.put(MetricAggregation.Max, MetricAggregation_maximum);
		metricAggr2UILabel.put(MetricAggregation.Avg, MetricAggregation_average);
		metricAggr2UILabel.put(MetricAggregation.StDev, MetricAggregation_stanDev);
	}

	private Messages() {
	}
}
