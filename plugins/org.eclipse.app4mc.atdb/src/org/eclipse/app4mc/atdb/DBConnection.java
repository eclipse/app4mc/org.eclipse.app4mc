/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb;

import java.io.File;
import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.sqlite.JDBC;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.JournalMode;
import org.sqlite.SQLiteConfig.SynchronousMode;
import org.sqlite.SQLiteConfig.TempStore;

public class DBConnection<T extends DBConnection<T>> implements AutoCloseable {

	/**
	 * Tri-state enumeration to specify the access mode.
	 */
	public enum AccessMode {
		/**
		 * read only access (update statements are not allowed)
		 */
		ReadOnly,
		/**
		 * read and write access (write accesses will asynchronously be written to the data base file after execution)
		 */
		ReadWrite,
		/**
		 * read and write access (data base will be in memory, write accesses will be written to the in memory data base,
		 * data base will be written to the file during connection closing)
		 */
		ReadWriteInMemory
	}

	private static final String TABLE_EXISTS_QUERY = "SELECT name FROM sqlite_master WHERE type = 'table' AND name = ?;";

	private final Connection connection;

	private final Map<String, PreparedStatement> prepStmts;
	private final Map<String, PreparedStatement> prepIdStmts;
	private final Collection<Statement> stmts;
	private final Set<PreparedStatement> currentBatches;
	private boolean isInBatchMode = false;
	private final File dbFile;
	private final AccessMode accessMode;

	/**
	 * Creates a connection to the given db file in read-only mode.
	 *
	 * @param dbFile The path to the db file.
	 * @throws SQLException
	 */
	public DBConnection(final String dbFile) throws SQLException {
		this(dbFile, AccessMode.ReadOnly);
	}

	/**
	 * Creates a connection to the given db file.
	 *
	 * @param dbFileName The path to the db file.
	 * @param accessMode Specify the access mode to open the db connection in Read/ReadWrite/ReadWriteInMemory access mode.
	 * @throws SQLException
	 */
	public DBConnection(final String dbFileName, final AccessMode accessMode) throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC"); // init JDBC driver
		} catch (ClassNotFoundException e) {
			// fail silently. DriverManager will throw an SQL exception complaining about a missing driver
		}
		this.dbFile = new File(dbFileName);
		this.accessMode = accessMode;
		if (accessMode == AccessMode.ReadOnly) {
			final SQLiteConfig config = new SQLiteConfig();
			config.setReadOnly(true);
			final String dbFileUrl = JDBC.PREFIX + this.dbFile.getAbsolutePath();
			this.connection = config.createConnection(dbFileUrl);
		} else {
			final SQLiteConfig config = new SQLiteConfig();
			config.enforceForeignKeys(true); // enable foreign key checks
			config.setJournalMode(JournalMode.OFF); // disable journal mode (faster insertions)
			if (accessMode == AccessMode.ReadWriteInMemory) {
				config.setTempStore(TempStore.MEMORY); // use memory for temp data bases and indices (if in memory)
			} else {
				config.setSynchronous(SynchronousMode.OFF); // write out db asynchronously (if not in memory)
			}
			// use more threads to speed up prepared statements (currently not supported by SQLite)
			// config.setPragma(Pragma.THREADS, Runtime.getRuntime().availableProcessors() - 1)

			final String dbFileUrl = JDBC.PREFIX + (accessMode == AccessMode.ReadWriteInMemory ? ":memory:" : this.dbFile.getAbsolutePath());
			this.connection = config.createConnection(dbFileUrl);
			if (accessMode == AccessMode.ReadWriteInMemory && this.dbFile.exists()) {
				try (final Statement fileRestoreStmt = this.connection.createStatement()) {
					final String restoreDB = String.format("restore from '%s'", this.dbFile.getAbsolutePath());
					fileRestoreStmt.executeUpdate(restoreDB);
				}
			}
		}
		this.prepStmts = new LinkedHashMap<>();
		this.prepIdStmts = new LinkedHashMap<>();
		this.stmts = new LinkedHashSet<>();
		this.currentBatches = new LinkedHashSet<>();
	}

	public void flushAllStatements() throws SQLException {
		for(final PreparedStatement ps:this.prepStmts.values()) {
			ps.close();
		}
		this.prepStmts.clear();
		this.prepIdStmts.clear();
	}

	/**
	 * Closes all statements created by this connection. Then closes the connection to the data base itself.
	 */
	@Override
	public void close() throws SQLException {
		flushAllStatements();
		if (this.accessMode == AccessMode.ReadWriteInMemory) {
			try (final Statement fileBackupStmt = this.connection.createStatement()) {
				final String backupDB = String.format("backup to '%s'", this.dbFile.getAbsolutePath());
				fileBackupStmt.executeUpdate(backupDB);
			}
		}
		this.connection.close();
	}

	/**
	 * Executes the statement (changing the contents of the db) given in the query.
	 *
	 * @param query The statement that performs the changes in the db.
	 * @throws SQLException
	 */
	public void executeUpdate(final String query) throws SQLException {
		try (final Statement statement = this.connection.createStatement()) {
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			statement.executeUpdate(query);
		}
	}

	/**
	 * Executes the statement (changing the contents of the db) given in the query. Expected to run long and produce many new rows.
	 *
	 * @param query The statement that performs the changes in the db.
	 * @throws SQLException
	 */
	public void executeLargeUpdate(final String query) throws SQLException {
		try (final Statement statement = this.connection.createStatement()) {
			statement.executeLargeUpdate(query);
		}
	}

	/**
	 * Same as {@link java.util.function.Consumer}, but may throw exception of type E.
	 * @param <T>
	 * @param <E>
	 */
	@FunctionalInterface
	public static interface ThrowingConsumer<T, E extends Exception> {
	    void accept(T t) throws E;
	}

	/**
	 * Executes a read-only query and consumes the result set via the given consumer function.
	 *
	 * @param query The read-only query to execute.
	 * @param consumer The function that consumes the result set.
	 * @throws SQLException
	 */
	public void queryAndConsumeResult(final String query, final ThrowingConsumer<ResultSet, SQLException> consumer) throws SQLException {
		try (final Statement tmpStatement = this.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)) {
			final ResultSet resultSet = tmpStatement.executeQuery(query);
			consumer.accept(resultSet);
		}
	}

	/**
	 * Same as {@link java.util.function.Function}, but may throw exception of type E.
	 * @param <T>
	 * @param <R>
	 * @param <E>
	 */
	@FunctionalInterface
	public static interface ThrowingFunction<T, R, E extends Exception> {
	    R apply(T t) throws E;
	}

	/**
	 * Can be used to send a prepared statement query to the data base,
	 * map each result row via the rowMapper parameter,
	 * and return the result for further processing.
	 *
	 * @param <R> Target type for the row mapper and returned stream.
	 * @param query The prepared statement query
	 * @param rowMapper The result row mapping function
	 * @return Stream of mapped row results.
	 * @throws SQLException
	 */
	public <R> Stream<R> queryAndMapToStream(final PreparedStatement query, final ThrowingFunction<ResultSet, R, SQLException> rowMapper)
			throws SQLException {
		try (final ResultSet resultSet = query.executeQuery()) {
			final Builder<R> result = Stream.builder();
			while (resultSet.next()) {
				result.accept(rowMapper.apply(resultSet));
			}
			return result.build();
		}
	}

	/**
	 * Returns a prepared statement for the the given query. Will return an existing one if the query was already created as a prepared statement.
	 * Since the prepared statement is internally cached, this DBConnection will also close it, when the connection is closed. Thus, the user must not
	 * close the prepared statement.
	 *
	 * @param query The query to put into the prepared statement.
	 * @return The prepared statement for the given query.
	 * @throws SQLException
	 */
	public PreparedStatement getPreparedStatementFor(final String query) throws SQLException {
		if (!this.prepStmts.containsKey(query)) {
			this.prepStmts.put(query, this.connection.prepareStatement(query));
		}
		return this.prepStmts.get(query);
	}

	/**
	 * Returns a prepared statement for the the given query. Will return an existing one if the query was already created as a prepared statement.
	 * Since the prepared statement is internally cached, this DBConnection will also close it, when the connection is closed. Thus, the user must not
	 * close the prepared statement. Use the additional field <code>idName</code> to specify the name of the column denoting the row ids of the table.
	 *
	 * @param query The query to put into the prepared statement.
	 * @param idName The name of the column denoting the id of the table.
	 * @return The prepared statement for the given query.
	 * @throws SQLException
	 */
	protected PreparedStatement getPreparedStatementFor(final String query, final String idName) throws SQLException {
		if (!this.prepIdStmts.containsKey(query)) {
			if (idName != null && !idName.isEmpty()) {
				this.prepIdStmts.put(query, this.connection.prepareStatement(query, new String[] {idName}));
			} else {
				this.prepIdStmts.put(query, this.getPreparedStatementFor(query));
			}
		}
		return this.prepIdStmts.get(query);
	}

	/**
	 * Returns a prepared query for the given query string. This query is only allowed read access to the data base. Will return an existing one if the query
	 * was already created as a prepared query. Since the prepared query is internally cached, this DBConnection will also close it, when the connection is
	 * closed. Thus, the user must not close the prepared query.
	 *
	 * @param query The query string to put into the prepared query.
	 * @return The prepared query for the given query string.
	 * @throws SQLException
	 */
	public PreparedStatement getPrepareQueryFor(final String query) throws SQLException {
		if (!this.prepStmts.containsKey(query)) {
			this.prepStmts.put(query, this.connection.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY));
		}
		return this.prepStmts.get(query);
	}

	/**
	 * Creates and returns a new statement. This DBConection will also close the statement, when the connection is closed. Thus, the user must not close the
	 * statement.
	 *
	 * @return A newly created statement.
	 * @throws SQLException
	 */
	public Statement createStatement() throws SQLException {
		final Statement stmt = this.connection.createStatement();
		this.stmts.add(stmt);
		return stmt;
	}

	/**
	 * Executes the given statement batches (calls {@link Statement#executeBatch()} on each provided statement).
	 *
	 * @param statements Batches of statements to be executed.
	 * @throws SQLException
	 */
	public void executeBatchStatements(final Statement...statements) throws SQLException {
		executeBatchStatements(Arrays.asList(statements));
	}

	/**
	 * Executes the given statement batches (calls {@link Statement#executeBatch()} on each provided statement).
	 *
	 * @param statements Batches of statements to be executed.
	 * @throws SQLException
	 */
	public void executeBatchStatements(final Collection<? extends Statement> statements) throws SQLException {
		final boolean oldAutoCommit = this.connection.getAutoCommit();
		if (oldAutoCommit) this.connection.setAutoCommit(false);
		try {
			for(final Statement stmt:statements) {
				stmt.executeBatch();
			}
		} finally {
			this.connection.setAutoCommit(oldAutoCommit);
		}
	}

	/**
	 * Checks whether a table with the given tableName exists in the data base.
	 *
	 * @param tableName The name of the table to check for existence.
	 * @return <code>TRUE</code> if a table with the specified name exists, <code>FALSE</code> otherwise.
	 * @throws SQLException
	 */
	public boolean tableExists(final String tableName) throws SQLException {
		final PreparedStatement prepStmt = getPrepareQueryFor(TABLE_EXISTS_QUERY);
		prepStmt.setString(1, tableName);
		try (final ResultSet resultSet = prepStmt.executeQuery()) {
			return resultSet.next();
		}
	}

	/**
	 * Can be used to execute updating statements (the ones that change the data base) in batches. These batches are only built up by calling
	 * {@link #executeBatchablePrepStmt(String, List)} (from a subclass, since it is protected).
	 *
	 * @param batchConsumer The consumer function in which all updating statements can be called to be batch executed.
	 * @throws SQLException
	 */
	public void executeBatchUpdate(final ThrowingConsumer<T, SQLException> batchConsumer) throws SQLException {
		this.isInBatchMode = true;
		// cast is safe because of recursive generic T
		@SuppressWarnings("unchecked")
		final T self = (T)this;
		batchConsumer.accept(self);
		executeBatchStatements(this.currentBatches);
		this.currentBatches.clear();
		this.isInBatchMode = false;
	}

	/**
	 * Convenience method to set all parameters in the prepared statement in one call.
	 * @param stmt The PreparedStatement for which to set the parameters.
	 * @param parameters The list of parameter values to be set for the prepared statement.
	 * @throws SQLException
	 */
	public void setPreparedParameters(final PreparedStatement stmt, final List<?> parameters) throws SQLException {
		final ParameterMetaData pmd = stmt.getParameterMetaData();
		if (pmd.getParameterCount() == parameters.size()) {
			for(int i = 0; i < parameters.size(); i++) {
				stmt.setObject(i+1, parameters.get(i));
			}
		}
	}

	/**
	 * Call this method to update the data base via a prepared statement with parameters. The generated key (generated by executing the statement) will
	 * be returned. It is assumed that the column name of the row id is called 'id'.
	 * @param prepStmt The prepared statement used to update the data base.
	 * @return The new id, generated by executing the statement. Will return -1 if no key in the column named 'id' has been generated.
	 * @throws SQLException
	 */
	protected long executePrepStmtAndGetGeneratedId(final String prepStmt, final List<?> parameters) throws SQLException {
		final PreparedStatement stmt = getPreparedStatementFor(prepStmt, "id");
		setPreparedParameters(stmt, parameters);
		final int affectedRows = stmt.executeUpdate();
		if (affectedRows == 0) {
            throw new SQLException("Statement: " + stmt + " failed. No rows affected.");
        }
		try (final ResultSet rs = stmt.getGeneratedKeys()) {
			if (rs.next()) {
				return rs.getLong(1);
			}
		}
		return -1;
	}

	/**
	 * Subclasses can call this method to update the data base via a prepared statement with parameters.
	 *
	 * @param prepStmt The prepared statement used to update the data base.
	 * @param parameters The parameters for the prepared statement.
	 * @throws SQLException
	 */
	protected void executeBatchablePrepStmt(final String prepStmt, final List<?> parameters) throws SQLException {
		final PreparedStatement stmt = getPreparedStatementFor(prepStmt);
		setPreparedParameters(stmt, parameters);
		if (this.isInBatchMode) {
			stmt.addBatch();
			this.currentBatches.add(stmt);
		} else {
			stmt.executeUpdate();
		}
	}

}
