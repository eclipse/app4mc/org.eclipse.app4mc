/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb;

import java.util.List;

public enum EntityProperty {
	SIMULATION_DURATION("time"),
	INITIAL_VALUE("object"),
	PROCESSORS("entityIdRef"),
	CORES("entityIdRef"),
	SYSTEM_START_TIME("time"),
	FREQUENCY_IN_HZ("long"),
	STIMULI("entityIdRef"),
	EXECUTING_CORE("entityIdRef"),
	RUNNABLES("entityIdRef"),
	READ_SIGNALS("entityIdRef"),
	WRITTEN_SIGNALS("entityIdRef"),
	SYSTEM_STOP_TIME("time"),
	EC_ITEMS("entityIdRef"),
	EC_STIMULUS("eventIdRef"),
	EC_RESPONSE("eventIdRef"),
	EC_MIN_ITEMS_COMPLETED("long");

	public static final List<EntityProperty> eventChainProperties = List.of(
			EC_ITEMS, EC_STIMULUS, EC_RESPONSE, EC_MIN_ITEMS_COMPLETED);

	public final String camelName;
	public final String type;

	private EntityProperty(final String type) {
		final StringBuilder sb = new StringBuilder();
		for (final String oneString : this.name().toLowerCase().toLowerCase().split("_")) {
		    sb.append(sb.length() > 0 ? oneString.substring(0, 1).toUpperCase() : oneString.substring(0, 1));
		    sb.append(oneString.substring(1));
		}
		this.camelName = sb.toString();
		this.type = type;
	}

	@Override
	public String toString() {
		return this.camelName;
	}

}
