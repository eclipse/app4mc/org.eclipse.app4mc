/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb;

public enum MetaInformation {
	TIME_BASE;

	public final String camelName;

	private MetaInformation() {
		final StringBuilder sb = new StringBuilder();
		for (final String oneString : this.name().toLowerCase().toLowerCase().split("_")) {
		    sb.append(sb.length() > 0 ? oneString.substring(0, 1).toUpperCase() : oneString.substring(0, 1));
		    sb.append(oneString.substring(1));
		}
		this.camelName = sb.toString();
	}

	@Override
	public String toString() {
		return this.camelName;
	}

}
