/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb;

public enum MetricAggregation {
	Min("minimum"),
	Max("maximum"),
	Avg("average", "printf('%.2f', ", ")"),
	StDev("stddev", "printf('%.2f', ", ")");

	private final String sqlStrPrefix;
	private final String sqlStrSuffix;
	private final String sqlLabel;

	private MetricAggregation(final String uiLabel) {
		this(uiLabel, "", "");
	}

	private MetricAggregation(final String sqlLabel, final String extraPrefix, final String extraSuffix) {
		this.sqlLabel = sqlLabel;
		this.sqlStrPrefix = extraPrefix + this.name().toUpperCase() + "(CAST(";
		this.sqlStrSuffix = " AS INTEGER))" + extraSuffix;
	}

	public String getSQLStr(final String valueProvider) {
		return this.sqlStrPrefix + valueProvider + this.sqlStrSuffix;
	}

	public String getSQLLabel() {
		return this.sqlLabel;
	}
}
