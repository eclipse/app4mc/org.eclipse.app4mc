/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.visualizations;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.visualization.ui.VisualizationParameters;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.app4mc.visualization.util.svg.PlantUmlDiagram;
import org.eclipse.app4mc.visualization.util.svg.SvgUtil;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.RowLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

@Component(property = {
		"name=Object references",
		"description=References Visualization for EObjects"
})
public class EObjectRefsVisualization implements Visualization {

	/**
	 * Entry point for the visualization framework
	 * 
	 * @param eObject		Ecore object that shall be visualized
	 * @param parameters	visualization parameters
	 * @param parent		parent component
	 * @param broker		event broker for element selection
	 */
	@PostConstruct
	public void createVisualization(
			EObject eObject,
			VisualizationParameters parameters,
			Composite parent,
			IEventBroker broker) {

		// Create central context object with all relevant inputs
		final Context context = new Context(eObject, parameters);

		Composite pane = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(pane);
		Composite buttonArea = new Composite(pane, SWT.NONE);

		addToggleButton(buttonArea, "Horizontal Layout", context.config::setHorizontalLayout, context.config.isHorizontalLayout());
		addToggleButton(buttonArea, "Show Labels", context.config::setShowReferenceLabels, context.config.isShowReferenceLabels());
		addToggleButton(buttonArea, "Show Derived Refs", context.config::setShowDerivedReferences, context.config.isShowDerivedReferences());

		addZoomBox(buttonArea, context);

		RowLayoutFactory.swtDefaults().fill(true).applyTo(buttonArea);

		Browser browser = addBrowser(pane, broker, context);
		
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(browser);

		// Create and display content
		updateContent(browser, context);
	}

	private Browser addBrowser(Composite pane, IEventBroker broker, final Context context) {
		Browser browser = new Browser(pane, SWT.NONE);

		// Setup navigation to a selected element in the model viewer
		if (broker != null) {
			browser.addLocationListener(LocationListener.changingAdapter(c -> {
				c.doit = true;
			
				Object target = null;
				int idx = c.location.lastIndexOf('#');
				if (idx >= 0) {
					target = context.diagram.getObjectById(c.location.substring(idx + 1));
				}
				if (target != null) {
					HashMap<String, Object> data = new HashMap<>();
					data.put("modelElements", Collections.singletonList(target));
					broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
			
					c.doit = false;
				}
			}));
		}

		// React to configuration parameter changes
		context.config.addChangeListener(e -> {
			if (e.getPropertyName().equals("scale"))
				updateSvgScale(browser, (int) e.getNewValue());
			if (e.getPropertyName().startsWith("parameter"))
				updateContent(browser, context);
		});

		return browser;
	}

	/**
	 * Helper for adding a toggle button
	 * 
	 * @param parent          container element
	 * @param text            label
	 * @param f               button select action; takes the button's selection status as an argument
	 * @param initialSelected initial selection state of the button
	 */
	private void addToggleButton(Composite parent, String text, final Consumer<Boolean> f, boolean initialSelected) {
		final Button btn = new Button(parent, SWT.TOGGLE | SWT.FLAT);
		btn.setText(text);
		btn.setSelection(initialSelected);
		btn.addListener(SWT.Selection, e -> f.accept(btn.getSelection()));
	}

	private void addZoomBox(Composite buttonArea, Context context) {
		// Align the box with the other buttons
		final Composite zoomArea = new Composite(buttonArea, SWT.NONE);
		RowLayoutFactory.fillDefaults().margins(1, 1).applyTo(zoomArea);

		final Composite box = new Composite(zoomArea, SWT.BORDER);

		final Button btnLeft = new Button(box, SWT.ARROW | SWT.LEFT | SWT.FLAT);
		btnLeft.addListener(SWT.Selection, e -> context.config.decrementScale());

		final CLabel scaleLabel = new CLabel(box, SWT.FLAT | SWT.CENTER);
		scaleLabel.setText(String.format("%d %%", context.config.getScale())); // set initial label text

		context.config.addChangeListener(e -> {
			if (e.getPropertyName().equals("scale"))
				scaleLabel.setText(String.format("%d %%", (int) e.getNewValue())); // update label text
		});

		final Button btnRight = new Button(box, SWT.ARROW | SWT.RIGHT | SWT.FLAT);
		btnRight.addListener(SWT.Selection, e -> context.config.incrementScale());

		RowLayoutFactory.fillDefaults().fill(true).applyTo(box);
	}

	private void updateSvgScale(Browser browser, int newScale) {
		// Update SVG size in browser via JavaScript/DOM
		if (browser != null) {
			browser.execute(SvgUtil.buildUpdateScaleCommand(newScale));
		}
	}

	/**
	 * Visualizes a given model element in a browser.
	 * <p>
	 * The plantUML graph is constructed and compiled in a separate thread.
	 * 
	 * @param browser 
	 * @param context 
	 */
	private void updateContent(Browser browser, Context context) {
		new Thread(() -> {
			// Build PlantUML diagram text
			EObjectRefsGenerator.updateDiagram(context.diagram, context.object, context.config);

			// Render to SVG
			String result;
			try {
				result = context.diagram.renderToSvg();
			} catch (IOException e) {
				result = "Error invoking PlantUML: \"" + e.getMessage()
				+ "\". Make sure you have configured the path to the dot executable properly in the PlantUML preferences.";
				Platform.getLog(EObjectRefsVisualization.class).error(result, e);
				return;
			}

			// Apply initial scale and display
			if (result != null && !browser.isDisposed()) {
				final String browserContent = SvgUtil.initiallyApplyScale(result, context.config.getScale());
				browser.getDisplay().asyncExec(() -> {
					if (!browser.isDisposed()) {
						browser.setText(browserContent);
					}
				});
			}

		}).start();
	}

	static class Context {
		public final EObject object;
		public final EObjectRefsConfig config;
		public final PlantUmlDiagram diagram = new PlantUmlDiagram();

		public Context(EObject eObject, VisualizationParameters viewParameters) {
			this.object = eObject;
			this.config =  new EObjectRefsConfig(viewParameters);
		}
	}

}
