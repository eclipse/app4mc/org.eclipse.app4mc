h3. AMALTHEA Trace Database (ATDB) Example

The ATDB provides tables and view definitions to hold trace data and represent it in comprehensive views. In this section we will show a small example BTF trace, convert it to its ATDB equivalent, and enrich it with event chains. The relevant tables and views will given for better understanding.

h4. Source BTF Trace

The source BTF trace has two cores, three stimuli, three tasks, and four runnables.

bc. #version 2.1.5
#timeScale ns
0,ECU_1,-1,SIM,SIM,-1,tag,ECU_INIT
0,Processor_1,-1,SIM,SIM,-1,tag,PROCESSOR_INIT
0,Core_1,-1,SIM,SIM,-1,tag,CORE_INIT
0,Core_2,-1,SIM,SIM,-1,tag,CORE_INIT
0,Core_1,0,C,Core_1,0,set_frequence,10000000
0,Core_2,0,C,Core_2,0,set_frequence,11000000
0,Stimulus_Task_1,0,STI,Stimulus_Task_1,0,trigger
0,Stimulus_Task_1,0,T,Task_1,0,activate
0,Stimulus_Task_2,0,STI,Stimulus_Task_2,0,trigger
0,Stimulus_Task_2,0,T,Task_2,0,activate
100,Core_1,0,T,Task_1,0,start
100,Task_1,0,R,Runnable_1_1,0,start
100,Core_2,0,T,Task_2,0,start
100,Task_2,0,R,Runnable_2_1,0,start
20800,Task_1,0,R,Runnable_1_1,0,terminate
20800,Task_1,0,R,Runnable_1_2,0,start
40900,Task_1,0,R,Runnable_1_2,0,terminate
40900,Core_1,0,T,Task_1,0,terminate
45000,Stimulus_Task_3,0,STI,Stimulus_Task_3,0,trigger
45000,Stimulus_Task_3,0,T,Task_3,0,activate
45100,Task_2,0,R,Runnable_2_1,0,suspend
45100,Core_2,0,T,Task_2,0,preempt
45100,Core_2,0,T,Task_3,0,start
45100,Task_3,0,R,Runnable_3_1,0,start
55800,Task_3,0,R,Runnable_3_1,0,terminate
55800,Core_2,0,T,Task_3,0,terminate
55900,Core_2,0,T,Task_2,0,resume
55900,Task_2,0,R,Runnable_2_1,0,resume
61000,Task_2,0,R,Runnable_2_1,0,terminate
61000,Core_2,0,T,Task_2,0,terminate

There is exacly one instance of each entity in the trace, so the tables in the database do not become to big for a small example. The visualized Gantt chart of the above trace is depicted in the following figure:

!(scale)../pictures/atdb_example_btf_visual.png!

h4. Equivalent ATDB Contents

The tables and/or relevant views for the above BTF trace are listed in the following: Note that if the table's header starts with a "v" it is a view for better readability.

table(classic){background:#f8f8f8}.
{background:#eee}.|\2{text-align:center}.*MetaInformation*|
{background:#eee}.|_.name|_.value|
|timeBase|ns|

<br/>

table(classic){background:#f8f8f8;display:inline-table}.
{background:#eee}.|\3{text-align:center}.*Entity*|
{background:#eee}.|_.id|_.name|_.entityTypeId|
|1|SIM|1|
|2|ECU_1|2|
|3|Processor_1|3|
|4|Core_1|4|
|5|Core_2|4|
|6|Stimulus_Task_1|5|
|7|Task_1|6|
|8|Stimulus_Task_2|5|
|9|Task_2|6|
|10|Runnable_1_1|7|
|11|Runnable_2_1|7|
|12|Runnable_1_2|7|
|13|Stimulus_Task_3|5|
|14|Task_3|6|
|15|Runnable_3_1|7|

table(classic){background:#f8f8f8;display:inline-table}.
{background:#eee}.|\2{text-align:center}.*EntityInstance*|
{background:#eee}.|_.entityId|_.sqcnr|
|1|-1|
|2|-1|
|3|-1|
|4|-1|
|4|0|
|5|-1|
|5|0|
|6|0|
|7|0|
|8|0|
|9|0|
|10|0|
|11|0|
|12|0|
|13|0|
|14|0|
|15|0|

table(classic){background:#f8f8f8;display:inline-table}.
{background:#eee}.|\2{text-align:center}.*EntityType*|
{background:#eee}.|_.id|_.name|
|1|SIM|
|2|ECU|
|3|Processor|
|4|C|
|5|STI|
|6|T|
|7|R|

table(classic){background:#f8f8f8;display:inline-table}.
{background:#eee}.|\3{text-align:center}.*Property*|
{background:#eee}.|_.id|_.name|_.type|
|1|processors|entityIdRef|
|2|cores|entityIdRef|
|3|frequencyInHz|long|
|4|stimuli|entityIdRef|
|5|executingCore|entityIdRef|
|6|runnables|entityIdRef|
|7|ecItems|entityIdRef|
|8|ecStimulus|eventIdRef|
|9|ecResponse|eventIdRef|
|10|ecMinItemsCompleted|long|

Note that in the entityInstance table there are actually two instances of the cores (entityIds 4 and 5). Also note, the event chain properties are already contained in the property table.

table(classic){background:#f8f8f8}.
{background:#eee}.|\10{text-align:center}.*vTraceEvent*|
{background:#eee}.|_.timestamp|_.sqcnr|_.entityName|_.entityType|_.entityInstance|_.sourceEntityName|_.sourceEntityType|_.sourceEntityInstance|_.eventType|_.value|
|0|0|Stimulus_Task_1|STI|0|Stimulus_Task_1|STI|0|trigger| |
|0|1|Task_1|T|0|Stimulus_Task_1|STI|0|activate| |
|0|2|Stimulus_Task_2|STI|0|Stimulus_Task_2|STI|0|trigger| |
|0|3|Task_2|T|0|Stimulus_Task_2|STI|0|activate| |
|100|0|Task_1|T|0|Core_1|C|0|start| |
|100|1|Runnable_1_1|R|0|Task_1|T|0|start| |
|100|2|Task_2|T|0|Core_2|C|0|start| |
|100|3|Runnable_2_1|R|0|Task_2|T|0|start| |
|20800|0|Runnable_1_1|R|0|Task_1|T|0|terminate| |
|20800|1|Runnable_1_2|R|0|Task_1|T|0|start| |
|40900|0|Runnable_1_2|R|0|Task_1|T|0|terminate| |
|40900|1|Task_1|T|0|Core_1|C|0|terminate| |
|45000|0|Stimulus_Task_3|STI|0|Stimulus_Task_3|STI|0|trigger| |
|45000|1|Task_3|T|0|Stimulus_Task_3|STI|0|activate| |
|45100|0|Runnable_2_1|R|0|Task_2|T|0|suspend| |
|45100|1|Task_2|T|0|Core_2|C|0|preempt| |
|45100|2|Task_3|T|0|Core_2|C|0|start| |
|45100|3|Runnable_3_1|R|0|Task_3|T|0|start| |
|55800|0|Runnable_3_1|R|0|Task_3|T|0|terminate| |
|55800|1|Task_3|T|0|Core_2|C|0|terminate| |
|55900|0|Task_2|T|0|Core_2|C|0|resume| |
|55900|1|Runnable_2_1|R|0|Task_2|T|0|resume| |
|61000|0|Runnable_2_1|R|0|Task_2|T|0|terminate| |
|61000|1|Task_2|T|0|Core_2|C|0|terminate| |

The vTraceEvent table view intentionally resembles the original BTF trace. However, some information is omitted since it is extracted and stored in other tables (e.g. the frequency of the cores). All tags (eventType "tag") have been stored in property values.

table(classic){background:#f8f8f8}.
{background:#eee}.|\5{text-align:center}.*vPropertyValue*|
{background:#eee}.|_.entityName|_.entityType|_.propertyName|_.propertyType|_.value|
|ECU_1|ECU|processors|entityIdRef|Processor_1|
|Processor_1|Processor|cores|entityIdRef|Core_1, Core_2|
|Core_1|C|frequencyInHz|long|10000000|
|Core_2|C|frequencyInHz|long|11000000|
|Task_1|T|stimuli|entityIdRef|Stimulus_Task_1|
|Task_1|T|executingCore|entityIdRef|Core_1|
|Task_1|T|runnables|entityIdRef|Runnable_1_1, Runnable_1_2|
|Task_2|T|stimuli|entityIdRef|Stimulus_Task_2|
|Task_2|T|executingCore|entityIdRef|Core_2|
|Task_2|T|runnables|entityIdRef|Runnable_2_1|
|Task_3|T|stimuli|entityIdRef|Stimulus_Task_3|
|Task_3|T|executingCore|entityIdRef|Core_2|
|Task_3|T|runnables|entityIdRef|Runnable_3_1|

<br/>

table(classic){background:#f8f8f8}.
{background:#eee}.|\7{text-align:center}.*vRunnableInstanceTraceInfo*|
{background:#eee}.|_.runnableName|_.entityInstance|_.startEventCount|_.resumeEventCount|_.terminateEventCount|_.suspendEventCount|_.isComplete|
|Runnable_1_1|0|1|0|1|0|1|
|Runnable_2_1|0|1|1|1|1|1|
|Runnable_1_2|0|1|0|1|0|1|
|Runnable_3_1|0|1|0|1|0|1|

All runnable instances are completely represented in the trace, as are the task instances. To keep it simple we leave out the vProcessInstanceTraceInfo table since it is too wide (it has more event types).

h4. Adding Event Chains

Thus far, all information contained in the database has been extracted from the original BTF trace. Event chains, however, are not actually visible in the trace. So we insert their specification (which may originate from an AMALTHEA model) manually. Our event chain is specified as follows:

*Runnable_1_1:start* -> *Runnable_1_1:terminate* -> *Runnable_3_1:start* -> *Runnable_3_1:terminate*

In order to define our event chain for the respective events (which must be present even without a trace), we define them in the event table:

table(classic){background:#f8f8f8}.
{background:#eee}.|\6{text-align:center}.*vEvent*|
{background:#eee}.|_.name|_.eventType|_.entityName|_.entityType|_.sourceEntityName|_.sourceEntityType|
|Runnable_1_1_start|start|Runnable_1_1|R| | |
|Runnable_1_1_terminate|terminate|Runnable_1_1|R| | |
|Runnable_3_1_start|start|Runnable_3_1|R| | |
|Runnable_3_1_terminate|terminate|Runnable_3_1|R| | |

The following two tables then extend the existing ones by the event chains.

table(classic){background:#f8f8f8;display:inline-table}.
{background:#eee}.|\3{text-align:center}.*Entity*|
{background:#eee}.|_.id|_.name|_.entityTypeId|
|16|EC1|8|
|17|EC1_segment1|8|
|18|EC1_segment2|8|
|19|EC1_segment3|8|

table(classic){background:#f8f8f8;display:inline-table}.
{background:#eee}.|\2{text-align:center}.*EntityType*|
{background:#eee}.|_.id|_.name|
|8|EC|

In the values of properties, we employ event chain items to represent the segments. As the vPropertyValue view indicates, at this point we refer to the stimulus/response events we created in the event table.

table(classic){background:#f8f8f8}.
{background:#eee}.|\5{text-align:center}.*vPropertyValue*|
{background:#eee}.|_.entityName|_.entityType|_.propertyName|_.propertyType|_.value|
|EC1|EC|ecItems|entityIdRef|EC1_segment1, EC1_segment2, EC1_segment3|
|EC1|EC|ecStimulus|eventIdRef|Runnable_1_1_start|
|EC1|EC|ecResponse|eventIdRef|Runnable_3_1_terminate|
|EC1_segment1|EC|ecStimulus|eventIdRef|Runnable_1_1_start|
|EC1_segment1|EC|ecResponse|eventIdRef|Runnable_1_1_terminate|
|EC1_segment2|EC|ecStimulus|eventIdRef|Runnable_1_1_terminate|
|EC1_segment2|EC|ecResponse|eventIdRef|Runnable_3_1_start|
|EC1_segment3|EC|ecStimulus|eventIdRef|Runnable_3_1_start|
|EC1_segment3|EC|ecResponse|eventIdRef|Runnable_3_1_terminate|

There is the additional view which explicitly combines event chains and their properties:

table(classic){background:#f8f8f8}.
{background:#eee}.|\6{text-align:center}.*vEventChainEntity*|
{background:#eee}.|_.eventChainName|_.stimulus|_.response|_.items|_.minItemsCompleted|_.isParallel|
|EC1|Runnable_1_1_start|Runnable_3_1_terminate|EC1_segment1, EC1_segment2, EC1_segment3| |0|
|EC1_segment1|Runnable_1_1_start|Runnable_1_1_terminate| | |0|
|EC1_segment2|Runnable_1_1_terminate|Runnable_3_1_start| | |0|
|EC1_segment3|Runnable_3_1_start|Runnable_3_1_terminate| | |0|

Each event chain has exactly one instance in the EntityInstance table. So the derived view for event chain instances is as follows:

table(classic){background:#f8f8f8}.
{background:#eee}.|\11{text-align:center}.*vEventChainInstanceInfo*|
{background:#eee}.|_.eventChainName|_.ecInstance|_.stimulusTimestamp|_.stimulusEntityName|_.stimulusEntityInstance|_.stimulusEvent|_.responseTimestamp|_.responseEntityName|_.responseEntityInstance|_.responseEvent|_.latencyType|
|EC1|0|100|Runnable_1_1|0|start|55800|Runnable_3_1|0|terminate|age/reaction|
|EC1_segment1|0|100|Runnable_1_1|0|start|20800|Runnable_1_1|0|terminate|age/reaction|
|EC1_segment2|0|20800|Runnable_1_1|0|terminate|45100|Runnable_3_1|0|start|age/reaction|
|EC1_segment3|0|45100|Runnable_3_1|0|start|55800|Runnable_3_1|0|terminate|age/reaction|

h4. Metrics on the Trace

There are numerous metrics for entities. The most important ones include the times that each entity instance remains in a specific state (according to their corresponding BTF state automaton). In addition to these metrics, there are also some counting metrics (e.g. how often a specific task instance was preempted). Lastly, there are also metrics for specific entity types and instances, e.g., the latency for a specific event chain instance (either age or reaction). The metrics themselves and these entity instance specific metrics for our example are listed in the following:

table(classic){background:#f8f8f8}.
{background:#eee}.|\3{text-align:center}.*Metric*|
{background:#eee}.|_.id|_.name|_.dimension|
|1|startDelayTime|time|
|2|runningTime|time|
|3|pollingTime|time|
|4|parkingTime|time|
|5|waitingTime|time|
|6|readyTime|time|
|7|responseTime|time|
|8|coreExecutionTime|time|
|9|netExecutionTime|time|
|10|grossExecutionTime|time|
|11|activateToActivate|time|
|12|startToStart|time|
|13|endToEnd|time|
|14|endToStart|time|
|15|slack|time|
|16|activations|count|
|17|preemptions|count|
|18|suspensions|count|
|79|ageLatency|time|
|84|reactionLatency|time|

As indicated by th id numbering, we omitted some of the metrics. These include the respective Min/Max/Avg/StDev of each metric with dimension "time". Here is the view of the entity instance metric values:

table(classic){background:#f8f8f8}.
{background:#eee}.|\5{text-align:center}.*vEntityInstanceMetricValue*|
{background:#eee}.|_.entityName|_.entityType|_.entityInstance|_.metricName|_.value|
|Task_1|T|0|startDelayTime|100|
|Task_1|T|0|runningTime|40800|
|Task_1|T|0|pollingTime|0|
|Task_1|T|0|parkingTime|0|
|Task_1|T|0|waitingTime|0|
|Task_1|T|0|readyTime|0|
|Task_1|T|0|responseTime|40900|
|Task_1|T|0|coreExecutionTime|40800|
|Task_1|T|0|netExecutionTime|40800|
|Task_1|T|0|grossExecutionTime|40800|
|Task_2|T|0|startDelayTime|100|
|Task_2|T|0|runningTime|50100|
|Task_2|T|0|pollingTime|0|
|Task_2|T|0|parkingTime|0|
|Task_2|T|0|waitingTime|0|
|Task_2|T|0|readyTime|10800|
|Task_2|T|0|responseTime|61000|
|Task_2|T|0|coreExecutionTime|50100|
|Task_2|T|0|netExecutionTime|50100|
|Task_2|T|0|grossExecutionTime|60900|
|Runnable_1_1|R|0|runningTime|20700|
|Runnable_1_1|R|0|readyTime|0|
|Runnable_2_1|R|0|runningTime|50100|
|Runnable_2_1|R|0|readyTime|10800|
|Runnable_1_2|R|0|runningTime|20100|
|Runnable_1_2|R|0|readyTime|0|
|Task_3|T|0|startDelayTime|100|
|Task_3|T|0|runningTime|10700|
|Task_3|T|0|pollingTime|0|
|Task_3|T|0|parkingTime|0|
|Task_3|T|0|waitingTime|0|
|Task_3|T|0|readyTime|0|
|Task_3|T|0|responseTime|10800|
|Task_3|T|0|coreExecutionTime|10700|
|Task_3|T|0|netExecutionTime|10700|
|Task_3|T|0|grossExecutionTime|10700|
|Runnable_3_1|R|0|runningTime|10700|
|Runnable_3_1|R|0|readyTime|0|
|EC1|EC|0|ageLatency|55700|
|EC1|EC|0|reactionLatency|55700|
|EC1_segment1|EC|0|ageLatency|20700|
|EC1_segment1|EC|0|reactionLatency|20700|
|EC1_segment2|EC|0|ageLatency|24300|
|EC1_segment2|EC|0|reactionLatency|24300|
|EC1_segment3|EC|0|ageLatency|10700|
|EC1_segment3|EC|0|reactionLatency|10700|

Note that since each event chain instance also is either age, reaction, or both, there are two latency metrics to distinguish between the two. There are also cumulated Min/Max/Avg/StDev values calculated from these entity instance metric values. Since there is only one instance of each entity in the example trace the values are all the same, so we omit the EntityMetricValue table here (it has roughly four times as many entries).
