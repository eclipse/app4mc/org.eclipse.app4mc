
h2. Overview


There are additional features of the APP4MC platform that are not included in the standard product. They can be installed in the current environment via "Install new software...".


h3. Available Extensions

**EMF Viewers**

**Additional examples how to extend the IDE**

* Custom visualizations
* Custom validations
* Custom editor actions

**Model Transformation**

* Framework and examples
* Transformation of AMALTHEA to Linux code
* Transformation of AMALTHEA to ROS2 code
* Transformation of AMALTHEA to SystemC code

**SystemC Timing Simulation**
