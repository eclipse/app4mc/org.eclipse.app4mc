/**
 ********************************************************************************
 * Copyright (c) 2021-2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.platform.ide.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.framework.FrameworkUtil;

public class APP4MCPreferences extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	public APP4MCPreferences() {
		super(GRID);
		setDescription("APP4MC settings");
		setPreferenceStore(new ScopedPreferenceStore(
				InstanceScope.INSTANCE, 
				FrameworkUtil.getBundle(getClass()).getSymbolicName()));
	}

	@Override
	public void init(IWorkbench workbench) {
		// no initialization required
	}

	@Override
	protected void createFieldEditors() {
		addField(new BooleanFieldEditor(
				"app4mc.showTypes", 
				"Show Types in Amalthea Model Editor (initial setting when opening the editor)", 
				BooleanFieldEditor.DEFAULT, 
				getFieldEditorParent()));
	}

}
