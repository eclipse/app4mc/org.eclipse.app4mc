/**
 ********************************************************************************
 * Copyright (c) 2013-2022 Timing-Architects Embedded Systems GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *  Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.tracing.converter.atdb;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.sql.SQLException;

import org.eclipse.app4mc.tracing.converter.atdb.model.EntityType;
import org.eclipse.app4mc.tracing.converter.ot1.OT1.OT1Type1;
import org.eclipse.app4mc.tracing.converter.ot1.OT1.SchedulingEntityElementType;
import org.eclipse.app4mc.tracing.converter.ot1.OT1.SchedulingEntityType;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;


public class ATDBConvertHandler extends AbstractHandler implements IRunnableWithProgress {

	private IFile iFile;
	private IFile atdbFile;

	/**
	 * Returns always <code>null</code> since the super interface declaration demands it.
	 * @see IHandler#execute(ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		doExecute(event);
		return null;
	}

	private void doExecute(final ExecutionEvent event) {
		final ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection.isEmpty() || !(currentSelection instanceof IStructuredSelection)) {
			return;
		}
		final IStructuredSelection ssel = (IStructuredSelection) currentSelection;
		final Object[] array = ssel.toArray();
		if (array.length != 1) {
			return;
		}

		final Object obj = array[0];
		if (!(obj instanceof IFile)) {
			return;
		}
		try {
			iFile = (IFile) obj;
			convert(event);
		}
		catch (final InterruptedException e) {
			// Log exception...
			Platform.getLog(this.getClass()).warn("Interrupted Exception", e);
			// Restore interrupted state...
		    Thread.currentThread().interrupt();
		}
	}

	private void convert(final ExecutionEvent event) throws InterruptedException {
		final Shell activeShell = HandlerUtil.getActiveShell(event);

		final String name = iFile.getName();

		final IPath atdbFilePath = new Path(name).removeFileExtension().addFileExtension("atdb");
		atdbFile = iFile.getParent().getFile(atdbFilePath);
		if (atdbFile.getLocation().toFile().exists()) {
			final boolean openConfirm = MessageDialog.openConfirm(activeShell, "AMALTHEA Trace DB already exists.",
					"There exists already an AMALTHEA Trace DB at location: " + atdbFilePath.toOSString()
							+ "\n\nDo you want to replace that file?");
			if (!openConfirm) {
				return;
			}
			final File file = atdbFile.getLocation().toFile();
			try {
				Files.delete(file.toPath());
			} catch (IOException e) {
				MessageDialog.openError(activeShell, "Deletion failed", "Couldn't delete existing Trace Database: "
						+ file.getAbsolutePath());
				return;
			}
		}

		final ProgressMonitorDialog pmd = new ProgressMonitorDialog(activeShell);
		try {
			pmd.run(false, false, this);
		}
		catch (final InvocationTargetException e) {
			e.printStackTrace();
			ErrorDialog.openError(activeShell, "Error", "Something went wrong during conversion", new Status(
					IStatus.ERROR, "org.eclipse.app4mc.tracing.converter.atdb", e.getMessage(), e.getTargetException()));
		}
	}

	@Override
	public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		monitor.beginTask("Converting OT1 to AMALTHEA Trace Database (ATDB) format.", 100);

		monitor.subTask("Loading OT1-Model");
		final OT1Type1 loadOT1Model = OT1Loader.loadOT1Model(iFile);
		monitor.worked(20);

		monitor.subTask("Creating initial ATDB structure");
		try (final ATDBWriter atdbWriter = new ATDBWriter()) {
			atdbWriter.createATDB(atdbFile);
			monitor.worked(5);

			final EList<SchedulingEntityType> schedulingEntities = loadOT1Model.getSchedulingEntity();
			schedulingEntities.forEach(schedEntityType -> convertSchedulingEntityTypes(schedEntityType, atdbWriter, monitor));

			schedulingEntities.forEach(schedEntityType -> insertTraceEntities(schedEntityType, atdbWriter, monitor));

			try {
				monitor.subTask("Insert EntitySource entries.");
				atdbWriter.fillEntitySource();
				monitor.worked(10);
			}
			catch (final SQLException e) {
				// Log exception...
				Platform.getLog(this.getClass()).warn("SQL Exception", e);
			}

			monitor.subTask("Set meta-information.");
			atdbWriter.fillMetaInformation(iFile.getLocation().toFile().getAbsolutePath());
			monitor.worked(1);
		}
		monitor.subTask("Finished.");
		try {
			atdbFile.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		}
		catch (final CoreException e) {
			throw new InvocationTargetException(e);
		}
	}

	private void convertSchedulingEntityTypes(final SchedulingEntityType schedEntityType, final ATDBWriter atdbWriter, final IProgressMonitor monitor) {
		try {
			final EList<SchedulingEntityElementType> element = schedEntityType.getElement();

			monitor.subTask("Insert entities for " + schedEntityType.getName());
			atdbWriter.insertEntities(schedEntityType, element);
			monitor.worked(5);

			monitor.subTask("Insert event-types for " + schedEntityType.getName());
			atdbWriter.insertEventTypes(schedEntityType);
			monitor.worked(5);
		}
		catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

	private void insertTraceEntities(final SchedulingEntityType schedEntityType, final ATDBWriter atdbWriter, final IProgressMonitor monitor) {
		try {

			final Long insertEntity = atdbWriter.insertEntity(schedEntityType, EntityType.CORE);
			monitor.subTask("Insert trace-entries for " + schedEntityType.getName());
			if (insertEntity != null) {
				atdbWriter.insertTraceEntries(schedEntityType, insertEntity);
				monitor.worked(50);
			}
		}
		catch (final Exception ex) {
			// Log exception...
			Platform.getLog(this.getClass()).warn("Exception", ex);
		}
	}
}
