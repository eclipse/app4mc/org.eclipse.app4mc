/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core;

import org.eclipse.app4mc.validation.util.ProfileManager;

/**
 * Service interface to register an {@link IProfile} as OSGi DS component that
 * is referenced by the {@link ProfileManager}.
 */
public interface IProfileConfiguration extends IProfile {

}
