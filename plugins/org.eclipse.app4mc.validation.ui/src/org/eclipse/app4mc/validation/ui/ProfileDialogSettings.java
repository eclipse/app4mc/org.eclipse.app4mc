/**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.util.CachedProfile;
import org.eclipse.app4mc.validation.util.ProfileManager;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.jface.dialogs.IDialogSettings;

public class ProfileDialogSettings {

	private static final String KEY_SELECTED_PROFILE_CLASSES = "Validation_SelectedProfileClasses";
	private static final String KEY_SELECTED_SCOPE = "Validation_SelectedScope";
	public static final String SCOPE_SELECTION = "Selection";
	public static final String SCOPE_FILE = "File";
	public static final String SCOPE_FOLDER = "Folder";

	// dialog input (defaults)
	private Set<String> selectedProfileClassNames = new HashSet<>();
	private String scope = SCOPE_FOLDER;

	// dialog result (defaults)
	private boolean dialogSuccess = false;
	private List<Class<? extends IProfile>> dialogResults = null;

	// dialog setup

	private ProfileManager profileManager;

	@Inject
	@Optional
	public void setProfileManager(@Service ProfileManager manager) {
		profileManager = manager;
	}

	// getters and setters

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public boolean isDialogSuccess() {
		return dialogSuccess;
	}

	public void setDialogSuccess(boolean dialogSuccess) {
		this.dialogSuccess = dialogSuccess;
	}

	public List<Class<? extends IProfile>> getDialogResults() {
		return dialogResults;
	}

	public void setDialogResults(List<Class<? extends IProfile>> dialogResults) {
		this.dialogResults = dialogResults;
	}

	// methods required by tree view

	public Object[] getProfiles() {
		return profileManager.getRegisteredValidationProfiles().values().stream()
				.sorted(Comparator.comparing(CachedProfile::getName))
				.toArray();
	}

	public List<CachedProfile> getInitialSelection() {
		List<CachedProfile> allNodes = new ArrayList<>();

		// get all profiles
		for (CachedProfile rootNode : profileManager.getRegisteredValidationProfiles().values()) {
			allNodes.add(rootNode);
			collectSubNodes(rootNode, allNodes);
		}

		// keep only selected profiles and group them
		Map<Object, List<CachedProfile>> filteredNodesMap = allNodes.stream()
				.filter( cache -> selectedProfileClassNames.contains(cache.getProfileClass().getName()) )
				.collect(Collectors.groupingBy( cache -> cache.getProfileClass().getName() ));

		// select one representative per group (the one that is highest in the tree)
		List<CachedProfile> result = new ArrayList<>();
		for (List<CachedProfile> list : filteredNodesMap.values()) {
			result.add(getRepresentative(list));
		}

		return result;
	}

	private void collectSubNodes(CachedProfile profile, List<CachedProfile> list) {
		if (!profile.getCachedProfiles().isEmpty()) {
			for (CachedProfile subProfile : profile.getCachedProfiles().values()) {
				list.add(subProfile);
				collectSubNodes(subProfile, list);
			}
		}
	}

	private CachedProfile getRepresentative(List<CachedProfile> profileList) {
		if (profileList.isEmpty()) return null;

		CachedProfile selectedProfile = profileList.get(0);
		int selectedLevel = getLevel(selectedProfile);

		// iterate over the rest of the list
		for (int i = 1; i < profileList.size(); i++) {
			CachedProfile tmpProfile = profileList.get(i);
			int tmpLevel = getLevel(tmpProfile);
			if (tmpLevel < selectedLevel) {
				selectedProfile = tmpProfile;
				selectedLevel = tmpLevel;
			}
		}

		return selectedProfile;
	}

	private int getLevel(CachedProfile profile) {
		if (profile == null) return -1;

		int level = 0;

		CachedProfile parent = profile.getParentProfile();
		while (parent != null) {
			level++;
			parent = parent.getParentProfile();
		}
		return level;
	}

	public void setSelectedProfileClassNames(List<String> classNames) {
		selectedProfileClassNames.clear();
		selectedProfileClassNames.addAll(classNames);
	}

	// dialog settings store

	public void loadFrom(IDialogSettings store) {
		if (store == null) return;

		// KEY_SELECTED_PROFILE_CLASSES
		String[] classNames = store.getArray(KEY_SELECTED_PROFILE_CLASSES);
		if (classNames != null) {
			setSelectedProfileClassNames(List.of(classNames));
		}

		// KEY_SELECTED_SCOPE
		String selectedScope = store.get(KEY_SELECTED_SCOPE);
		if (selectedScope != null) {
			setScope(selectedScope);
		}
	}

	public void saveTo(IDialogSettings store) {
		if ((store == null) || !dialogSuccess || dialogResults == null) return;

		// KEY_SELECTED_PROFILE_CLASSES
		String[] classNames = dialogResults.stream().map(Class::getName).toArray(String[]::new);
		store.put(KEY_SELECTED_PROFILE_CLASSES, classNames);

		// KEY_SELECTED_SCOPE
		store.put(KEY_SELECTED_SCOPE, scope);
	}

}
