/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.ui.provider;

import org.eclipse.app4mc.validation.util.CachedProfile;
import org.eclipse.app4mc.validation.util.CachedValidator;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;

public class ProfileLabelProvider extends CellLabelProvider implements ILabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof CachedProfile) {
			CachedProfile node = (CachedProfile) element;
			return node.getName();
		}
		return element == null ? "" : element.toString();
	}

	@Override
	public String getToolTipText(Object element) {
		if (element instanceof CachedProfile) {
			CachedProfile node = (CachedProfile) element;

			StringBuilder sb = new StringBuilder();
			if (node.getDescription().isEmpty()) {
				sb.append(node.getName());
			} else {
				sb.append(node.getDescription());
			}
			for (CachedValidator validator : node.getCachedValidations().values()) {
				sb.append("\n   " + validator.getValidationID());
				for (String check : validator.getValidationChecks()) {
					sb.append("\n      * " + check);
				}
			}
			if (sb.length() > 0) {
				return sb.toString();
			}
		}
		return null;
	}

	@Override
	public void update(ViewerCell cell) {
		Object element = cell.getElement();
		cell.setText(getText(element));
	}

	@Override
	public Image getImage(Object element) {
		return null;
	}

}
