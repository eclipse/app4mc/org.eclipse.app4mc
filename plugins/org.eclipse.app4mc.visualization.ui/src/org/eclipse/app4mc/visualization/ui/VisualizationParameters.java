/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.ui;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class VisualizationParameters {

	final Map<String, String> parameters;

	public VisualizationParameters() {
		this.parameters = new HashMap<>();
	}

	public VisualizationParameters(String initString) {
		this.parameters = readFromString(initString);
	}

	public String get(String key) {
		return parameters.get(key);
	}

	public String getOrDefault(Object key, String defaultValue) {
		return parameters.getOrDefault(key, defaultValue);
	}

	public String put(String key, String value) {
		return parameters.put(key, value);
	}

	public boolean isEmpty() {
		return parameters.isEmpty();
	}

	// persistence handling
	//  - does not interfere with serialization of surrounding map (toString())
	//  - does not use "," or "=" as delimiters
	//  - encodes key and value
	public String toString() {
		return parameters.entrySet().stream()
				.map(e -> encode(e.getKey()) + ":" + encode(e.getValue()))
				.collect(Collectors.joining(" # ", "{", "}"));
	}

	private Map<String, String> readFromString(String string) {
		Map<String, String> map = new HashMap<>();
	
		if (string != null && string.startsWith("{") && string.endsWith("}")) {
			String entries = string.substring(1, string.length() - 1);
			for (String entry : entries.split("#")) {
				String[] keyValue = entry.split(":");
				if (keyValue.length == 2) {
					map.put(decode(keyValue[0].trim()), decode(keyValue[1].trim()));
				}
			}
		}
	
		return map;
	}

	private String decode(String s) {
	    try {
			return URLDecoder.decode(s, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
	        throw new IllegalStateException(e);
		}
	}

	private String encode(String s){
	    try{
	        return URLEncoder.encode(s, StandardCharsets.UTF_8.toString());
	    } catch(UnsupportedEncodingException e){
	        throw new IllegalStateException(e);
	    }
	}

}
