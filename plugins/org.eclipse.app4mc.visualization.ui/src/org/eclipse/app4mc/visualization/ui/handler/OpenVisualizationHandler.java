/*********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.ui.handler;

import java.util.UUID;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;

/**
 * Simple handler to open a new instance of the {@link VisualizationPart}.
 */
public class OpenVisualizationHandler {

	@Execute
	public void execute(EPartService partService) {
		MPart part = partService.createPart(VisualizationPart.ID);

		// set a unique id for each view instance
		String oldID = part.getElementId();
		part.setElementId(oldID + "-" + UUID.randomUUID());

		partService.showPart(part, PartState.ACTIVATE);
	}

}