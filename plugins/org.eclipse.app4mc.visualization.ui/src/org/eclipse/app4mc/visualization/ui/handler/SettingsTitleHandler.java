/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.ui.handler;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

/**
 * Handler that is used to set a custom label for the current view
 */
public class SettingsTitleHandler {

	@Execute
	public void execute(Shell shell, @Active MPart activePart) {
		if (activePart != null) {

			InputDialog dialog = new InputDialog(shell, "APP4MC Visualization", "Enter view title", activePart.getLabel(), null);
			int result = dialog.open();
			if (result == Window.OK) {
				activePart.setLabel(dialog.getValue());				
			}
		}
	}

}
