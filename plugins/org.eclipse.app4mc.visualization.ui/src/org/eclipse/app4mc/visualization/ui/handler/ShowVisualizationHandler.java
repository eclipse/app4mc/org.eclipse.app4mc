/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.visualization.ui.handler;

import javax.inject.Named;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

/**
 * Handler that is assigned with the command
 * <i>org.eclipse.app4mc.visualization.ui.command.showapp4mcvisualization</i>
 * which is used by toolbar menu items added by the
 * {@link VisualizationMenuContribution}.
 */
public class ShowVisualizationHandler {

	@Execute
    public void execute(
    		@Named("app4mc.visualization.id") String visualizationId,
    		@Active MPart part) {

		VisualizationPart visualizationPart = (VisualizationPart) part.getObject();
		visualizationPart.showVisualization(visualizationId);
    }
}
