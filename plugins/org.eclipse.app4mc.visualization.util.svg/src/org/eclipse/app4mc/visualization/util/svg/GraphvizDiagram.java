/**
 ********************************************************************************
 * Copyright (c) 2022-2024 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.IOException;

import net.sourceforge.plantuml.FileFormat;

public class GraphvizDiagram extends AbstractDiagram {

	/**
	 * Render DOT source (wrapped by PlantUML) to SVG
	 * 
	 * @return the rendered SVG diagram
	 */
	public String renderToSvg() throws IOException {
		String source = getDiagramText();

		// ensure non empty content
		if (source == null || source.isEmpty()) {
			return null;			
		}

		// wrap Graphviz definition with PlantUML
		source = "@startuml\n" + source + "\n@enduml";

		return PlantUmlUtil.renderToString(source, FileFormat.SVG);
	}

	public void exportToFile(String path) throws IOException {
		String source = getDiagramText();

		// ensure non empty content
		if (source == null || source.isEmpty()) {
			return;			
		}

		// wrap Graphviz definition with PlantUML
		source = "@startuml\n" + source + "\n@enduml";

		PlantUmlUtil.exportToFile(source, path);
	}

}
