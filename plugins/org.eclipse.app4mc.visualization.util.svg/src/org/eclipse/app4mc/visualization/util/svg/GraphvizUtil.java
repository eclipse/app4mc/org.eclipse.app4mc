/**
 ********************************************************************************
 * Copyright (c) 2022-2024 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

import net.sourceforge.plantuml.eclipse.utils.PlantumlConstants;

/**
 * Provides helper methods to render DOT files.
 * <p>
 * Uses direct calls to the dot executable.
 * <p>
 * Comment: The alternate approach to wrap the DOT source as PlantUML was more stable
 * in our use cases. Therefore the PlantUmlUtil class is our preferred solution.
 */
public class GraphvizUtil {

	// Suppress default constructor
	private GraphvizUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Renders DOT source to SVG.
	 * 
	 * @return the rendered SVG diagram
	 */
	public static String renderToSvg(String source) throws IOException {
		if (source == null || source.isEmpty()) {
			return null;
		}
	
		List<String> command = new ArrayList<>(2);
		command.add(getGraphvizPath());
		command.add("-Tsvg");
	
		Process process = new ProcessBuilder(command).redirectError(Redirect.PIPE).start();
	
		StringBuilder sb = new StringBuilder();
		try (OutputStreamWriter writer = new OutputStreamWriter(process.getOutputStream());
			 InputStreamReader reader = new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8);)
		{
			// provide process input
			writer.append(source);
			writer.close();
	
			// get process output
			char[] buf = new char[1024];
			int len;
			while ((len = reader.read(buf)) >= 0) {
				sb.append(buf, 0, len);
			}
			reader.close();
		}
	
		return sb.toString();
	}

	/**
	 * Renders DOT source to a file (file extension determines the format).
	 */
	public static void exportToFile(String source, String path) throws IOException {
		if (source == null || path == null || source.isEmpty() || path.isEmpty()) {
			return;
		}

		String ext = path.substring(path.lastIndexOf('.') + 1);
		if (ext == null || ext.isEmpty()) {
			return;
		}

		runGraphviz(source, "-T" + ext, "-o", path);
	}

	/**
	 * Executes the DOT command, passes the source to stdin.
	 * 
	 * @param source the DOT source file
	 * @param args arguments to the dot command
	 */
	public static void runGraphviz(String source, String... args) throws IOException {
		if (source == null || source.isEmpty()) {
			return;
		}

		List<String> command = new ArrayList<>(args.length + 1);
		command.add(getGraphvizPath());
		command.addAll(Arrays.asList(args));
		Process process = new ProcessBuilder(command).redirectError(Redirect.PIPE).start();
		OutputStreamWriter writer = new OutputStreamWriter(process.getOutputStream());
		writer.append(source);
		writer.close();
	}

	/**
	 * Tries to figure out the path to a locally installed Graphviz executable.
	 * 
	 * @return path to dot executable
	 */
	private static String getGraphvizPath() {
		// read GRAPHVIZ_PATH value from preferences
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode("net.sourceforge.plantuml.eclipse");
		String dotPath1 = prefs.get(PlantumlConstants.GRAPHVIZ_PATH, null);
	
		if (dotPath1 != null && !dotPath1.isEmpty()) {
			return dotPath1;
		}
	
		// Check if environment variable GRAPHVIZ_DOT is set
		String dotPath2 = System.getenv("GRAPHVIZ_DOT");
		if (dotPath2 != null && !dotPath2.isEmpty()) {
			return dotPath2;
		}
	
		// Try with command only (works if included in PATH)
		if (Platform.getOS().equals(Platform.OS_WIN32)) {
			return "dot.exe";
		} else {
			return "dot";
		}
	}

}
