/**
 ********************************************************************************
 * Copyright (c) 2022-2024 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.IOException;

import net.sourceforge.plantuml.FileFormat;

public class PlantUmlDiagram extends AbstractDiagram {

	/**
	 * Renders PlantUML source to SVG.
	 * 
	 * @return the rendered SVG diagram
	 * @throws IOException
	 */
	public String renderToSvg() throws IOException {
		return renderToFormat(FileFormat.SVG);
	}

	/**
	 * Renders PlantUML source to a file format.
	 * 
	 * @return the rendered diagram
	 * @throws IOException
	 */
	public String renderToFormat(FileFormat format) throws IOException {
		String source = getDiagramText();
		return PlantUmlUtil.renderToString(source, format);
	}

	/**
	 * Renders PlantUML source and write result to a file (file extension determines the format).
	 * 
	 * @throws IOException
	 */
	public void exportToFile(String path) throws IOException {
		String source = getDiagramText();
		PlantUmlUtil.exportToFile(source, path);
	}

}
