/**
 ********************************************************************************
 * Copyright (c) 2024 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;
import net.sourceforge.plantuml.core.DiagramDescription;

/**
 * Provides helper methods to render PlantUML files.
 */
public class PlantUmlUtil {

	// Suppress default constructor
	private PlantUmlUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Renders PlantUML source to a file (file extension determines the format).
	 * 
	 * @throws IOException
	 */
	public static void exportToFile(String source, String path) throws IOException {
		if (source == null || path == null || source.isEmpty() || path.isEmpty()) {
			return;
		}

		// determine format
		String extension = path.substring(path.lastIndexOf('.') + 1);
		FileFormat format = getFileformatFor(extension);

		// render diagram
		ByteArrayOutputStream baos = renderToStream(source, format);
		if (baos == null) {
			return;			
		}
	
		// write to file
		final File file = new File(path);

		try (final FileOutputStream fos = new FileOutputStream(file)) {
			baos.writeTo(fos);
			fos.close();
		}
	}

	/**
	 * Renders PlantUML source to a file format.
	 * 
	 * @return the rendered diagram
	 * @throws IOException
	 */
	public static String renderToString(String source, FileFormat format) throws IOException {
		ByteArrayOutputStream baos = renderToStream(source, format);
		if (baos != null) {
			return baos.toString(StandardCharsets.UTF_8.name());			
		}
	
		return null;
	}

	/**
	 * Renders PlantUML source to a file format.
	 * 
	 * @return the rendered diagram
	 * @throws IOException
	 */
	public static ByteArrayOutputStream renderToStream(String source, FileFormat format) throws IOException {
		// ensure non empty content
		if (source == null || source.isEmpty()) {
			return null;			
		}

		// ensure valid output format
		if (format == null) {
			return null;			
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		// call PlantUML and render to file format
		final SourceStringReader reader = new SourceStringReader(source);
		final DiagramDescription desc = reader.outputImage(baos, new FileFormatOption(format));
		if (desc != null && !desc.getDescription().isEmpty()) {
			return baos;
		}

		return null;
	}

	/**
	 * Tries to find a corresponding PlantUML file format.
	 * 
	 * @param extension
	 * @return file format or null
	 */
	private static FileFormat getFileformatFor(String extension) {
		if (extension == null || extension.isEmpty()) {
			return null;
		}

		for (FileFormat format : FileFormat.values()) {
			if (format.name().equalsIgnoreCase(extension)) {
				return format;
			}
		}

		return null;
	}

}
