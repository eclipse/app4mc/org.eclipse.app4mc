/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides helper methods to scale SVG images according to a external zoom factor.
 * <p>
 * Assumptions:
 * <ul>
 * <li> svg element contains attributes "width" and "height" (initial size; will not be changed) </li>
 * <li> svg element "style" influences the size of the displayed svg (will be modified) </li>
 * </ul>
 */
public class SvgUtil {

	// Suppress default constructor
	private SvgUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Modifies the SVG document on text level (before it is loaded by the browser)
	 * 
	 * @param svg		the initial SVG document
	 * @param scale		the scale in percent
	 * @return the modified SVG document
	 */
	public static String initiallyApplyScale(String svg, int scale) {
		Pattern pattern1 = Pattern.compile("<svg (.*?)>", Pattern.DOTALL); // Reluctant: match first appearance of '>'
		Matcher matcher1 = pattern1.matcher(svg);
		if (matcher1.find()) {
			String svgAttributes = matcher1.group(1);
			// get width and height
			String width = stringBetween(svgAttributes, "width=\"", "\"");
			String height = stringBetween(svgAttributes, "height=\"", "\"");

			if (width == null || height == null)
				return svg;

			// build style string
			int newWidthValue = Integer.valueOf(width.replaceAll("[^0-9]", "")) * scale / 100;
			int newHeightValue = Integer.valueOf(height.replaceAll("[^0-9]", "")) * scale / 100;
			String newWidthUnit = width.replaceAll("[0-9.]", "");
			String newHeightUnit = height.replaceAll("[0-9.]", "");
			String newStyle = "style=\"width:" + newWidthValue + newWidthUnit + ";height:" + newHeightValue + newHeightUnit + ";\"";

			String newSvgAttributes = null;
			Pattern pattern2 = Pattern.compile("style=\"(.*?)\""); // Reluctant: match first appearance of '"'
			Matcher matcher2 = pattern2.matcher(svgAttributes);
			if (matcher2.find()) {
				// replace existing style attribute
				newSvgAttributes = matcher2.replaceFirst(newStyle);
			} else {
				// add new style attribute
				newSvgAttributes = svgAttributes + " " + newStyle;
			}

			return matcher1.replaceFirst("<svg " + newSvgAttributes + ">");
		}
		return svg;
	}

	private static String stringBetween(String str, String left, String right) {
		int start = str.indexOf(left);
		if (start == -1)
			return null;

		int end = str.indexOf(right, start + left.length());
		if (end == -1)
			return null;

		return str.substring(start + left.length(), end);
	}

	/**
	 * Builds a JavaScript command string that modifies the DOM objects of the displayed page.<br>
	 * The command reads the attributes "width" and "height" and modifies the attribute "style".
	 * <p>
	 * Can be executed by the browser via browser.execute(<command>).
	 * 
	 * @param newScale	the desired scale (after execution of the command)
	 * @return the command string
	 */
	public static String buildUpdateScaleCommand(int newScale) {
		if (newScale < 10 || newScale > 200) {
			return "";
		}

		return String.format(
				"var svg = document.getElementsByTagName('svg')[0];"
				+ "var w_value = svg.width.baseVal.valueInSpecifiedUnits;"
				+ "var w_unit = svg.width.baseVal.valueAsString.replace(/\\d+/g, '');"
				+ "var h_value = svg.height.baseVal.valueInSpecifiedUnits;"
				+ "var h_unit = svg.height.baseVal.valueAsString.replace(/\\d+/g, '');"
				+ "svg.style.width = (w_value * %d / 100) + w_unit;"
				+ "svg.style.height = (h_value * %d / 100) + h_unit;",
				newScale, newScale);
	}

}
