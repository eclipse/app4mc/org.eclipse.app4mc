/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.tests;

import org.eclipse.app4mc.amalthea.model.util.tests.AmaltheaIndexTest;
import org.eclipse.app4mc.amalthea.model.util.tests.ConstraintsUtilsTest;
import org.eclipse.app4mc.amalthea.model.util.tests.CustomPropertyUtilsTest;
import org.eclipse.app4mc.amalthea.model.util.tests.DeploymentUtilsTest;
import org.eclipse.app4mc.amalthea.model.util.tests.HardwareUtilsTest;
import org.eclipse.app4mc.amalthea.model.util.tests.RuntimeUtilsTest;
import org.eclipse.app4mc.amalthea.model.util.tests.SoftwareUtilsTest;
import org.eclipse.app4mc.amalthea.model.util.tests.WeibullUtilsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	DeviationsTest.class,
	MetamodelStructureTest.class,
	NameTest.class,
	TimeComparisonTest.class,
	StandardSchedulerTests.class,

	CustomPropertyUtilsTest.class,
	DeploymentUtilsTest.class,
	HardwareUtilsTest.class,
	RuntimeUtilsTest.class,
	WeibullUtilsTest.class,

	AmaltheaIndexTest.class,
	ConstraintsUtilsTest.class,
	SoftwareUtilsTest.class
})

public class AllTests {
	// empty class to start test suite
}
