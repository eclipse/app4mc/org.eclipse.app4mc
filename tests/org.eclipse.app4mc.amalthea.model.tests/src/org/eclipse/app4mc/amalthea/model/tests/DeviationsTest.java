/**
 ********************************************************************************
 * Copyright (c) 2015-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeBetaDistribution;
import org.eclipse.app4mc.amalthea.model.TimeBoundaries;
import org.eclipse.app4mc.amalthea.model.TimeConstant;
import org.eclipse.app4mc.amalthea.model.TimeGaussDistribution;
import org.eclipse.app4mc.amalthea.model.TimeStatistics;
import org.eclipse.app4mc.amalthea.model.TimeUniformDistribution;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.TimeWeibullEstimatorsDistribution;
import org.junit.Test;

public class DeviationsTest {

	private static final String MS = "ms";

	AmaltheaFactory fac = AmaltheaFactory.eINSTANCE;

	/**
	 * Method getAverage() - Simple tests: Constant, Boundaries,
	 * UniformDistribution, Statistics, Weibull
	 */
	@Test
	public void testGetAverage1() {
		Time avg;

		TimeConstant constant = fac.createTimeConstant();
		constant.setValue(createTime("75", MS));

		avg = constant.getAverage();
		assertEquals("TimeConstant getAverage()", 0, avg.compareTo(createTime("75", MS)));

		TimeBoundaries boundaries = fac.createTimeBoundaries();
		boundaries.setLowerBound(createTime("50", MS));
		boundaries.setUpperBound(createTime("100", MS));

		avg = boundaries.getAverage().adjustUnit();
		assertEquals("TimeBoundaries getAverage()", 0, avg.compareTo(createTime("75", MS)));

		TimeUniformDistribution uniform = fac.createTimeUniformDistribution();
		uniform.setLowerBound(createTime("50", MS));
		uniform.setUpperBound(createTime("100", MS));

		avg = uniform.getAverage().adjustUnit();
		assertEquals("TimeUniformDistribution getAverage()", 0, avg.compareTo(createTime("75", MS)));

		TimeStatistics statistics = fac.createTimeStatistics();
		statistics.setLowerBound(createTime("50", MS));
		statistics.setAverage(createTime("80", MS));
		statistics.setUpperBound(createTime("100", MS));

		avg = statistics.getAverage();
		assertEquals("TimeStatistics getAverage()", 0, avg.compareTo(createTime("80", MS)));

		TimeWeibullEstimatorsDistribution weibull = fac.createTimeWeibullEstimatorsDistribution();
		weibull.setLowerBound(createTime("50", MS));
		weibull.setAverage(createTime("80", MS));
		weibull.setUpperBound(createTime("100", MS));

		avg = weibull.getAverage();
		assertEquals("Weibull getAverage()", 0, avg.compareTo(createTime("80", MS)));
	}

	/**
	 * Method getAverage() - Extended tests: Histogram, BetaDistribution,
	 * GaussDistribution
	 */
	@Test
	public void testGetAverage2() {
		Time avg;

		TimeBetaDistribution beta = fac.createTimeBetaDistribution();
		beta.setLowerBound(createTime("50", MS));
		beta.setUpperBound(createTime("100", MS));
		// Symmetric case: alpha == beta
		beta.setAlpha(10.0);
		beta.setBeta(10.0);

		avg = beta.getAverage().adjustUnit();
		assertEquals("BetaDistribution getAverage()", 0, avg.compareTo(createTime("75", MS)));

		TimeGaussDistribution gauss = fac.createTimeGaussDistribution();
		gauss.setMean(createTime("75", MS));
		gauss.setSd(createTime("10", MS));

		// no truncation
		avg = gauss.getAverage().adjustUnit();
		assertEquals("GaussDistribution getAverage() - no truncation", 0, avg.compareTo(createTime("75", MS)));

		// symmetric bounds
		gauss.setLowerBound(createTime("50", MS));
		gauss.setUpperBound(createTime("100", MS));
		avg = gauss.getAverage().adjustUnit();
		assertEquals("GaussDistribution getAverage() - symmetric bounds", 0, avg.compareTo(createTime("75", MS)));

		// asymmetric bounds
		gauss.setLowerBound(createTime("70", MS));
		avg = gauss.getAverage().adjustUnit();
		// System.out.println("Avg: " + avg);
		assertTrue("GaussDistribution getAverage() - asymmetric bounds",
				avg.subtract(createTime("79881950", "ns")).compareTo(createTime("1", "ns")) < 0);

	}

	private Time createTime(String value1, String unit1) {
		Time time1 = AmaltheaFactory.eINSTANCE.createTime();
		time1.setValue(new BigInteger(value1));
		time1.setUnit(TimeUnit.get(unit1));

		return time1;
	}

}
