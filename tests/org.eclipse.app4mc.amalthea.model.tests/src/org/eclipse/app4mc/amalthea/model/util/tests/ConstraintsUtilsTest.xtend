/** 
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 * Robert Bosch GmbH - initial API and implementation
 */
 
package org.eclipse.app4mc.amalthea.model.util.tests

import org.eclipse.app4mc.amalthea.model.AbstractEventChain
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex
import org.eclipse.app4mc.amalthea.model.EventChain
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.ConstraintsBuilder
import org.eclipse.app4mc.amalthea.model.util.ConstraintsUtil
import org.eclipse.app4mc.amalthea.models.ConstraintsModels
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

class ConstraintsUtilsTest {
	
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension ConstraintsBuilder b3 = new ConstraintsBuilder
	
	Amalthea model
	Runnable run1
	Runnable run2
	Task task1

	@Before
	def void initalizeModel() {
		model = ConstraintsModels.createModel1()
		run1 = AmaltheaIndex.getElements(model, "Run1", Runnable).head
		run2 = AmaltheaIndex.getElements(model, "Run2", Runnable).head
		task1 = AmaltheaIndex.getElements(model, "Task1", Task).head
	}
	
	@Test
	def void testRunnableDeadline() {
		val deadline1 = ConstraintsUtil.getDeadline(run1);
		assertEquals(
			"testRunnableDeadline: null expected", null, deadline1);
		
		val deadline2 = ConstraintsUtil.getDeadline(run2, model.constraintsModel);
		assertEquals(
			"testRunnableDeadline: 80 ns expected", 80, deadline2.value.intValue);
		assertEquals(
			"testRunnableDeadline: 80 ns expected", TimeUnit::NS, deadline2.unit);
	}
	
	@Test
	def void testProcessDeadline() {
		val deadline3 = ConstraintsUtil.getDeadline(task1);
		assertEquals(
			"testProcessDeadline: 20 ms expected", 20, deadline3.value.intValue);
		assertEquals(
			"testProcessDeadline: 20 ms expected", TimeUnit::MS, deadline3.unit);

		val deadline4 = ConstraintsUtil.getDeadline(task1, model.constraintsModel);
		assertEquals(
			"testProcessDeadline: same result expected", deadline3, deadline4);
	}
	
	@Test
	def void testAllAbstractEventChainGetter_Flat() {
		val model = amalthea [
			constraintsModel [
				eventChain [ name = "ec1" ]
				eventChain [ name = "ec2" ]
				eventChain [ name = "ec3" ]
			]
		]
		val allECs = ConstraintsUtil.getAllAbstractEventChains(model.constraintsModel)
		assertTrue("testAllAbstractEventChainGetter_Flat: expected ec1, ec2, and ec3",
			allECs.size == 3 &&
			allECs.get(0) === AmaltheaIndex.getElements(model, "ec1", AbstractEventChain).head &&
			allECs.get(1) === AmaltheaIndex.getElements(model, "ec2", AbstractEventChain).head &&
			allECs.get(2) === AmaltheaIndex.getElements(model, "ec3", AbstractEventChain).head
		)
	}
	
	@Test
	def void testAllAbstractEventChainGetter_NestedRef() {
		val model = amalthea [
			constraintsModel [
				eventChain [ name = "ec1" ]
				eventChain [
					name = "ec2"
					subchain_ref( _find(EventChain, "ec1") )
				]
				eventChain [
					name = "ec3"
					subchain_ref( _find(EventChain, "ec2") )
				]
			]
		]
		val allECs = ConstraintsUtil.getAllAbstractEventChains(model.constraintsModel)
		assertTrue("testAllAbstractEventChainGetter_NestedRef: expected ec1, ec2, and ec3",
			allECs.size == 3 &&
			allECs.get(0) === AmaltheaIndex.getElements(model, "ec1", AbstractEventChain).head &&
			allECs.get(1) === AmaltheaIndex.getElements(model, "ec2", AbstractEventChain).head &&
			allECs.get(2) === AmaltheaIndex.getElements(model, "ec3", AbstractEventChain).head
		)
	}
	
	@Test
	def void testAllAbstractEventChainGetter_Nested() {
		val model = amalthea [
			constraintsModel [
				eventChain [
					name = "ec1"
					subchain [ name = "ec11" ]
				]
				eventChain [
					name = "ec2"
					subchain [
						name = "ec21"
						subchain [ name = "ec211" ]
					]
				]
				eventChain [
					name = "ec3"
					subchain [
						name = "ec31"
						subchain [
							name = "ec311"
							subchain [
								name = "ec3111"
							]
						]
					]
				]
			]
		]
		val allECs = ConstraintsUtil.getAllAbstractEventChains(model.constraintsModel)
		assertTrue("testAllAbstractEventChainGetter_Nested: expected ec1, ec2, ec3, ec11, ec21, ec31, ec211, ec311, and ec3111",
			allECs.size == 9 &&
			allECs.get(0) === AmaltheaIndex.getElements(model, "ec1",    AbstractEventChain).head &&
			allECs.get(1) === AmaltheaIndex.getElements(model, "ec2",    AbstractEventChain).head &&
			allECs.get(2) === AmaltheaIndex.getElements(model, "ec3",    AbstractEventChain).head &&
			allECs.get(3) === AmaltheaIndex.getElements(model, "ec11",   AbstractEventChain).head &&
			allECs.get(4) === AmaltheaIndex.getElements(model, "ec21",   AbstractEventChain).head &&
			allECs.get(5) === AmaltheaIndex.getElements(model, "ec31",   AbstractEventChain).head &&
			allECs.get(6) === AmaltheaIndex.getElements(model, "ec211",  AbstractEventChain).head &&
			allECs.get(7) === AmaltheaIndex.getElements(model, "ec311",  AbstractEventChain).head &&
			allECs.get(8) === AmaltheaIndex.getElements(model, "ec3111", AbstractEventChain).head
		)
	}

}
