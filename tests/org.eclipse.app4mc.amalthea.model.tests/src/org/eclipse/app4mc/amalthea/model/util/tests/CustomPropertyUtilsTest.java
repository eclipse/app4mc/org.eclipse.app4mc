/**
 ********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.tests;

import static org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.*;
import static org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.customGetBoolean;
import static org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.customGetInteger;
import static org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.customGetReference;
import static org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.customGetString;
import static org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.customGetValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.BooleanObject;
import org.eclipse.app4mc.amalthea.model.DoubleObject;
import org.eclipse.app4mc.amalthea.model.ListObject;
import org.eclipse.app4mc.amalthea.model.LongObject;
import org.eclipse.app4mc.amalthea.model.MapObject;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.StringObject;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.junit.Before;
import org.junit.Test;

public class CustomPropertyUtilsTest {

	Amalthea model;

	ListObject listObject;
	MapObject mapObject1;
	MapObject mapObject2;
	Time time1;
	Time time2;
	StringObject stringObject;
	LongObject longObject;
	DoubleObject doubleObject;
	BooleanObject booleanObject;

	@Before
	public void initalizeModel() {
		AmaltheaFactory fac = AmaltheaFactory.eINSTANCE;
		model = fac.createAmalthea();

		// add top level entries

		listObject = fac.createListObject();
		time1 = FactoryUtil.createTime("100 ms");

		model.getCustomProperties().put("a", listObject);
		model.getCustomProperties().put("b", time1);
		model.getCustomProperties().put("c", FactoryUtil.createBooleanObject(true));

		// add list items

		stringObject = FactoryUtil.createStringObject("hello");
		time2 = FactoryUtil.createTime("50 ms");
		mapObject1 = fac.createMapObject();

		listObject.getValues().add(stringObject);
		listObject.getValues().add(time2);
		listObject.getValues().add(mapObject1);

		// add map entries

		mapObject2 = fac.createMapObject();
		longObject = FactoryUtil.createLongObject(500);
		doubleObject = FactoryUtil.createDoubleObject(5.46238);
		booleanObject = FactoryUtil.createBooleanObject(true);

		mapObject1.getEntries().put("map", mapObject2);
		mapObject1.getEntries().put("x", longObject);
		mapObject1.getEntries().put("y", doubleObject);
		mapObject1.getEntries().put("z", booleanObject);

		mapObject2.getEntries().put("one", FactoryUtil.createIntegerObject(1));
		mapObject2.getEntries().put("ten", FactoryUtil.createIntegerObject(10));
	}

	@Test
	public void testGetMethods() {

		// access existing top level values

		assertEquals(listObject, CustomPropertyUtil.customGetValue(model, "a"));
		assertEquals(time1, CustomPropertyUtil.customGetValue(model, "b"));

		// access missing values

		assertNull(customGetValue(model, "d"));
		assertNull(customGetValue(model, "a", "7"));
		assertNull(customGetValue(model, "a", "x"));
		assertNull(customGetValue(model, "a", "2", "x", "y"));

		assertNull(customGetValue(model, "a.7", '.'));
		assertNull(customGetValue(model, "a.x", '.'));
		assertNull(customGetValue(model, "a-2-x-y", '-'));
		assertNull(customGetValue(model, "a.2.nix", '.'));

		// access existing values

		assertEquals(time2, customGetValue(model, "a", "1"));
		assertEquals(mapObject1, customGetValue(model, "a", "2"));

		assertEquals(longObject, customGetValue(model, "a 2 x", ' '));
		assertEquals(doubleObject, customGetValue(model, "a.2.y", '.'));
		assertEquals(booleanObject, customGetValue(model, "a/2/z", '/'));

		// access typed properties

		assertEquals(stringObject.getValue(), customGetString(model, "a", "0"));
		assertEquals(Integer.valueOf(1), customGetInteger(model, "a", "2", "map", "one"));
		assertEquals(Integer.valueOf(10), customGetInteger(model, "a", "2",  "map", "ten"));
		assertTrue(customGetBoolean(model, "a", "2", "z"));
		
		assertNull(customGetInteger(model, "b"));
		assertNull(customGetReference(model, "c"));
	}

	@Test
	public void testPutMethods() {
		AmaltheaFactory fac = AmaltheaFactory.eINSTANCE;
		SWModel sw = ModelUtil.getOrCreateSwModel(model);

		// add top level entries

		ListObject list1 = fac.createListObject();

		sw.getCustomProperties().put("a", list1);
		customPut(sw,"b", FactoryUtil.createTime("100 ms"));
		customPut(sw,"c", true);
		customPut(sw,"d", "dummy");
		customPut(sw,"i", 128);

		// add list items

		stringObject = FactoryUtil.createStringObject("hello");
		time2 = FactoryUtil.createTime("50 ms");
		MapObject map1 = fac.createMapObject();

		list1.getValues().add(stringObject);
		list1.getValues().add(time2);
		list1.getValues().add(map1);

		// add map entries

		MapObject map2 = fac.createMapObject();

		map1.getEntries().put("map", mapObject2);
		map1.getEntries().put("x", FactoryUtil.createLongObject(500));
		map1.getEntries().put("y", FactoryUtil.createDoubleObject(5.46238));
		map1.getEntries().put("z", FactoryUtil.createBooleanObject(true));

		map2.getEntries().put("one", FactoryUtil.createIntegerObject(1));
		map2.getEntries().put("ten", FactoryUtil.createIntegerObject(10));

		assertEquals(customGetBoolean(model, "a", "2", "z"), customGetBoolean(sw, "a", "2", "z"));
	}

}
