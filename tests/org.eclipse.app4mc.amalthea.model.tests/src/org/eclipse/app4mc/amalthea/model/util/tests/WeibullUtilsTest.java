/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.tests;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.commons.math3.distribution.WeibullDistribution;
import org.eclipse.app4mc.amalthea.model.util.WeibullUtil;
import org.junit.Test;

public class WeibullUtilsTest {

	private List<List<Double>> testData_shape_scale_min_max_avg() {
		return List.of(
			List.of(  1.5,  25.0, 12.0,  44.0, 27.764390695525933),
			List.of(  2.3, 134.0, 20.0, 200.0, 123.50808267228767),
			List.of(  9.0, 150.0, -3.0, 179.5, 138.91807604408876),
			List.of(  6.0, 100.7,  4.0,  80.0, 68.67632157455436),
			List.of(  6.1, 105.5,  0.0, 131.0, 97.04753670063731),
			List.of( 12.0,  14.0, 34.0,  50.0, 47.396211180607864),
			List.of(  2.6,  14.0, 34.0,  44.4, 41.228475647997506),
			List.of( 21.6,  10.0, 20.5,  31.0, 30.200121335690838),
			List.of(  3.6, 134.0, 34.0, 275.0, 154.71554944499297),
			List.of(  3.6, 124.0, 34.0, 281.3, 145.73624106559464)
		);
	}

	@Test
	public void computeAverage() {
		List<List<Double>> testData;

		testData = testData_shape_scale_min_max_avg();

		for (List<Double> list : testData) {
			Double shape = list.get(0);
			Double scale = list.get(1);
			Double min = list.get(2);
			Double max = list.get(3);
			Double avg1 = list.get(4);

			double avg2 = WeibullUtil.computeAverage(shape, scale, min, max);

			assertTrue(Math.abs(avg2 - avg1) < 0.01);
		}
	}

	@Test
	public void computeMedian() {
		List<List<Double>> testData;

		testData = testData_shape_scale_min_max_avg();

		for (List<Double> list : testData) {
			Double shape = list.get(0);
			Double scale = list.get(1);
			Double min = list.get(2);
			Double max = list.get(3);

			double median = WeibullUtil.computeMedian(shape, scale, min, max);

			// check with Apache Commons Math
			final WeibullDistribution mathFunction = new WeibullDistribution(null, shape, scale);

			double cdfAvg = mathFunction.cumulativeProbability(median - min);
			double cdfMax = mathFunction.cumulativeProbability(max - min);

			// check: ratio cdfAvg / cdfMax (expected: 0.5)
			double ratio = cdfAvg / cdfMax;
			assertTrue(ratio > 0.49 && ratio < 0.51);
		}
	}

	@Test
	public void computePRemainPromille() {
		List<List<Double>> testData;

		testData = testData_shape_scale_min_max_avg();

		for (List<Double> list : testData) {
			Double shape = list.get(0);
			Double scale = list.get(1);
			Double min = list.get(2);
			Double max = list.get(3);

			double rem1 = WeibullUtil.computePRemainPromille(shape, scale, min, max);

			// check with Apache Commons Math
			final WeibullDistribution mathFunction = new WeibullDistribution(null, shape, scale);
			double cdfMax = mathFunction.cumulativeProbability(max - min);
			double rem2 = (1.0 - cdfMax) * 1000.0;

			assertTrue(Math.abs(rem2 - rem1) < 0.000001);
		}
	}

}
