/**
 * *******************************************************************************
 * Copyright (c) 2018-2020 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.models

import org.eclipse.app4mc.amalthea.model.ISR
import org.eclipse.app4mc.amalthea.model.InterruptController
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.StructureType
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter

class DeploymentModels {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension HardwareBuilder b2 = new HardwareBuilder
	extension SoftwareBuilder b3 = new SoftwareBuilder
	extension OperatingSystemBuilder b5 = new OperatingSystemBuilder
	extension MappingBuilder b6 = new MappingBuilder

	def static void main(String[] args) {
		val model1 = (new DeploymentModels).model1()
		AmaltheaWriter.writeToFileNamed(model1, "test-data/DeploymentUtilTestModel-gen.amxmi")
	}

	def static createModel1() {
		(new DeploymentModels).model1()
	}

	def model1() {
		val model = amalthea [

			hardwareModel [

				// ***** Definitions *****
				definition_ProcessingUnit [
					name = "Pu1_def"
				]
				definition_ProcessingUnit [
					name = "Pu2_def"
				]

				// ***** Structures *****
				structure [
					name = "SoC"
					structureType = StructureType::SO_C

					module_ProcessingUnit [name = "core1"]
					module_ProcessingUnit [name = "core2"]
					module_ProcessingUnit [name = "core3"]
				]
			]

			softwareModel [

				task [name = "t1"]
				task [name = "t2"]
				task [name = "t3"]
				task [name = "t4"]
				task [name = "t5"]
				task [name = "t6"]
				task [name = "t7"]
				isr [name = "isr1"]
				isr [name = "isr2"]
				isr [name = "isr3"]
			]

			osModel [
				operatingSystem [
					name = "Os1"
					taskScheduler [name = "Scheduler1"]
					taskScheduler [name = "Scheduler2"]
					taskScheduler [
						name = "Scheduler3"
						parentAssociation [
							parent = _find(TaskScheduler, "Scheduler2")
						]
					]
					taskScheduler [
						name = "Scheduler4"
						parentAssociation [
							parent = _find(TaskScheduler, "Scheduler3")
						]
					]
					interruptController [
						name = "controller1"
					]
					interruptController [
						name = "controller2"
					]
				]

			]

			mappingModel [
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "Scheduler2")
					responsibility += _find(ProcessingUnit, "core2")
					executingPU = _find(ProcessingUnit, "core2")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "Scheduler1")
					responsibility += _find(ProcessingUnit, "core1")
					executingPU = _find(ProcessingUnit, "core1")
				]

				schedulerAllocation [
					scheduler = _find(TaskScheduler, "Scheduler3")
					responsibility += _find(ProcessingUnit, "core2")
					executingPU = _find(ProcessingUnit, "core1")
				]

				schedulerAllocation [
					scheduler = _find(TaskScheduler, "Scheduler4")
					responsibility += _find(ProcessingUnit, "core2")
					executingPU = _find(ProcessingUnit, "core2")
				]

				schedulerAllocation [
					scheduler = _find(TaskScheduler, "Scheduler5")
				]

				taskAllocation [
					task = _find(Task, "t1")
					scheduler = _find(TaskScheduler, "Scheduler1")
					affinity += _find(ProcessingUnit, "core1")
				]
				taskAllocation [
					task = _find(Task, "t2")
					scheduler = _find(TaskScheduler, "Scheduler2")
					affinity += _find(ProcessingUnit, "core2")
				]
				taskAllocation [
					task = _find(Task, "t3")
					scheduler = _find(TaskScheduler, "Scheduler1")
					affinity += _find(ProcessingUnit, "core1")
				]

				taskAllocation [
					task = _find(Task, "t4")
					scheduler = _find(TaskScheduler, "Scheduler3")
					affinity += _find(ProcessingUnit, "core3")
				]
				taskAllocation [
					task = _find(Task, "t5")
					scheduler = _find(TaskScheduler, "Scheduler4")
				// affinity += _find(ProcessingUnit, "core1")
				]
				taskAllocation [
					task = _find(Task, "t6")
					scheduler = _find(TaskScheduler, "Scheduler3")
				// affinity += _find(ProcessingUnit, "core1")
				]
				taskAllocation [
					task = _find(Task, "t7")
					scheduler = _find(TaskScheduler, "Scheduler5")
				]

				isrAllocation [
					isr = _find(ISR, "isr1")
					controller = _find(InterruptController, "controller1")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "controller1")
					responsibility += _find(ProcessingUnit, "core1")
					executingPU = _find(ProcessingUnit, "core1")
				]
				isrAllocation [
					isr = _find(ISR, "isr2")
					controller = _find(InterruptController, "controller2")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "controller2")
					responsibility += _find(ProcessingUnit, "core2")
				]
				isrAllocation [
					isr = _find(ISR, "isr3")
					controller = _find(InterruptController, "controller1")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "controller1")
					responsibility += _find(ProcessingUnit, "core1")
					executingPU = _find(ProcessingUnit, "core1")
				]

			]
		]

		return model
	}
}
