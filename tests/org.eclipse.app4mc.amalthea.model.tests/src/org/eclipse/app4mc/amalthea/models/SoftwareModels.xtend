/**
 ********************************************************************************
 * Copyright (c) 2018-2019 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.models

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.EnumMode
import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.amalthea.model.ModeLabel
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter

import static org.eclipse.app4mc.amalthea.model.LabelAccessEnum.*
import static org.eclipse.app4mc.amalthea.model.RelationalOperator.*

class SoftwareModels {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder

	def static void main(String[] args) {
		val swModels = new SoftwareModels
		
		val model1 = swModels.model1()
		AmaltheaWriter.writeToFileNamed(model1, "test-data/SoftwareUtilTestModel1-gen.amxmi")
		
		val model2 = swModels.model2()
		AmaltheaWriter.writeToFileNamed(model2, "test-data/SoftwareUtilTestModel2-gen.amxmi")

		val model3 = swModels.model3()
		AmaltheaWriter.writeToFileNamed(model3, "test-data/SoftwareUtilTestModel3-gen.amxmi")
	}

	def static createModel1() {
		return (new SoftwareModels).model1()
	}

	def static createModel2() {
		return (new SoftwareModels).model2()
	}

	def static createModel3() {
		return (new SoftwareModels).model3()
	}

	def model1() {
		val model = 
			amalthea [
				softwareModel [
					mode_Enum [ name = "state"
						literal [name = "pre-drive" ]
						literal [name = "drive" ]
						literal [name = "post-drive" ]
					]
					modeLabel [ name = "car-state" mode = _find(EnumMode, "state")  initialValue = "pre-drive"]
					
					label [name = "Lab1"]
					label [name = "Lab2"]
					label [name = "Lab3"]
					label [name = "Lab4"]
					label [name = "Lab5"]

					runnable [ name = "Run1"
						activityGraph [
							ticks [ defaultConstant(200) ]
							conditionalSwitch [
								defaultEntry [
									labelAccess [ access = READ; data = _find(Label, "Lab1") ]
									labelAccess [ access = READ; data = _find(Label, "Lab2") ]
									ticks [ defaultConstant(333) ]
									labelAccess [ access = WRITE; data = _find(Label, "Lab3") ]
									labelAccess [ access = WRITE; data = _find(Label, "Lab4") ]
								]
							]
						]
					]
					runnable [ name = "Run2"
						activityGraph [
							ticks [ defaultConstant(400) ]
							ticks [ defaultConstant(40) ]
							ticks [ defaultConstant(4) ]
							runnableCall [ runnable = _find(Runnable, "Run4") ]
							labelAccess [ access = WRITE; data = _find(Label, "Lab5") ]
						]
					]
					runnable [ name = "Run3"
						activityGraph [
							labelAccess [ access = READ; data = _find(Label, "Lab5") ]
							ticks [ defaultConstant(600) ]
						]
					]
					runnable [ name = "Run4"
						activityGraph [
							ticks [ defaultConstant(700) ]
						]
					]
				]
			]
		return model
	}


	def model2() {
		val fac = AmaltheaFactory.eINSTANCE
		val model = 
			amalthea [
				softwareModel [
					label [name = "Lab1"]
					label [name = "Lab2"]
					label [name = "Lab3"]
					label [name = "Lab4"]
					label [name = "Lab5"]

					runnable [ name = "Run1"
						activityGraph [
							ticks [ defaultConstant(200) ]
							// incomplete label accesses
							labelAccess [ statistic = fac.createLabelAccessStatistic ]
							labelAccess [ data = _find(Label, "Lab1") statistic = fac.createLabelAccessStatistic ]
							labelAccess [ access = READ statistic = fac.createLabelAccessStatistic]
							
							// valid label accesses
							labelAccess [ access = READ; data = _find(Label, "Lab1") statistic = fac.createLabelAccessStatistic ]
							labelAccess [ access = READ; data = _find(Label, "Lab2") ]
							labelAccess [ access = READ; data = _find(Label, "Lab3") ]
							labelAccess [ access = READ; data = _find(Label, "Lab4") statistic = fac.createLabelAccessStatistic]
							ticks [ defaultConstant(333) ]
							labelAccess [ access = WRITE; data = _find(Label, "Lab3") ]
							labelAccess [ access = WRITE; data = _find(Label, "Lab4") ]
							labelAccess [ access = WRITE; data = _find(Label, "Lab5") statistic = fac.createLabelAccessStatistic]
						]
					]
				]
			]
		return model
	}


	def model3() {
		val model = 
			amalthea [
				softwareModel [
					mode_Enum [ name = "state"
						literal [name = "pre-drive" ]
						literal [name = "drive" ]
						literal [name = "post-drive" ]
					]
					modeLabel [ name = "car-state" mode = _find(EnumMode, "state")  initialValue = "pre-drive"]
					modeLabel [ name = "car-state-previous" mode = _find(EnumMode, "state")  initialValue = "pre-drive"]
					
					label [name = "Lab1"]
					label [name = "Lab2"]
					label [name = "Lab3"]
					label [name = "Lab4"]
					label [name = "Lab5"]

					// create the runnables first (stubs)
					
					runnable [ name = "Run1" ]
					runnable [ name = "Run2" ]
					runnable [ name = "Run3" ]
					runnable [ name = "Run4" ]
				]
			]

		// populate the activity graphs (may contain references to other runnables)
		
		val r1 = _find(model, Runnable, "Run1")
		r1.activityGraph [
			ticks [ defaultConstant(200) ]
			conditionalSwitch [
				entry [
					condition_OR [
						condition_AND [
							condition(_find(ModeLabel, "car-state-previous"), EQUAL, "pre-drive")
							condition(_find(ModeLabel, "car-state"), EQUAL, "drive")
						]
					]
					labelAccess [ access = READ; data = _find(Label, "Lab4") ]
					labelAccess [ access = WRITE; data = _find(Label, "Lab1") ]
				]
				entry [
					condition_OR [
						condition(_find(ModeLabel, "car-state"), EQUAL, "pre-drive")
						condition(_find(ModeLabel, "car-state"), EQUAL, "drive")
					]
					labelAccess [ access = READ; data = _find(Label, "Lab5") ]
					labelAccess [ access = WRITE; data = _find(Label, "Lab5") ]
				]
				entry [
					condition_OR [
						condition(_find(ModeLabel, "car-state-previous"), EQUAL, _find(ModeLabel, "car-state"))
					]
					runnableCall [ runnable = _find(Runnable, "Run2") ]
					runnableCall [ runnable = _find(Runnable, "Run4") ]
				]
				defaultEntry [
					labelAccess [ access = READ; data = _find(Label, "Lab1") ]
					labelAccess [ access = READ; data = _find(Label, "Lab2") ]
					ticks [ defaultConstant(333) ]
					runnableCall [ runnable = _find(Runnable, "Run1") ]
					runnableCall [ runnable = _find(Runnable, "Run3") ]
					labelAccess [ access = WRITE; data = _find(Label, "Lab3") ]
					labelAccess [ access = WRITE; data = _find(Label, "Lab4") ]
				]
			]
		]

		val r2 = _find(model, Runnable, "Run2")
		r2.activityGraph [
			ticks [ defaultConstant(400) ]
			ticks [ defaultConstant(40) ]
			ticks [ defaultConstant(4) ]
			runnableCall [ runnable = _find(Runnable, "Run4") ]
			labelAccess [ access = WRITE; data = _find(Label, "Lab5") ]
		]

		val r3 = _find(model, Runnable, "Run3")
		r3.activityGraph [
			labelAccess [ access = READ; data = _find(Label, "Lab5") ]
			ticks [ defaultConstant(600) ]
		]
					
		val r4 = _find(model, Runnable, "Run4")		
		r4.activityGraph [
			ticks [ defaultConstant(700) ]
		]
		
		return model
	}

}