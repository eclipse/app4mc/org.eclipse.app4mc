/**
 * Copyright (c) 2018-2020 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.models;

import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.StructureType;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class DeploymentModels {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private HardwareBuilder b2 = new HardwareBuilder();

  @Extension
  private SoftwareBuilder b3 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b5 = new OperatingSystemBuilder();

  @Extension
  private MappingBuilder b6 = new MappingBuilder();

  public static void main(final String[] args) {
    final Amalthea model1 = new DeploymentModels().model1();
    AmaltheaWriter.writeToFileNamed(model1, "test-data/DeploymentUtilTestModel-gen.amxmi");
  }

  public static Amalthea createModel1() {
    return new DeploymentModels().model1();
  }

  public Amalthea model1() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<ProcessingUnitDefinition> _function_2 = (ProcessingUnitDefinition it_2) -> {
          it_2.setName("Pu1_def");
        };
        this.b2.definition_ProcessingUnit(it_1, _function_2);
        final Consumer<ProcessingUnitDefinition> _function_3 = (ProcessingUnitDefinition it_2) -> {
          it_2.setName("Pu2_def");
        };
        this.b2.definition_ProcessingUnit(it_1, _function_3);
        final Consumer<HwStructure> _function_4 = (HwStructure it_2) -> {
          it_2.setName("SoC");
          it_2.setStructureType(StructureType.SO_C);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("core1");
          };
          this.b2.module_ProcessingUnit(it_2, _function_5);
          final Consumer<ProcessingUnit> _function_6 = (ProcessingUnit it_3) -> {
            it_3.setName("core2");
          };
          this.b2.module_ProcessingUnit(it_2, _function_6);
          final Consumer<ProcessingUnit> _function_7 = (ProcessingUnit it_3) -> {
            it_3.setName("core3");
          };
          this.b2.module_ProcessingUnit(it_2, _function_7);
        };
        this.b2.structure(it_1, _function_4);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Task> _function_3 = (Task it_2) -> {
          it_2.setName("t1");
        };
        this.b3.task(it_1, _function_3);
        final Consumer<Task> _function_4 = (Task it_2) -> {
          it_2.setName("t2");
        };
        this.b3.task(it_1, _function_4);
        final Consumer<Task> _function_5 = (Task it_2) -> {
          it_2.setName("t3");
        };
        this.b3.task(it_1, _function_5);
        final Consumer<Task> _function_6 = (Task it_2) -> {
          it_2.setName("t4");
        };
        this.b3.task(it_1, _function_6);
        final Consumer<Task> _function_7 = (Task it_2) -> {
          it_2.setName("t5");
        };
        this.b3.task(it_1, _function_7);
        final Consumer<Task> _function_8 = (Task it_2) -> {
          it_2.setName("t6");
        };
        this.b3.task(it_1, _function_8);
        final Consumer<Task> _function_9 = (Task it_2) -> {
          it_2.setName("t7");
        };
        this.b3.task(it_1, _function_9);
        final Consumer<ISR> _function_10 = (ISR it_2) -> {
          it_2.setName("isr1");
        };
        this.b3.isr(it_1, _function_10);
        final Consumer<ISR> _function_11 = (ISR it_2) -> {
          it_2.setName("isr2");
        };
        this.b3.isr(it_1, _function_11);
        final Consumer<ISR> _function_12 = (ISR it_2) -> {
          it_2.setName("isr3");
        };
        this.b3.isr(it_1, _function_12);
      };
      this.b1.softwareModel(it, _function_2);
      final Consumer<OSModel> _function_3 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_4 = (OperatingSystem it_2) -> {
          it_2.setName("Os1");
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("Scheduler1");
          };
          this.b5.taskScheduler(it_2, _function_5);
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("Scheduler2");
          };
          this.b5.taskScheduler(it_2, _function_6);
          final Consumer<TaskScheduler> _function_7 = (TaskScheduler it_3) -> {
            it_3.setName("Scheduler3");
            final Consumer<SchedulerAssociation> _function_8 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "Scheduler2"));
            };
            this.b5.parentAssociation(it_3, _function_8);
          };
          this.b5.taskScheduler(it_2, _function_7);
          final Consumer<TaskScheduler> _function_8 = (TaskScheduler it_3) -> {
            it_3.setName("Scheduler4");
            final Consumer<SchedulerAssociation> _function_9 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "Scheduler3"));
            };
            this.b5.parentAssociation(it_3, _function_9);
          };
          this.b5.taskScheduler(it_2, _function_8);
          final Consumer<InterruptController> _function_9 = (InterruptController it_3) -> {
            it_3.setName("controller1");
          };
          this.b5.interruptController(it_2, _function_9);
          final Consumer<InterruptController> _function_10 = (InterruptController it_3) -> {
            it_3.setName("controller2");
          };
          this.b5.interruptController(it_2, _function_10);
        };
        this.b5.operatingSystem(it_1, _function_4);
      };
      this.b1.osModel(it, _function_3);
      final Consumer<MappingModel> _function_4 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2"));
        };
        this.b6.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1"));
        };
        this.b6.schedulerAllocation(it_1, _function_6);
        final Consumer<SchedulerAllocation> _function_7 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler3"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1"));
        };
        this.b6.schedulerAllocation(it_1, _function_7);
        final Consumer<SchedulerAllocation> _function_8 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler4"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2"));
        };
        this.b6.schedulerAllocation(it_1, _function_8);
        final Consumer<SchedulerAllocation> _function_9 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler5"));
        };
        this.b6.schedulerAllocation(it_1, _function_9);
        final Consumer<TaskAllocation> _function_10 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler1"));
          EList<ProcessingUnit> _affinity = it_2.getAffinity();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _affinity.add(__find);
        };
        this.b6.taskAllocation(it_1, _function_10);
        final Consumer<TaskAllocation> _function_11 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t2"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler2"));
          EList<ProcessingUnit> _affinity = it_2.getAffinity();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _affinity.add(__find);
        };
        this.b6.taskAllocation(it_1, _function_11);
        final Consumer<TaskAllocation> _function_12 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t3"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler1"));
          EList<ProcessingUnit> _affinity = it_2.getAffinity();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _affinity.add(__find);
        };
        this.b6.taskAllocation(it_1, _function_12);
        final Consumer<TaskAllocation> _function_13 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t4"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler3"));
          EList<ProcessingUnit> _affinity = it_2.getAffinity();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          _affinity.add(__find);
        };
        this.b6.taskAllocation(it_1, _function_13);
        final Consumer<TaskAllocation> _function_14 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t5"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler4"));
        };
        this.b6.taskAllocation(it_1, _function_14);
        final Consumer<TaskAllocation> _function_15 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t6"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler3"));
        };
        this.b6.taskAllocation(it_1, _function_15);
        final Consumer<TaskAllocation> _function_16 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t7"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler5"));
        };
        this.b6.taskAllocation(it_1, _function_16);
        final Consumer<ISRAllocation> _function_17 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "isr1"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "controller1"));
        };
        this.b6.isrAllocation(it_1, _function_17);
        final Consumer<SchedulerAllocation> _function_18 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "controller1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1"));
        };
        this.b6.schedulerAllocation(it_1, _function_18);
        final Consumer<ISRAllocation> _function_19 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "isr2"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "controller2"));
        };
        this.b6.isrAllocation(it_1, _function_19);
        final Consumer<SchedulerAllocation> _function_20 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "controller2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b6.schedulerAllocation(it_1, _function_20);
        final Consumer<ISRAllocation> _function_21 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "isr3"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "controller1"));
        };
        this.b6.isrAllocation(it_1, _function_21);
        final Consumer<SchedulerAllocation> _function_22 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "controller1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1"));
        };
        this.b6.schedulerAllocation(it_1, _function_22);
      };
      this.b1.mappingModel(it, _function_4);
    };
    final Amalthea model = this.b1.amalthea(_function);
    return model;
  }
}
