/**
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.models;

import com.google.common.collect.Iterables;
import java.util.Collections;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.ParameterType;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.StructureType;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Test;

@SuppressWarnings("all")
public class SchedulerModels {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private HardwareBuilder b2 = new HardwareBuilder();

  @Extension
  private SoftwareBuilder b3 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b5 = new OperatingSystemBuilder();

  @Extension
  private MappingBuilder b6 = new MappingBuilder();

  @Test
  public void model1() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("MC1");
          it_2.setStructureType(StructureType.MICROCONTROLLER);
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("core1");
          };
          this.b2.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("core2");
          };
          this.b2.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("core3");
          };
          this.b2.module_ProcessingUnit(it_2, _function_5);
        };
        this.b2.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Task> _function_3 = (Task it_2) -> {
          it_2.setName("t1");
        };
        this.b3.task(it_1, _function_3);
        final Consumer<Task> _function_4 = (Task it_2) -> {
          it_2.setName("t2");
        };
        this.b3.task(it_1, _function_4);
        final Consumer<Task> _function_5 = (Task it_2) -> {
          it_2.setName("t3");
        };
        this.b3.task(it_1, _function_5);
        final Consumer<ISR> _function_6 = (ISR it_2) -> {
          it_2.setName("isr1");
        };
        this.b3.isr(it_1, _function_6);
        final Consumer<ISR> _function_7 = (ISR it_2) -> {
          it_2.setName("isr2");
        };
        this.b3.isr(it_1, _function_7);
        final Consumer<ISR> _function_8 = (ISR it_2) -> {
          it_2.setName("isr3");
        };
        this.b3.isr(it_1, _function_8);
      };
      this.b1.softwareModel(it, _function_2);
      final Consumer<OSModel> _function_3 = (OSModel it_1) -> {
        final Consumer<SchedulingParameterDefinition> _function_4 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("hyperPeriod");
          it_2.setType(ParameterType.TIME);
        };
        this.b5.parameterDefinition(it_1, _function_4);
        final Consumer<SchedulingParameterDefinition> _function_5 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("timeStamps");
          it_2.setMany(true);
          it_2.setType(ParameterType.TIME);
        };
        this.b5.parameterDefinition(it_1, _function_5);
        final Consumer<SchedulingParameterDefinition> _function_6 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("priority");
          it_2.setType(ParameterType.INTEGER);
        };
        this.b5.parameterDefinition(it_1, _function_6);
        final Consumer<SchedulingParameterDefinition> _function_7 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("preemptible");
          it_2.setType(ParameterType.BOOL);
          it_2.setDefaultValue(FactoryUtil.createBooleanObject(false));
        };
        this.b5.parameterDefinition(it_1, _function_7);
        final Consumer<SchedulingParameterDefinition> _function_8 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("taskGroup");
          it_2.setType(ParameterType.INTEGER);
        };
        this.b5.parameterDefinition(it_1, _function_8);
        final Consumer<SchedulerDefinition> _function_9 = (SchedulerDefinition it_2) -> {
          it_2.setName("TDMA");
          EList<SchedulingParameterDefinition> _algorithmParameters = it_2.getAlgorithmParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "hyperPeriod");
          _algorithmParameters.add(__find);
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "timeStamps");
          _processParameters.add(__find_1);
        };
        this.b5.schedulerDefinition(it_1, _function_9);
        final Consumer<SchedulerDefinition> _function_10 = (SchedulerDefinition it_2) -> {
          it_2.setName("OSEK");
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "priority");
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "preemptible");
          SchedulingParameterDefinition __find_2 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "taskGroup");
          Iterables.<SchedulingParameterDefinition>addAll(_processParameters, Collections.<SchedulingParameterDefinition>unmodifiableList(CollectionLiterals.<SchedulingParameterDefinition>newArrayList(__find, __find_1, __find_2)));
        };
        this.b5.schedulerDefinition(it_1, _function_10);
        final Consumer<SchedulerDefinition> _function_11 = (SchedulerDefinition it_2) -> {
          it_2.setName("FixedPriorityPreemptive");
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "priority");
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "preemptible");
          Iterables.<SchedulingParameterDefinition>addAll(_processParameters, Collections.<SchedulingParameterDefinition>unmodifiableList(CollectionLiterals.<SchedulingParameterDefinition>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerDefinition(it_1, _function_11);
        final Consumer<SchedulerDefinition> _function_12 = (SchedulerDefinition it_2) -> {
          it_2.setName("PriorityBased");
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "priority");
          _processParameters.add(__find);
        };
        this.b5.schedulerDefinition(it_1, _function_12);
        final Consumer<OperatingSystem> _function_13 = (OperatingSystem it_2) -> {
          it_2.setName("Os1");
          final Consumer<TaskScheduler> _function_14 = (TaskScheduler it_3) -> {
            it_3.setName("sched1");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "OSEK"));
          };
          this.b5.taskScheduler(it_2, _function_14);
          final Consumer<TaskScheduler> _function_15 = (TaskScheduler it_3) -> {
            it_3.setName("sched2");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "FixedPriorityPreemptive"));
          };
          this.b5.taskScheduler(it_2, _function_15);
          final Consumer<TaskScheduler> _function_16 = (TaskScheduler it_3) -> {
            it_3.setName("sched3");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "TDMA"));
            this.b5.schedulingParameter(it_3, "hyperPeriod", FactoryUtil.createTime(5, TimeUnit.MS));
          };
          this.b5.taskScheduler(it_2, _function_16);
          final Consumer<InterruptController> _function_17 = (InterruptController it_3) -> {
            it_3.setName("iCtrl1");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "PriorityBased"));
          };
          this.b5.interruptController(it_2, _function_17);
          final Consumer<InterruptController> _function_18 = (InterruptController it_3) -> {
            it_3.setName("iCtrl2");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "PriorityBased"));
          };
          this.b5.interruptController(it_2, _function_18);
        };
        this.b5.operatingSystem(it_1, _function_13);
      };
      this.b1.osModel(it, _function_3);
      final Consumer<MappingModel> _function_4 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b6.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
        };
        this.b6.schedulerAllocation(it_1, _function_6);
        final Consumer<SchedulerAllocation> _function_7 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched3"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          _responsibility.add(__find);
        };
        this.b6.schedulerAllocation(it_1, _function_7);
        final Consumer<TaskAllocation> _function_8 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched1"));
          this.b6.schedulingParameter(it_2, "priority", FactoryUtil.createIntegerObject(3));
        };
        this.b6.taskAllocation(it_1, _function_8);
        final Consumer<TaskAllocation> _function_9 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t2"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched2"));
          this.b6.schedulingParameter(it_2, "priority", FactoryUtil.createIntegerObject(40));
          this.b6.schedulingParameter(it_2, "preemptible", FactoryUtil.createBooleanObject(false));
        };
        this.b6.taskAllocation(it_1, _function_9);
        final Consumer<TaskAllocation> _function_10 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t3"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched3"));
          Time _createTime = FactoryUtil.createTime(0, TimeUnit.MS);
          Time _createTime_1 = FactoryUtil.createTime(2, TimeUnit.MS);
          this.b6.schedulingParameter(it_2, "timeStamps", FactoryUtil.createListObject(
            Collections.<Value>unmodifiableList(CollectionLiterals.<Value>newArrayList(_createTime, _createTime_1))));
        };
        this.b6.taskAllocation(it_1, _function_10);
        final Consumer<ISRAllocation> _function_11 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "isr1"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "iCtrl1"));
        };
        this.b6.isrAllocation(it_1, _function_11);
        final Consumer<SchedulerAllocation> _function_12 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "iCtrl1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1"));
        };
        this.b6.schedulerAllocation(it_1, _function_12);
        final Consumer<ISRAllocation> _function_13 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "isr2"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "iCtrl2"));
        };
        this.b6.isrAllocation(it_1, _function_13);
        final Consumer<SchedulerAllocation> _function_14 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "iCtrl2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b6.schedulerAllocation(it_1, _function_14);
      };
      this.b1.mappingModel(it, _function_4);
    };
    this.b1.amalthea(_function);
  }
}
