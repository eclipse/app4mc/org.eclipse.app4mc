/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.app4mc.amalthea.validations.inchron.tests

import java.util.List
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.ProcessRequirement
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.ConstraintsBuilder
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class InchronConstraintsValidator {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension ConstraintsBuilder b2 = new ConstraintsBuilder

	val executor = new ValidationExecutor(#[InchronProfile])

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}

	def ProcessRequirement createProcessReqWithHwContext(String name, String PUname) {
		val req = AmaltheaFactory.eINSTANCE.createProcessRequirement
		req.name = name
		val cpulimit = AmaltheaFactory.eINSTANCE.createCPUPercentageRequirementLimit
		val pu = AmaltheaFactory.eINSTANCE.createProcessingUnit
		pu.name = PUname
		cpulimit.hardwareContext = pu
		req.limit = cpulimit
		req
	}

	@Test
	def void test_InchronConstraintsCPUPercentageLimit() {
		val model = amalthea [
			constraintsModel [
				requirement_Process [name = "req_2" limit_CPUPercentage []]
				requirements += createProcessReqWithHwContext("req_1", "PU_1")
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)	
		assertFalse(
			result.contains("CPU Percentage Requirement Limit of Requirement \"req_1\" is missing Hardware context"))
		assertTrue(
			result.contains("CPU Percentage Requirement Limit of Requirement \"req_2\" is missing Hardware context"))
	}

}