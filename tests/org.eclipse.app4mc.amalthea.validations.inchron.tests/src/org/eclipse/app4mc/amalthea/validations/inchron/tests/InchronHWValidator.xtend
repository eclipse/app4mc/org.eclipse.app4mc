/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.tests

import java.util.List
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.FrequencyDomain
import org.eclipse.app4mc.amalthea.model.PortInterface
import org.eclipse.app4mc.amalthea.model.PortType
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class InchronHWValidator {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension HardwareBuilder b2 = new HardwareBuilder
	

	val executor = new ValidationExecutor(#[InchronProfile])

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}

	@Test
	def void test_HardwareMemoryPort() {
		val model = amalthea [
			hardwareModel [
				structure [
					name = "hw_struct_1"
					module_Memory [
						name = "mem1"
						port [name = "port1" portType = PortType.INITIATOR]
						port [name = "port2"]
						port [name = "port3" portType = PortType.RESPONDER]
					]
				]
			]
		]

		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)
		assertTrue(result.contains("HW Module \"mem1\" missing its 'Frequency Domain' reference"))
		assertTrue(result.contains("Memory \"mem1\" has a HW port \"port1\" of type not equal to RESPONDER"))
		assertTrue(result.contains("Memory \"mem1\" has a HW port \"port2\" of type not equal to RESPONDER"))
		assertFalse(result.contains("Memory \"mem1\" has a HW port \"port3\" of type not equal to RESPONDER"))
	}

	@Test
	def void test_HardwarePUPort() {

		val model = amalthea [
			hardwareModel [
				structure [
					name = "hw_struct_1"
					module_ProcessingUnit [
						name = "PU_1"
						port [name = "port1" portType = PortType.INITIATOR]
						port [name = "port2"]
						port [name = "port3" portType = PortType.RESPONDER]
					]
				]
			]
		]

		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)

		assertTrue(result.contains("HW Module \"PU_1\" missing its 'Frequency Domain' reference"))
		assertTrue(result.contains("Processing Unit \"PU_1\" has a HW port \"port2\" of type not equal to INITIATOR"))
		assertTrue(result.contains("Processing Unit \"PU_1\" has a HW port \"port3\" of type not equal to INITIATOR"))
		assertFalse(result.contains("Processing Unit \"PU_1\" has a HW port \"port1\" of type not equal to INITIATOR"))

	}

	@Test
	def void test_HWModuleMissingFrequencyDomain() {

		val model = amalthea [
			hardwareModel [
				domain_Frequency [name = "fd_1"]
				structure [
					name = "hw_struct_1"
					module_ProcessingUnit [name = "PU_1" frequencyDomain = _find(FrequencyDomain, "fd_1")]
					module_ProcessingUnit [name = "PU_2"]
				]
			]
		]

		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)

		assertTrue(result.contains("HW Module \"PU_2\" missing its 'Frequency Domain' reference"))
		assertFalse(result.contains("HW Module \"PU_1\" missing its 'Frequency Domain' reference"))
	}

	@Test
	def void test_HWModulePortBitWidths() {

		val model = amalthea [
			hardwareModel [
				structure [
					name = "hw_struct_1"
					module_ProcessingUnit [
						name = "PU_1"
						port [name = "port1" portInterface = PortInterface.AXI bitWidth = 8]
						port [name = "port2" portInterface = PortInterface.APB bitWidth = 8]
						port [name = "port3" portInterface = PortInterface.AHB bitWidth = 8]
						port [name = "port4" portInterface = PortInterface.CUSTOM bitWidth = 8]
					]

					module_ProcessingUnit [
						name = "PU_2"
						port [name = "port1" portInterface = PortInterface.AXI bitWidth = 8]
						port [name = "port2" portInterface = PortInterface.APB bitWidth = 8]
						port [name = "port3" portInterface = PortInterface.AHB bitWidth = 16]
						port [name = "port4" portInterface = PortInterface.CUSTOM bitWidth = 8]
					]
				]
			]
		]

		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)

		assertTrue(result.contains("HW Module \"PU_2\" has custom/AHB/APB/AXI ports with unequal bitwidths"))
		assertFalse(result.contains("HW Module \"PU_1\" has custom/AHB/APB/AXI ports with unequal bitwidths"))
	}

}