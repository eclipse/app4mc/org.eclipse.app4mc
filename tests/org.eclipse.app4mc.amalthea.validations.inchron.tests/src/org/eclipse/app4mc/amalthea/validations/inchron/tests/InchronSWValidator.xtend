/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.tests

import java.util.List
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class InchronSWValidator {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension OperatingSystemBuilder b2 = new OperatingSystemBuilder
	extension MappingBuilder b4 = new MappingBuilder
	extension SoftwareBuilder b5 = new SoftwareBuilder

	val executor = new ValidationExecutor(#[InchronProfile])

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}

	@Test
	def void test_SWTaskRunnable() {

		val model = amalthea [
			osModel [
				operatingSystem [name = "OS_1" taskScheduler [name = "sched_1"]]
				operatingSystem [name = "OS_2" taskScheduler [name = "sched_2"]]
			]
			softwareModel [
				task [name = "Task_1"]
				task [
					name = "Task_2"
					activityGraph[enforcedMigration [resourceOwner = _find(TaskScheduler, "sched_1")]]
				]
				runnable [name = "Runnable_1"]
				runnable [name = "Runnable_2" activityGraph[]]
			]

			mappingModel [
				taskAllocation [
					task = _find(Task, "Task_1")
					scheduler = _find(TaskScheduler, "sched_1")
				]
				taskAllocation [
					task = _find(Task, "Task_1")
					scheduler = _find(TaskScheduler, "sched_2")
				]
				runnableAllocation [
					entity = _find(Runnable, "Runnable_1")
					scheduler = _find(TaskScheduler, "sched_1")
				]
				runnableAllocation [
					entity = _find(Runnable, "Runnable_1")
					scheduler = _find(TaskScheduler, "sched_2")
				]
			]

		]

		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)

		assertTrue(result.contains("Task \"Task_1\" has no ActivityGraph"))
		assertFalse(result.contains("Task \"Task_2\" has no ActivityGraph"))
		assertTrue(result.contains("Runnable \"Runnable_1\" has no ActivityGraph"))
		assertFalse(result.contains("Runnable \"Runnable_2\" has no ActivityGraph"))
		assertTrue(result.contains("Task \"Task_1\" is scheduled by more than one operating system: OS_1,OS_2"))
		assertFalse(result.contains("Task \"Task_2\" is scheduled by more than one operating system: OS_1,OS_2"))
		assertTrue(result.contains("Runnable \"Runnable_1\" is scheduled by more than one operating system: OS_1,OS_2"))
		assertFalse(
			result.contains("Runnable \"Runnable_2\" is scheduled by more than one operating system: OS_1,OS_2"))
		assertTrue(
			result.contains(
				"Enforced Migration of task \"Task_2\" to Task Scheduler: \"sched_1\" that is not part of the list of schedulers allocated to the task"))
		assertFalse(
			result.contains(
				"Enforced Migration of task \"Task_1\" to Task Scheduler: \"sched_1\" that is not part of the list of schedulers allocated to the task"))
		assertTrue(result.contains("RunnableAllocation of runnable \"Runnable_1\" is not supported"))
		assertFalse(result.contains("RunnableAllocation of runnable \"Runnable_2\" is not supported"))
	}

}