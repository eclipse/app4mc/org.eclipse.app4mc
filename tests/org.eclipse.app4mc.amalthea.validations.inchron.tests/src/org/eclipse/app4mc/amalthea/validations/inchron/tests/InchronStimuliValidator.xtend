/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.tests

import java.util.List
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.ArrivalCurveStimulus
import org.eclipse.app4mc.amalthea.model.CustomStimulus
import org.eclipse.app4mc.amalthea.model.EventStimulus
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class InchronStimuliValidator {
	
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension StimuliBuilder b2 = new StimuliBuilder
	val executor = new ValidationExecutor(#[InchronProfile])

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}
	
	def ArrivalCurveStimulus createACS(String name) {
		val ret = AmaltheaFactory.eINSTANCE.createArrivalCurveStimulus
		ret.name = name
		ret
	}
	
	
	def CustomStimulus createCS(String name) {
		val ret = AmaltheaFactory.eINSTANCE.createCustomStimulus
		ret.name = name
		ret
	}
	
	def EventStimulus createES(String name) {
		val ret = AmaltheaFactory.eINSTANCE.createEventStimulus
		ret.name = name
		ret
	}
	
	
	@Test
	def void test_InchronStimuliGenerators() {
		val model = amalthea [
			stimuliModel [
				variableRateStimulus [name = "vrs_1"]
				periodicStimulus [name = "ps_1"]
				stimuli += createACS("acs_1")
				stimuli += createCS("cs_1")
				stimuli += createES("es_1")
				
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		
		assertTrue(result.contains("Stimulus \"vrs_1\" is of unsupported type: VariableRateStimulus"))
		assertTrue(result.contains("Stimulus \"acs_1\" is of unsupported type: ArrivalCurveStimulus"))
		assertTrue(result.contains("Stimulus \"cs_1\" is of unsupported type: CustomStimulus"))
		assertTrue(result.contains("Stimulus \"es_1\" is of unsupported type: EventStimulus"))
		assertFalse(result.contains("Stimulus \"ps_1\" is of unsupported type: PeriodicStimulus"))
	}
	
}