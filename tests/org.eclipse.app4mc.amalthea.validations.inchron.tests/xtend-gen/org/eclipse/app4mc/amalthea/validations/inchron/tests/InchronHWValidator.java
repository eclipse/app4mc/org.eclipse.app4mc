/**
 * Copyright (c) 2020 INCHRON AG and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     INCHRON AG - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.inchron.tests;

import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.PortInterface;
import org.eclipse.app4mc.amalthea.model.PortType;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class InchronHWValidator {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private HardwareBuilder b2 = new HardwareBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(InchronProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  @Test
  public void test_HardwareMemoryPort() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_1");
          final Consumer<Memory> _function_3 = (Memory it_3) -> {
            it_3.setName("mem1");
            final Consumer<HwPort> _function_4 = (HwPort it_4) -> {
              it_4.setName("port1");
              it_4.setPortType(PortType.INITIATOR);
            };
            this.b2.port(it_3, _function_4);
            final Consumer<HwPort> _function_5 = (HwPort it_4) -> {
              it_4.setName("port2");
            };
            this.b2.port(it_3, _function_5);
            final Consumer<HwPort> _function_6 = (HwPort it_4) -> {
              it_4.setName("port3");
              it_4.setPortType(PortType.RESPONDER);
            };
            this.b2.port(it_3, _function_6);
          };
          this.b2.module_Memory(it_2, _function_3);
        };
        this.b2.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(result.contains("HW Module \"mem1\" missing its \'Frequency Domain\' reference"));
    Assert.assertTrue(result.contains("Memory \"mem1\" has a HW port \"port1\" of type not equal to RESPONDER"));
    Assert.assertTrue(result.contains("Memory \"mem1\" has a HW port \"port2\" of type not equal to RESPONDER"));
    Assert.assertFalse(result.contains("Memory \"mem1\" has a HW port \"port3\" of type not equal to RESPONDER"));
  }

  @Test
  public void test_HardwarePUPort() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_1");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_1");
            final Consumer<HwPort> _function_4 = (HwPort it_4) -> {
              it_4.setName("port1");
              it_4.setPortType(PortType.INITIATOR);
            };
            this.b2.port(it_3, _function_4);
            final Consumer<HwPort> _function_5 = (HwPort it_4) -> {
              it_4.setName("port2");
            };
            this.b2.port(it_3, _function_5);
            final Consumer<HwPort> _function_6 = (HwPort it_4) -> {
              it_4.setName("port3");
              it_4.setPortType(PortType.RESPONDER);
            };
            this.b2.port(it_3, _function_6);
          };
          this.b2.module_ProcessingUnit(it_2, _function_3);
        };
        this.b2.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(result.contains("HW Module \"PU_1\" missing its \'Frequency Domain\' reference"));
    Assert.assertTrue(result.contains("Processing Unit \"PU_1\" has a HW port \"port2\" of type not equal to INITIATOR"));
    Assert.assertTrue(result.contains("Processing Unit \"PU_1\" has a HW port \"port3\" of type not equal to INITIATOR"));
    Assert.assertFalse(result.contains("Processing Unit \"PU_1\" has a HW port \"port1\" of type not equal to INITIATOR"));
  }

  @Test
  public void test_HWModuleMissingFrequencyDomain() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<FrequencyDomain> _function_2 = (FrequencyDomain it_2) -> {
          it_2.setName("fd_1");
        };
        this.b2.domain_Frequency(it_1, _function_2);
        final Consumer<HwStructure> _function_3 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_1");
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_1");
            it_3.setFrequencyDomain(this.b1.<FrequencyDomain>_find(it_3, FrequencyDomain.class, "fd_1"));
          };
          this.b2.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_2");
          };
          this.b2.module_ProcessingUnit(it_2, _function_5);
        };
        this.b2.structure(it_1, _function_3);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(result.contains("HW Module \"PU_2\" missing its \'Frequency Domain\' reference"));
    Assert.assertFalse(result.contains("HW Module \"PU_1\" missing its \'Frequency Domain\' reference"));
  }

  @Test
  public void test_HWModulePortBitWidths() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_1");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_1");
            final Consumer<HwPort> _function_4 = (HwPort it_4) -> {
              it_4.setName("port1");
              it_4.setPortInterface(PortInterface.AXI);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_4);
            final Consumer<HwPort> _function_5 = (HwPort it_4) -> {
              it_4.setName("port2");
              it_4.setPortInterface(PortInterface.APB);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_5);
            final Consumer<HwPort> _function_6 = (HwPort it_4) -> {
              it_4.setName("port3");
              it_4.setPortInterface(PortInterface.AHB);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_6);
            final Consumer<HwPort> _function_7 = (HwPort it_4) -> {
              it_4.setName("port4");
              it_4.setPortInterface(PortInterface.CUSTOM);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_7);
          };
          this.b2.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_2");
            final Consumer<HwPort> _function_5 = (HwPort it_4) -> {
              it_4.setName("port1");
              it_4.setPortInterface(PortInterface.AXI);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_5);
            final Consumer<HwPort> _function_6 = (HwPort it_4) -> {
              it_4.setName("port2");
              it_4.setPortInterface(PortInterface.APB);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_6);
            final Consumer<HwPort> _function_7 = (HwPort it_4) -> {
              it_4.setName("port3");
              it_4.setPortInterface(PortInterface.AHB);
              it_4.setBitWidth(16);
            };
            this.b2.port(it_3, _function_7);
            final Consumer<HwPort> _function_8 = (HwPort it_4) -> {
              it_4.setName("port4");
              it_4.setPortInterface(PortInterface.CUSTOM);
              it_4.setBitWidth(8);
            };
            this.b2.port(it_3, _function_8);
          };
          this.b2.module_ProcessingUnit(it_2, _function_4);
        };
        this.b2.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(result.contains("HW Module \"PU_2\" has custom/AHB/APB/AXI ports with unequal bitwidths"));
    Assert.assertFalse(result.contains("HW Module \"PU_1\" has custom/AHB/APB/AXI ports with unequal bitwidths"));
  }
}
