/**
 * Copyright (c) 2020 INCHRON AG and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     INCHRON AG - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.inchron.tests;

import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class InchronOSValidator {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private OperatingSystemBuilder b2 = new OperatingSystemBuilder();

  @Extension
  private HardwareBuilder b3 = new HardwareBuilder();

  @Extension
  private MappingBuilder b4 = new MappingBuilder();

  @Extension
  private SoftwareBuilder b5 = new SoftwareBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(InchronProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  @Test
  public void test_OSToPUMapping() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_1");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_1");
          };
          this.b3.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_2");
          };
          this.b3.module_ProcessingUnit(it_2, _function_4);
        };
        this.b3.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<OSModel> _function_2 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("OS_1");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("sched_1");
          };
          this.b2.taskScheduler(it_2, _function_4);
        };
        this.b2.operatingSystem(it_1, _function_3);
        final Consumer<OperatingSystem> _function_4 = (OperatingSystem it_2) -> {
          it_2.setName("OS_2");
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("sched_2");
          };
          this.b2.taskScheduler(it_2, _function_5);
        };
        this.b2.operatingSystem(it_1, _function_4);
        final Consumer<OperatingSystem> _function_5 = (OperatingSystem it_2) -> {
          it_2.setName("OS_3");
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("sched_3");
          };
          this.b2.taskScheduler(it_2, _function_6);
        };
        this.b2.operatingSystem(it_1, _function_5);
      };
      this.b1.osModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_4 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "PU_1");
          _responsibility.add(__find);
        };
        this.b4.schedulerAllocation(it_1, _function_4);
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "PU_1");
          _responsibility.add(__find);
        };
        this.b4.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_3"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "PU_2");
          _responsibility.add(__find);
        };
        this.b4.schedulerAllocation(it_1, _function_6);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(
      result.contains(
        "Operating system \"OS_1\" consists of task schedulers allocated to processing units: PU_1 referenced by other operating systems"));
    Assert.assertTrue(
      result.contains(
        "Operating system \"OS_2\" consists of task schedulers allocated to processing units: PU_1 referenced by other operating systems"));
    Assert.assertFalse(
      result.contains(
        "Operating system \"OS_3\" consists of task schedulers allocated to processing units: PU_1 referenced by other operating systems"));
  }

  @Test
  public void test_OSSchedulerAllocation() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_1");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_1");
          };
          this.b3.module_ProcessingUnit(it_2, _function_3);
        };
        this.b3.structure(it_1, _function_2);
        final Consumer<HwStructure> _function_3 = (HwStructure it_2) -> {
          it_2.setName("hw_struct_2");
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_2");
          };
          this.b3.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("PU_3");
          };
          this.b3.module_ProcessingUnit(it_2, _function_5);
        };
        this.b3.structure(it_1, _function_3);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<OSModel> _function_2 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("OS_1");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("sched_1");
          };
          this.b2.taskScheduler(it_2, _function_4);
        };
        this.b2.operatingSystem(it_1, _function_3);
        final Consumer<OperatingSystem> _function_4 = (OperatingSystem it_2) -> {
          it_2.setName("OS_2");
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("sched_2");
          };
          this.b2.taskScheduler(it_2, _function_5);
        };
        this.b2.operatingSystem(it_1, _function_4);
      };
      this.b1.osModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_4 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "PU_1");
          _responsibility.add(__find);
          EList<ProcessingUnit> _responsibility_1 = it_2.getResponsibility();
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "PU_2");
          _responsibility_1.add(__find_1);
        };
        this.b4.schedulerAllocation(it_1, _function_4);
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "PU_3");
          _responsibility.add(__find);
        };
        this.b4.schedulerAllocation(it_1, _function_5);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(
      result.contains(
        "Task Scheduler \"sched_1\" allocated to more than one HwStructure: hw_struct_1,hw_struct_2"));
    Assert.assertFalse(
      result.contains(
        "Task Scheduler \"sched_2\" allocated to more than one HwStructure: hw_struct_1,hw_struct_2"));
  }

  @Test
  public void test_UserSpecificScheduler() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<Task> _function_2 = (Task it_2) -> {
          it_2.setName("Task_1");
        };
        this.b5.task(it_1, _function_2);
      };
      this.b1.softwareModel(it, _function_1);
      final Consumer<OSModel> _function_2 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("OS_1");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("sched_1");
          };
          this.b2.taskScheduler(it_2, _function_4);
        };
        this.b2.operatingSystem(it_1, _function_3);
        final Consumer<OperatingSystem> _function_4 = (OperatingSystem it_2) -> {
          it_2.setName("OS_2");
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("sched_2");
          };
          this.b2.taskScheduler(it_2, _function_5);
        };
        this.b2.operatingSystem(it_1, _function_4);
      };
      this.b1.osModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<TaskAllocation> _function_4 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "Task_1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_2"));
        };
        this.b4.taskAllocation(it_1, _function_4);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
  }
}
