/**
 * Copyright (c) 2020 INCHRON AG and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     INCHRON AG - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.inchron.tests;

import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ArrivalCurveStimulus;
import org.eclipse.app4mc.amalthea.model.CustomStimulus;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.VariableRateStimulus;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder;
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class InchronStimuliValidator {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private StimuliBuilder b2 = new StimuliBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(InchronProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  public ArrivalCurveStimulus createACS(final String name) {
    ArrivalCurveStimulus _xblockexpression = null;
    {
      final ArrivalCurveStimulus ret = AmaltheaFactory.eINSTANCE.createArrivalCurveStimulus();
      ret.setName(name);
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }

  public CustomStimulus createCS(final String name) {
    CustomStimulus _xblockexpression = null;
    {
      final CustomStimulus ret = AmaltheaFactory.eINSTANCE.createCustomStimulus();
      ret.setName(name);
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }

  public EventStimulus createES(final String name) {
    EventStimulus _xblockexpression = null;
    {
      final EventStimulus ret = AmaltheaFactory.eINSTANCE.createEventStimulus();
      ret.setName(name);
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }

  @Test
  public void test_InchronStimuliGenerators() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<StimuliModel> _function_1 = (StimuliModel it_1) -> {
        final Consumer<VariableRateStimulus> _function_2 = (VariableRateStimulus it_2) -> {
          it_2.setName("vrs_1");
        };
        this.b2.variableRateStimulus(it_1, _function_2);
        final Consumer<PeriodicStimulus> _function_3 = (PeriodicStimulus it_2) -> {
          it_2.setName("ps_1");
        };
        this.b2.periodicStimulus(it_1, _function_3);
        EList<Stimulus> _stimuli = it_1.getStimuli();
        ArrivalCurveStimulus _createACS = this.createACS("acs_1");
        _stimuli.add(_createACS);
        EList<Stimulus> _stimuli_1 = it_1.getStimuli();
        CustomStimulus _createCS = this.createCS("cs_1");
        _stimuli_1.add(_createCS);
        EList<Stimulus> _stimuli_2 = it_1.getStimuli();
        EventStimulus _createES = this.createES("es_1");
        _stimuli_2.add(_createES);
      };
      this.b1.stimuliModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("Stimulus \"vrs_1\" is of unsupported type: VariableRateStimulus"));
    Assert.assertTrue(result.contains("Stimulus \"acs_1\" is of unsupported type: ArrivalCurveStimulus"));
    Assert.assertTrue(result.contains("Stimulus \"cs_1\" is of unsupported type: CustomStimulus"));
    Assert.assertTrue(result.contains("Stimulus \"es_1\" is of unsupported type: EventStimulus"));
    Assert.assertFalse(result.contains("Stimulus \"ps_1\" is of unsupported type: PeriodicStimulus"));
  }
}
