/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.tests

import java.util.regex.Pattern
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex
import org.eclipse.app4mc.amalthea.model.FrequencyDomain
import org.eclipse.app4mc.amalthea.model.HwConnection
import org.eclipse.app4mc.amalthea.model.Memory
import org.eclipse.app4mc.amalthea.model.MemoryDefinition
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.validations.sim.SimHardwareProfile
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwAccessElement
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwConnection
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwMemoryDefinition
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwProcessingUnit
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertEquals

class SimHardwareModelTests {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension HardwareBuilder b4 = new HardwareBuilder
	
	val executor = new ValidationExecutor(SimHardwareProfile)
	
	
	@Test
	def void test_SimHardwareProcessingUnitDefinition() {
		val model = amalthea [
			hardwareModel [
				definition_ProcessingUnit [ name = "TestCoreDef" ]
				structure [
					name = "System"
					module_ProcessingUnit [
						name = "TestCore1"
						definition = _find(ProcessingUnitDefinition, "TestCoreDef")
					]
					module_ProcessingUnit [
						name = "TestCore2"
					]
					module_ProcessingUnit [
						name = "TestCore3"
					]
				]
			]
		]

		executor.validate(model)

		val messages = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertEquals(2, messages.stream().filter(i | i.contains(SimHwProcessingUnit.MISSING_DEFINITION)).count())
	}
	
	@Test
	def void test_SimHardwareModuleFrequencyDomain() {
		val model = amalthea [
			hardwareModel [
				domain_Frequency [ name = "TestFreqDomain" ]
				structure [
					name = "System"
					module_Memory[
						name="TestMemory"
						frequencyDomain = _find(FrequencyDomain, "TestFreqDomain")
					]
					module_ProcessingUnit [
						name = "TestCore"
						frequencyDomain = _find(FrequencyDomain, "TestFreqDomain")
					]
					module_ConnectionHandler [
						name = "TestCon"
						frequencyDomain = _find(FrequencyDomain, "TestFreqDomain")
					]
				]
			]
		]

		executor.validate(model)

		// all frequencyDomains are set
		var messages = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains("missing its 'Frequency Domain' reference")).count())
		
		// remove frequencyDomains
		AmaltheaIndex.buildIndex(model)
		val frequencyDomains = AmaltheaIndex.getElements(model, Pattern.compile(".*"), FrequencyDomain)
		AmaltheaIndex.deleteAll(frequencyDomains)
	
		executor.validate(model)
		messages = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertEquals(3, messages.stream().filter(i | i.contains("missing its 'Frequency Domain' reference")).count())
	}
	
	
	@Test
	def void test_SimMemoryDefinitionAccessLatencyDataRate() {
		val model = amalthea [
			hardwareModel [
				definition_Memory [ 
					name = "TestMemDef"
				]
				structure [
					name = "System"
					module_Memory[
						name="TestMemory"
						definition = _find(MemoryDefinition, "TestMemDef")
					]
				]
			]
		]

		// neither access latency nor data rate are set
		executor.validate(model)
		var messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwMemoryDefinition.ACCESS_MSG)).count())
		
		AmaltheaIndex.buildIndex(model)
		val memoryDef = AmaltheaIndex.getElements(model, Pattern.compile(".*"), MemoryDefinition).iterator.next
		
		// set access latency, no data rate is set
		memoryDef.accessLatency = FactoryUtil.createDiscreteValueConstant(10)
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwMemoryDefinition.ACCESS_MSG)).count())
		
		
		// set data rate, remove access latency
		memoryDef.accessLatency = null
		memoryDef.dataRate = FactoryUtil.createDataRate()
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwMemoryDefinition.ACCESS_MSG)).count())
	}
	
	@Test
	def void test_SimHwAccessElemetLatencyDataRate() {
		val model = amalthea [
			hardwareModel [
				structure [
					name = "System"
					module_Memory[
						name="TestMemory"
					]
					module_ProcessingUnit [
						name = "TestCore"
						access[
							name = "TestAccessElement"
							destination = _find(Memory, "TestMemory")
						]
					]
				]
			]
		]

		// HwAccessPath not set and neither read/write latency nor data rate are set
		executor.validate(model)
		var messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwAccessElement.READ_ACCESS_MSG)).count())
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwAccessElement.WRITE_ACCESS_MSG)).count())
		
		AmaltheaIndex.buildIndex(model)
		val accessElem = AmaltheaIndex.getElements(model, Pattern.compile(".*"), ProcessingUnit).iterator.next.accessElements.iterator.next
		
		
		// set HwAccessPath
		accessElem.accessPath = AmaltheaFactory.eINSTANCE.createHwAccessPath();
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.WRITE_ACCESS_MSG)).count())
		accessElem.accessPath = null;
				
		
		// set read/write latency
		accessElem.readLatency = FactoryUtil.createDiscreteValueConstant(10)
		accessElem.writeLatency = FactoryUtil.createDiscreteValueConstant(10)
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.WRITE_ACCESS_MSG)).count())
		
		// remove read/write latency, set data rate
		accessElem.readLatency = null
		accessElem.writeLatency = null
		accessElem.dataRate = FactoryUtil.createDataRate()
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.WRITE_ACCESS_MSG)).count())
		
		// remove data rate, set read latency
		accessElem.readLatency = FactoryUtil.createDiscreteValueConstant(10)
		accessElem.dataRate = null
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwAccessElement.READ_ACCESS_MSG)).count())
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwAccessElement.WRITE_ACCESS_MSG)).count())
		
	}
	
	@Test
	def void test_SimHwConnectionLatencyDataRate() {
		val model = amalthea [
			hardwareModel [
				structure [
					name = "System"
					connection[
						name="TestConn"
						readLatency = FactoryUtil.createDiscreteValueConstant(10)
						writeLatency = FactoryUtil.createDiscreteValueConstant(10)
						dataRate = FactoryUtil.createDataRate()
					]
				]
			]
		]

		//  read/write latency and data rate are set
		executor.validate(model)
		var messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.WRITE_ACCESS_MSG)).count())
		
		AmaltheaIndex.buildIndex(model)
		val conn = AmaltheaIndex.getElements(model, Pattern.compile(".*"), HwConnection).iterator.next
		
		// remove read latency
		conn.readLatency = null
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.WRITE_ACCESS_MSG)).count())
		
		// remove data rate
		conn.dataRate = null
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwConnection.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.WRITE_ACCESS_MSG)).count())
		
		// remove write latency
		conn.writeLatency = null
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwConnection.READ_ACCESS_MSG)).count())
		assertEquals(1, messages.stream().filter(i | i.contains(SimHwConnection.WRITE_ACCESS_MSG)).count())
		
		// set data rate
		conn.dataRate = FactoryUtil.createDataRate()
		executor.validate(model)
		messages = executor.results.stream.map[it.message].collect(Collectors.toList)
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.READ_ACCESS_MSG)).count())
		assertEquals(0, messages.stream().filter(i | i.contains(SimHwConnection.WRITE_ACCESS_MSG)).count())
	}

}
