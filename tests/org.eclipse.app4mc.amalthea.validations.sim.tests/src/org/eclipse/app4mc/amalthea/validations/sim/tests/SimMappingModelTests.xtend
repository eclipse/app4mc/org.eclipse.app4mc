/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.tests

import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.ISR
import org.eclipse.app4mc.amalthea.model.InterruptController
import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum
import org.eclipse.app4mc.amalthea.model.Memory
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.validations.sim.SimMappingProfile
import org.eclipse.app4mc.amalthea.validations.sim.mapping.SimMappingSchedulerAllocation_ExecutingPuSet
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertTrue

class SimMappingModelTests {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder
	extension OperatingSystemBuilder b3 = new OperatingSystemBuilder
	extension HardwareBuilder b4 = new HardwareBuilder
	extension MappingBuilder b5 = new MappingBuilder
	extension StimuliBuilder b6 = new StimuliBuilder
	
	
	val executor = new ValidationExecutor(SimMappingProfile)
	
	private def createValidTestModel(){
		val model =
		amalthea [
			stimuliModel[
				periodicStimulus[
					name="periodicStimulus"
					offset= FactoryUtil.createTime("100 ms")
					recurrence = FactoryUtil.createTime("10 ms")
				]
			]
			softwareModel [
				task [ 
					name = "Task_valid" 

					activityGraph [
						runnableCall[runnable = _find(Runnable, "R_1")]
						labelAccess[ 
							data = _find(Label, "label")
							access = LabelAccessEnum.WRITE
						]
					]
					stimuli += _find(PeriodicStimulus, "periodicStimulus")
				]			
				isr [ 
					name = "ISR_valid" 
					activityGraph[
						runnableCall[runnable = _find(Runnable, "R_1")]
						labelAccess[ 
							data = _find(Label, "label")
							access = LabelAccessEnum.WRITE
						]
					]
					stimuli += _find(PeriodicStimulus, "periodicStimulus")
				]
				
			]
			hardwareModel [
				definition_ProcessingUnit [ name = "CoreDef" ]
				structure [
					name = "System"
					module_Memory[
						name="Memory"
					]
					module_ProcessingUnit [
						name = "Core"
						definition = _find(ProcessingUnitDefinition, "CoreDef")
						access[
							destination = _find(Memory, "Memory")
						]
					]
					
				]
			]
			osModel [
				operatingSystem [ name = "OS"
					taskScheduler [ name = "Scheduler" ]
					interruptController [name = "InterruptController"]
				] 
			]
			mappingModel [
				taskAllocation [
					task = _find(Task, "Task_valid")
					scheduler = _find(TaskScheduler, "Scheduler")
				]
				isrAllocation [
					isr = _find(ISR, "ISR_valid")
					controller = _find(InterruptController, "InterruptController")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "Scheduler")
					executingPU = _find(ProcessingUnit, "Core")
					responsibility += _find(ProcessingUnit, "Core")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "InterruptController")
					executingPU = _find(ProcessingUnit, "Core")
					responsibility += _find(ProcessingUnit, "Core")
				]
			]
			
		];
		return model;
	}
	

	@Test
	def void test_SimMappingSchedulerAllocation_ExecutingPuSet() {	
		val model = createValidTestModel();
		
		//remove executing PU
		model.mappingModel.schedulerAllocation.forEach[
			it.executingPU = null;
		]
		
		executor.validate(model)
		executor.results
		
		val warnings = executor.results.stream.filter[it.severityLevel == Severity.WARNING].map[it.message].collect(Collectors.toList)
		assertTrue(warnings.size == 2)
		// from missing scheduler allocation
		assertTrue(warnings.contains(SimMappingSchedulerAllocation_ExecutingPuSet.MESSAGE_EXECUTING_PU_NOT_SET)); 		
	}
	
}
	
