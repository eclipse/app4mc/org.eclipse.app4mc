/**
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.sim.tests;

import com.google.common.base.Objects;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.ConnectionHandler;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryDefinition;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.validations.sim.SimHardwareProfile;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwAccessElement;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwConnection;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwMemoryDefinition;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwProcessingUnit;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class SimHardwareModelTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private HardwareBuilder b4 = new HardwareBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(SimHardwareProfile.class);

  @Test
  public void test_SimHardwareProcessingUnitDefinition() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<ProcessingUnitDefinition> _function_2 = (ProcessingUnitDefinition it_2) -> {
          it_2.setName("TestCoreDef");
        };
        this.b4.definition_ProcessingUnit(it_1, _function_2);
        final Consumer<HwStructure> _function_3 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore1");
            it_3.setDefinition(this.b1.<ProcessingUnitDefinition>_find(it_3, ProcessingUnitDefinition.class, "TestCoreDef"));
          };
          this.b4.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore2");
          };
          this.b4.module_ProcessingUnit(it_2, _function_5);
          final Consumer<ProcessingUnit> _function_6 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore3");
          };
          this.b4.module_ProcessingUnit(it_2, _function_6);
        };
        this.b4.structure(it_1, _function_3);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    this.executor.validate(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> messages = this.executor.getResults().stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    final Predicate<String> _function_3 = (String i) -> {
      return i.contains(SimHwProcessingUnit.MISSING_DEFINITION);
    };
    Assert.assertEquals(2, messages.stream().filter(_function_3).count());
  }

  @Test
  public void test_SimHardwareModuleFrequencyDomain() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<FrequencyDomain> _function_2 = (FrequencyDomain it_2) -> {
          it_2.setName("TestFreqDomain");
        };
        this.b4.domain_Frequency(it_1, _function_2);
        final Consumer<HwStructure> _function_3 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<Memory> _function_4 = (Memory it_3) -> {
            it_3.setName("TestMemory");
            it_3.setFrequencyDomain(this.b1.<FrequencyDomain>_find(it_3, FrequencyDomain.class, "TestFreqDomain"));
          };
          this.b4.module_Memory(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore");
            it_3.setFrequencyDomain(this.b1.<FrequencyDomain>_find(it_3, FrequencyDomain.class, "TestFreqDomain"));
          };
          this.b4.module_ProcessingUnit(it_2, _function_5);
          final Consumer<ConnectionHandler> _function_6 = (ConnectionHandler it_3) -> {
            it_3.setName("TestCon");
            it_3.setFrequencyDomain(this.b1.<FrequencyDomain>_find(it_3, FrequencyDomain.class, "TestFreqDomain"));
          };
          this.b4.module_ConnectionHandler(it_2, _function_6);
        };
        this.b4.structure(it_1, _function_3);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    this.executor.validate(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    List<String> messages = this.executor.getResults().stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    final Predicate<String> _function_3 = (String i) -> {
      return i.contains("missing its \'Frequency Domain\' reference");
    };
    Assert.assertEquals(0, messages.stream().filter(_function_3).count());
    AmaltheaIndex.buildIndex(model);
    final Set<FrequencyDomain> frequencyDomains = AmaltheaIndex.<FrequencyDomain>getElements(model, Pattern.compile(".*"), FrequencyDomain.class);
    AmaltheaIndex.deleteAll(frequencyDomains);
    this.executor.validate(model);
    final Predicate<ValidationDiagnostic> _function_4 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_5 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().filter(_function_4).<String>map(_function_5).collect(Collectors.<String>toList());
    final Predicate<String> _function_6 = (String i) -> {
      return i.contains("missing its \'Frequency Domain\' reference");
    };
    Assert.assertEquals(3, messages.stream().filter(_function_6).count());
  }

  @Test
  public void test_SimMemoryDefinitionAccessLatencyDataRate() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<MemoryDefinition> _function_2 = (MemoryDefinition it_2) -> {
          it_2.setName("TestMemDef");
        };
        this.b4.definition_Memory(it_1, _function_2);
        final Consumer<HwStructure> _function_3 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<Memory> _function_4 = (Memory it_3) -> {
            it_3.setName("TestMemory");
            it_3.setDefinition(this.b1.<MemoryDefinition>_find(it_3, MemoryDefinition.class, "TestMemDef"));
          };
          this.b4.module_Memory(it_2, _function_4);
        };
        this.b4.structure(it_1, _function_3);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    List<String> messages = this.executor.getResults().stream().<String>map(_function_1).collect(Collectors.<String>toList());
    final Predicate<String> _function_2 = (String i) -> {
      return i.contains(SimHwMemoryDefinition.ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_2).count());
    AmaltheaIndex.buildIndex(model);
    final MemoryDefinition memoryDef = AmaltheaIndex.<MemoryDefinition>getElements(model, Pattern.compile(".*"), MemoryDefinition.class).iterator().next();
    memoryDef.setAccessLatency(FactoryUtil.createDiscreteValueConstant(10));
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_3).collect(Collectors.<String>toList());
    final Predicate<String> _function_4 = (String i) -> {
      return i.contains(SimHwMemoryDefinition.ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_4).count());
    memoryDef.setAccessLatency(null);
    memoryDef.setDataRate(FactoryUtil.createDataRate());
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_5 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_5).collect(Collectors.<String>toList());
    final Predicate<String> _function_6 = (String i) -> {
      return i.contains(SimHwMemoryDefinition.ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_6).count());
  }

  @Test
  public void test_SimHwAccessElemetLatencyDataRate() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<Memory> _function_3 = (Memory it_3) -> {
            it_3.setName("TestMemory");
          };
          this.b4.module_Memory(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore");
            final Consumer<HwAccessElement> _function_5 = (HwAccessElement it_4) -> {
              it_4.setName("TestAccessElement");
              it_4.setDestination(this.b1.<Memory>_find(it_4, Memory.class, "TestMemory"));
            };
            this.b4.access(it_3, _function_5);
          };
          this.b4.module_ProcessingUnit(it_2, _function_4);
        };
        this.b4.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    List<String> messages = this.executor.getResults().stream().<String>map(_function_1).collect(Collectors.<String>toList());
    final Predicate<String> _function_2 = (String i) -> {
      return i.contains(SimHwAccessElement.READ_ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_2).count());
    final Predicate<String> _function_3 = (String i) -> {
      return i.contains(SimHwAccessElement.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_3).count());
    AmaltheaIndex.buildIndex(model);
    final HwAccessElement accessElem = AmaltheaIndex.<ProcessingUnit>getElements(model, Pattern.compile(".*"), ProcessingUnit.class).iterator().next().getAccessElements().iterator().next();
    accessElem.setAccessPath(AmaltheaFactory.eINSTANCE.createHwAccessPath());
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_4 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_4).collect(Collectors.<String>toList());
    final Predicate<String> _function_5 = (String i) -> {
      return i.contains(SimHwAccessElement.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_5).count());
    final Predicate<String> _function_6 = (String i) -> {
      return i.contains(SimHwAccessElement.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_6).count());
    accessElem.setAccessPath(null);
    accessElem.setReadLatency(FactoryUtil.createDiscreteValueConstant(10));
    accessElem.setWriteLatency(FactoryUtil.createDiscreteValueConstant(10));
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_7 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_7).collect(Collectors.<String>toList());
    final Predicate<String> _function_8 = (String i) -> {
      return i.contains(SimHwAccessElement.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_8).count());
    final Predicate<String> _function_9 = (String i) -> {
      return i.contains(SimHwAccessElement.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_9).count());
    accessElem.setReadLatency(null);
    accessElem.setWriteLatency(null);
    accessElem.setDataRate(FactoryUtil.createDataRate());
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_10 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_10).collect(Collectors.<String>toList());
    final Predicate<String> _function_11 = (String i) -> {
      return i.contains(SimHwAccessElement.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_11).count());
    final Predicate<String> _function_12 = (String i) -> {
      return i.contains(SimHwAccessElement.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_12).count());
    accessElem.setReadLatency(FactoryUtil.createDiscreteValueConstant(10));
    accessElem.setDataRate(null);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_13 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_13).collect(Collectors.<String>toList());
    final Predicate<String> _function_14 = (String i) -> {
      return i.contains(SimHwAccessElement.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_14).count());
    final Predicate<String> _function_15 = (String i) -> {
      return i.contains(SimHwAccessElement.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_15).count());
  }

  @Test
  public void test_SimHwConnectionLatencyDataRate() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<HwConnection> _function_3 = (HwConnection it_3) -> {
            it_3.setName("TestConn");
            it_3.setReadLatency(FactoryUtil.createDiscreteValueConstant(10));
            it_3.setWriteLatency(FactoryUtil.createDiscreteValueConstant(10));
            it_3.setDataRate(FactoryUtil.createDataRate());
          };
          this.b4.connection(it_2, _function_3);
        };
        this.b4.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    List<String> messages = this.executor.getResults().stream().<String>map(_function_1).collect(Collectors.<String>toList());
    final Predicate<String> _function_2 = (String i) -> {
      return i.contains(SimHwConnection.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_2).count());
    final Predicate<String> _function_3 = (String i) -> {
      return i.contains(SimHwConnection.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_3).count());
    AmaltheaIndex.buildIndex(model);
    final HwConnection conn = AmaltheaIndex.<HwConnection>getElements(model, Pattern.compile(".*"), HwConnection.class).iterator().next();
    conn.setReadLatency(null);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_4 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_4).collect(Collectors.<String>toList());
    final Predicate<String> _function_5 = (String i) -> {
      return i.contains(SimHwConnection.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_5).count());
    final Predicate<String> _function_6 = (String i) -> {
      return i.contains(SimHwConnection.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_6).count());
    conn.setDataRate(null);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_7 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_7).collect(Collectors.<String>toList());
    final Predicate<String> _function_8 = (String i) -> {
      return i.contains(SimHwConnection.READ_ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_8).count());
    final Predicate<String> _function_9 = (String i) -> {
      return i.contains(SimHwConnection.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_9).count());
    conn.setWriteLatency(null);
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_10 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_10).collect(Collectors.<String>toList());
    final Predicate<String> _function_11 = (String i) -> {
      return i.contains(SimHwConnection.READ_ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_11).count());
    final Predicate<String> _function_12 = (String i) -> {
      return i.contains(SimHwConnection.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(1, messages.stream().filter(_function_12).count());
    conn.setDataRate(FactoryUtil.createDataRate());
    this.executor.validate(model);
    final Function<ValidationDiagnostic, String> _function_13 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    messages = this.executor.getResults().stream().<String>map(_function_13).collect(Collectors.<String>toList());
    final Predicate<String> _function_14 = (String i) -> {
      return i.contains(SimHwConnection.READ_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_14).count());
    final Predicate<String> _function_15 = (String i) -> {
      return i.contains(SimHwConnection.WRITE_ACCESS_MSG);
    };
    Assert.assertEquals(0, messages.stream().filter(_function_15).count());
  }
}
