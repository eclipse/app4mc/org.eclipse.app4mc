/**
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.sim.tests;

import com.google.common.base.Objects;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.validations.sim.SimMappingProfile;
import org.eclipse.app4mc.amalthea.validations.sim.mapping.SimMappingSchedulerAllocation_ExecutingPuSet;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class SimMappingModelTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b3 = new OperatingSystemBuilder();

  @Extension
  private HardwareBuilder b4 = new HardwareBuilder();

  @Extension
  private MappingBuilder b5 = new MappingBuilder();

  @Extension
  private StimuliBuilder b6 = new StimuliBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(SimMappingProfile.class);

  private Amalthea createValidTestModel() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<StimuliModel> _function_1 = (StimuliModel it_1) -> {
        final Consumer<PeriodicStimulus> _function_2 = (PeriodicStimulus it_2) -> {
          it_2.setName("periodicStimulus");
          it_2.setOffset(FactoryUtil.createTime("100 ms"));
          it_2.setRecurrence(FactoryUtil.createTime("10 ms"));
        };
        this.b6.periodicStimulus(it_1, _function_2);
      };
      this.b1.stimuliModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Task> _function_3 = (Task it_2) -> {
          it_2.setName("Task_valid");
          final Consumer<ActivityGraph> _function_4 = (ActivityGraph it_3) -> {
            final Consumer<RunnableCall> _function_5 = (RunnableCall it_4) -> {
              it_4.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "R_1"));
            };
            this.b2.runnableCall(it_3, _function_5);
            final Consumer<LabelAccess> _function_6 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "label"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_6);
          };
          this.b2.activityGraph(it_2, _function_4);
          EList<Stimulus> _stimuli = it_2.getStimuli();
          PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it_2, PeriodicStimulus.class, "periodicStimulus");
          _stimuli.add(__find);
        };
        this.b2.task(it_1, _function_3);
        final Consumer<ISR> _function_4 = (ISR it_2) -> {
          it_2.setName("ISR_valid");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<RunnableCall> _function_6 = (RunnableCall it_4) -> {
              it_4.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "R_1"));
            };
            this.b2.runnableCall(it_3, _function_6);
            final Consumer<LabelAccess> _function_7 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "label"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_7);
          };
          this.b2.activityGraph(it_2, _function_5);
          EList<Stimulus> _stimuli = it_2.getStimuli();
          PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it_2, PeriodicStimulus.class, "periodicStimulus");
          _stimuli.add(__find);
        };
        this.b2.isr(it_1, _function_4);
      };
      this.b1.softwareModel(it, _function_2);
      final Consumer<HWModel> _function_3 = (HWModel it_1) -> {
        final Consumer<ProcessingUnitDefinition> _function_4 = (ProcessingUnitDefinition it_2) -> {
          it_2.setName("CoreDef");
        };
        this.b4.definition_ProcessingUnit(it_1, _function_4);
        final Consumer<HwStructure> _function_5 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<Memory> _function_6 = (Memory it_3) -> {
            it_3.setName("Memory");
          };
          this.b4.module_Memory(it_2, _function_6);
          final Consumer<ProcessingUnit> _function_7 = (ProcessingUnit it_3) -> {
            it_3.setName("Core");
            it_3.setDefinition(this.b1.<ProcessingUnitDefinition>_find(it_3, ProcessingUnitDefinition.class, "CoreDef"));
            final Consumer<HwAccessElement> _function_8 = (HwAccessElement it_4) -> {
              it_4.setDestination(this.b1.<Memory>_find(it_4, Memory.class, "Memory"));
            };
            this.b4.access(it_3, _function_8);
          };
          this.b4.module_ProcessingUnit(it_2, _function_7);
        };
        this.b4.structure(it_1, _function_5);
      };
      this.b1.hardwareModel(it, _function_3);
      final Consumer<OSModel> _function_4 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_5 = (OperatingSystem it_2) -> {
          it_2.setName("OS");
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("Scheduler");
          };
          this.b3.taskScheduler(it_2, _function_6);
          final Consumer<InterruptController> _function_7 = (InterruptController it_3) -> {
            it_3.setName("InterruptController");
          };
          this.b3.interruptController(it_2, _function_7);
        };
        this.b3.operatingSystem(it_1, _function_5);
      };
      this.b1.osModel(it, _function_4);
      final Consumer<MappingModel> _function_5 = (MappingModel it_1) -> {
        final Consumer<TaskAllocation> _function_6 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "Task_valid"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler"));
        };
        this.b5.taskAllocation(it_1, _function_6);
        final Consumer<ISRAllocation> _function_7 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "ISR_valid"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "InterruptController"));
        };
        this.b5.isrAllocation(it_1, _function_7);
        final Consumer<SchedulerAllocation> _function_8 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "Scheduler"));
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "Core"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "Core");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_8);
        final Consumer<SchedulerAllocation> _function_9 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "InterruptController"));
          it_2.setExecutingPU(this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "Core"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "Core");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_9);
      };
      this.b1.mappingModel(it, _function_5);
    };
    final Amalthea model = this.b1.amalthea(_function);
    return model;
  }

  @Test
  public void test_SimMappingSchedulerAllocation_ExecutingPuSet() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<SchedulerAllocation> _function = (SchedulerAllocation it) -> {
      it.setExecutingPU(null);
    };
    model.getMappingModel().getSchedulerAllocation().forEach(_function);
    this.executor.validate(model);
    this.executor.getResults();
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.WARNING);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = this.executor.getResults().stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    int _size = warnings.size();
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    Assert.assertTrue(warnings.contains(SimMappingSchedulerAllocation_ExecutingPuSet.MESSAGE_EXECUTING_PU_NOT_SET));
  }
}
