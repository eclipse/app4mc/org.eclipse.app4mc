/**
 * *******************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.tests

import java.util.List
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.EnumMode
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer
import org.eclipse.app4mc.amalthea.model.ModeLabel
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum
import org.eclipse.app4mc.amalthea.model.ModeValueCondition
import org.eclipse.app4mc.amalthea.model.RelationalOperator
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess
import org.eclipse.app4mc.amalthea.model.SemaphoreType
import org.eclipse.app4mc.amalthea.model.SwitchEntry
import org.eclipse.app4mc.amalthea.model.WaitingBehaviour
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.validations.standard.EMFProfile
import org.eclipse.app4mc.amalthea.validations.standard.SoftwareProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1
import org.junit.Test

import static org.junit.Assert.*

class SoftwareModelTests {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder
    extension OperatingSystemBuilder b3 = new OperatingSystemBuilder
	val executor = new ValidationExecutor(#[EMFProfile, SoftwareProfile])

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}
	
	def void condition(SwitchEntry container, Procedure1<ModeValueCondition> initializer) {
		if (container.condition === null) {
			container.condition = AmaltheaFactory.eINSTANCE.createConditionDisjunction
		}
		val obj = AmaltheaFactory.eINSTANCE.createModeValueCondition
		container.condition.entries += obj
		initializer.apply(obj)
	}
	
	@Test
	def void testModeLabel() {
		val model = amalthea [
			softwareModel [
				mode_Enum[
					name = "enumerated"
					literal [name = "first"]
					literal [name = "second"]
				]
				modeLabel [name = "ml_ok" mode = _find(EnumMode, "enumerated") initialValue = "first"]
				modeLabel [name = "ml_notOk" mode = _find(EnumMode, "enumerated") initialValue = "third"]
				modeLabel [name = "ml_okok" mode = _find(EnumMode, "enumerated")]
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(result.contains("The initialValue 'third' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_notOk\" )"))
		assertFalse(result.contains("The initialValue 'first' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_ok\" )"))
		assertFalse(result.contains("The initialValue '' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_okok\" )"))
		assertFalse(result.contains("The initialValue 'null' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_okok\" )"))
	}
	
	@Test
	def void testModeValue() {
		val model = amalthea [
			softwareModel [
				mode_Enum[
					name = "enumerated"
					literal [name = "first"]
					literal [name = "second"]
				]
				modeLabel [name = "ml" mode = _find(EnumMode, "enumerated") initialValue = "first"]
				runnable [
					name = "r1"
					activityGraph [conditionalSwitch [entry [name = "r1_mse_ok" condition [label = _find(ModeLabel, "ml") value = "first" relation = RelationalOperator.EQUAL]]]]]
				runnable [
					name = "r2"
					activityGraph [conditionalSwitch [entry [name = "r2_mse_notOk" condition [label = _find(ModeLabel, "ml") value = "third" relation = RelationalOperator.EQUAL]]]]
				]
				task [
					name = "t1"
					activityGraph [conditionalSwitch [entry [name = "t1_mse_ok" condition [label = _find(ModeLabel, "ml") value = "second" relation = RelationalOperator.EQUAL]]]]
				]
				task [
					name = "t2"
					activityGraph [conditionalSwitch [entry [name = "t2_mse_notOk" condition [label = _find(ModeLabel, "ml") value = "fourth" relation = RelationalOperator.EQUAL]]]]
				]
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(result.contains("The value 'third' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r2\" )"))
		assertFalse(result.contains("The value 'first' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r1\" )"))
		assertTrue(result.contains("The value 'fourth' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t2\" )"))
		assertFalse(result.contains("The value 'second' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t1\" )"))
	}
	
	@Test
	def void testModeLabelAccess() {
		val model = amalthea [
			softwareModel [
				mode_Enum[
					name = "enumerated"
					literal [name = "first"]
					literal [name = "second"]
				]
				modeLabel [name = "ml" mode = _find(EnumMode, "enumerated") initialValue = "first"]
				runnable [
					name = "r_ok"
					activityGraph [modeLabelAccess [access = ModeLabelAccessEnum.SET
						data = _find(ModeLabel, "ml")
						value = "first"
					]]
				]
				runnable [
					name = "r_notOk"
					activityGraph [modeLabelAccess [access = ModeLabelAccessEnum.SET
						data = _find(ModeLabel, "ml")
						value = "third"
					]]
				]
				task [
					name = "t_ok"
					activityGraph [modeLabelAccess [access = ModeLabelAccessEnum.SET
						data = _find(ModeLabel, "ml")
						value = "second"
					]]
				]
				task [
					name = "t_notOk"
					activityGraph [modeLabelAccess [access = ModeLabelAccessEnum.SET
						data = _find(ModeLabel, "ml")
						value = "fourth"
					]]
				]
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(result.contains("The value 'third' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r_notOk\" )"))
		assertFalse(result.contains("The value 'first' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r_ok\" )"))
		assertTrue(result.contains("The value 'fourth' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t_notOk\" )"))
		assertFalse(result.contains("The value 'second' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t_ok\" )"))
	}
	
	@Test
	def void testGroup() {
		val model = amalthea [
			softwareModel [
				runnable [
					name = "r_ok"
					activityGraph [
						group [
							name = "g_ok"
							group [
								name = "g_nested_ok"
							]
						]
					]
				]
				runnable [
					name = "r_notOk"
					activityGraph [
						group [
							name = "g_notOk"
							interruptible = false;
							group [
								name = "g_illegal_nested"
							]
						]
					]
				]
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(result.contains("Group: uninterruptible groups must not contain nested groups (in group \"g_notOk\")"))
		assertFalse(result.contains("Group: uninterruptible groups must not contain nested groups (in group \"g_ok\")"))
	}
	
	@Test
	def void testSemaphoreAccess() {
        val model = amalthea [
            osModel [
                semaphore [
                    name = "semCountingSemaphore"
                    semaphoreType = SemaphoreType.COUNTING_SEMAPHORE
                    initialValue = 0
                    maxValue =  3
                ]
                semaphore [
                    name = "semResource"
                    semaphoreType = SemaphoreType.RESOURCE
                    initialValue = 0
                    maxValue =  3
                ]
                semaphore [
                    name = "semSpinlock"
                    semaphoreType = SemaphoreType.SPINLOCK
                    initialValue = 0
                    maxValue =  1
                ]
                semaphore [
                    name = "semMutex"
                    semaphoreType = SemaphoreType.MUTEX
                    initialValue = 0
                    maxValue =  1
                ]
            ]
            softwareModel [
                runnable [
                    name = "r1"
                    activityGraph [
                        val ag = it as IActivityGraphItemContainer
                        (eContainer.eContainer.eContainer as Amalthea).osModel.semaphores.forEach[
                            val sem = it
                            ag.semaphoreRequest [
                                semaphore = sem
                                waitingBehaviour = WaitingBehaviour.ACTIVE
                            ]
                        ]
                    ]
                ]
            ]
        ]
        
        val noIssues = runExecutor(model).filter[it.validationID == "AM-SW-Semaphore-Access"].toList
        assertTrue(noIssues.empty)
        
        model.swModel.runnables.head.activityGraph.items.filter(SemaphoreAccess)
                .forEach[waitingBehaviour = WaitingBehaviour.PASSIVE]
        
        val issues = runExecutor(model).filter[it.validationID == "AM-SW-Semaphore-Access"].toList
        assertTrue(issues.size == 1)
        assertTrue((issues.head.targetObject as SemaphoreAccess).semaphore.semaphoreType == SemaphoreType.SPINLOCK)
	}

}
