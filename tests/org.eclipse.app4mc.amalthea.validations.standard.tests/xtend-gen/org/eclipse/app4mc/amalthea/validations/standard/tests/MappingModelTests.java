/**
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.standard.tests;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.util.HardwareUtil;
import org.eclipse.app4mc.amalthea.validations.standard.EMFProfile;
import org.eclipse.app4mc.amalthea.validations.standard.MappingProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class MappingModelTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b3 = new OperatingSystemBuilder();

  @Extension
  private HardwareBuilder b4 = new HardwareBuilder();

  @Extension
  private MappingBuilder b5 = new MappingBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(MappingProfile.class, EMFProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  public Amalthea createValidTestModel() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<Task> _function_2 = (Task it_2) -> {
          it_2.setName("TestTask");
        };
        this.b2.task(it_1, _function_2);
      };
      this.b1.softwareModel(it, _function_1);
      final Consumer<HWModel> _function_2 = (HWModel it_1) -> {
        final Consumer<ProcessingUnitDefinition> _function_3 = (ProcessingUnitDefinition it_2) -> {
          it_2.setName("TestCoreDef");
        };
        this.b4.definition_ProcessingUnit(it_1, _function_3);
        final Consumer<HwStructure> _function_4 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore");
            it_3.setDefinition(this.b1.<ProcessingUnitDefinition>_find(it_3, ProcessingUnitDefinition.class, "TestCoreDef"));
          };
          this.b4.module_ProcessingUnit(it_2, _function_5);
        };
        this.b4.structure(it_1, _function_4);
      };
      this.b1.hardwareModel(it, _function_2);
      final Consumer<OSModel> _function_3 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_4 = (OperatingSystem it_2) -> {
          it_2.setName("TestOS");
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("TestScheduler");
          };
          this.b3.taskScheduler(it_2, _function_5);
        };
        this.b3.operatingSystem(it_1, _function_4);
      };
      this.b1.osModel(it, _function_3);
      final Consumer<MappingModel> _function_4 = (MappingModel it_1) -> {
        final Consumer<TaskAllocation> _function_5 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "TestTask"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "TestScheduler"));
        };
        this.b5.taskAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "TestScheduler"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "TestCore");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_6);
      };
      this.b1.mappingModel(it, _function_4);
    };
    return this.b1.amalthea(_function);
  }

  @Test
  public void testTaskToSchedulerToCoreMapping() {
    final Amalthea model = this.createValidTestModel();
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    Assert.assertTrue(result.isEmpty());
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_UnmappedTasks() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<Task> _function = (Task it) -> {
      it.setName("TestTask_left");
    };
    this.b2.task(model.getSwModel(), _function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.WARNING));
    };
    final Function1<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function_1), _function_2));
    Assert.assertTrue(result.contains("Unmapped task found: \"TestTask_left\""));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_UnmappedScheduler() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<TaskScheduler> _function = (TaskScheduler it) -> {
      it.setName("TestScheduler_left");
    };
    this.b3.taskScheduler(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()), _function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.WARNING));
    };
    final Function1<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function_1), _function_2));
    Assert.assertTrue(result.contains("Scheduler not responsible for any core: \"TestScheduler_left\""));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_MissingSchedulerInTaskAlloc() {
    final Amalthea model = this.createValidTestModel();
    TaskAllocation _head = IterableExtensions.<TaskAllocation>head(model.getMappingModel().getTaskAllocation());
    _head.setScheduler(null);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    Assert.assertTrue(result.contains("The required feature \'scheduler\' of \'TaskAllocation\' must be set"));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_MissingTaskInTaskAlloc() {
    final Amalthea model = this.createValidTestModel();
    TaskAllocation _head = IterableExtensions.<TaskAllocation>head(model.getMappingModel().getTaskAllocation());
    _head.setTask(null);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.WARNING));
    };
    final Function1<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function_2), _function_3));
    Assert.assertTrue(errors.contains("The required feature \'task\' of \'TaskAllocation\' must be set"));
    Assert.assertTrue(warnings.contains("Unmapped task found: \"TestTask\""));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_MissingTaskAndSchedulerInTaskAlloc() {
    final Amalthea model = this.createValidTestModel();
    TaskAllocation _head = IterableExtensions.<TaskAllocation>head(model.getMappingModel().getTaskAllocation());
    _head.setScheduler(null);
    TaskAllocation _head_1 = IterableExtensions.<TaskAllocation>head(model.getMappingModel().getTaskAllocation());
    _head_1.setTask(null);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.WARNING));
    };
    final Function1<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function_2), _function_3));
    Assert.assertTrue(errors.contains("The required feature \'scheduler\' of \'TaskAllocation\' must be set"));
    Assert.assertTrue(errors.contains("The required feature \'task\' of \'TaskAllocation\' must be set"));
    Assert.assertTrue(warnings.contains("Unmapped task found: \"TestTask\""));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_MissingSchedulerInSchedulerAlloc() {
    final Amalthea model = this.createValidTestModel();
    SchedulerAllocation _head = IterableExtensions.<SchedulerAllocation>head(model.getMappingModel().getSchedulerAllocation());
    _head.setScheduler(null);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.WARNING));
    };
    final Function1<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function_2), _function_3));
    Assert.assertTrue(errors.contains("The required feature \'scheduler\' of \'SchedulerAllocation\' must be set"));
    Assert.assertTrue(warnings.contains("Scheduler not responsible for any core: \"TestScheduler\""));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_MissingCoreInSchedulerAlloc() {
    final Amalthea model = this.createValidTestModel();
    IterableExtensions.<SchedulerAllocation>head(model.getMappingModel().getSchedulerAllocation()).getResponsibility().clear();
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    Assert.assertTrue(result.contains("The feature \'responsibility\' of \'SchedulerAllocation\' with 0 values must have at least 1 values"));
  }

  @Test
  public void testTaskToSchedulerToCoreMapping_MissingSchedulerAndCoreInSchedulerAlloc() {
    final Amalthea model = this.createValidTestModel();
    SchedulerAllocation _head = IterableExtensions.<SchedulerAllocation>head(model.getMappingModel().getSchedulerAllocation());
    _head.setScheduler(null);
    IterableExtensions.<SchedulerAllocation>head(model.getMappingModel().getSchedulerAllocation()).getResponsibility().clear();
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.ERROR));
    };
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function), _function_1));
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Boolean.valueOf(Objects.equal(_severityLevel, Severity.WARNING));
    };
    final Function1<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(validationResult, _function_2), _function_3));
    Assert.assertTrue(errors.contains("The required feature \'scheduler\' of \'SchedulerAllocation\' must be set"));
    Assert.assertTrue(errors.contains("The feature \'responsibility\' of \'SchedulerAllocation\' with 0 values must have at least 1 values"));
    Assert.assertTrue(warnings.contains("Scheduler not responsible for any core: \"TestScheduler\""));
  }

  @Test
  public void testSchedulerAllocation_MultipleTopLevelSchedulerResponsibilities() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("core1");
          };
          this.b4.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("core2");
          };
          this.b4.module_ProcessingUnit(it_2, _function_4);
        };
        this.b4.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<OSModel> _function_2 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("TestOS");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("scheduler_ok");
          };
          this.b3.taskScheduler(it_2, _function_4);
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("child_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_6 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_6);
          };
          this.b3.taskScheduler(it_2, _function_5);
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("scheduler_notOk");
          };
          this.b3.taskScheduler(it_2, _function_6);
          final Consumer<InterruptController> _function_7 = (InterruptController it_3) -> {
            it_3.setName("ic_ok");
          };
          this.b3.interruptController(it_2, _function_7);
          final Consumer<InterruptController> _function_8 = (InterruptController it_3) -> {
            it_3.setName("ic_notOk");
          };
          this.b3.interruptController(it_2, _function_8);
        };
        this.b3.operatingSystem(it_1, _function_3);
      };
      this.b1.osModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_4 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_4);
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "child_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "scheduler_notOk"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_6);
        final Consumer<SchedulerAllocation> _function_7 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "ic_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_7);
        final Consumer<SchedulerAllocation> _function_8 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "ic_notOk"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_8);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-Mapping-Scheduler-Allocation-Top-Level-Responsibility"));
    };
    final Iterable<ValidationDiagnostic> tlRespIssues = IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1);
    int _size = IterableExtensions.size(tlRespIssues);
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      EObject _targetObject = it.getTargetObject();
      Scheduler _scheduler = ((SchedulerAllocation) _targetObject).getScheduler();
      return Boolean.valueOf((_scheduler instanceof TaskScheduler));
    };
    final ValidationDiagnostic tlSchedResp = IterableExtensions.<ValidationDiagnostic>head(IterableExtensions.<ValidationDiagnostic>filter(tlRespIssues, _function_2));
    EObject _targetObject = tlSchedResp.getTargetObject();
    final String tlSchedName = ((SchedulerAllocation) _targetObject).getScheduler().getName();
    boolean _equals_1 = Objects.equal(tlSchedName, "child_scheduler_ok");
    Assert.assertFalse(_equals_1);
    Assert.assertTrue((Objects.equal(tlSchedResp.getMessage(), "Processing Unit \"core2\" should have at most one top level scheduler that is responsible for it") && Objects.equal(tlSchedName, "scheduler_notOk")));
    final Function1<ValidationDiagnostic, Boolean> _function_3 = (ValidationDiagnostic it) -> {
      EObject _targetObject_1 = it.getTargetObject();
      Scheduler _scheduler = ((SchedulerAllocation) _targetObject_1).getScheduler();
      return Boolean.valueOf((_scheduler instanceof InterruptController));
    };
    final ValidationDiagnostic iCResp = IterableExtensions.<ValidationDiagnostic>head(IterableExtensions.<ValidationDiagnostic>filter(tlRespIssues, _function_3));
    EObject _targetObject_1 = iCResp.getTargetObject();
    final String iCName = ((SchedulerAllocation) _targetObject_1).getScheduler().getName();
    Assert.assertTrue((Objects.equal(iCResp.getMessage(), "Processing Unit \"core1\" should have at most one interrupt controller that is responsible for it") && Objects.equal(iCName, "ic_notOk")));
  }

  @Test
  public void testSchedulerAllocation_NestedResponsibilities_SimpleHierarchy() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("core1");
          };
          this.b4.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("core2");
          };
          this.b4.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("core3");
          };
          this.b4.module_ProcessingUnit(it_2, _function_5);
          final Consumer<ProcessingUnit> _function_6 = (ProcessingUnit it_3) -> {
            it_3.setName("core4");
          };
          this.b4.module_ProcessingUnit(it_2, _function_6);
        };
        this.b4.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<OSModel> _function_2 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("TestOS");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("parent_scheduler_ok");
          };
          this.b3.taskScheduler(it_2, _function_4);
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("child1_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_6 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "parent_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_6);
          };
          this.b3.taskScheduler(it_2, _function_5);
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("child2_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_7 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "parent_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_7);
          };
          this.b3.taskScheduler(it_2, _function_6);
          final Consumer<TaskScheduler> _function_7 = (TaskScheduler it_3) -> {
            it_3.setName("child3_scheduler_notOk");
            final Consumer<SchedulerAssociation> _function_8 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "parent_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_8);
          };
          this.b3.taskScheduler(it_2, _function_7);
        };
        this.b3.operatingSystem(it_1, _function_3);
      };
      this.b1.osModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_4 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "parent_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          ProcessingUnit __find_2 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1, __find_2)));
        };
        this.b5.schedulerAllocation(it_1, _function_4);
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "child1_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "child2_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_6);
        final Consumer<SchedulerAllocation> _function_7 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "child3_scheduler_notOk"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core4");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_7);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-Mapping-Scheduler-Allocation-Hierarchy"));
    };
    final Iterable<ValidationDiagnostic> hierarchyIssues = IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1);
    int _size = IterableExtensions.size(hierarchyIssues);
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    String _message = IterableExtensions.<ValidationDiagnostic>head(hierarchyIssues).getMessage();
    boolean _equals_1 = Objects.equal(_message, ("Scheduler Allocation for child Task Scheduler \"child3_scheduler_notOk\" " + "should only be responsible for a subset of processing units of its parent schedulers"));
    Assert.assertTrue(_equals_1);
  }

  @Test
  public void testSchedulerAllocation_NestedResponsibilities_DeeperHierarchy() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("core1");
          };
          this.b4.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("core2");
          };
          this.b4.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("core3");
          };
          this.b4.module_ProcessingUnit(it_2, _function_5);
          final Consumer<ProcessingUnit> _function_6 = (ProcessingUnit it_3) -> {
            it_3.setName("core4");
          };
          this.b4.module_ProcessingUnit(it_2, _function_6);
          final Consumer<ProcessingUnit> _function_7 = (ProcessingUnit it_3) -> {
            it_3.setName("core5");
          };
          this.b4.module_ProcessingUnit(it_2, _function_7);
          final Consumer<ProcessingUnit> _function_8 = (ProcessingUnit it_3) -> {
            it_3.setName("core6");
          };
          this.b4.module_ProcessingUnit(it_2, _function_8);
        };
        this.b4.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<OSModel> _function_2 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("TestOS");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("a1_scheduler_ok");
          };
          this.b3.taskScheduler(it_2, _function_4);
          final Consumer<TaskScheduler> _function_5 = (TaskScheduler it_3) -> {
            it_3.setName("b1_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_6 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "a1_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_6);
          };
          this.b3.taskScheduler(it_2, _function_5);
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("c1_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_7 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b1_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_7);
          };
          this.b3.taskScheduler(it_2, _function_6);
          final Consumer<TaskScheduler> _function_7 = (TaskScheduler it_3) -> {
            it_3.setName("d1_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_8 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "c1_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_8);
          };
          this.b3.taskScheduler(it_2, _function_7);
          final Consumer<TaskScheduler> _function_8 = (TaskScheduler it_3) -> {
            it_3.setName("d2_scheduler_notOk");
            final Consumer<SchedulerAssociation> _function_9 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "c1_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_9);
          };
          this.b3.taskScheduler(it_2, _function_8);
          final Consumer<TaskScheduler> _function_9 = (TaskScheduler it_3) -> {
            it_3.setName("c2_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_10 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b1_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_10);
          };
          this.b3.taskScheduler(it_2, _function_9);
          final Consumer<TaskScheduler> _function_10 = (TaskScheduler it_3) -> {
            it_3.setName("b2_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_11 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "a1_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_11);
          };
          this.b3.taskScheduler(it_2, _function_10);
          final Consumer<TaskScheduler> _function_11 = (TaskScheduler it_3) -> {
            it_3.setName("c3_scheduler_notOk");
            final Consumer<SchedulerAssociation> _function_12 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b2_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_12);
          };
          this.b3.taskScheduler(it_2, _function_11);
          final Consumer<TaskScheduler> _function_12 = (TaskScheduler it_3) -> {
            it_3.setName("c4_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_13 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b2_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_13);
          };
          this.b3.taskScheduler(it_2, _function_12);
          final Consumer<TaskScheduler> _function_13 = (TaskScheduler it_3) -> {
            it_3.setName("d3_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_14 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "c4_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_14);
          };
          this.b3.taskScheduler(it_2, _function_13);
          final Consumer<TaskScheduler> _function_14 = (TaskScheduler it_3) -> {
            it_3.setName("c5_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_15 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b2_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_15);
          };
          this.b3.taskScheduler(it_2, _function_14);
          final Consumer<TaskScheduler> _function_15 = (TaskScheduler it_3) -> {
            it_3.setName("a2_scheduler_ok");
          };
          this.b3.taskScheduler(it_2, _function_15);
          final Consumer<TaskScheduler> _function_16 = (TaskScheduler it_3) -> {
            it_3.setName("b3_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_17 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "a2_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_17);
          };
          this.b3.taskScheduler(it_2, _function_16);
          final Consumer<TaskScheduler> _function_17 = (TaskScheduler it_3) -> {
            it_3.setName("c6_scheduler_notOk");
            final Consumer<SchedulerAssociation> _function_18 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b3_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_18);
          };
          this.b3.taskScheduler(it_2, _function_17);
          final Consumer<TaskScheduler> _function_18 = (TaskScheduler it_3) -> {
            it_3.setName("c7_scheduler_ok");
            final Consumer<SchedulerAssociation> _function_19 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "b3_scheduler_ok"));
            };
            this.b3.parentAssociation(it_3, _function_19);
          };
          this.b3.taskScheduler(it_2, _function_18);
        };
        this.b3.operatingSystem(it_1, _function_3);
      };
      this.b1.osModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_4 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "a1_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          List<ProcessingUnit> _modulesFromHWStructure = HardwareUtil.<ProcessingUnit>getModulesFromHWStructure(ProcessingUnit.class, this.b1.<HwStructure>_find(it_2, HwStructure.class, "System"));
          Iterables.<ProcessingUnit>addAll(_responsibility, _modulesFromHWStructure);
          EList<ProcessingUnit> _responsibility_1 = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core6");
          _responsibility_1.remove(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_4);
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "b1_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "c2_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find)));
        };
        this.b5.schedulerAllocation(it_1, _function_6);
        final Consumer<SchedulerAllocation> _function_7 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "c3_scheduler_notOk"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core4");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core6");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_7);
        final Consumer<SchedulerAllocation> _function_8 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "c5_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core4");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core5");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_8);
        final Consumer<SchedulerAllocation> _function_9 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "d1_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_9);
        final Consumer<SchedulerAllocation> _function_10 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "d2_scheduler_notOk"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_10);
        final Consumer<SchedulerAllocation> _function_11 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "a2_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core6");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_11);
        final Consumer<SchedulerAllocation> _function_12 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "c6_scheduler_notOk"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core5");
          ProcessingUnit __find_1 = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core6");
          Iterables.<ProcessingUnit>addAll(_responsibility, Collections.<ProcessingUnit>unmodifiableList(CollectionLiterals.<ProcessingUnit>newArrayList(__find, __find_1)));
        };
        this.b5.schedulerAllocation(it_1, _function_12);
        final Consumer<SchedulerAllocation> _function_13 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "c7_scheduler_ok"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core6");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_13);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-Mapping-Scheduler-Allocation-Hierarchy"));
    };
    final Function1<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> hierarchyIssues = IterableExtensions.<String>toList(IterableExtensions.<ValidationDiagnostic, String>map(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1), _function_2));
    int _size = hierarchyIssues.size();
    boolean _equals = (_size == 3);
    Assert.assertTrue(_equals);
    Assert.assertTrue(
      hierarchyIssues.contains(
        ("Scheduler Allocation for child Task Scheduler \"c3_scheduler_notOk\" " + "should only be responsible for a subset of processing units of its parent schedulers")));
    Assert.assertTrue(
      hierarchyIssues.contains(
        ("Scheduler Allocation for child Task Scheduler \"d2_scheduler_notOk\" " + "should only be responsible for a subset of processing units of its parent schedulers")));
    Assert.assertTrue(
      hierarchyIssues.contains(
        ("Scheduler Allocation for child Task Scheduler \"c6_scheduler_notOk\" " + "should only be responsible for a subset of processing units of its parent schedulers")));
  }
}
