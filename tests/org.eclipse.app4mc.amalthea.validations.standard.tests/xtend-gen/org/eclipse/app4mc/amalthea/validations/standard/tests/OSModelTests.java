/**
 * Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.standard.tests;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.ListObject;
import org.eclipse.app4mc.amalthea.model.LongObject;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.ParameterType;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerAssociation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.SemaphoreType;
import org.eclipse.app4mc.amalthea.model.StructureType;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.predefined.AmaltheaTemplates;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.validations.standard.OSProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class OSModelTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b3 = new OperatingSystemBuilder();

  @Extension
  private HardwareBuilder b4 = new HardwareBuilder();

  @Extension
  private MappingBuilder b5 = new MappingBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(OSProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  public List<ValidationDiagnostic> validateParameterTypes(final Amalthea model) {
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Scheduling-Parameter-Value-Type-Matches-Defined-Type"));
    };
    return IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function));
  }

  public List<ValidationDiagnostic> validateParameterMultiplicity(final Amalthea model) {
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Scheduling-Parameter-Value-Number-Matches-Defined-Multiplicity"));
    };
    return IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function));
  }

  public List<ValidationDiagnostic> validateMandatoryParameters(final Amalthea model) {
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Mandatory-Scheduling-Parameters-Set"));
    };
    return IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function));
  }

  public List<ValidationDiagnostic> validateStandardSchedulerDefinitions(final Amalthea model) {
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Standard-Scheduler-Definition-Conformance"));
    };
    return IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function));
  }

  public List<ValidationDiagnostic> validateStandardSchedulingParameterDefinitions(final Amalthea model) {
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Standard-Scheduling-Parameter-Definition-Conformance"));
    };
    return IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function));
  }

  public List<ValidationDiagnostic> validateOverriddenSchedulingParameterEmptyValue(final Amalthea model) {
    final Function1<ValidationDiagnostic, Boolean> _function = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Scheduling-Parameter-Empty-Overriden-Value"));
    };
    return IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function));
  }

  public Amalthea createValidTestModel() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<HWModel> _function_1 = (HWModel it_1) -> {
        final Consumer<HwStructure> _function_2 = (HwStructure it_2) -> {
          it_2.setName("MC1");
          it_2.setStructureType(StructureType.MICROCONTROLLER);
          final Consumer<ProcessingUnit> _function_3 = (ProcessingUnit it_3) -> {
            it_3.setName("core1");
          };
          this.b4.module_ProcessingUnit(it_2, _function_3);
          final Consumer<ProcessingUnit> _function_4 = (ProcessingUnit it_3) -> {
            it_3.setName("core2");
          };
          this.b4.module_ProcessingUnit(it_2, _function_4);
          final Consumer<ProcessingUnit> _function_5 = (ProcessingUnit it_3) -> {
            it_3.setName("core3");
          };
          this.b4.module_ProcessingUnit(it_2, _function_5);
        };
        this.b4.structure(it_1, _function_2);
      };
      this.b1.hardwareModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Task> _function_3 = (Task it_2) -> {
          it_2.setName("t1");
        };
        this.b2.task(it_1, _function_3);
        final Consumer<Task> _function_4 = (Task it_2) -> {
          it_2.setName("t2");
        };
        this.b2.task(it_1, _function_4);
        final Consumer<Task> _function_5 = (Task it_2) -> {
          it_2.setName("t3");
        };
        this.b2.task(it_1, _function_5);
        final Consumer<ISR> _function_6 = (ISR it_2) -> {
          it_2.setName("isr1");
        };
        this.b2.isr(it_1, _function_6);
      };
      this.b1.softwareModel(it, _function_2);
      final Consumer<OSModel> _function_3 = (OSModel it_1) -> {
        final Consumer<SchedulingParameterDefinition> _function_4 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("hyperPeriod");
          it_2.setType(ParameterType.TIME);
        };
        this.b3.parameterDefinition(it_1, _function_4);
        final Consumer<SchedulingParameterDefinition> _function_5 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("timeStamps");
          it_2.setMany(true);
          it_2.setType(ParameterType.TIME);
        };
        this.b3.parameterDefinition(it_1, _function_5);
        AmaltheaTemplates.addStandardSchedulingParameterDefinition(it_1, StandardSchedulers.Parameter.PRIORITY);
        final Consumer<SchedulingParameterDefinition> _function_6 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("preemptible");
          it_2.setMandatory(false);
          it_2.setType(ParameterType.BOOL);
          it_2.setDefaultValue(FactoryUtil.createBooleanObject(false));
        };
        this.b3.parameterDefinition(it_1, _function_6);
        AmaltheaTemplates.addStandardSchedulingParameterDefinition(it_1, StandardSchedulers.Parameter.TASK_GROUP);
        AmaltheaTemplates.addStandardSchedulingParameterDefinition(it_1, StandardSchedulers.Parameter.QUANT_SIZE);
        final Consumer<SchedulingParameterDefinition> _function_7 = (SchedulingParameterDefinition it_2) -> {
          it_2.setName("interruptFlag");
          it_2.setMandatory(false);
          it_2.setType(ParameterType.INTEGER);
        };
        this.b3.parameterDefinition(it_1, _function_7);
        final Consumer<SchedulerDefinition> _function_8 = (SchedulerDefinition it_2) -> {
          it_2.setName("TDMA");
          EList<SchedulingParameterDefinition> _algorithmParameters = it_2.getAlgorithmParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "hyperPeriod");
          _algorithmParameters.add(__find);
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "timeStamps");
          _processParameters.add(__find_1);
        };
        this.b3.schedulerDefinition(it_1, _function_8);
        final Consumer<SchedulerDefinition> _function_9 = (SchedulerDefinition it_2) -> {
          it_2.setName("OSEK");
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "priority");
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "preemptible");
          SchedulingParameterDefinition __find_2 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "taskGroup");
          Iterables.<SchedulingParameterDefinition>addAll(_processParameters, Collections.<SchedulingParameterDefinition>unmodifiableList(CollectionLiterals.<SchedulingParameterDefinition>newArrayList(__find, __find_1, __find_2)));
        };
        this.b3.schedulerDefinition(it_1, _function_9);
        final Consumer<SchedulerDefinition> _function_10 = (SchedulerDefinition it_2) -> {
          it_2.setName("FixedPriorityPreemptive");
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "priority");
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "preemptible");
          Iterables.<SchedulingParameterDefinition>addAll(_processParameters, Collections.<SchedulingParameterDefinition>unmodifiableList(CollectionLiterals.<SchedulingParameterDefinition>newArrayList(__find, __find_1)));
        };
        this.b3.schedulerDefinition(it_1, _function_10);
        final Consumer<SchedulerDefinition> _function_11 = (SchedulerDefinition it_2) -> {
          it_2.setName("PriorityBased");
          EList<SchedulingParameterDefinition> _processParameters = it_2.getProcessParameters();
          SchedulingParameterDefinition __find = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "priority");
          SchedulingParameterDefinition __find_1 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "preemptible");
          SchedulingParameterDefinition __find_2 = this.b1.<SchedulingParameterDefinition>_find(it_2, SchedulingParameterDefinition.class, "interruptFlag");
          Iterables.<SchedulingParameterDefinition>addAll(_processParameters, Collections.<SchedulingParameterDefinition>unmodifiableList(CollectionLiterals.<SchedulingParameterDefinition>newArrayList(__find, __find_1, __find_2)));
        };
        this.b3.schedulerDefinition(it_1, _function_11);
        AmaltheaTemplates.addStandardSchedulerDefinition(it_1, StandardSchedulers.Algorithm.P_FAIR_PD2);
        final Consumer<OperatingSystem> _function_12 = (OperatingSystem it_2) -> {
          it_2.setName("Os1");
          final Consumer<TaskScheduler> _function_13 = (TaskScheduler it_3) -> {
            it_3.setName("sched1");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "OSEK"));
          };
          this.b3.taskScheduler(it_2, _function_13);
          final Consumer<TaskScheduler> _function_14 = (TaskScheduler it_3) -> {
            it_3.setName("sched2");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "FixedPriorityPreemptive"));
          };
          this.b3.taskScheduler(it_2, _function_14);
          final Consumer<TaskScheduler> _function_15 = (TaskScheduler it_3) -> {
            it_3.setName("sched3");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "TDMA"));
            this.b3.schedulingParameter(it_3, "hyperPeriod", FactoryUtil.createTime(10, TimeUnit.MS));
          };
          this.b3.taskScheduler(it_2, _function_15);
          final Consumer<TaskScheduler> _function_16 = (TaskScheduler it_3) -> {
            it_3.setName("nestedSched");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "FixedPriorityPreemptive"));
            final Consumer<SchedulerAssociation> _function_17 = (SchedulerAssociation it_4) -> {
              it_4.setParent(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "sched3"));
              this.b3.schedulingParameter(it_4, "timeStamps", FactoryUtil.createTime(5, TimeUnit.MS));
            };
            this.b3.parentAssociation(it_3, _function_17);
          };
          this.b3.taskScheduler(it_2, _function_16);
          final Consumer<InterruptController> _function_17 = (InterruptController it_3) -> {
            it_3.setName("ic1");
            it_3.setDefinition(this.b1.<SchedulerDefinition>_find(it_3, SchedulerDefinition.class, "PriorityBased"));
          };
          this.b3.interruptController(it_2, _function_17);
        };
        this.b3.operatingSystem(it_1, _function_12);
        final Consumer<Semaphore> _function_13 = (Semaphore it_2) -> {
          it_2.setName("semCountingSemaphore");
          it_2.setSemaphoreType(SemaphoreType.COUNTING_SEMAPHORE);
          it_2.setInitialValue(0);
          it_2.setMaxValue(3);
          it_2.setOwnership(false);
        };
        this.b3.semaphore(it_1, _function_13);
        final Consumer<Semaphore> _function_14 = (Semaphore it_2) -> {
          it_2.setName("semResource");
          it_2.setSemaphoreType(SemaphoreType.RESOURCE);
          it_2.setInitialValue(0);
          it_2.setMaxValue(3);
          it_2.setOwnership(true);
        };
        this.b3.semaphore(it_1, _function_14);
        final Consumer<Semaphore> _function_15 = (Semaphore it_2) -> {
          it_2.setName("semSpinlock");
          it_2.setSemaphoreType(SemaphoreType.SPINLOCK);
          it_2.setInitialValue(0);
          it_2.setMaxValue(1);
          it_2.setOwnership(true);
        };
        this.b3.semaphore(it_1, _function_15);
        final Consumer<Semaphore> _function_16 = (Semaphore it_2) -> {
          it_2.setName("semMutex");
          it_2.setSemaphoreType(SemaphoreType.MUTEX);
          it_2.setInitialValue(0);
          it_2.setMaxValue(1);
          it_2.setOwnership(true);
        };
        this.b3.semaphore(it_1, _function_16);
      };
      this.b1.osModel(it, _function_3);
      final Consumer<MappingModel> _function_4 = (MappingModel it_1) -> {
        final Consumer<SchedulerAllocation> _function_5 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched2"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core2");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_5);
        final Consumer<SchedulerAllocation> _function_6 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_6);
        final Consumer<SchedulerAllocation> _function_7 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched3"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core3");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_7);
        final Consumer<SchedulerAllocation> _function_8 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "ic1"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "core1");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_8);
        final Consumer<TaskAllocation> _function_9 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched1"));
          this.b5.schedulingParameter(it_2, "priority", FactoryUtil.createIntegerObject(3));
          this.b5.schedulingParameter(it_2, "taskGroup", FactoryUtil.createIntegerObject(10));
        };
        this.b5.taskAllocation(it_1, _function_9);
        final Consumer<TaskAllocation> _function_10 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t2"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched2"));
          this.b5.schedulingParameter(it_2, "priority", FactoryUtil.createIntegerObject(40));
          this.b5.schedulingParameter(it_2, "preemptible", FactoryUtil.createBooleanObject(false));
        };
        this.b5.taskAllocation(it_1, _function_10);
        final Consumer<TaskAllocation> _function_11 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "t3"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched3"));
          Time _createTime = FactoryUtil.createTime(0, TimeUnit.MS);
          Time _createTime_1 = FactoryUtil.createTime(2, TimeUnit.MS);
          this.b5.schedulingParameter(it_2, "timeStamps", FactoryUtil.createListObject(
            Collections.<Value>unmodifiableList(CollectionLiterals.<Value>newArrayList(_createTime, _createTime_1))));
        };
        this.b5.taskAllocation(it_1, _function_11);
        final Consumer<ISRAllocation> _function_12 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "isr1"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "ic1"));
        };
        this.b5.isrAllocation(it_1, _function_12);
      };
      this.b1.mappingModel(it, _function_4);
    };
    return this.b1.amalthea(_function);
  }

  @Test
  public void testValidModel() {
    final Amalthea model = this.createValidTestModel();
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Function1<ValidationDiagnostic, String> _function = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = IterableExtensions.<String>toList(ListExtensions.<ValidationDiagnostic, String>map(validationResult, _function));
    Assert.assertTrue(result.isEmpty());
  }

  @Test
  public void testAlgorithmParameterTypeValidity() {
    final Amalthea model = this.createValidTestModel();
    final Function1<TaskScheduler, Boolean> _function = (TaskScheduler it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "sched3"));
    };
    Map.Entry<SchedulingParameterDefinition, Value> _head = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskScheduler>findFirst(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()).getTaskSchedulers(), _function).getSchedulingParameters());
    _head.setValue(FactoryUtil.createLongObject(5));
    final List<ValidationDiagnostic> typeIssues = this.validateParameterTypes(model);
    int _size = typeIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _eContainingFeature = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_eContainingFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "The value of Scheduling Parameter \"hyperPeriod\" does not conform to the defined type Time");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testAllocationSingleParameterTypeValidity() {
    final Amalthea model = this.createValidTestModel();
    Map.Entry<SchedulingParameterDefinition, Value> _head = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskAllocation>last(model.getMappingModel().getTaskAllocation()).getSchedulingParameters());
    _head.setValue(FactoryUtil.createTime(0, TimeUnit.MS));
    Assert.assertTrue(this.validateParameterTypes(model).isEmpty());
    Map.Entry<SchedulingParameterDefinition, Value> _head_1 = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskAllocation>last(model.getMappingModel().getTaskAllocation()).getSchedulingParameters());
    _head_1.setValue(FactoryUtil.createLongObject(0));
    final List<ValidationDiagnostic> typeIssues = this.validateParameterTypes(model);
    int _size = typeIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _eContainingFeature = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_eContainingFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "The value of Scheduling Parameter \"timeStamps\" does not conform to the defined type Time");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testAllocationMultiParameterTypeValidity() {
    final Amalthea model = this.createValidTestModel();
    Value _value = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskAllocation>last(model.getMappingModel().getTaskAllocation()).getSchedulingParameters()).getValue();
    final ListObject sps = ((ListObject) _value);
    EList<Value> _values = sps.getValues();
    LongObject _createLongObject = FactoryUtil.createLongObject(4);
    _values.add(_createLongObject);
    final List<ValidationDiagnostic> typeIssues = this.validateParameterTypes(model);
    int _size = typeIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _eContainingFeature = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_eContainingFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "The value of Scheduling Parameter \"timeStamps\" does not conform to the defined type Time");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testAllocationMultiNestedParameterTypeValidity() {
    final Amalthea model = this.createValidTestModel();
    Value _value = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskAllocation>last(model.getMappingModel().getTaskAllocation()).getSchedulingParameters()).getValue();
    final ListObject sps = ((ListObject) _value);
    Time _createTime = FactoryUtil.createTime(3, TimeUnit.MS);
    final ListObject nested = FactoryUtil.createListObject(Collections.<Value>unmodifiableList(CollectionLiterals.<Value>newArrayList(_createTime)));
    EList<Value> _values = sps.getValues();
    _values.add(nested);
    Assert.assertTrue(this.validateParameterTypes(model).isEmpty());
    EList<Value> _values_1 = nested.getValues();
    LongObject _createLongObject = FactoryUtil.createLongObject(4);
    _values_1.add(_createLongObject);
    final List<ValidationDiagnostic> typeIssues = this.validateParameterTypes(model);
    int _size = typeIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _eContainingFeature = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_eContainingFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(typeIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "The value of Scheduling Parameter \"timeStamps\" does not conform to the defined type Time");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testSchedulingParameterMultiplicityLower() {
    final Amalthea model = this.createValidTestModel();
    final Function1<TaskScheduler, Boolean> _function = (TaskScheduler it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "sched3"));
    };
    Map.Entry<SchedulingParameterDefinition, Value> _head = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskScheduler>findFirst(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()).getTaskSchedulers(), _function).getSchedulingParameters());
    _head.setValue(null);
    List<ValidationDiagnostic> multiIssues = this.validateParameterMultiplicity(model);
    int _size = multiIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _eContainingFeature = IterableExtensions.<ValidationDiagnostic>head(multiIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_eContainingFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(multiIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("There is no value for Scheduling Parameter \"hyperPeriod\" - " + "the Scheduling Parameter Definition requires exactly one value"));
    Assert.assertTrue(_equals_2);
    final Function1<TaskScheduler, Boolean> _function_1 = (TaskScheduler it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "sched3"));
    };
    Map.Entry<SchedulingParameterDefinition, Value> _head_1 = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskScheduler>findFirst(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()).getTaskSchedulers(), _function_1).getSchedulingParameters());
    _head_1.setValue(FactoryUtil.createTime(10, TimeUnit.MS));
    Value _value = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskAllocation>last(model.getMappingModel().getTaskAllocation()).getSchedulingParameters()).getValue();
    final ListObject sps = ((ListObject) _value);
    sps.getValues().clear();
    multiIssues = this.validateParameterMultiplicity(model);
    int _size_1 = multiIssues.size();
    boolean _equals_3 = (_size_1 == 1);
    Assert.assertTrue(_equals_3);
    EStructuralFeature _eContainingFeature_1 = IterableExtensions.<ValidationDiagnostic>head(multiIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters_1 = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_4 = Objects.equal(_eContainingFeature_1, _iSchedulingParameterContainer_SchedulingParameters_1);
    Assert.assertTrue(_equals_4);
    String _message_1 = IterableExtensions.<ValidationDiagnostic>head(multiIssues).getMessage();
    boolean _equals_5 = Objects.equal(_message_1, ("There is no value for Scheduling Parameter \"timeStamps\" - " + "the Scheduling Parameter Definition requires at least one value"));
    Assert.assertTrue(_equals_5);
  }

  @Test
  public void testSchedulingParameterMultiplicityUpper() {
    final Amalthea model = this.createValidTestModel();
    final Function1<TaskScheduler, Boolean> _function = (TaskScheduler it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "sched3"));
    };
    final Map.Entry<SchedulingParameterDefinition, Value> hpVal = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>head(IterableExtensions.<TaskScheduler>findFirst(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()).getTaskSchedulers(), _function).getSchedulingParameters());
    Time _createTime = FactoryUtil.createTime(10, TimeUnit.MS);
    hpVal.setValue(FactoryUtil.createListObject(Collections.<Value>unmodifiableList(CollectionLiterals.<Value>newArrayList(_createTime))));
    Assert.assertTrue(this.validateParameterMultiplicity(model).isEmpty());
    Value _value = hpVal.getValue();
    EList<Value> _values = ((ListObject) _value).getValues();
    Time _createTime_1 = FactoryUtil.createTime(12, TimeUnit.MS);
    _values.add(_createTime_1);
    final List<ValidationDiagnostic> multiIssues = this.validateParameterMultiplicity(model);
    int _size = multiIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _eContainingFeature = IterableExtensions.<ValidationDiagnostic>head(multiIssues).getTargetObject().eContainingFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_eContainingFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(multiIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("There are multiple values for Scheduling Parameter \"hyperPeriod\" - " + "the Scheduling Parameter Definition allows not more than one value"));
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testMandatorySchedulerAlgorithmParameters() {
    final Amalthea model = this.createValidTestModel();
    final Function1<TaskScheduler, Boolean> _function = (TaskScheduler it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "sched3"));
    };
    IterableExtensions.<TaskScheduler>findFirst(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()).getTaskSchedulers(), _function).getSchedulingParameters().clear();
    final List<ValidationDiagnostic> mandatorySPIssues = this.validateMandatoryParameters(model);
    int _size = mandatorySPIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(mandatorySPIssues).getTargetFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_targetFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(mandatorySPIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "Mandatory scheduling parameter \"hyperPeriod\" is not set for Task Scheduler \"sched3\"");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testMandatoryTaskAllocationSchedulingParameters() {
    final Amalthea model = this.createValidTestModel();
    IterableExtensions.<TaskAllocation>head(model.getMappingModel().getTaskAllocation()).getSchedulingParameters().clear();
    final List<ValidationDiagnostic> mandatorySPIssues = this.validateMandatoryParameters(model);
    int _size = mandatorySPIssues.size();
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    final Predicate<ValidationDiagnostic> _function = (ValidationDiagnostic it) -> {
      EStructuralFeature _targetFeature = it.getTargetFeature();
      EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
      return Objects.equal(_targetFeature, _iSchedulingParameterContainer_SchedulingParameters);
    };
    Assert.assertTrue(mandatorySPIssues.stream().allMatch(_function));
    final Function1<ValidationDiagnostic, String> _function_1 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> messages = ListExtensions.<ValidationDiagnostic, String>map(mandatorySPIssues, _function_1);
    Assert.assertTrue(messages.contains("Mandatory scheduling parameter \"priority\" is not set for Task Allocation"));
    Assert.assertTrue(messages.contains("Mandatory scheduling parameter \"taskGroup\" is not set for Task Allocation"));
  }

  @Test
  public void testMandatorySchedulerAssociationSchedulingParameters() {
    final Amalthea model = this.createValidTestModel();
    IterableExtensions.<TaskScheduler>last(IterableExtensions.<OperatingSystem>head(model.getOsModel().getOperatingSystems()).getTaskSchedulers()).getParentAssociation().getSchedulingParameters().clear();
    final List<ValidationDiagnostic> mandatorySPIssues = this.validateMandatoryParameters(model);
    int _size = mandatorySPIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(mandatorySPIssues).getTargetFeature();
    EReference _iSchedulingParameterContainer_SchedulingParameters = AmaltheaPackage.eINSTANCE.getISchedulingParameterContainer_SchedulingParameters();
    boolean _equals_1 = Objects.equal(_targetFeature, _iSchedulingParameterContainer_SchedulingParameters);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(mandatorySPIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "Mandatory scheduling parameter \"timeStamps\" is not set for Scheduler Association");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testMandatoryISRAllocationSchedulingParameters() {
    final Amalthea model = this.createValidTestModel();
    SchedulingParameterDefinition _last = IterableExtensions.<SchedulingParameterDefinition>last(model.getOsModel().getSchedulingParameterDefinitions());
    _last.setMandatory(true);
    final List<ValidationDiagnostic> mandatorySPIssues = this.validateMandatoryParameters(model);
    int _size = mandatorySPIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(mandatorySPIssues).getTargetFeature();
    EAttribute _iSRAllocation_Priority = AmaltheaPackage.eINSTANCE.getISRAllocation_Priority();
    boolean _equals_1 = Objects.equal(_targetFeature, _iSRAllocation_Priority);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(mandatorySPIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, "Mandatory scheduling parameter \"interruptFlag\" is not set for ISR Allocation");
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testStandardSDWillBeValidated() {
    final Amalthea model = this.createValidTestModel();
    final Function1<SchedulerDefinition, Boolean> _function = (SchedulerDefinition it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "OSEK"));
    };
    final SchedulerDefinition osekSD = IterableExtensions.<SchedulerDefinition>findFirst(model.getOsModel().getSchedulerDefinitions(), _function);
    SchedulingParameterDefinition _head = IterableExtensions.<SchedulingParameterDefinition>head(osekSD.getProcessParameters());
    _head.setName("invalid");
    final List<ValidationDiagnostic> standardSDIssues = this.validateStandardSchedulerDefinitions(model);
    int _size = standardSDIssues.size();
    boolean _equals = (_size == 3);
    Assert.assertTrue(_equals);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      EStructuralFeature _targetFeature = it.getTargetFeature();
      EReference _schedulerDefinition_ProcessParameters = AmaltheaPackage.eINSTANCE.getSchedulerDefinition_ProcessParameters();
      return Objects.equal(_targetFeature, _schedulerDefinition_ProcessParameters);
    };
    Assert.assertTrue(standardSDIssues.stream().allMatch(_function_1));
    final Function1<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> messages = ListExtensions.<ValidationDiagnostic, String>map(standardSDIssues, _function_2);
    final Consumer<StandardSchedulers.Algorithm> _function_3 = (StandardSchedulers.Algorithm it) -> {
      String _algorithmName = it.getAlgorithmName();
      String _plus = (("Expected scheduling parameter definition \"priority\" is not provided in processParameters for " + "standard Scheduler Definition \"") + _algorithmName);
      String _plus_1 = (_plus + "\"");
      Assert.assertTrue(
        messages.contains(_plus_1));
    };
    Collections.<StandardSchedulers.Algorithm>unmodifiableList(CollectionLiterals.<StandardSchedulers.Algorithm>newArrayList(StandardSchedulers.Algorithm.OSEK, StandardSchedulers.Algorithm.FIXED_PRIORITY_PREEMPTIVE, StandardSchedulers.Algorithm.PRIORITY_BASED)).forEach(_function_3);
    osekSD.setName("osek");
    final List<ValidationDiagnostic> standardSDIssues2 = this.validateStandardSchedulerDefinitions(model);
    int _size_1 = standardSDIssues2.size();
    boolean _equals_1 = (_size_1 == 2);
    Assert.assertTrue(_equals_1);
    final Function1<ValidationDiagnostic, String> _function_4 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> messages2 = ListExtensions.<ValidationDiagnostic, String>map(standardSDIssues2, _function_4);
    Assert.assertFalse(
      messages2.contains(
        ("Expected scheduling parameter definition \"priority\" is not provided in processParameters for " + "standard Scheduler Definition \"OSEK\"")));
  }

  @Test
  public void testStandardSDParameterTypeValidated() {
    final Amalthea model = this.createValidTestModel();
    final Function1<SchedulingParameterDefinition, Boolean> _function = (SchedulingParameterDefinition it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "priority"));
    };
    final SchedulingParameterDefinition prioritySPD = IterableExtensions.<SchedulingParameterDefinition>findFirst(model.getOsModel().getSchedulingParameterDefinitions(), _function);
    prioritySPD.setType(ParameterType.BOOL);
    final List<ValidationDiagnostic> standardSPDIssues = this.validateStandardSchedulingParameterDefinitions(model);
    int _size = standardSPDIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getTargetFeature();
    EAttribute _schedulingParameterDefinition_Type = AmaltheaPackage.eINSTANCE.getSchedulingParameterDefinition_Type();
    boolean _equals_1 = Objects.equal(_targetFeature, _schedulingParameterDefinition_Type);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("Standard scheduling parameter definition \"priority\" expects type set to \'Integer\', " + "but found \'Bool\' in Scheduling Parameter Definition \"priority\""));
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testStandardSDParameterManyValidated() {
    final Amalthea model = this.createValidTestModel();
    final Function1<SchedulingParameterDefinition, Boolean> _function = (SchedulingParameterDefinition it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "priority"));
    };
    final SchedulingParameterDefinition prioritySPD = IterableExtensions.<SchedulingParameterDefinition>findFirst(model.getOsModel().getSchedulingParameterDefinitions(), _function);
    prioritySPD.setMany(true);
    final List<ValidationDiagnostic> standardSPDIssues = this.validateStandardSchedulingParameterDefinitions(model);
    int _size = standardSPDIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getTargetFeature();
    EAttribute _schedulingParameterDefinition_Many = AmaltheaPackage.eINSTANCE.getSchedulingParameterDefinition_Many();
    boolean _equals_1 = Objects.equal(_targetFeature, _schedulingParameterDefinition_Many);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("Standard scheduling parameter definition \"priority\" expects many set to \'false\', " + "but found \'true\' in Scheduling Parameter Definition \"priority\""));
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testStandardSDParameterMandatoryValidated() {
    final Amalthea model = this.createValidTestModel();
    final Function1<SchedulingParameterDefinition, Boolean> _function = (SchedulingParameterDefinition it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "priority"));
    };
    final SchedulingParameterDefinition prioritySPD = IterableExtensions.<SchedulingParameterDefinition>findFirst(model.getOsModel().getSchedulingParameterDefinitions(), _function);
    prioritySPD.setMandatory(false);
    final List<ValidationDiagnostic> standardSPDIssues = this.validateStandardSchedulingParameterDefinitions(model);
    int _size = standardSPDIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getTargetFeature();
    EAttribute _schedulingParameterDefinition_Mandatory = AmaltheaPackage.eINSTANCE.getSchedulingParameterDefinition_Mandatory();
    boolean _equals_1 = Objects.equal(_targetFeature, _schedulingParameterDefinition_Mandatory);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("Standard scheduling parameter definition \"priority\" expects mandatory set to \'true\', " + "but found \'false\' in Scheduling Parameter Definition \"priority\""));
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testStandardSDParameterDefaultValueValidated() {
    final Amalthea model = this.createValidTestModel();
    final Function1<SchedulingParameterDefinition, Boolean> _function = (SchedulingParameterDefinition it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "quantSize"));
    };
    final SchedulingParameterDefinition quantSizeSPD = IterableExtensions.<SchedulingParameterDefinition>findFirst(model.getOsModel().getSchedulingParameterDefinitions(), _function);
    quantSizeSPD.setDefaultValue(null);
    final List<ValidationDiagnostic> standardSPDIssues = this.validateStandardSchedulingParameterDefinitions(model);
    int _size = standardSPDIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getTargetFeature();
    EReference _schedulingParameterDefinition_DefaultValue = AmaltheaPackage.eINSTANCE.getSchedulingParameterDefinition_DefaultValue();
    boolean _equals_1 = Objects.equal(_targetFeature, _schedulingParameterDefinition_DefaultValue);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(standardSPDIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("Standard scheduling parameter definition \"quantSize\" defines a default value, " + "which should also be provided here, since Scheduling Parameter Definition \"quantSize\" is optional"));
    Assert.assertTrue(_equals_2);
  }

  @Test
  public void testOverriddenSchedulerParameterHasNullValidated() {
    final Amalthea model = this.createValidTestModel();
    final Function1<TaskAllocation, Boolean> _function = (TaskAllocation it) -> {
      String _name = it.getTask().getName();
      return Boolean.valueOf(Objects.equal(_name, "t2"));
    };
    final Function1<Map.Entry<SchedulingParameterDefinition, Value>, Boolean> _function_1 = (Map.Entry<SchedulingParameterDefinition, Value> it) -> {
      String _name = it.getKey().getName();
      return Boolean.valueOf(Objects.equal(_name, "preemptible"));
    };
    Map.Entry<SchedulingParameterDefinition, Value> _findFirst = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>findFirst(IterableExtensions.<TaskAllocation>findFirst(model.getMappingModel().getTaskAllocation(), _function).getSchedulingParameters(), _function_1);
    _findFirst.setValue(null);
    final List<ValidationDiagnostic> overriddenSPValueNullIssues = this.validateOverriddenSchedulingParameterEmptyValue(model);
    int _size = overriddenSPValueNullIssues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EStructuralFeature _targetFeature = IterableExtensions.<ValidationDiagnostic>head(overriddenSPValueNullIssues).getTargetFeature();
    EReference _schedulingParameter_Value = AmaltheaPackage.eINSTANCE.getSchedulingParameter_Value();
    boolean _equals_1 = Objects.equal(_targetFeature, _schedulingParameter_Value);
    Assert.assertTrue(_equals_1);
    String _message = IterableExtensions.<ValidationDiagnostic>head(overriddenSPValueNullIssues).getMessage();
    boolean _equals_2 = Objects.equal(_message, ("There should be a value provided for the overridden Scheduling Parameter \"preemptible\", " + "otherwise the default value will be used"));
    Assert.assertTrue(_equals_2);
    final Function1<TaskAllocation, Boolean> _function_2 = (TaskAllocation it) -> {
      String _name = it.getTask().getName();
      return Boolean.valueOf(Objects.equal(_name, "t2"));
    };
    final Function1<Map.Entry<SchedulingParameterDefinition, Value>, Boolean> _function_3 = (Map.Entry<SchedulingParameterDefinition, Value> it) -> {
      String _name = it.getKey().getName();
      return Boolean.valueOf(Objects.equal(_name, "preemptible"));
    };
    Map.Entry<SchedulingParameterDefinition, Value> _findFirst_1 = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>findFirst(IterableExtensions.<TaskAllocation>findFirst(model.getMappingModel().getTaskAllocation(), _function_2).getSchedulingParameters(), _function_3);
    _findFirst_1.setValue(FactoryUtil.createBooleanObject(true));
    final Function1<TaskAllocation, Boolean> _function_4 = (TaskAllocation it) -> {
      String _name = it.getTask().getName();
      return Boolean.valueOf(Objects.equal(_name, "t2"));
    };
    final Function1<Map.Entry<SchedulingParameterDefinition, Value>, Boolean> _function_5 = (Map.Entry<SchedulingParameterDefinition, Value> it) -> {
      String _name = it.getKey().getName();
      return Boolean.valueOf(Objects.equal(_name, "priority"));
    };
    Map.Entry<SchedulingParameterDefinition, Value> _findFirst_2 = IterableExtensions.<Map.Entry<SchedulingParameterDefinition, Value>>findFirst(IterableExtensions.<TaskAllocation>findFirst(model.getMappingModel().getTaskAllocation(), _function_4).getSchedulingParameters(), _function_5);
    _findFirst_2.setValue(null);
    final List<ValidationDiagnostic> overriddenSPValueNullIssues2 = this.validateOverriddenSchedulingParameterEmptyValue(model);
    Assert.assertTrue(overriddenSPValueNullIssues2.isEmpty());
  }

  @Test
  public void testSemaphoreInitialNotGreaterThanMaxValueValidated() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<OSModel> _function_1 = (OSModel it_1) -> {
        final Consumer<Semaphore> _function_2 = (Semaphore it_2) -> {
          it_2.setName("semCorrectEqual");
          it_2.setInitialValue(2);
          it_2.setMaxValue(2);
        };
        this.b3.semaphore(it_1, _function_2);
        final Consumer<Semaphore> _function_3 = (Semaphore it_2) -> {
          it_2.setName("semCorrectLess");
          it_2.setInitialValue(1);
          it_2.setMaxValue(3);
        };
        this.b3.semaphore(it_1, _function_3);
        final Consumer<Semaphore> _function_4 = (Semaphore it_2) -> {
          it_2.setName("semIncorrectGreater");
          it_2.setInitialValue(2);
          it_2.setMaxValue(1);
        };
        this.b3.semaphore(it_1, _function_4);
      };
      this.b1.osModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Semaphore-Properties-Conform-Type"));
    };
    final List<ValidationDiagnostic> issues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1));
    int _size = issues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EObject _targetObject = IterableExtensions.<ValidationDiagnostic>head(issues).getTargetObject();
    String _name = ((Semaphore) _targetObject).getName();
    boolean _equals_1 = Objects.equal(_name, "semIncorrectGreater");
    Assert.assertTrue(_equals_1);
  }

  @Test
  public void testSemaphoreInitialValueZeroValidated() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<Semaphore> _function = (Semaphore it) -> {
      it.setInitialValue(1);
    };
    model.getOsModel().getSemaphores().forEach(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Semaphore-Properties-Conform-Type"));
    };
    final List<ValidationDiagnostic> issues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1));
    int _size = issues.size();
    boolean _equals = (_size == 3);
    Assert.assertTrue(_equals);
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      EObject _targetObject = it.getTargetObject();
      SemaphoreType _semaphoreType = ((Semaphore) _targetObject).getSemaphoreType();
      return Boolean.valueOf(Objects.equal(_semaphoreType, SemaphoreType.RESOURCE));
    };
    Assert.assertTrue(IterableExtensions.<ValidationDiagnostic>exists(issues, _function_2));
    final Function1<ValidationDiagnostic, Boolean> _function_3 = (ValidationDiagnostic it) -> {
      EObject _targetObject = it.getTargetObject();
      SemaphoreType _semaphoreType = ((Semaphore) _targetObject).getSemaphoreType();
      return Boolean.valueOf(Objects.equal(_semaphoreType, SemaphoreType.SPINLOCK));
    };
    Assert.assertTrue(IterableExtensions.<ValidationDiagnostic>exists(issues, _function_3));
    final Function1<ValidationDiagnostic, Boolean> _function_4 = (ValidationDiagnostic it) -> {
      EObject _targetObject = it.getTargetObject();
      SemaphoreType _semaphoreType = ((Semaphore) _targetObject).getSemaphoreType();
      return Boolean.valueOf(Objects.equal(_semaphoreType, SemaphoreType.MUTEX));
    };
    Assert.assertTrue(IterableExtensions.<ValidationDiagnostic>exists(issues, _function_4));
  }

  @Test
  public void testSemaphoreMaxValueOneValidated() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<Semaphore> _function = (Semaphore it) -> {
      it.setMaxValue(2);
    };
    model.getOsModel().getSemaphores().forEach(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Semaphore-Properties-Conform-Type"));
    };
    final List<ValidationDiagnostic> issues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1));
    int _size = issues.size();
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    final Function1<ValidationDiagnostic, Boolean> _function_2 = (ValidationDiagnostic it) -> {
      EObject _targetObject = it.getTargetObject();
      SemaphoreType _semaphoreType = ((Semaphore) _targetObject).getSemaphoreType();
      return Boolean.valueOf(Objects.equal(_semaphoreType, SemaphoreType.SPINLOCK));
    };
    Assert.assertTrue(IterableExtensions.<ValidationDiagnostic>exists(issues, _function_2));
    final Function1<ValidationDiagnostic, Boolean> _function_3 = (ValidationDiagnostic it) -> {
      EObject _targetObject = it.getTargetObject();
      SemaphoreType _semaphoreType = ((Semaphore) _targetObject).getSemaphoreType();
      return Boolean.valueOf(Objects.equal(_semaphoreType, SemaphoreType.MUTEX));
    };
    Assert.assertTrue(IterableExtensions.<ValidationDiagnostic>exists(issues, _function_3));
  }

  @Test
  public void testSemaphoreCountingSemaphoreHasNoPCPValidated() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<Semaphore> _function = (Semaphore it) -> {
      it.setPriorityCeilingProtocol(true);
    };
    model.getOsModel().getSemaphores().forEach(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Semaphore-Properties-Conform-Type"));
    };
    final List<ValidationDiagnostic> issues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1));
    int _size = issues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EObject _targetObject = IterableExtensions.<ValidationDiagnostic>head(issues).getTargetObject();
    SemaphoreType _semaphoreType = ((Semaphore) _targetObject).getSemaphoreType();
    boolean _equals_1 = Objects.equal(_semaphoreType, SemaphoreType.COUNTING_SEMAPHORE);
    Assert.assertTrue(_equals_1);
  }

  @Test
  public void testSemaphoreWrongOwnership() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<Semaphore> _function = (Semaphore it) -> {
      SemaphoreType _semaphoreType = it.getSemaphoreType();
      boolean _equals = Objects.equal(_semaphoreType, SemaphoreType.COUNTING_SEMAPHORE);
      it.setOwnership(_equals);
    };
    model.getOsModel().getSemaphores().forEach(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-OS-Semaphore-Properties-Conform-Type"));
    };
    final List<ValidationDiagnostic> issues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1));
    int _size = issues.size();
    boolean _equals = (_size == 3);
    Assert.assertTrue(_equals);
    final Consumer<ValidationDiagnostic> _function_2 = (ValidationDiagnostic it) -> {
      Assert.assertTrue(it.getMessage().startsWith("Ownership must be set to TRUE "));
    };
    issues.forEach(_function_2);
  }
}
