/**
 ********************************************************************************
 * Copyright (c) 2022 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb.tests;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.atdb.EntityType;

public enum MusicEntityType implements EntityType<MusicEventType> {
	C0_NOTE,
	D0_NOTE,
	E0_NOTE,
	F0_NOTE,
	G0_NOTE,
	A0_NOTE,
	B0_NOTE,
	C1_NOTE,
	INSTRUMENT;

	private static final Set<MusicEventType> possibleNoteEvents = EnumSet.of(
			MusicEventType.start, MusicEventType.finish);
	private static final Set<MusicEventType> possibleInstrumentEvents = EnumSet.of(
			MusicEventType.start, MusicEventType.finish, MusicEventType.set_volume, MusicEventType.set_sustain);
	private static final List<String> noteValConstrs = List.of(
			MusicEventType.start + "EventCount = 1", MusicEventType.finish + "EventCount = 1");
	private static final List<String> instrumentValConstrs = List.of("1");
	public static final List<MusicEntityType> allNoteEntityTypes = List.of(
			C0_NOTE, D0_NOTE, E0_NOTE, F0_NOTE, G0_NOTE, A0_NOTE, B0_NOTE, C1_NOTE);

	@Override
	public String getName() {
		return this.name().toLowerCase();
	}

	@Override
	public List<String> getTraceAliases() {
		return List.of(this.name().substring(0, 2));
	}

	@Override
	public Set<MusicEventType> getPossibleEvents() {
		if (this.getName().equals("instrument")) {
			return possibleInstrumentEvents;
		} else {
			return possibleNoteEvents;
		}
	}

	@Override
	public List<String> getValidityConstraints() {
		if (this.getName().equals("instrument")) {
			return instrumentValConstrs;
		} else {
			return noteValConstrs;
		}
	}

}
