/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Normal Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalDescription <em>Normal Description</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalCount <em>Normal Count</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getNormalObject()
 * @model abstract="true"
 * @generated
 */
public interface NormalObject extends BaseObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Normal Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normal Description</em>' attribute.
	 * @see #setNormalDescription(String)
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getNormalObject_NormalDescription()
	 * @model
	 * @generated
	 */
	String getNormalDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalDescription <em>Normal Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normal Description</em>' attribute.
	 * @see #getNormalDescription()
	 * @generated
	 */
	void setNormalDescription(String value);

	/**
	 * Returns the value of the '<em><b>Normal Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normal Count</em>' attribute.
	 * @see #setNormalCount(int)
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getNormalObject_NormalCount()
	 * @model
	 * @generated
	 */
	int getNormalCount();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalCount <em>Normal Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normal Count</em>' attribute.
	 * @see #getNormalCount()
	 * @generated
	 */
	void setNormalCount(int value);

} // NormalObject
