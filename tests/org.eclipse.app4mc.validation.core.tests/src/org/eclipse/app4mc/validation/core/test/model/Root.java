/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.Root#getBaseObjects <em>Base Objects</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.Root#getNormalObjects <em>Normal Objects</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.Root#getSpecialObjects <em>Special Objects</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getRoot()
 * @model
 * @generated
 */
public interface Root extends BaseObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Base Objects</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.validation.core.test.model.BaseObject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Objects</em>' containment reference list.
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getRoot_BaseObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<BaseObject> getBaseObjects();

	/**
	 * Returns the value of the '<em><b>Normal Objects</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.validation.core.test.model.NormalObject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normal Objects</em>' containment reference list.
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getRoot_NormalObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<NormalObject> getNormalObjects();

	/**
	 * Returns the value of the '<em><b>Special Objects</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.validation.core.test.model.SpecialObject}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Objects</em>' containment reference list.
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getRoot_SpecialObjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<SpecialObject> getSpecialObjects();

} // Root
