/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Special Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialDescription <em>Special Description</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialCount <em>Special Count</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getSpecialObject()
 * @model abstract="true"
 * @generated
 */
public interface SpecialObject extends BaseObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Returns the value of the '<em><b>Special Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Description</em>' attribute.
	 * @see #setSpecialDescription(String)
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getSpecialObject_SpecialDescription()
	 * @model
	 * @generated
	 */
	String getSpecialDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialDescription <em>Special Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Description</em>' attribute.
	 * @see #getSpecialDescription()
	 * @generated
	 */
	void setSpecialDescription(String value);

	/**
	 * Returns the value of the '<em><b>Special Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Count</em>' attribute.
	 * @see #setSpecialCount(int)
	 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage#getSpecialObject_SpecialCount()
	 * @model
	 * @generated
	 */
	int getSpecialCount();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialCount <em>Special Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Count</em>' attribute.
	 * @see #getSpecialCount()
	 * @generated
	 */
	void setSpecialCount(int value);

} // SpecialObject
