/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelFactory
 * @model kind="package"
 * @generated
 */
public interface TestmodelPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://app4mc.eclipse.org/testmodel/1.0.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestmodelPackage eINSTANCE = org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.BaseObjectImpl <em>Base Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.BaseObjectImpl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getBaseObject()
	 * @generated
	 */
	int BASE_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Base Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Base Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_OBJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.NormalObjectImpl <em>Normal Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.NormalObjectImpl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getNormalObject()
	 * @generated
	 */
	int NORMAL_OBJECT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NORMAL_OBJECT__NAME = BASE_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Normal Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NORMAL_OBJECT__NORMAL_DESCRIPTION = BASE_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Normal Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NORMAL_OBJECT__NORMAL_COUNT = BASE_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Normal Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NORMAL_OBJECT_FEATURE_COUNT = BASE_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Normal Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NORMAL_OBJECT_OPERATION_COUNT = BASE_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.SpecialObjectImpl <em>Special Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.SpecialObjectImpl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getSpecialObject()
	 * @generated
	 */
	int SPECIAL_OBJECT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIAL_OBJECT__NAME = BASE_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Special Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIAL_OBJECT__SPECIAL_DESCRIPTION = BASE_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Special Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIAL_OBJECT__SPECIAL_COUNT = BASE_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Special Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIAL_OBJECT_FEATURE_COUNT = BASE_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Special Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIAL_OBJECT_OPERATION_COUNT = BASE_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.RootImpl <em>Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.RootImpl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getRoot()
	 * @generated
	 */
	int ROOT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT__NAME = BASE_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Base Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT__BASE_OBJECTS = BASE_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Normal Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT__NORMAL_OBJECTS = BASE_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Special Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT__SPECIAL_OBJECTS = BASE_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_FEATURE_COUNT = BASE_OBJECT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_OPERATION_COUNT = BASE_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementB1Impl <em>Element B1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementB1Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementB1()
	 * @generated
	 */
	int ELEMENT_B1 = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_B1__NAME = BASE_OBJECT__NAME;

	/**
	 * The number of structural features of the '<em>Element B1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_B1_FEATURE_COUNT = BASE_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element B1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_B1_OPERATION_COUNT = BASE_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementN1Impl <em>Element N1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementN1Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementN1()
	 * @generated
	 */
	int ELEMENT_N1 = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N1__NAME = NORMAL_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Normal Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N1__NORMAL_DESCRIPTION = NORMAL_OBJECT__NORMAL_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Normal Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N1__NORMAL_COUNT = NORMAL_OBJECT__NORMAL_COUNT;

	/**
	 * The number of structural features of the '<em>Element N1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N1_FEATURE_COUNT = NORMAL_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element N1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N1_OPERATION_COUNT = NORMAL_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementN2Impl <em>Element N2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementN2Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementN2()
	 * @generated
	 */
	int ELEMENT_N2 = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N2__NAME = NORMAL_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Normal Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N2__NORMAL_DESCRIPTION = NORMAL_OBJECT__NORMAL_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Normal Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N2__NORMAL_COUNT = NORMAL_OBJECT__NORMAL_COUNT;

	/**
	 * The number of structural features of the '<em>Element N2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N2_FEATURE_COUNT = NORMAL_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element N2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N2_OPERATION_COUNT = NORMAL_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementN3Impl <em>Element N3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementN3Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementN3()
	 * @generated
	 */
	int ELEMENT_N3 = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N3__NAME = NORMAL_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Normal Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N3__NORMAL_DESCRIPTION = NORMAL_OBJECT__NORMAL_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Normal Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N3__NORMAL_COUNT = NORMAL_OBJECT__NORMAL_COUNT;

	/**
	 * The number of structural features of the '<em>Element N3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N3_FEATURE_COUNT = NORMAL_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element N3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_N3_OPERATION_COUNT = NORMAL_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementS1Impl <em>Element S1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementS1Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementS1()
	 * @generated
	 */
	int ELEMENT_S1 = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S1__NAME = SPECIAL_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Special Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S1__SPECIAL_DESCRIPTION = SPECIAL_OBJECT__SPECIAL_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Special Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S1__SPECIAL_COUNT = SPECIAL_OBJECT__SPECIAL_COUNT;

	/**
	 * The number of structural features of the '<em>Element S1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S1_FEATURE_COUNT = SPECIAL_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element S1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S1_OPERATION_COUNT = SPECIAL_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementS2Impl <em>Element S2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementS2Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementS2()
	 * @generated
	 */
	int ELEMENT_S2 = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S2__NAME = SPECIAL_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Special Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S2__SPECIAL_DESCRIPTION = SPECIAL_OBJECT__SPECIAL_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Special Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S2__SPECIAL_COUNT = SPECIAL_OBJECT__SPECIAL_COUNT;

	/**
	 * The number of structural features of the '<em>Element S2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S2_FEATURE_COUNT = SPECIAL_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element S2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S2_OPERATION_COUNT = SPECIAL_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementS3Impl <em>Element S3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementS3Impl
	 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementS3()
	 * @generated
	 */
	int ELEMENT_S3 = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S3__NAME = SPECIAL_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Special Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S3__SPECIAL_DESCRIPTION = SPECIAL_OBJECT__SPECIAL_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Special Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S3__SPECIAL_COUNT = SPECIAL_OBJECT__SPECIAL_COUNT;

	/**
	 * The number of structural features of the '<em>Element S3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S3_FEATURE_COUNT = SPECIAL_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element S3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_S3_OPERATION_COUNT = SPECIAL_OBJECT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.BaseObject <em>Base Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Object</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.BaseObject
	 * @generated
	 */
	EClass getBaseObject();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.validation.core.test.model.BaseObject#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.BaseObject#getName()
	 * @see #getBaseObject()
	 * @generated
	 */
	EAttribute getBaseObject_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.NormalObject <em>Normal Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Normal Object</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.NormalObject
	 * @generated
	 */
	EClass getNormalObject();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalDescription <em>Normal Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Normal Description</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalDescription()
	 * @see #getNormalObject()
	 * @generated
	 */
	EAttribute getNormalObject_NormalDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalCount <em>Normal Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Normal Count</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.NormalObject#getNormalCount()
	 * @see #getNormalObject()
	 * @generated
	 */
	EAttribute getNormalObject_NormalCount();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject <em>Special Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Special Object</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.SpecialObject
	 * @generated
	 */
	EClass getSpecialObject();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialDescription <em>Special Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special Description</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialDescription()
	 * @see #getSpecialObject()
	 * @generated
	 */
	EAttribute getSpecialObject_SpecialDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialCount <em>Special Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special Count</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.SpecialObject#getSpecialCount()
	 * @see #getSpecialObject()
	 * @generated
	 */
	EAttribute getSpecialObject_SpecialCount();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.Root <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Root</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.Root
	 * @generated
	 */
	EClass getRoot();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.app4mc.validation.core.test.model.Root#getBaseObjects <em>Base Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Base Objects</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.Root#getBaseObjects()
	 * @see #getRoot()
	 * @generated
	 */
	EReference getRoot_BaseObjects();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.app4mc.validation.core.test.model.Root#getNormalObjects <em>Normal Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Normal Objects</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.Root#getNormalObjects()
	 * @see #getRoot()
	 * @generated
	 */
	EReference getRoot_NormalObjects();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.app4mc.validation.core.test.model.Root#getSpecialObjects <em>Special Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Special Objects</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.Root#getSpecialObjects()
	 * @see #getRoot()
	 * @generated
	 */
	EReference getRoot_SpecialObjects();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementB1 <em>Element B1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element B1</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementB1
	 * @generated
	 */
	EClass getElementB1();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementN1 <em>Element N1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element N1</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementN1
	 * @generated
	 */
	EClass getElementN1();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementN2 <em>Element N2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element N2</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementN2
	 * @generated
	 */
	EClass getElementN2();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementN3 <em>Element N3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element N3</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementN3
	 * @generated
	 */
	EClass getElementN3();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementS1 <em>Element S1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element S1</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementS1
	 * @generated
	 */
	EClass getElementS1();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementS2 <em>Element S2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element S2</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementS2
	 * @generated
	 */
	EClass getElementS2();

	/**
	 * Returns the meta object for class '{@link org.eclipse.app4mc.validation.core.test.model.ElementS3 <em>Element S3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element S3</em>'.
	 * @see org.eclipse.app4mc.validation.core.test.model.ElementS3
	 * @generated
	 */
	EClass getElementS3();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestmodelFactory getTestmodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.BaseObjectImpl <em>Base Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.BaseObjectImpl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getBaseObject()
		 * @generated
		 */
		EClass BASE_OBJECT = eINSTANCE.getBaseObject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_OBJECT__NAME = eINSTANCE.getBaseObject_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.NormalObjectImpl <em>Normal Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.NormalObjectImpl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getNormalObject()
		 * @generated
		 */
		EClass NORMAL_OBJECT = eINSTANCE.getNormalObject();

		/**
		 * The meta object literal for the '<em><b>Normal Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NORMAL_OBJECT__NORMAL_DESCRIPTION = eINSTANCE.getNormalObject_NormalDescription();

		/**
		 * The meta object literal for the '<em><b>Normal Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NORMAL_OBJECT__NORMAL_COUNT = eINSTANCE.getNormalObject_NormalCount();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.SpecialObjectImpl <em>Special Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.SpecialObjectImpl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getSpecialObject()
		 * @generated
		 */
		EClass SPECIAL_OBJECT = eINSTANCE.getSpecialObject();

		/**
		 * The meta object literal for the '<em><b>Special Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPECIAL_OBJECT__SPECIAL_DESCRIPTION = eINSTANCE.getSpecialObject_SpecialDescription();

		/**
		 * The meta object literal for the '<em><b>Special Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPECIAL_OBJECT__SPECIAL_COUNT = eINSTANCE.getSpecialObject_SpecialCount();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.RootImpl <em>Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.RootImpl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getRoot()
		 * @generated
		 */
		EClass ROOT = eINSTANCE.getRoot();

		/**
		 * The meta object literal for the '<em><b>Base Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOT__BASE_OBJECTS = eINSTANCE.getRoot_BaseObjects();

		/**
		 * The meta object literal for the '<em><b>Normal Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOT__NORMAL_OBJECTS = eINSTANCE.getRoot_NormalObjects();

		/**
		 * The meta object literal for the '<em><b>Special Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOT__SPECIAL_OBJECTS = eINSTANCE.getRoot_SpecialObjects();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementB1Impl <em>Element B1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementB1Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementB1()
		 * @generated
		 */
		EClass ELEMENT_B1 = eINSTANCE.getElementB1();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementN1Impl <em>Element N1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementN1Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementN1()
		 * @generated
		 */
		EClass ELEMENT_N1 = eINSTANCE.getElementN1();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementN2Impl <em>Element N2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementN2Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementN2()
		 * @generated
		 */
		EClass ELEMENT_N2 = eINSTANCE.getElementN2();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementN3Impl <em>Element N3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementN3Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementN3()
		 * @generated
		 */
		EClass ELEMENT_N3 = eINSTANCE.getElementN3();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementS1Impl <em>Element S1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementS1Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementS1()
		 * @generated
		 */
		EClass ELEMENT_S1 = eINSTANCE.getElementS1();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementS2Impl <em>Element S2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementS2Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementS2()
		 * @generated
		 */
		EClass ELEMENT_S2 = eINSTANCE.getElementS2();

		/**
		 * The meta object literal for the '{@link org.eclipse.app4mc.validation.core.test.model.impl.ElementS3Impl <em>Element S3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.ElementS3Impl
		 * @see org.eclipse.app4mc.validation.core.test.model.impl.TestmodelPackageImpl#getElementS3()
		 * @generated
		 */
		EClass ELEMENT_S3 = eINSTANCE.getElementS3();

	}

} //TestmodelPackage
