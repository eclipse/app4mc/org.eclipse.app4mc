/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model.impl;

import org.eclipse.app4mc.validation.core.test.model.SpecialObject;
import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Special Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.SpecialObjectImpl#getSpecialDescription <em>Special Description</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.SpecialObjectImpl#getSpecialCount <em>Special Count</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SpecialObjectImpl extends BaseObjectImpl implements SpecialObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The default value of the '{@link #getSpecialDescription() <em>Special Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialDescription() <em>Special Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialDescription()
	 * @generated
	 * @ordered
	 */
	protected String specialDescription = SPECIAL_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpecialCount() <em>Special Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialCount()
	 * @generated
	 * @ordered
	 */
	protected static final int SPECIAL_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSpecialCount() <em>Special Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialCount()
	 * @generated
	 * @ordered
	 */
	protected int specialCount = SPECIAL_COUNT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecialObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestmodelPackage.Literals.SPECIAL_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSpecialDescription() {
		return specialDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecialDescription(String newSpecialDescription) {
		String oldSpecialDescription = specialDescription;
		specialDescription = newSpecialDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestmodelPackage.SPECIAL_OBJECT__SPECIAL_DESCRIPTION, oldSpecialDescription, specialDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getSpecialCount() {
		return specialCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecialCount(int newSpecialCount) {
		int oldSpecialCount = specialCount;
		specialCount = newSpecialCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestmodelPackage.SPECIAL_OBJECT__SPECIAL_COUNT, oldSpecialCount, specialCount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_DESCRIPTION:
				return getSpecialDescription();
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_COUNT:
				return getSpecialCount();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_DESCRIPTION:
				setSpecialDescription((String)newValue);
				return;
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_COUNT:
				setSpecialCount((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_DESCRIPTION:
				setSpecialDescription(SPECIAL_DESCRIPTION_EDEFAULT);
				return;
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_COUNT:
				setSpecialCount(SPECIAL_COUNT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_DESCRIPTION:
				return SPECIAL_DESCRIPTION_EDEFAULT == null ? specialDescription != null : !SPECIAL_DESCRIPTION_EDEFAULT.equals(specialDescription);
			case TestmodelPackage.SPECIAL_OBJECT__SPECIAL_COUNT:
				return specialCount != SPECIAL_COUNT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (specialDescription: ");
		result.append(specialDescription);
		result.append(", specialCount: ");
		result.append(specialCount);
		result.append(')');
		return result.toString();
	}

} //SpecialObjectImpl
