/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model.impl;

import org.eclipse.app4mc.validation.core.test.model.ElementB1;
import org.eclipse.app4mc.validation.core.test.model.ElementN1;
import org.eclipse.app4mc.validation.core.test.model.ElementN2;
import org.eclipse.app4mc.validation.core.test.model.ElementN3;
import org.eclipse.app4mc.validation.core.test.model.ElementS1;
import org.eclipse.app4mc.validation.core.test.model.ElementS2;
import org.eclipse.app4mc.validation.core.test.model.ElementS3;
import org.eclipse.app4mc.validation.core.test.model.Root;
import org.eclipse.app4mc.validation.core.test.model.TestmodelFactory;
import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestmodelFactoryImpl extends EFactoryImpl implements TestmodelFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TestmodelFactory init() {
		try {
			TestmodelFactory theTestmodelFactory = (TestmodelFactory)EPackage.Registry.INSTANCE.getEFactory(TestmodelPackage.eNS_URI);
			if (theTestmodelFactory != null) {
				return theTestmodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TestmodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestmodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TestmodelPackage.ROOT: return createRoot();
			case TestmodelPackage.ELEMENT_B1: return createElementB1();
			case TestmodelPackage.ELEMENT_N1: return createElementN1();
			case TestmodelPackage.ELEMENT_N2: return createElementN2();
			case TestmodelPackage.ELEMENT_N3: return createElementN3();
			case TestmodelPackage.ELEMENT_S1: return createElementS1();
			case TestmodelPackage.ELEMENT_S2: return createElementS2();
			case TestmodelPackage.ELEMENT_S3: return createElementS3();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Root createRoot() {
		RootImpl root = new RootImpl();
		return root;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementB1 createElementB1() {
		ElementB1Impl elementB1 = new ElementB1Impl();
		return elementB1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementN1 createElementN1() {
		ElementN1Impl elementN1 = new ElementN1Impl();
		return elementN1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementN2 createElementN2() {
		ElementN2Impl elementN2 = new ElementN2Impl();
		return elementN2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementN3 createElementN3() {
		ElementN3Impl elementN3 = new ElementN3Impl();
		return elementN3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementS1 createElementS1() {
		ElementS1Impl elementS1 = new ElementS1Impl();
		return elementS1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementS2 createElementS2() {
		ElementS2Impl elementS2 = new ElementS2Impl();
		return elementS2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElementS3 createElementS3() {
		ElementS3Impl elementS3 = new ElementS3Impl();
		return elementS3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TestmodelPackage getTestmodelPackage() {
		return (TestmodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TestmodelPackage getPackage() {
		return TestmodelPackage.eINSTANCE;
	}

} //TestmodelFactoryImpl
