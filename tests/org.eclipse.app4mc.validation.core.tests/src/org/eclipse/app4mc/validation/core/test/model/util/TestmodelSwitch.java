/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model.util;

import org.eclipse.app4mc.validation.core.test.model.BaseObject;
import org.eclipse.app4mc.validation.core.test.model.ElementB1;
import org.eclipse.app4mc.validation.core.test.model.ElementN1;
import org.eclipse.app4mc.validation.core.test.model.ElementN2;
import org.eclipse.app4mc.validation.core.test.model.ElementN3;
import org.eclipse.app4mc.validation.core.test.model.ElementS1;
import org.eclipse.app4mc.validation.core.test.model.ElementS2;
import org.eclipse.app4mc.validation.core.test.model.ElementS3;
import org.eclipse.app4mc.validation.core.test.model.NormalObject;
import org.eclipse.app4mc.validation.core.test.model.Root;
import org.eclipse.app4mc.validation.core.test.model.SpecialObject;
import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.app4mc.validation.core.test.model.TestmodelPackage
 * @generated
 */
public class TestmodelSwitch<T> extends Switch<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TestmodelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestmodelSwitch() {
		if (modelPackage == null) {
			modelPackage = TestmodelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TestmodelPackage.BASE_OBJECT: {
				BaseObject baseObject = (BaseObject)theEObject;
				T result = caseBaseObject(baseObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.NORMAL_OBJECT: {
				NormalObject normalObject = (NormalObject)theEObject;
				T result = caseNormalObject(normalObject);
				if (result == null) result = caseBaseObject(normalObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.SPECIAL_OBJECT: {
				SpecialObject specialObject = (SpecialObject)theEObject;
				T result = caseSpecialObject(specialObject);
				if (result == null) result = caseBaseObject(specialObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ROOT: {
				Root root = (Root)theEObject;
				T result = caseRoot(root);
				if (result == null) result = caseBaseObject(root);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_B1: {
				ElementB1 elementB1 = (ElementB1)theEObject;
				T result = caseElementB1(elementB1);
				if (result == null) result = caseBaseObject(elementB1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_N1: {
				ElementN1 elementN1 = (ElementN1)theEObject;
				T result = caseElementN1(elementN1);
				if (result == null) result = caseNormalObject(elementN1);
				if (result == null) result = caseBaseObject(elementN1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_N2: {
				ElementN2 elementN2 = (ElementN2)theEObject;
				T result = caseElementN2(elementN2);
				if (result == null) result = caseNormalObject(elementN2);
				if (result == null) result = caseBaseObject(elementN2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_N3: {
				ElementN3 elementN3 = (ElementN3)theEObject;
				T result = caseElementN3(elementN3);
				if (result == null) result = caseNormalObject(elementN3);
				if (result == null) result = caseBaseObject(elementN3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_S1: {
				ElementS1 elementS1 = (ElementS1)theEObject;
				T result = caseElementS1(elementS1);
				if (result == null) result = caseSpecialObject(elementS1);
				if (result == null) result = caseBaseObject(elementS1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_S2: {
				ElementS2 elementS2 = (ElementS2)theEObject;
				T result = caseElementS2(elementS2);
				if (result == null) result = caseSpecialObject(elementS2);
				if (result == null) result = caseBaseObject(elementS2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TestmodelPackage.ELEMENT_S3: {
				ElementS3 elementS3 = (ElementS3)theEObject;
				T result = caseElementS3(elementS3);
				if (result == null) result = caseSpecialObject(elementS3);
				if (result == null) result = caseBaseObject(elementS3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseObject(BaseObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Normal Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Normal Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNormalObject(NormalObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Special Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Special Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpecialObject(SpecialObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoot(Root object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element B1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element B1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementB1(ElementB1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element N1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element N1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementN1(ElementN1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element N2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element N2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementN2(ElementN2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element N3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element N3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementN3(ElementN3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element S1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element S1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementS1(ElementS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element S2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element S2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementS2(ElementS2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element S3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element S3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementS3(ElementS3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TestmodelSwitch
