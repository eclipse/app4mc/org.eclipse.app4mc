/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core.test.validations;

import java.util.List;

import org.eclipse.app4mc.validation.core.IValidation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;

public abstract class TestmodelValidation implements IValidation {

	protected static TestmodelPackage ePackage = TestmodelPackage.eINSTANCE;

	// general setting

	@Override
	public EPackage getEPackage() {
		return ePackage;
	}

	// helper methods

	protected void addIssue(final List<ValidationDiagnostic> results, final EObject object, final EStructuralFeature feature, final String message) {
		results.add(new ValidationDiagnostic(message, object, feature));
	}

}
