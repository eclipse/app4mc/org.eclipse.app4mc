/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core.tests;

import java.util.List;

import org.eclipse.app4mc.validation.core.test.model.ElementB1;
import org.eclipse.app4mc.validation.core.test.model.ElementN1;
import org.eclipse.app4mc.validation.core.test.model.ElementN2;
import org.eclipse.app4mc.validation.core.test.model.ElementN3;
import org.eclipse.app4mc.validation.core.test.model.ElementS1;
import org.eclipse.app4mc.validation.core.test.model.ElementS2;
import org.eclipse.app4mc.validation.core.test.model.ElementS3;
import org.eclipse.app4mc.validation.core.test.model.Root;
import org.eclipse.app4mc.validation.core.test.model.TestmodelFactory;

public class TestModels {

	private TestModels() {
		throw new IllegalStateException("Utility class");
	}

	public static Root createModel1() {
		TestmodelFactory fac = TestmodelFactory.eINSTANCE;

		// Root object

		Root root = fac.createRoot();
		root.setName("Test1");

		// Base objects

		ElementB1 b1a = fac.createElementB1();
		b1a.setName("b-anything");

		ElementB1 b1b = fac.createElementB1(); // Error: Name not set

		root.getBaseObjects().addAll(List.of(b1a, b1b));

		// Normal objects

		ElementN1 n1 = fac.createElementN1();
		n1.setName("n-xxx");
		n1.setNormalCount(3);

		ElementN2 n2 = fac.createElementN2();
		n2.setName("n-x");
		n2.setNormalCount(3);

		ElementN3 n3a = fac.createElementN3();
		n3a.setName("n-zzz");
		n3a.setNormalCount(3); // Error: Count should be 5 (length of name)

		ElementN3 n3b = fac.createElementN3();
		n3b.setName("n-zzz");
		n3b.setNormalCount(5);

		root.getNormalObjects().addAll(List.of(n1, n2, n3a, n3b));

		// Special objects

		ElementS1 s1 = fac.createElementS1();
		s1.setName("s-xxx");
		s1.setSpecialCount(3);

		ElementS2 s2 = fac.createElementS2();
		s2.setName("s-yyyyyyy");
		s2.setSpecialCount(7);

		ElementS3 s3a = fac.createElementS3();
		s3a.setName("s-zz");
		s3a.setSpecialCount(3); // Error: Count should be 2 (occurrences of z)

		ElementS3 s3b = fac.createElementS3();
		s3b.setName("s-zzz");
		s3b.setSpecialCount(3);

		root.getSpecialObjects().addAll(List.of(s1, s2, s3a, s3b));

		return root;
	}

}
